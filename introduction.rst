.. vim: set et ts=3 sts=3 sw=3 tw=79:
.. vim: :set fileencoding=utf-8:

************
Introduction
************

.. figure:: collection/cover/kastner-3mt-cover.png

   Molecular Adsorptions on a Single-Wall Carbon Nanotube.

**Down-scaling of electronic circuits** has undoubtedly determined the
evolution of technology in the past 50 years. For decades, the semiconductor
industry relied on an exponential growth predicted by Moore in 1965
:cite:`moore1965`. While being a landmark for the prediction of demands in
information technology, Moore’s Law is at its limit in current silicon
technology. Economic pressure has forced mass production in 7nm technology
stacking circuits in three dimensions, but further down-scaling of the
two-dimensional base circuits is not likely to happen in silicon-based
fabrication technology. Bringing the production of electronics below the 10nm
limit will require the use of **new technologies for electronic components**.

**Carbon nanotubes** have attracted considerable attention due to their defined
nano-scaled structure and strongly varying electronic properties. A :doc:`long
search for synthesis conditions <history>` finally led to an excellent
:doc:`control over the process <cvd>` using sustainable carbon sources.\
:cite:`maruyama2002` The electronic properties originate from monolayer
graphene and are highly adjustable to give either semiconducting or metallic
nanotubes.  The diameter and roll-up angle of a nanotube control the band
structure by changing the symmetric arrangement of p-orbitals. As the diameter
gets smaller, electronic effects become more prominent, and changes in the
dielectric medium have a higher impact.\ :cite:`capaz2007`

Recent **improvements to the synthesis** :cite:`hou2017,thurakitseree2012c`
allow much better control of the production process and significantly increase
the number of nanotubes with diameters and emission wavelengths that are below those of the (6,5)
type. Today, almost every synthesis of semiconducting carbon nanotubes leads to
a mixture of nanotubes with similar diameters. For applications like the
production of transistors, it is, however, favorable to **produce only specific
types**. Preselection can be performed through synthesis, but today, such
selection is better achieved through wet processing with polymers
:cite:`nish2007` that selectively adsorb on the surface of specific types and
remove metallic nanotubes. This adsorption is a combination of solvent,
π-stacking, and dispersion forces and is not fully understood.\
:cite:`fong2017` A more detailed look into these **adsorption mechanisms**
could give a better insight for more efficient separation of nanotube types.

**Nanotube-based electronics** are an alternative to silicon-based electronics.
Such electronics are down-scalable, do not require the use of rare earth metals
to achieve their unique properties, and can be synthesized from sustainable
precursors. Metallic nanotubes can be used to transport charges and
semiconducting nanotubes to form electronic components. In 2012, *IBM*
presented the first implementation of semiconducting carbon nanotubes as a
**transistor** in a sub-10nm geometry\ :cite:`franklin2012`.  These
investigations enabled to build the first operable computer
:cite:`shulaker2013` using carbon nanotubes as transistors.  To increase
computation power, a caching memory for storing intermediate calculation
results, and a power source are crucial. Supercapacitors fulfill both of these
needs.  Following theoretical predictions, carbon nanotubes are *very* suitable
as **electrodes in supercapacitors** :cite:`kalugin2008`.  Research on
all-carbon electronics should, therefore, be highly favorable.  However, almost
no experimental evidence for the usage of carbon nanotubes as supercapacitors
are available since **adsorption processes** on the single nanotube level are
not so easily be observed and are generally hard to understand.

The photoluminescence of semiconducting carbon nanotubes is in the short wave
infrared (SWIR) region.  This region is important for enhancing the image
contrast in biological samples as it avoids blood and water absorptions
typically present in biological tissue.\ :cite:`boghossian2011` Also, glass
fibers do not show absorption in the SWIR range, which enables a better
transmission of data at telecom wavelengths.  Moreover, the photoluminescence
has shown **single-photon emission** :cite:`hogele2008,hofmann2013`, which
enables the use of semiconducting carbon nanotubes in quantum cryptography.
Such a transmission technique is operable on any existing fiber glass network.
As carbon nanotubes also show electroluminescence with high carrier-to-photon
conversion efficiencies :cite:`mueller2010` nano-scaled lasing components
became possible that can be coupled efficiently to wave-guides.  It was also
recently found that sp³-defects highly enhance these emission properties. These
defects both show **luminescence up-conversion** in biological tissues
:cite:`danne2018`, and photon anti-bunching :cite:`he2017,he2017b,he2018` at
room temperature.  Such defects can be efficiently synthesized through
**chemisorbed hydrogen** :cite:`nikitin2005` on the surface of nanotubes.

The highest culprit, however, is the generally **low photoluminescence quantum yield** of
nanotubes that is caused by further irradiative decay paths in these systems.
The transfer to solvent for wet processing, for example, leads to a major loss
in nanotube emission through non-radiative decay paths of the excitonic states.
The structural arrangement of such molecules on a carbon nanotube can also be
used to form various properties.  The emission strength of these excitons is
highly influenced by **physisorbed molecules** on the nanotube surface.

Carbon Nanotubes only consist of surface atoms and exhibit a high surface to
volume ratio. All nanotube-based technology is, therefore, based on **surface
adsorptions**. In the following, I will investigate these surface interactions
on the single-molecule level focussing on :doc:`nanotubes with diameters of
0.7-1.2nm <nitrogen>`. It will be shown that the initial :ref:`gas adsorption
<physisorption>` that is inherent in air significantly shifts the emission
energy. Gas adsorption is also responsible for the introduction of :ref:`new
states <chemisorption>` through sp³-defects. Further findings allow us to
estimate the quantum yield loss in :doc:`different solvents <solvent>` and help
to shed light on the different processes during :doc:`polymer adsorption
<molecular>` that are responsible for a selective sorting of specific carbon
nanotube roll-up angles. The first chapter focusses on the experiment design
for the :doc:`synthesis <cvd>` and :doc:`microscopic investigation <images>` of
:doc:`suspended nanotubes <samples>` which allows making :doc:`statistically
significant analyses <spectra>`. It starts by giving a short introduction on
the :doc:`history of nanotube synthesis <history>` before introducing the
:doc:`setup <images>`.
