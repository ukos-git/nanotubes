Chemical Vapor Deposition
=========================

.. figure:: collection/cvd/setup-simple.*

   The general CVD process consists of a carbon precursor and a reaction
   chamber with typically higher temperatures. A pump directs the precursor
   flow from the right to the left and controls the pressure.

Technique
---------

Upon all synthesis techniques, chemical vapor deposition is mostly preferred
for laboratory scale production of carbon nanotubes. The technique allows
synthesis in a controllable environment with constant temperature, an
adjustable synthesis pressure and a wide range of variability when it comes to
carbon precursors. Gaseous precursors include carbon monoxide\ :cite:`dai1996`,
methane\ :cite:`kong1998,colomer2000` or ethene\ :cite:`hafner1998` and various
liquid precursors like xylene :cite:`keskar2005`, camphor :cite:`kumar2005` or
benzene :cite:`sen1997`, or ethanol :cite:`maruyama2002`.  In all synthesis
routes, metal catalysts are used. While in the early beginnings, only iron,
cobalt, and nickel were confirmed to catalyze carbon nanotube growth, CVD has
since then shown to apply on a large variety of metals:

Pb :cite:`zhang2008`,
Sn, Mg, Al, Cr :cite:`yuan2008`,
W :cite:`yang2014`,
Mn :cite:`yuan2008,loebick2009,zoicanloebick2010`,
Fe :cite:`chiangi.w.2001,maruyama2002,miyauchi2004,yuan2008,zheng2009,yang2014`,
Co :cite:`kong1998,maruyama2002,bachilo2003,miyauchi2004,yuan2008,yang2014,cui2016`,
Ni :cite:`lee1997,kong1998,kataura2000,reich2006,yuan2008,mansoor2014`,
Cu :cite:`zhou2006,yuan2008,he2010,cui2016`,
Mo :cite:`dai1996,kataura2000,bachilo2003,yuan2008,cui2016`,
Ru :cite:`li2007`,
Rh :cite:`kataura2000,thurakitseree2012`,
Pd :cite:`kataura2000,yuan2008`,
Pt :cite:`kataura2000,yuan2008,liu2012`, and
Au :cite:`bhaviripudi2007,yuan2008`

Most catalysts today are combinations of different metals as co-alloy
catalysts. Although iron and cobalt play a significant role as catalysts,
almost all combinations are possible.

.. figure:: collection/cvd/length/length/comparison.*

   Even unprocessed, raw CoMoCat material\ :cite:`kitiyanan2000` like the
   *black sand* material (left image) shows overall smaller tube length than
   the CVD material (right image).

Its generally low yield as compared to continuous installations like the
commercial HipCo material contrasts to the overall good quality giving carbon
nanotubes with fewer lattice defects and higher quantum yields
:cite:`kastner2015`. Single tube lengths of around 5µm are a common result of
an unoptimized procedure, which contrasts the 200nm length of the commercially
available CoMoCat material\ :cite:`kitiyanan2000`. Optimized Processes have
even shown that nanotubes from sub-millimeter\ :cite:`oshima2008` up to 4cm\
:cite:`zheng2009,zheng2004` length and more are achievable.

Reactor
-------

.. figure:: collection/cvd/setup.*

   The setup for Chemical Vapor Deposition from the presented study allows
   low-pressure CVD, rapid cooldown, and exchange of gas sources during a
   reaction.


The reaction chamber in the presented study consists of an oven that heats the
sample for the reaction. The reaction chamber needs to stay evacuated during
the whole CVD process to avoid reactions of the produced carbon nanotubes with
oxygen which increases hetero atom defects in the graphene lattice.\
:cite:`bom2002,qingwen2002`

Due to reaction temperatures of 800℃ and more and temperature jumps of around
100℃ per minute, the reaction chamber consists out of quartz, which easily
withstands such conditions. A PID controller adjusts the reaction temperature
to a value measured at the center of the oven. It is, therefore, crucial to
place the sample in the middle of the quartz tube. The oven is placed on rails
to allow the proper positioning of the sample. This approach also reduces
cooldown time as the oven can be quickly get moved away from the sample and
therefore allows laboratory-scale mass production of carbon nanotubes as
heat-up is the limiting time factor during synthesis.

The pump evacuates the chamber from the left, and any potentially toxic
reaction by-products remain in the cooling trap. A needle valve controls the
pressure between the pump and the reaction chamber and allows an exact
adjustment of the pressure during synthesis. Additional argon flow as a carrier
gas allows increasing the pressure during the reaction. The gas sources are
exchangeable, but argon and argon with 3% hydrogen are pre-installed to the
system.

.. figure:: collection/cvd/setup-2017.jpg

   The Chemical Vapor Deposition Reactor consists of an oven in the center of
   the image and a gas control with different inlets on the right. The pump at
   the left can achieve constant flow synthesis at 10mbar.  The quartz tube
   connects the gas inlet and pump and allows temperatures of 800℃.


The most important part of the reaction is the carbon source, which is plugged
in with a glass flask and can hold pre-evacuated solvents. A water bath helps
to adjust the vapor pressure, and a needle ventile right after the flask
controls the maximum exhaust from the source. A digital flow meter is installed
to control precursor inflow. The exchangeable flask and programmable flow meter
allow a wide range of carbon sources with vapor pressures of up to around
100mbar.

Nearly all carbon sources that were tested with this setup allow synthesis of
carbon nanotubes of fairly small diameters (<1nm).  Among the tested carbon
sources were methanol, ethanol, *n*-propanol, *i*-propanol, butanol, toluene,
o-xylene, and p-xylene. Tested hetero-atom carbon sources include acetonitrile,
triphenylborane, and triisopropylborate.  This large variety of precursors
probably also explains the aforementioned early discovery of carbon nanotube
synthesis using vastly undefined carbon precursors and metal catalysts.

Due to the chosen setup with a cooling trap, glass flask, and a quartz tube, it
is easily cleanable and usable even if potentially toxic, and volatile reaction
intermediates are likely to form. The advantage as compared to pure-gas-flow
setups is also the low risk of combustion of common gaseous carbon precursors
like acetylene.

.. _cvd_accvd:

Alcohol Catalytic CVD
---------------------

Shigeo Maruyama and Shohei Chiashi introduced alcohol catalytic chemical vapor
deposition (ACCVD) for the synthesis of semiconducting single-wall carbon
nanotubes at the University of Tokyo in 2002.\ :cite:`maruyama2002`  Today,
typically ethanol is used as a precursor in this synthesis route for carbon
nanotubes. The carbon nanotube community readily accepted this technique due to
its simplicity of the setup and its generally secure handling and a-toxic
impact of the carbon precursor to western haplotypes\ :cite:`osier2002` if
compared to conventional alternatives like toluene or acetylene.

Despite its complexity when it comes to the growth mechanism itself, the
combustion into reactive precursors is well studied and understood using
theoretical investigations.\ :cite:`hou2011` The liquid combustion product of
the ACCVD process is mainly acetaldehyde.\ :cite:`kastner2014` More elaborate
in-situ monitoring of the reactive intermediates via infrared spectroscopy
shows that "ethanol decomposes within 3min and generates ethylene, acetylene,
acetaldehyde, methane, carbon monoxide, carbon dioxide, and water"\
:cite:`oshima2008`. ¹³C labeling has shown that ethanol reaches the reaction
center without splitting into symmetric intermediates like acetylene or ethene.
Instead, only the methylic carbon opposite to the hydroxyl group in ethanol is
incorporated into the nanotube lattice.\ :cite:`xiang2013` Therefore, the
reactive asymmetric species of the ethanol combustion process are C-C-OH and
C-C-O.\ :cite:`oguri2014`.  Molecular dynamics simulation studies typically
underline the insights into the actual growth process.\
:cite:`hisama2018,izu2010`

Not only the mechanism but also the growth conditions are widely understood:
There is a close relationship between pressure and synthesis temperature for
(A)CCVD which is usually reflected by a phase diagram that relates temperature,
pressure, and quality of the synthesized material.\ :cite:`vinten2013,hou2017`
Hou *et al.* showed that the temperature for the synthesis of carbon nanotubes
using ACCVD ranges between 350 and 1000℃ with a pressure of 10mPa to 10kPa.
Such a wide growth range lets the author assume that carbon nanotubes are
likely to form during every carbon combustion process that involves metal
catalysts.  Despite this wide range of acceptable growth conditions, a material
with lower defects is typically produced from ACCVD at 750-850℃ with a
pressure of 0.1 to 100mbar. Lower pressure is only needed when lowering the
synthesis temperature. No-flow techniques \ :cite:`oshima2008`, where the
ethanol flow is turned off during the reaction, allow to achieve lower
pressures. While lower temperatures give access to the synthesis of
single-wall carbon nanotubes with diameters below 0.8nm, lower temperatures
generally also lead to lower sample quality.\ :cite:`kastner2015` Smaller
diameters grow kinetically at lower temperatures and become thermodynamically
unstable at higher temperatures\ :cite:`miyauchi2004` due to their curvature\
:cite:`zhou2001,bom2002,unalan2005,kwok2010`. Therefore, amorphous carbon and
defective nanotubes are only avoidable by raising the synthesis temperature.\
:cite:`unalan2005,vinten2009,vinten2013`

.. figure:: collection/cvd/CoMoCat/CoMoCatvs725/combined.*

   A comparison between commercial CoMoCat Material and as-synthesized CVD
   material for different synthesis strategies shows the difference in the
   chirality distribution. The commercial material is post-processed after
   synthesis to achieve high enrichment in the (6,5) type. It is extensively
   studied and serves as a reference.


Within a constant flow setup like the one presented in this study, the
precursor flow mostly determines the minimal achievable reaction pressure. The
lower limit of this pressure is at 1-5mbar, and a typical reaction was
therefore carried out at 800℃ with a pressure between 5 and 15mbar in a
constant ethanol flow environment of 80-300mg/min. At constant pressure, the
flow does not play any significant role for reaction times of 10min and below.\
:cite:`kastner2014,oshima2008`

Notable for the chosen synthesis route is that even though a typical reaction
finishes after around 1min, it was carried out for 10min in all experiments to
facilitate defect healing due to the reductive properties of combusted
ethanol.\ :cite:`marinov1999,oshima2008,hou2011,oguri2014`

The high-temperature trade-off with this setup yields both, good quality and
diameters that are in the range of the silicon detector's quantum efficiency.
If low defects and nanotubes between 0.8 and 1.2nm are the preferred synthesis
target, these conditions have proven to be best for our CVD setup without the
complications that emerge from going to lower pressures.
