.. vim: set et ts=3 sts=3 sw=3 tw=79:

***************************************************************************************************************
Spectroscopic investigation of molecular adsorption and desorption from individual single-wall carbon nanotubes
***************************************************************************************************************

.. only:: html

   .. image:: collection/cover/paperback.png

   `pdf version <_static/thesis.pdf>`_

.. toctree::
   :maxdepth: 2

   introduction.rst
   setup.rst
   adsorption.rst
   summary.rst
   zbibliography.rst

