#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
//#include "lineprofilev2"

#define IMAGES_EXPORT_PXP
#include "utilities-images"

function plot_traces()
	variable i

	wave source = root:source
	wave wavelength = root:wavelength
	display
	for(i = 0; i < dimsize(source, 0); i += 1)
		appendtograph source[i][]/TN=$("spectrum" + num2str(i)) vs wavelength
	endfor
end

Function export()
	SaveWindow("statistics")
End

