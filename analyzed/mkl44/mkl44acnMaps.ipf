#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("combined", saveJSON = 0)
	SaveWindow("mkl44TRISimap_acn_0_waterfall", saveJSON = 0)
	SaveWindow("series0", saveJSON = 0, saveVector=0)
	SaveWindow("series1", saveJSON = 0, saveVector=0)
	SaveWindow("series2", saveJSON = 0, saveVector=0)
	SaveWindow("series3", saveJSON = 0, saveVector=0)
	SaveWindow("series4", saveJSON = 0, saveVector=0)
	SaveWindow("series5", saveJSON = 0, saveVector=0)
	SaveWindow("raman_modes", saveVector = 1)
End

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(495,65,952.5,452) as "combined"
	DefineGuide third1={FT,0.33,FB},third2={FT,0.66,FB}
	Display/W=(0.2,0.2,0.8,0.8)/FG=(FL,FT,FR,third1)/HOST=# /B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_53:yExcitation/TN='00h910nm' vs average50
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_3:yExcitation/TN='12h946nm' vs average51
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_5:yExcitation/TN='27h946nm' vs average52
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_53:PLEM
	ModifyImage PLEM ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_3:PLEM
	ModifyImage PLEM#1 ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_5:PLEM
	ModifyImage PLEM#2 ctab= {0,3e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h910nm')=(0,0,0),rgb('12h946nm')=(65535,24030,24030),rgb('27h946nm')=(1,16019,65535)
	ModifyGraph zero(bottom3)=1
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=1,lblPosMode(bottom1)=1,lblPosMode(bottom2)=1
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom1 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,3e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "AcN"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "27h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h910nm') air\r\\s('12h946nm') AcN\r\\s('27h946nm') 27h"
	RenameWindow #,G1
	SetActiveSubwindow ##
	Display/W=(0.2,0.2,0.8,0.8)/FG=(FL,third1,FR,third2)/HOST=# /B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_14:yExcitation/TN='00h928nm' vs average10
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_5:yExcitation/TN='14h946nm' vs average11
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_1:yExcitation/TN='26h946nm' vs average12
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_14:PLEM
	ModifyImage PLEM ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_5:PLEM
	ModifyImage PLEM#1 ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_1:PLEM
	ModifyImage PLEM#2 ctab= {0,3e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h928nm')=(0,0,0),rgb('14h946nm')=(65535,24030,24030),rgb('26h946nm')=(1,16019,65535)
	ModifyGraph zero(bottom3)=1
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=1,lblPosMode(bottom1)=1,lblPosMode(bottom2)=1
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom1 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,3e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "AcN"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "26h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h928nm') air\r\\s('14h946nm') AcN\r\\s('26h946nm') 26h"
	DefineGuide third={FT,0.66,FB}
	RenameWindow #,G2
	SetActiveSubwindow ##
	Display/W=(0.2,0.2,0.8,0.8)/FG=(FL,third2,FR,FB)/HOST=# /B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_8:yExcitation/TN='00h973nm' vs average40
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_2:yExcitation/TN='11h946nm' vs average41
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_4:yExcitation/TN='27h946nm' vs average42
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_8:PLEM
	ModifyImage PLEM ctab= {0,4e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_2:PLEM
	ModifyImage PLEM#1 ctab= {0,4e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_4:PLEM
	ModifyImage PLEM#2 ctab= {0,4e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h973nm')=(0,0,0),rgb('11h946nm')=(65535,24030,24030),rgb('27h946nm')=(1,16019,65535)
	ModifyGraph zero(bottom3)=1
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=1,lblPosMode(bottom1)=1,lblPosMode(bottom2)=1
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom1 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,4e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	Cursor/P/I A PLEM#2 522,9
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "AcN"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "27h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h973nm') air\r\\s('11h946nm') AcN\r\\s('27h946nm') 27h"
	RenameWindow #,G0
	SetActiveSubwindow ##
EndMacro

Window mkl44TRISimap_acn_0_waterfall() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:PLEMd2:maps:mkl44TRISimap_acn_0:
	NewWaterfall /W=(26.25,268.25,485.25,452.75) PLEM vs {xWavelength,yExcitation} as "mkl44TRISimap_acn_0: localized solvent phonon"
	ModifyWaterfall angle=30, axlen= 0.3, hidden= 0
	SetDataFolder fldrSav0
	ModifyGraph mode=7
	ModifyGraph hbFill=5
	ModifyGraph negRGB=(0,0,65535)
	ModifyGraph zColor(PLEM)={:PLEMd2:maps:mkl44TRISimap_acn_0:PLEM,0,5e-09,Terrain}
	Label left "intensity [a.u.]"
	Label bottom "emission [nm]"
	Label right "excitation [nm]"
	SetAxis left 0,1.1e-09
	SetAxis bottom 830,1050
	SetAxis right 550,*
EndMacro

Window series0() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(452.25,52.25,1170,244.25)/B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_10:yExcitation/TN='00h913nm' vs average00 as "series0"
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_0:yExcitation/TN='09h946nm' vs average01
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_0:yExcitation/TN='26h946nm' vs average02
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_10:PLEM
	ModifyImage PLEM ctab= {0,6e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_0:PLEM
	ModifyImage PLEM#1 ctab= {0,6e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_0:PLEM
	ModifyImage PLEM#2 ctab= {0,6e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h913nm')=(0,0,0),rgb('09h946nm')=(65535,24030,24030),rgb('26h946nm')=(39321,1,1)
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=2
	ModifyGraph lblLatPos(bottom0)=200
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom0 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,6e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "emission in air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "localized solvent phonon"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "26h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h913nm') '00h913nm'\r\\s('09h946nm') '09h946nm'\r\\s('26h946nm') '26h946nm'"
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6/I@EbT*+0KM8"
	MoveWindow 0, 0, 0, 0		// Minimize the window.
EndMacro

Window series1() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(39.75,482,758.25,674.75)/B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_14:yExcitation/TN='00h928nm' vs average10 as "series1"
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_5:yExcitation/TN='14h946nm' vs average11
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_1:yExcitation/TN='26h946nm' vs average12
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_14:PLEM
	ModifyImage PLEM ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_5:PLEM
	ModifyImage PLEM#1 ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_1:PLEM
	ModifyImage PLEM#2 ctab= {0,3e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h928nm')=(0,0,0),rgb('14h946nm')=(65535,24030,24030),rgb('26h946nm')=(39321,1,1)
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=2
	ModifyGraph lblLatPos(bottom0)=200
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom0 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,3e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "emission in air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "localized solvent phonon"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "26h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h928nm') '00h928nm'\r\\s('14h946nm') '14h946nm'\r\\s('26h946nm') '26h946nm'"
	MoveWindow 0, 0, 0, 0		// Minimize the window.
EndMacro

Window series2() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(600.75,52.25,1319.25,245)/B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_15:yExcitation/TN='00h909nm' vs average20 as "series2"
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_6:yExcitation/TN='15h946nm' vs average21
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_2:yExcitation/TN='27h946nm' vs average22
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_15:PLEM
	ModifyImage PLEM ctab= {0,5e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_6:PLEM
	ModifyImage PLEM#1 ctab= {0,5e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_2:PLEM
	ModifyImage PLEM#2 ctab= {0,5e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h909nm')=(0,0,0),rgb('15h946nm')=(65535,24030,24030),rgb('27h946nm')=(39321,1,1)
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=2
	ModifyGraph lblLatPos(bottom0)=200
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom0 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,5e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "emission in air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "localized solvent phonon"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "27h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h909nm') '00h909nm'\r\\s('15h946nm') '15h946nm'\r\\s('27h946nm') '27h946nm'"
	MoveWindow 0, 0, 0, 0		// Minimize the window.
EndMacro

Window series3() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(396.75,458.75,1115.25,652.25)/B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_3:yExcitation/TN='00h909nm' vs average30 as "series3"
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_1:yExcitation/TN='10h946nm' vs average31
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_3:yExcitation/TN='27h946nm' vs average32
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_3:PLEM
	ModifyImage PLEM ctab= {0,4e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_1:PLEM
	ModifyImage PLEM#1 ctab= {0,4e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_3:PLEM
	ModifyImage PLEM#2 ctab= {0,4e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h909nm')=(0,0,0),rgb('10h946nm')=(65535,24030,24030),rgb('27h946nm')=(39321,1,1)
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=2
	ModifyGraph lblLatPos(bottom0)=200
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom0 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,4e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "emission in air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "localized solvent phonon"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "27h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h909nm') '00h909nm'\r\\s('10h946nm') '10h946nm'\r\\s('27h946nm') '27h946nm'"
	MoveWindow 0, 0, 0, 0		// Minimize the window.
EndMacro

Window series4() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(365.25,484.25,1083,677)/B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_8:yExcitation/TN='00h973nm' vs average40 as "series4"
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_2:yExcitation/TN='11h946nm' vs average41
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_4:yExcitation/TN='27h946nm' vs average42
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_8:PLEM
	ModifyImage PLEM ctab= {0,4e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_2:PLEM
	ModifyImage PLEM#1 ctab= {0,4e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_4:PLEM
	ModifyImage PLEM#2 ctab= {0,4e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h973nm')=(0,0,0),rgb('11h946nm')=(65535,24030,24030),rgb('27h946nm')=(39321,1,1)
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=2
	ModifyGraph lblLatPos(bottom0)=200
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom0 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,4e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "emission in air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "localized solvent phonon"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "27h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h973nm') '00h973nm'\r\\s('11h946nm') '11h946nm'\r\\s('27h946nm') '27h946nm'"
	MoveWindow 0, 0, 0, 0		// Minimize the window.
EndMacro

Window series5() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(39,262.25,758.25,455)/B=bottom3 :PLEMd2:maps:mkl44TRSimap_air_53:yExcitation/TN='00h910nm' vs average50 as "series5"
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRISimap_acn_3:yExcitation/TN='12h946nm' vs average51
	AppendToGraph/B=bottom3 :PLEMd2:maps:mkl44TRmaps_acn_5:yExcitation/TN='27h946nm' vs average52
	AppendImage/B=bottom0 :PLEMd2:maps:mkl44TRSimap_air_53:PLEM
	ModifyImage PLEM ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom1 :PLEMd2:maps:mkl44TRISimap_acn_3:PLEM
	ModifyImage PLEM#1 ctab= {0,3e-09,colortable_plotly_earth,1}
	AppendImage/B=bottom2 :PLEMd2:maps:mkl44TRmaps_acn_5:PLEM
	ModifyImage PLEM#2 ctab= {0,3e-09,colortable_plotly_earth,1}
	ModifyGraph margin(top)=15,width={Plan,1,bottom0,left},height=141.732
	ModifyGraph lSize=1.5
	ModifyGraph rgb('00h910nm')=(0,0,0),rgb('12h946nm')=(65535,24030,24030),rgb('27h946nm')=(39321,1,1)
	ModifyGraph mirror(left)=3
	ModifyGraph lblPosMode(bottom3)=1,lblPosMode(bottom0)=2
	ModifyGraph lblLatPos(bottom0)=200
	ModifyGraph freePos(bottom3)={0,kwFraction}
	ModifyGraph freePos(bottom0)={0,kwFraction}
	ModifyGraph freePos(bottom1)={0,kwFraction}
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom0)={0,0.24}
	ModifyGraph axisEnab(bottom1)={0.25,0.49}
	ModifyGraph axisEnab(bottom2)={0.5,0.74}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={3,2}
	ModifyGraph manTick(bottom0)={0,100,0,0},manMinor(bottom0)={3,2}
	ModifyGraph manTick(bottom1)={0,100,0,0},manMinor(bottom1)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom3 "max. em. intensity [a.u.]"
	Label bottom0 "emission wavelength [nm]"
	SetAxis left 530,725
	SetAxis bottom3 0,3e-09
	SetAxis bottom0 810,1050
	SetAxis bottom1 810,1050
	SetAxis bottom2 810,1050
	TextBox/C/N=bottom0/A=LB/X=0.00/Y=0.00 "emission in air"
	TextBox/C/N=bottom1/A=LB/X=25.00/Y=0.00 "localized solvent phonon"
	TextBox/C/N=bottom2/A=LB/X=50.00/Y=0.00 "27h"
	Legend/C/N=legend_profiles/J/F=0/X=0.00 "\\s('00h910nm') '00h910nm'\r\\s('12h946nm') '12h946nm'\r\\s('27h946nm') '27h946nm'"
	MoveWindow 0, 0, 0, 0		// Minimize the window.
EndMacro

Window raman_modes() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(25.5,63.5,485.25,248)/R=ple/B=right_bottom average42[2,*] vs :PLEMd2:maps:mkl44TRmaps_acn_4:yExcitation[2,*] as "raman_modes"
	AppendToGraph extracted735 vs xRaman735
	AppendToGraph :mkl44_raman2:fit_extract740nm
	AppendToGraph :mkl44_raman2:extract740nm/TN=pure_acn740 vs :mkl44_raman2:xRaman740nm
	AppendToGraph :mkl44_raman2:ifs120acn vs :mkl44_raman2:ifs120wavenumber
	AppendToGraph/R=absorption/B=right_bottom acetonitrile
	AppendToGraph/R=ple/B=right_bottom average01 vs :PLEMd2:maps:mkl44TRISimap_acn_0:yExcitation
	ModifyGraph margin(left)=28,margin(right)=14,width={Aspect,3}
	ModifyGraph mode(average42)=7,mode(extracted735)=7,mode(fit_extract740nm)=7,mode(pure_acn740)=3
	ModifyGraph mode(ifs120acn)=7,mode(acetonitrile)=7,mode(average01)=7
	ModifyGraph marker(pure_acn740)=8
	ModifyGraph rgb(average42)=(34952,34952,34952),rgb(extracted735)=(1,16019,65535)
	ModifyGraph rgb(fit_extract740nm)=(65535,16385,16385),rgb(pure_acn740)=(65535,16385,16385)
	ModifyGraph rgb(ifs120acn)=(0,0,0),rgb(acetonitrile)=(65535,16385,16385),rgb(average01)=(1,16019,65535)
	ModifyGraph msize(pure_acn740)=0.1
	ModifyGraph hbFill(average42)=5,hbFill(extracted735)=5,hbFill(fit_extract740nm)=5
	ModifyGraph hbFill(ifs120acn)=5,hbFill(acetonitrile)=5,hbFill(average01)=5
	ModifyGraph useNegPat(extracted735)=1
	ModifyGraph hBarNegFill(extracted735)=5
	ModifyGraph muloffset(extracted735)={0,500000000000},muloffset(fit_extract740nm)={0,3000}
	ModifyGraph muloffset(pure_acn740)={0,3000},muloffset(ifs120acn)={0,1000},muloffset(average01)={0,2}
	ModifyGraph nticks(ple)=0,nticks(left)=0,nticks(absorption)=0
	ModifyGraph noLabel(ple)=2,noLabel(absorption)=2
	ModifyGraph standoff(ple)=0,standoff(absorption)=0
	ModifyGraph axThick(ple)=0,axThick(left)=0,axThick(absorption)=0
	ModifyGraph lblPosMode(right_bottom)=1,lblPosMode(absorption)=1
	ModifyGraph lblPos(bottom)=36
	ModifyGraph freePos(ple)={0,kwFraction}
	ModifyGraph freePos(right_bottom)={0,kwFraction}
	ModifyGraph freePos(absorption)={0,kwFraction}
	ModifyGraph axisEnab(right_bottom)={0.7,1}
	ModifyGraph axisEnab(bottom)={0,0.66}
	ModifyGraph manTick(bottom)={0,1000,0,0},manMinor(bottom)={3,2}
	Label ple "ple emission intensity"
	Label right_bottom "wavelength [nm]"
	Label left "intensity [arb.u.]"
	Label bottom "wavenumber [cm⁻¹]"
	Label absorption "optical density"
	SetAxis ple 0,5e-09
	SetAxis right_bottom 525,750
	SetAxis left 0,750
	SetAxis bottom 500,3900
	SetAxis absorption 0.515808054570089,0.553004018541887
	TextBox/C/N=raman/F=0/A=LB/X=0.00/Y=80.00 "\\s(ifs120acn) Raman (AcN) exc. 1064nm (Nd:YAG)\r\n\\s(fit_extract740nm) DOC wrapped SWNT in H₂O/AcN (exc. 740±5nm)"
	AppendText "\n\\s(extracted735) suspended SWNT in AcN after 11h (exc. 735±5nm)"
	TextBox/C/N=CCsstr/F=0/A=LT/X=49.05/Y=21.88 "\\JCAcN\rsstr. CH"
	TextBox/C/N=CNstr/F=0/A=MC/X=-26.58/Y=17.19 "\\JCAcN\rstr. CN"
	TextBox/C/N=CHdef/F=0/B=1/A=LT/X=3.16/Y=45.83 "\\JCSWNT\rG-band"
	TextBox/C/N=absorpton/F=0/B=(65535,65535,65535,16384)/A=LT/X=75.00/Y=0.00 "PLE abs. at 946±5nm\r\\s(average01) after 11h (x2)\r\\s(average42) after 27h"
	AppendText "Absorbance\r\\s(acetonitrile) pure AcN"
	SetDrawLayer UserFront
	SetDrawEnv arrow= 1
	DrawLine 0.242324638086579,0.401041666666667,0.323328042294403,0.622326203208556
	SetDrawEnv arrow= 1
	DrawLine 0.0914049933561786,0.635416666666667,0.186722812430905,0.807235962566845
EndMacro

