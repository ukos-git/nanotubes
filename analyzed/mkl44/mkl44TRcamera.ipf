#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("ingaas", saveJSON=0, saveVector=0, saveImages=1)
	saveWindow("silicon", saveJSON=0, saveVector=0, saveImages=1)
	saveWindow("acetonitrile_air", saveJSON=0, saveVector=0, saveImages=1)
	saveWindow("acetonitrile_detectors", saveJSON=0, saveVector=0, saveImages=1)
End

Window acetonitrile_detectors() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(601.5,71.75,1164.75,422.75) as "acetonitrile_detectors"
	AppendImage :mkl44TRcamera:mkl44acnClarascan_zoom
	ModifyImage mkl44acnClarascan_zoom ctab= {2000,6000,:mkl44TRcamera:colortable_image_blue,1}
	ModifyImage mkl44acnClarascan_zoom minRGB=NaN,maxRGB=0
	ModifyImage mkl44acnClarascan_zoom log= 1
	AppendImage :mkl44TRcamera:mkl44acnXevascan_zoom
	ModifyImage mkl44acnXevascan_zoom ctab= {0,300,:mkl44TRcamera:colortable_image_red,1}
	ModifyImage mkl44acnXevascan_zoom minRGB=NaN,maxRGB=0
	ModifyGraph margin(left)=28,margin(bottom)=28,margin(top)=28,margin(right)=216,width={Plan,1,bottom,left}
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph axRGB=(0,0,0,0)
	SetAxis left -5,55
	SetAxis bottom 75,140
	ColorScale/C/N=text0/F=0/B=1/A=LB/X=102.44/Y=50.00 image=mkl44acnClarascan_zoom
	ColorScale/C/N=text0 heightPct=45
	ColorScale/C/N=text0 userTicks={:mkl44TRcamera:clarascanacnticks,:mkl44TRcamera:clarascanacnticksT}
	ColorScale/C/N=text0 lblMargin=-15, frameRGB=(17476,17476,17476)
	ColorScale/C/N=text0 axisRange={0,6000,0}
	AppendText "Silicon detector [cts/s]"
	ColorScale/C/N=text1/F=0/B=1/A=LB/X=102.44/Y=0.00 image=mkl44acnXevascan_zoom
	ColorScale/C/N=text1 heightPct=45
	ColorScale/C/N=text1 userTicks={:mkl44TRcamera:xevascanacnticks,:mkl44TRcamera:xevascanacnticksT}
	ColorScale/C/N=text1 lblMargin=-18, axisRange={0,NaN,0}
	AppendText "InGaAs detector [cts/s]"
	SetDrawLayer UserFront
	SetDrawEnv gstart,gname= scalebarBottom
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 5,linefgc= (17476,17476,17476)
	DrawLine 128.026836541873,0.908085808580858,138.026836541873,0.908085808580858
	SetDrawEnv fsize= 18,textrgb= (17476,17476,17476)
	DrawText 0.822944251043868,0.977638169908361,"10 µm"
	SetDrawEnv gstop
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0,1.3,0.49
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0.51,1.3,1
	SetDrawEnv xcoord= bottom,ycoord= left,fillfgc= (65535,65535,65535,32768)
	DrawRect 90,20.5,110,11.5
	DrawLine 0.331428571428571,0.0412371134020619,0.232380952380952,0.575257731958763
	DrawLine 0.965714285714286,0.34639175257732,0.540952380952381,0.723711340206186
	SetWindow kwTopWin,hook=CursorMagKillGraphWindowHook
	SetWindow kwTopWin,userdata(mkl44acnXevascan_zoom)=  "image_sliderLimits={-72,500};"
	Display/W=(0.216,0.071,0.865,0.404)/FG=($"",$"",PR,$"")/HOST=# 
	AppendImage :mkl44TRcamera:mkl44acnXevascan_zoom
	ModifyImage mkl44acnXevascan_zoom ctab= {75,300,:mkl44TRcamera:colortable_image_red_opaque,1}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=14,width={Plan,1,bottom,left}
	ModifyGraph mirror=1
	ModifyGraph nticks=0
	ModifyGraph standoff=0
	ModifyGraph axThick=2
	SetAxis left 11.5,20.5
	SetAxis bottom 90,110
	SetWindow kwTopWin,hook=CursorMagKillGraphWindowHook
	SetWindow kwTopWin,userdata(mkl44acnXevascan_zoom)=  "image_sliderLimits={-72,500};"
	RenameWindow #,acetonitrile_detectors_1
	SetActiveSubwindow ##
EndMacro

Window ingaas() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl44TRcamera:
	Display /W=(29.25,68.75,592.5,419.75) coordinatesX[*][0]/TN=coordinates vs coordinatesX[*][1] as "ingaas"
	AppendImage mkl44TRxevascan660_zoom
	ModifyImage mkl44TRxevascan660_zoom ctab= {0,2000,colortable_image_red,1}
	ModifyImage mkl44TRxevascan660_zoom minRGB=(0,0,0),maxRGB=0
	AppendImage mkl44TRxevascan565_zoom
	ModifyImage mkl44TRxevascan565_zoom ctab= {15,*,colortable_image_green,1}
	ModifyImage mkl44TRxevascan565_zoom minRGB=NaN,maxRGB=0
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=28,margin(bottom)=28,margin(top)=28,margin(right)=216,width={Plan,1,bottom,left}
	ModifyGraph mode=3
	ModifyGraph marker=8
	ModifyGraph rgb=(0,0,0)
	ModifyGraph msize=10
	ModifyGraph mrkThick=2
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph fSize=8
	ModifyGraph axRGB=(0,0,0,0)
	SetAxis left -5,55
	SetAxis bottom 75,140
	ColorScale/C/N=text0/F=0/B=1/A=LB/X=102.50/Y=50.00 image=mkl44TRxevascan660_zoom
	ColorScale/C/N=text0 heightPct=50
	ColorScale/C/N=text0 userTicks={:mkl44TRcamera:xevascan660ticks,:mkl44TRcamera:xevascan660ticksT}
	ColorScale/C/N=text0 lblMargin=-15, frameRGB=(17476,17476,17476)
	AppendText "excitation at 660 nm [cts/s]"
	ColorScale/C/N=text1/F=0/B=1/A=LT/X=102.50/Y=50.00 image=mkl44TRxevascan565_zoom
	ColorScale/C/N=text1 heightPct=50
	ColorScale/C/N=text1 userTicks={:mkl44TRcamera:xevascan565ticks,:mkl44TRcamera:xevascan565ticksT}
	ColorScale/C/N=text1 lblMargin=-28, axisRange={0,100,0}
	AppendText "excitation at 565 nm [cts/s]"
	SetDrawLayer UserFront
	SetDrawEnv gstart,gname= scalebarBottom
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 5,linefgc= (17476,17476,17476)
	DrawLine 128.026836541873,0.908085808580858,138.026836541873,0.908085808580858
	SetDrawEnv fsize= 18,textrgb= (17476,17476,17476)
	DrawText 0.822944251043868,0.977638169908361,"10 µm"
	SetDrawEnv gstop
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0,1.3,0.49
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0.51,1.3,1
	SetWindow kwTopWin,userdata(mkl44TRxevascan565v2_zoom)=  "image_sliderLimits={*,*};"
EndMacro

Window silicon() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl44TRcamera:
	Display /W=(29.25,71,594,423.5) coordinatesC[*][0]/TN=coordinates vs coordinatesC[*][1] as "silicon"
	AppendImage mkl44TRclarascan660_zoom
	ModifyImage mkl44TRclarascan660_zoom ctab= {*,*,colortable_image_red,1}
	ModifyImage mkl44TRclarascan660_zoom minRGB=(0,0,0),maxRGB=0
	ModifyImage mkl44TRclarascan660_zoom log= 1
	AppendImage mkl44TRclarascan565_zoom
	ModifyImage mkl44TRclarascan565_zoom ctab= {10,100,colortable_image_green,1}
	ModifyImage mkl44TRclarascan565_zoom minRGB=NaN,maxRGB=0
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=28,margin(bottom)=28,margin(top)=28,margin(right)=216,width={Plan,1,bottom,left}
	ModifyGraph mode=3
	ModifyGraph marker=8
	ModifyGraph rgb=(0,0,0)
	ModifyGraph msize=10
	ModifyGraph mrkThick=2
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph axRGB=(0,0,0,0)
	SetAxis left -5,55
	SetAxis bottom 75,140
	ColorScale/C/N=text0/F=0/B=1/A=LB/X=102.50/Y=50.00
	ColorScale/C/N=text0 image=mkl44TRclarascan660_zoom, heightPct=50
	ColorScale/C/N=text0 userTicks={:mkl44TRcamera:clarascan660ticks,:mkl44TRcamera:clarascan660ticksT}
	ColorScale/C/N=text0 lblMargin=-15, frameRGB=(17476,17476,17476)
	AppendText "excitation at 660nm [cts/s]"
	ColorScale/C/N=text1/F=0/B=1/A=LT/X=102.50/Y=50.00
	ColorScale/C/N=text1 image=mkl44TRclarascan565_zoom, heightPct=50
	ColorScale/C/N=text1 userTicks={:mkl44TRcamera:clarascan565ticks,:mkl44TRcamera:clarascan565ticksT}
	ColorScale/C/N=text1 lblMargin=-18, axisRange={0,100,0}
	AppendText "excitation at 565nm [cts/s]"
	SetDrawLayer UserFront
	SetDrawEnv gstart,gname= scalebarBottom
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 5,linefgc= (17476,17476,17476)
	DrawLine 128.026836541873,0.908085808580858,138.026836541873,0.908085808580858
	SetDrawEnv fsize= 18,textrgb= (17476,17476,17476)
	DrawText 0.822944251043868,0.977638169908361,"10 µm"
	SetDrawEnv gstop
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0,1.3,0.49
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0.51,1.3,1
EndMacro

Window acetonitrile_air() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl44TRcamera:
	Display /W=(32.25,193.25,595.5,544.25) coordinatesA[*][0]/TN=coordinates vs coordinatesA[*][1] as "acetonitrile_air"
	AppendImage mkl44TRclarascan660_zoom
	ModifyImage mkl44TRclarascan660_zoom ctab= {*,*,colortable_image_red,1}
	ModifyImage mkl44TRclarascan660_zoom minRGB=(0,0,0),maxRGB=0
	ModifyImage mkl44TRclarascan660_zoom log= 1
	AppendImage mkl44TRclarascan565_zoom
	ModifyImage mkl44TRclarascan565_zoom ctab= {10,100,colortable_image_green,1}
	ModifyImage mkl44TRclarascan565_zoom minRGB=NaN,maxRGB=0
	AppendImage mkl44acnClarascan_zoom
	ModifyImage mkl44acnClarascan_zoom ctab= {2000,6000,colortable_image_blue,1}
	ModifyImage mkl44acnClarascan_zoom minRGB=NaN,maxRGB=0
	ModifyImage mkl44acnClarascan_zoom log= 1
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=28,margin(bottom)=28,margin(top)=28,margin(right)=216,width={Plan,1,bottom,left}
	ModifyGraph mode=3
	ModifyGraph marker=8
	ModifyGraph rgb=(0,0,0)
	ModifyGraph msize=10
	ModifyGraph mrkThick=2
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph axRGB=(0,0,0,0)
	SetAxis left -5,55
	SetAxis bottom 75,140
	ColorScale/C/N=text0/F=0/B=1/A=LT/X=102.35/Y=5.08 image=mkl44TRclarascan660_zoom
	ColorScale/C/N=text0 heightPct=45
	ColorScale/C/N=text0 userTicks={:mkl44TRcamera:clarascan660ticks,:mkl44TRcamera:clarascan660ticksT}
	ColorScale/C/N=text0 lblMargin=-15, frameRGB=(17476,17476,17476)
	AppendText "emission in air [cts/s]"
	ColorScale/C/N=text1/F=0/B=1/A=LB/X=102.44/Y=0.00 image=mkl44TRclarascan565_zoom
	ColorScale/C/N=text1 heightPct=45
	ColorScale/C/N=text1 userTicks={:mkl44TRcamera:clarascan565ticks,:mkl44TRcamera:clarascan565ticksT}
	ColorScale/C/N=text1 lblMargin=-18, axisRange={0,100,0}
	AppendText "emission in air [cts/s]"
	ColorScale/C/N=text2/F=0/B=1/A=LT/X=131.46/Y=5.33 image=mkl44acnClarascan_zoom
	ColorScale/C/N=text2 heightPct=45
	ColorScale/C/N=text2 userTicks={:mkl44TRcamera:clarascanacnticks,:mkl44TRcamera:clarascanacnticksT}
	ColorScale/C/N=text2 axisRange={0,NaN,0}
	AppendText "emission in AcN [cts/s]"
	TextBox/C/N=excitation/F=0/B=1/A=LT/X=112.50/Y=1.00 "excitation at 660nm"
	TextBox/C/N=excitation565/F=0/B=1/A=LT/X=112.50/Y=52.00 "excitation at 565nm"
	SetDrawLayer UserFront
	SetDrawEnv gstart,gname= scalebarBottom
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 5,linefgc= (17476,17476,17476)
	DrawLine 128.026836541873,0.908085808580858,138.026836541873,0.908085808580858
	SetDrawEnv fsize= 18,textrgb= (17476,17476,17476)
	DrawText 0.822944251043868,0.977638169908361,"10 µm"
	SetDrawEnv gstop
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0,1.55,0.49
	SetDrawEnv linethick= 2,linefgc= (34952,34952,34952)
	DrawRRect 1.01,0.51,1.55,1
EndMacro

