#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "sma"
#include "utilities-images"

Function displaymaps()
	Variable i, numMaps = PLEMd2getmapsavailable()
	String strMap, win

	Struct PLEMd2stats stats

	numMaps = 34 // no air mmps
	for(i = 0; i < numMaps; i += 1)
		Plemd2displaybynum(i)
		strMap = PLEMd2strPLEM(i)
		PLEMd2statsLoad(stats, strMap)
		WAVE PLEM = stats.wavPLEM
		Duplicate/FREE/R=[ScaleToIndex(PLEM, 850, 0),ScaleToIndex(PLEM, 1e3, 0)][ScaleToIndex(PLEM, 550, 1),ScaleToIndex(PLEM, 750, 1)] PLEM ScaleRange
		WaveStats/Q ScaleRange
		ModifyImage PLEM ctab= {V_min,V_max,colortable_plotly_earth,1}
		WaveClear ScaleRange
		SetAxis left 525,750;SetAxis bottom 750,1050
		win = PLEMd2getWindow(strMap)
		Killwindow/Z $win //!
	endfor
End

// set filter to PLEMd2FILTER_FEHL0750
Function setFilter()
	Variable i
	string strPLEM, filters
	Struct Plemd2stats stats

	// set filter to PLEMd2FILTER_FEHL0750
	String FELHmaps = "mkl44TRmaps_acn_6;mkl44TRmapv4_acn_0;mkl44TRmaps_acn_0;mkl44TRmaps_acn_1;mkl44TRmaps_acn_2;mkl44TRmaps_acn_3;mkl44TRmaps_acn_4;mkl44TRmaps_acn_5;"
	for(i = 0; i < ItemsInList(FELHmaps); i += 1)
		strPLEM = StringFromList(i, FELHmaps)
		PLEMd2statsload(stats, strPLEM)

		filters = PLEMd2getFilterEmiString(PLEMd2getSystem(stats.strUser), stats.numDetector)
		filters = "filterFELH0750;filterChroma760trans;reflSilver;reflSilver;reflSilver"
		PLEMd2SetCorrection(filters, stats.wavFilterEmi, stats.wavWavelength)
		filters = PLEMd2getFilterExcString(PLEMd2getSystem(stats.strUser), stats.numDetector)
		//filters = "reflSilver;reflSilver"
		PLEMd2SetCorrection(filters, stats.wavFilterExc, stats.wavExcitation)

		PLEMd2BuildMaps(strPLEM)
	endfor
End

Function findCoordinates()
	Variable i, j, numMaps, numIndices, relTime, offset, maxVal, maxLoc, color, maxXVal
	string axis, win, trace, text, strPLEM
	Struct Plemd2stats stats

	setFilter()

	numMaps = PLEMd2getMapsavailable()
	Make/N=(numMaps)/FREE/L minutes
	offset = PLEMd2Date2Minutes("25.03.2018/19:27")
	for(i = 0; i < numMaps; i += 1)
		PLEMd2statsload(stats, PLEMd2strplem(i))
		minutes[i] = max(0, PLEMd2Date2Minutes(stats.strDate) - offset)
	endfor

	WAVE coordinates = root:mkl44acnMaps_coordinates
	WAVE/WAVE matching = SMAfindCoordinatesInPLEM(coordinates, accuracy = 5)
	numMaps = DimSize(matching, 0)
	for(i = numMaps - 1; i > -1; i -= 1)
		WAVE indices = matching[i]
		numIndices = DimSize(indices, 0)
		Make/FREE/N=(numIndices)/U/I timematching = minutes[indices[p]]
		Sort timematching, indices
		win = "series" + num2str(i)
		KillWindow/Z $win
		Display/N=$win as "series" + num2str(i)

		// delete duplicates
		for(j = numIndices - 1; j > 0; j -= 1)
			PLEMd2statsload(stats, PLEMd2strPLEM(indices[j]))
			WAVE wv1 = stats.wavPLEM
			// remove spectra that are max
			if(WaveMax(stats.wavMeasure) > 64e3)
				DeletePoints j, 1, indices
				numIndices -= 1
				continue
			endif
			// remove spectra which are too close
			if(minutes[indices[j]] - minutes[indices[j - 1]] < 180)
				PLEMd2statsload(stats, PLEMd2strPLEM(indices[j - 1]))
				WAVE wv0 = stats.wavPLEM
				if(DimSize(wv1, 1) > DimSize(wv0, 1)) // remove wave with less excitation points
					DeletePoints j, 1, indices
				else
					DeletePoints j - 1, 1, indices
				endif
				numIndices -= 1
				continue
			endif
			// remove spectra at 20h
			if(round(minutes[indices[j]] / 120) == 10)
				DeletePoints j, 1, indices
				numIndices -= 1
			endif
			if(round(minutes[indices[j]] / 60) > 30) // ingaas
				DeletePoints j, 1, indices
				numIndices -= 1
			endif
		endfor
		Make/FREE/N=(numIndices + 1) axisPosition = p * 0.01 + p * (1 - (numIndices + 1) * 0.01) / (numIndices + 1)

		// find max
		Variable yMin = 550, yMax = 725
		Variable xMin = 850, xMax = 1000 // search maximum here
		maxVal = 0
		for(j = 0; j < numIndices; j += 1)
			PLEMd2statsload(stats, PLEMd2strPLEM(indices[j]))
			WAVE PLEM = stats.wavPLEM
			stats.wavPLEM[][DimSize(stats.wavPLEM, 1) - 1] = NaN // delete last points
			Duplicate/FREE/R=[ScaleToIndex(PLEM, xMin, 0),ScaleToIndex(PLEM, xMax, 0)][ScaleToIndex(PLEM, yMin, 1),ScaleToIndex(PLEM, yMax, 1)] PLEM ScaleRange
			MatrixFilter/N=10 median ScaleRange
			WaveStats/M=1/Q ScaleRange
			maxLoc = V_maxColLoc
			maxVal = max(V_max, maxVal)
			maxXval = V_maxRowLoc
			if(j > 0)
				maxXval = 946
			endif

			Duplicate/FREE/R=[ScaleToIndex(PLEM, maxXval - 5, 0), ScaleToIndex(PLEM, maxXval + 5, 0)][] PLEM ScaleRange
			MatrixOP/O $("root:average" + num2str(i) + num2str(j))/WAVE=average = averageCols(ScaleRange)^T
			CopyDimLabels/COLS=0 PLEM, average

			axis = "bottom" + num2str(numIndices)
			sprintf text, "%02dh%dnm", round(minutes[indices[j]] / 60), maxXval
			if(j == -1) // disabled
				AppendToGraph/T stats.wavExcitation/TN=$text vs average
				ModifyGraph rgb($text)=(0,0,0)
				ModifyGraph axisEnab(top)={axisPosition[numIndices], 1}
				SetAxis top 0,*
			else
				// AppendToGraph/B=$axis stats.wavExcitation[ScaleToIndex(PLEM, yMin, 1),ScaleToIndex(PLEM, yMax, 1)]/TN=$text vs average
				// AppendToGraph/T stats.wavExcitation/TN=$text vs PLEM[ScaleToIndex(PLEM, maxLoc, 0)][]
				AppendToGraph/B=$axis stats.wavExcitation/TN=$text vs average
			endif
			color = max(0, round(65535 * (0.7 - j / numIndices)))
			ModifyGraph rgb($text)=(65535,color,color)

			WaveClear ScaleRange, PLEM
		endfor
		ModifyGraph rgb($text)=(39321,1,1)
		ModifyGraph axisEnab($axis)={axisPosition[numIndices] + 0.05, 1}
		ModifyGraph lblPosMode($axis)=1

		maxVal = ceil(maxval * 1e9) / 1e9
		//maxVal = 5e-09 // manually set for comparability (empiric)
		SetAxis $axis 0, maxVal
		Label $axis "max. em. intensity [a.u.]"

		// add images
		for(j = 0; j < numIndices; j += 1)
			PLEMd2statsload(stats, PLEMd2strPLEM(indices[j]))
			WAVE PLEM = stats.wavPLEM

			axis = "bottom" + num2str(j)
			trace = "PLEM"
			if(j > 0)
				trace += "#" + num2str(j)
			endif

			AppendImage/B=$axis PLEM
			ModifyGraph margin(top)=15
			ModifyImage $trace ctab= {0,maxVal,colortable_plotly_earth,1}
			SetAxis $axis 810,1050

			ModifyGraph axisEnab($axis)={axisPosition[j], axisPosition[j + 1] - 0.01}
			sprintf text, "%02dh", round(minutes[indices[j]] / 60)
			TextBox/C/N=$axis/F=2/A=LB/X=(axisPosition[j] * 100)/Y=0 text

			WaveClear PLEM
		endfor
		ModifyGraph lsize=1.5
		//Legend/C/N=legend_profiles/F=0/X=(axisPosition[(numIndices)] * 100)/Y=5.00/A=LT
		Legend/C/N=legend_profiles/F=0/X=0.00/Y=5.00/A=RT
		ModifyGraph mirror(left)=3
		ModifyGraph freePos={0,kwFraction}
		ModifyGraph manTick={0,100,0,0},manMinor={3,2}
		SetAxis left 530,725
		ModifyGraph width={Plan,1,bottom0,left}
		ModifyGraph lblPosMode(bottom0)=2,lblLatPos(bottom0)=200
		ModifyGraph height=141.732
		Label left "excitation wavelength [nm]"
		Label bottom0 "emission wavelength [nm]"
		TextBox/C/N=bottom0 "emission in air"
		TextBox/C/N=bottom1 "localized solvent phonon"
		ModifyGraph rgb($"#0")=(0,0,0)
	endfor
End

Function SliderProc(sa) : SliderControl
	STRUCT WMSliderAction &sa

	DFREF dfr = root:PLEMd2:maps:mkl44TRmaps_acn_1
	//DFREF dfr = root:PLEMd2:maps:mkl44TRISimap_acn_0:

	switch( sa.eventCode )
		case -1: // control being killed
			break
		default:
			if( sa.eventCode & 1 ) // value set
				Variable curval = sa.curval
				WAVE wv = dfr:PLEM
				Duplicate/O/R=[0,*][curval] wv root:extracted
				WAVE wl = dfr:xWavelength
				duplicate/O wl root:xraman/WAVE=xraman
				wave excitation = dfr:yExcitation
				Variable laserWl = excitation[min(curval, DimSize(excitation, 0) - 1)]// + (excitation[1] - excitation[0])
				xraman = 10e6/laserWl - 10e6/wl[p]
				print laserWl
			endif
			break
	endswitch

	return 0
End

Window absorption_acetonitrile() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(1175.25,61.25,1404.75,338) acetonitrile as "absorption_acetonitrile"
	AppendToGraph/L=excitation average02/TN=series0resonance vs :PLEMd2:maps:mkl44TRmaps_acn_0:yExcitation
	AppendToGraph/L=excitation average00/TN=series0air vs :PLEMd2:maps:mkl44TRSimap_air_10:yExcitation
	AppendToGraph/L=excitation average01/TN=series0solvent vs :PLEMd2:maps:mkl44TRISimap_acn_0:yExcitation
	ModifyGraph margin(top)=85
	ModifyGraph muloffset(series0air)={0,5},muloffset(series0solvent)={0,2}
	ModifyGraph nticks(left)=0
	ModifyGraph lblPosMode(excitation)=1
	ModifyGraph lblPos(left)=54
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph axisEnab(left)={0,0.49}
	ModifyGraph axisEnab(excitation)={0.51,1}
	ModifyGraph manTick(bottom)={0,100,0,0},manMinor(bottom)={3,2}
	Label left "OD"
	Label bottom "wavelength [nm]"
	Label excitation "PLE intensity"
	SetAxis left 0.515808054570089,0.553004018541887
	SetAxis bottom 525,750
	SetAxis excitation 0,5.1e-09
	Cursor/P A acetonitrile 618
	ShowInfo
	Legend/C/N=text0/J/A=LB/X=-2.02/Y=111.22 "\\s(acetonitrile) acetonitrile\r\\s(series0resonance) SWNT emission in acetonitrile"
	AppendText "\\s(series0air) SWNT emission in air (x5)\r\\s(series0solvent) AcN phonon (x2)"
EndMacro
