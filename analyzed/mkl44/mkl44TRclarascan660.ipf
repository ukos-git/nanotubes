#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "sma"
#include "utilities-images"

Function export()
	saveWindow("SMAgetCoordinatesfullImage", customName = IgorInfo(1) + "_fullImage", saveTiff = 1, saveImages = 0)
	WAVE coordinates = root:mkl44airSi_coordinates
	SMADuplicateRangeFromCoordinates(coordinates)
	// available: mkl44mapsAir_coordinates
End