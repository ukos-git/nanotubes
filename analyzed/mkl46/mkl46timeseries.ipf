#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include <Split Axis>

Function export()
	SaveWindow("win_peakLocation", saveJSON = 0, saveImages = 1, saveVector = 0)
	SaveWindow("extracted", saveJSON = 0, saveImages = 1, saveVector = 0)
End

Function extractTimeSeries()
	WAVE peakLocation, peakLocationErr
	make/O/N=(DimSize(peakLocation, 0) / 8) peakLocation0 = peakLocation[p*8]
	make/O/N=(DimSize(peakLocation, 0) / 8) peakLocation0err = peakLocationErr[p*8]
	make/O/N=(DimSize(peakLocation, 0) / 8) peakLocation5 = peakLocation[p*8 + 5]
	make/O/N=(DimSize(peakLocation, 0) / 8) peakLocation5err = peakLocationErr[p*8 + 5]

	WAVE peakHeight, peakHeightErr
	make/O/N=(DimSize(peakHeight, 0) / 8) peakHeight0 = peakHeight[p*8]
	make/O/N=(DimSize(peakHeight, 0) / 8) peakHeight0err = peakHeightErr[p*8]

	make/O/N=(DimSize(peakHeight, 0) / 8) peakHeight5 = peakHeight[p*8 + 5]
	make/O/N=(DimSize(peakHeight, 0) / 8) peakHeight5err = peakHeightErr[p*8 + 5]

	// exposure time 60s

	SetScale/P x, 0, 1, peakHeight, peakLocation
	SetScale/P x, 0, 8, peakHeight0, peakHeight5, peakLocation0, peakLocation5

	WAVE source
	make/O/N=(DimSize(source, 0) / 8, DimSize(source, 1)) source0 = source[p*8][q]
	make/O/N=(DimSize(source, 0) / 8, DimSize(source, 1)) source5 = source[p*8+5][q]
	setscale/P x, 0, 8, source0, source5
End


Window win_peakLocation() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(695.25,49.25,1067.25,265.25) peakLocation as "win_peakLocation"
	ModifyGraph mode=3
	ModifyGraph marker=8
	ModifyGraph zmrkSize(peakLocation)={peakHeight,0,*,1,10}
	SetAxis left 800,1000
	Cursor/P A peakLocation 45
EndMacro

Window extracted() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(369,59,828,352.25) peakLocation5 as "extracted"
	AppendToGraph/L=left_P2 peakLocation0
	AppendImage/L=left_P2 source0 vs {*,wavelengthImage}
	ModifyImage source0 ctab= {0,1100,BlueHot,0}
	AppendImage source5 vs {*,wavelengthImage}
	ModifyImage source5 ctab= {0,1100,BlueHot,0}
	ModifyGraph margin(right)=85,height={Aspect,0.75}
	ModifyGraph mode=3
	ModifyGraph marker(peakLocation5)=16,marker(peakLocation0)=19
	ModifyGraph opaque(peakLocation5)=1
	ModifyGraph useMrkStrokeRGB=1
	ModifyGraph zmrkSize(peakLocation5)={peakHeight5,0,25,1,10},zmrkSize(peakLocation0)={peakHeight0,0,29,1,10}
	ModifyGraph zColor(peakLocation5)={peakHeight5,0,25,BlueHot},zColor(peakLocation0)={peakHeight0,0,29,BlueHot}
	ModifyGraph mirror(left)=0,mirror(bottom)=0
	ModifyGraph standoff(left)=0,standoff(left_P2)=0
	ModifyGraph lblPosMode(left_P2)=1
	ModifyGraph lblPos(left)=51
	ModifyGraph lblLatPos(left_P2)=50
	ModifyGraph freePos(left_P2)={0,kwFraction}
	ModifyGraph axisEnab(left)={0,0.479}
	ModifyGraph axisEnab(left_P2)={0.521,1}
	ModifyGraph manTick(bottom)={0,30,0,0},manMinor(bottom)={2,50}
	Label bottom "time [min]"
	Label left_P2 "wavelength [nm]"
	SetAxis left 887,902
	SetAxis bottom -1,200
	SetAxis left_P2 950,965
	ErrorBars/RGB=(0,0,0) peakLocation5 Y,wave=(peakLocation5err,peakLocation5err)
	ErrorBars/RGB=(0,0,0) peakLocation0 Y,wave=(peakLocation0err,peakLocation0err)
	ColorScale/C/N=text0/F=0/X=105.00/Y=0.00 image=source0, nticks=2, minor=1
	AppendText "spectral intensity [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv gstart,gname= WM_SplitMarkGroup0
	DrawLine -0.013745704467354,0.510501312335958,0.013745704467354,0.531498687664042
	DrawLine -0.013745704467354,0.468501312335958,0.013745704467354,0.489498687664042
	SetDrawEnv gstop
	SetWindow kwTopWin,userdata(WMSplitAxisList)=  "WMSplitAxis0;"
	SetWindow kwTopWin,userdata(WMSplitAxis0)=  "AXIS=left;NEWAXIS=left_P2;AXISSTART=882.19;AXISEND=967.73;AXISENABSTART=0;AXISENABEND=1;AXISHASSLASHR=0;SPLITMARKS=WM_SplitMarkGroup0;"
	SetWindow kwTopWin,userdata(WM_SplitMarkGroup0)=  "AXIS1=left;AXIS2=left_P2;"
	SetWindow kwTopWin,userdata(WM_SplitMarkGroupList)=  "WM_SplitMarkGroup0;"
EndMacro

