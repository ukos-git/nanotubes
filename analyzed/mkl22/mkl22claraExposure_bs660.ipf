#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindows("*")
End

Function ManualExport()
	Variable i
	WAVE/T wv = ListToTextWave(WinList("!movie", ";",""), ";")
	Sort/A=2 wv, wv

	DoWindow/F movie
	NewMovie/CF=0/Z/F=2/O/P=home as IgorInfo(1)
	For(i=0; i < DimSize(wv, 0); i += 1)
		saveWindow(wv[i], saveJSON=0, saveImages=1, saveVector=0)
		WAVE/T images = ListtoTextWave(ImageNameList(wv[i], ";"), ";")
		Make/FREE/WAVE/N=(DimSize(images, 0)) waves = ImageNameToWaveRef(wv[i], images[p])
		NVAR numTime = $(GetWaveSDataFolder(waves[0], 1) + "INFO:gnumExposure")
		ReplaceWave/W=movie image=plem, waves[0]
		TextBox/C/N=time "\\JLt=" + num2str(numTime) + "s"
		DoUpdate/W=movie
		AddMovieFrame
	EndFor
	CloseMovie
End

Window mkl22claraExposure300bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(26.25,50.75,299.25,273.5) as "mkl22claraExposure300bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure300_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=300s"
EndMacro

Window mkl22claraExposure150bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(312,50,585,272.75) as "mkl22claraExposure150bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure150_1:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=150s"
EndMacro

Window mkl22claraExposure60bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(598.5,48.5,871.5,271.25) as "mkl22claraExposure60bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure60_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=60s"
EndMacro

Window mkl22claraExposure30bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(511.5,297.5,784.5,520.25) as "mkl22claraExposure30bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure30_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=30s"
EndMacro

Window mkl22claraExposure15bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(792,298.25,1065,521) as "mkl22claraExposure15bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure15_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=15s"
EndMacro

Window mkl22claraExposure5bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(883.5,48.5,1156.5,271.25) as "mkl22claraExposure5bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure5_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=5s"
EndMacro

Window mkl22claraExposure1bs660() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(1081.5,293,1354.5,515.75) as "mkl22claraExposure1bs660"
	AppendImage :PLEMd2:maps:mkl22claraExposure1_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(right)=56,width=175,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,4,0,0},manMinor(left)={1,50}
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=110.00/Y=0.00 "\\JLt=1s"
EndMacro

Window movie() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(27.75,296,499.5,673.25) as "movie"
	AppendImage :PLEMd2:maps:mkl22claraExposure300_0:PLEM
	ModifyImage PLEM ctab= {0,150,YellowHot,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=108,gfSize=16
	ModifyGraph width=350,height={Aspect,1}
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axThick=0
	Label left "position [µm]"
	Label bottom "position [µm]"
	SetAxis left 19,34
	SetAxis bottom 122,137
	ColorScale/C/N=scale/F=0/Z=1/A=LB/X=100.00/Y=-2.50 image=PLEM, heightPct=85
	ColorScale/C/N=scale nticks=3, minor=1
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/B=1/A=LT/X=103.18/Y=2.83 "\\JLt=300s"
EndMacro

