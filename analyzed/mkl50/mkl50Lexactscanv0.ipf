#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("desorption", saveVector = 1)
End
Window desorption() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(372.75,177.5,831,667.25)/HIDE=1 /L=four peakLocation[77,87] vs exposuretime as "desorption"
	AppendToGraph/L=four peakLocation[253,263] vs exposuretime
	AppendToGraph/L=one peakLocation[605,615] vs exposuretime
	AppendToGraph/L=two peakLocation[374,383] vs exposuretime
	AppendToGraph/L=three peakLocation[506,516] vs exposuretime
	AppendToGraph/L=laser/B=laserprofile laserprofile2[*][20],laserprofile3[*][70]
	ModifyGraph margin(top)=14,expand=-2,gfSize=5,height={Aspect,1.25},gbRGB=(62451,62451,62451)
	ModifyGraph mode(peakLocation)=4,mode(peakLocation#1)=4,mode(peakLocation#2)=4,mode(peakLocation#3)=4
	ModifyGraph mode(peakLocation#4)=4,mode(laserprofile2)=3
	ModifyGraph marker(peakLocation)=19,marker(peakLocation#1)=19,marker(peakLocation#2)=19
	ModifyGraph marker(peakLocation#3)=19,marker(peakLocation#4)=19,marker(laserprofile2)=10
	ModifyGraph lSize(peakLocation)=0.5,lSize(peakLocation#1)=0.5,lSize(peakLocation#2)=0.5
	ModifyGraph lSize(peakLocation#3)=0.5,lSize(peakLocation#4)=0.5
	ModifyGraph mrkThick=0
	ModifyGraph useMrkStrokeRGB(peakLocation)=1,useMrkStrokeRGB(peakLocation#1)=1,useMrkStrokeRGB(peakLocation#2)=1
	ModifyGraph useMrkStrokeRGB(peakLocation#3)=1,useMrkStrokeRGB(peakLocation#4)=1
	ModifyGraph useMrkStrokeRGB(laserprofile2)=1
	ModifyGraph offset(peakLocation)={4,0},offset(peakLocation#1)={2.25,0},offset(peakLocation#2)={3,0}
	ModifyGraph offset(peakLocation#3)={1,0},offset(peakLocation#4)={2.25,0}
	ModifyGraph zColor(peakLocation)={peakHeight[77,87],0,*,YellowHot},zColor(peakLocation#1)={peakHeight[253,263],0,*,YellowHot}
	ModifyGraph zColor(peakLocation#2)={peakHeight[605,615],0,*,YellowHot},zColor(peakLocation#3)={peakHeight[374,383],0,*,YellowHot}
	ModifyGraph zColor(peakLocation#4)={peakHeight[506,516],0,*,YellowHot},zColor(laserprofile2)={laserprofile2[*][20],0,*,YellowHot}
	ModifyGraph zColor(laserprofile3)={laserprofile3[*][70],0,*,YellowHot}
	ModifyGraph grid(four)=2,grid(one)=2,grid(two)=2,grid(three)=2
	ModifyGraph nticks(laserprofile)=0
	ModifyGraph minor(bottom)=1
	ModifyGraph fSize=8
	ModifyGraph axRGB(four)=(30583,30583,30583),axRGB(two)=(30583,30583,30583),axRGB(laser)=(65535,0,0)
	ModifyGraph tlblRGB(laser)=(65535,0,0)
	ModifyGraph alblRGB(laser)=(65535,0,0)
	ModifyGraph lblPosMode(two)=4,lblPosMode(three)=1,lblPosMode(laser)=4
	ModifyGraph lblPos(four)=57,lblPos(bottom)=36,lblPos(two)=50,lblPos(laser)=65
	ModifyGraph freePos(four)={0,kwFraction}
	ModifyGraph freePos(one)={0,kwFraction}
	ModifyGraph freePos(two)={0,kwFraction}
	ModifyGraph freePos(three)={0,kwFraction}
	ModifyGraph freePos(laser)={0,kwFraction}
	ModifyGraph freePos(laserprofile)={0,kwFraction}
	ModifyGraph axisEnab(four)={0.8,1}
	ModifyGraph axisEnab(one)={0.2,0.39}
	ModifyGraph axisEnab(two)={0.4,0.59}
	ModifyGraph axisEnab(three)={0.6,0.79}
	ModifyGraph axisEnab(laser)={0,0.19}
	ModifyGraph manTick(four)={0,2,0,0},manMinor(four)={1,0}
	ModifyGraph manTick(one)={0,2,0,0},manMinor(one)={1,0}
	ModifyGraph manTick(two)={0,2,0,0},manMinor(two)={1,0}
	ModifyGraph manTick(three)={0,2,0,0},manMinor(three)={1,0}
	ModifyGraph manTick(laser)={0,100,0,0},manMinor(laser)={1,50}
	Label bottom "excitation time [s]"
	Label two "wavelength [nm]"
	Label laser "exc. power\r[µW]"
	SetAxis four 1004.9,1009.9
	SetAxis bottom 0,25
	SetAxis one 912,917
	SetAxis two 962,967
	SetAxis three 975.6,980.6
	SetAxis laser 0,*
	SetAxis laserprofile -2.5,3.75
EndMacro

