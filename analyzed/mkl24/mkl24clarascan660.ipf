#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "sma"
#include "utilities-images"

Function export()
	saveWindow("SMAgetCoordinatesfullImage", customName = IgorInfo(1) + "_fullImage", saveTiff = 1, saveImages = 0)
	WAVE coordinates = root:mkl24airSi_coordinates
	SMADuplicateRangeFromCoordinates(coordinates)
	// available: root:PLEMd2:mkl24exactscanNewtonV2_coordinates,root:PLEMd2:mkl24exactscantoluenev2_coordinates
End