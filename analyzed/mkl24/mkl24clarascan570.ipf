#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "sma"
#include "utilities-images"

Function export()
	WAVE coordinates = root:mkl24airSi_coordinates
	SMADuplicateRangeFromCoordinates(coordinates)
	// available: root:mkl24exactscanIdus_coordinates,root:mkl24exactscanNewtonV2_coordinates,root:mkl24exactscantoluenev2_coordinates
End