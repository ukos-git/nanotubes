#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("statistics", saveVector = 1)
End

//Function getMaximum()
//	Variable i, numSpectra
//	variable limit_low, limit_high, dummy
//	variable verbose = 0
//	
//	WAVE source = root:source
//	WAVE wavelength = root:wavelength
//	
//	numSpectra = DimSize(source, 0)
//	Make/O/N=(numSpectra) root:peak_maximum_energy/WAVE=energy = NaN, root:peak_maximum_intensity/WAVE=intensity = NaN, root:peak_maximum_fwhm/WAVE=fwhm = NaN
//	Make/O/N=(numSpectra) root:peak_maximum_energy_err/WAVE=energyErr = NaN, root:peak_maximum_intensity_err/WAVE=intensityErr = NaN, root:peak_maximum_fwhm_err/WAVE=fwhmErr = NaN
//	
//	FindValue/V=870/T=5 wavelength
//	limit_low = V_value
//	FindValue/V=900/T=5 wavelength
//	limit_high = V_value
//	
//	if(limit_low > limit_high)
//		dummy=limit_low
//		limit_low = limit_high
//		limit_high = dummy
//	endif
//	
//	Make/FREE/N=(limit_high - limit_low + 1) currentSpectrum, currentWavelength
//	currentWavelength = wavelength[limit_low + p]
//	
//	for(i = 0; i < numSpectra; i += 1)
//		currentSpectrum[] = source[i][limit_low + p]
//		
//		WAVE guess = Utilities#PeakFind(currentSpectrum, wvXdata = currentWavelength, maxPeaks = 1, verbose = verbose)
//		WAVE/WAVE coef = Utilities#BuildCoefWv(currentSpectrum, wvXdata = currentWavelength, peaks = guess, verbose = verbose)
//		WAVE/WAVE peakParam = Utilities#fitGauss(currentSpectrum, wvXdata = currentWavelength, wvCoef = coef, verbose = verbose)
//		
//		if(WaveExists(peakParam) && DimSize(peakParam, 0) > 0)
//			WAVE peak = peakParam[0]
//			energy[i] = peak[0][0]
//			energyErr[i] = peak[0][1]
//	
//			intensity[i] = peak[1][0]
//			intensityErr[i] = peak[1][1]
//			
//			fwhm[i] = peak[3][0]
//			fwhmErr[i] = peak[3][1]
//		else
//			print "error in" , i
//		endif
//	endfor
//
//End
Window statistics() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(35.25,42.5,493.5,323.75) source_maxHeight vs source_time as "statistics"
	AppendToGraph/R source_maxFWHM vs source_time
	AppendToGraph/L=energy source_maxPosition vs source_time
	ModifyGraph margin(left)=47
	ModifyGraph mode=4
	ModifyGraph marker(source_maxFWHM)=7,marker(source_maxPosition)=8
	ModifyGraph rgb(source_maxPosition)=(0,0,0)
	ModifyGraph standoff(left)=0
	ModifyGraph lblPosMode(left)=1,lblPosMode(energy)=1
	ModifyGraph lblPos(left)=42
	ModifyGraph freePos(energy)={0,bottom}
	ModifyGraph axisEnab(left)={0.55,1}
	ModifyGraph axisEnab(right)={0,0.45}
	ModifyGraph axisEnab(energy)={0,0.45}
	ModifyGraph manTick(energy)={0,2,0,0},manMinor(energy)={1,50}
	Label left "intensity [a.u.]"
	Label bottom "time [min]"
	Label right "FWHM [nm]"
	Label energy "wavelength [nm]"
	Cursor/P A source_maxHeight 54;Cursor/P B source_maxHeight 76
	TextBox/C/N=text1/F=0/A=MC/X=-27.94/Y=-2.81 "r (FWHM, wavelength) = -0.76"
	TextBox/C/N=text2/F=0/A=MC/X=-22.17/Y=47.79 "r (intensity, wavelength) = 0.73"
	Legend/C/N=text0/J/S=1/A=MC/X=4.07/Y=-41.53 "\\s(source_maxFWHM) FWHM\r\\s(source_maxPosition) wavelength"
EndMacro

