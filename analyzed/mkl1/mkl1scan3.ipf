#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include "plem"
#include "sma"

Function export()
	SaveWindow("overview", saveVector = 1)
End

Window overview() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:PLEMd2:
	Display /W=(20.25,44.75,489.75,408.5) coordinates[*][0] vs coordinates[*][1] as "overview"
	AppendImage borders
	ModifyImage borders explicit= 1
	ModifyImage borders eval={0,65535,65535,65535}
	ModifyImage borders eval={255,-1,-1,-1}
	ModifyImage borders eval={1,30583,30583,30583}
	AppendImage trenches
	ModifyImage trenches explicit= 1
	ModifyImage trenches eval={0,65535,65535,65535}
	ModifyImage trenches eval={255,-1,-1,-1}
	ModifyImage trenches eval={1,30583,30583,30583}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=7,margin(bottom)=7,margin(top)=7,margin(right)=113,width=350
	ModifyGraph height={Plan,1,left,bottom}
	ModifyGraph mode=3
	ModifyGraph marker=19
	ModifyGraph useMrkStrokeRGB=1
	ModifyGraph mrkStrokeRGB=(30583,30583,30583)
	ModifyGraph zmrkSize(coordinates)={source_maxHeight,-1,5,1,10}
	ModifyGraph zColor(coordinates)={source_maxPosition,800,1300,dBZ14}
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph axThick=0
	SetAxis left -2,100
	SetAxis bottom -2,100
	ColorScale/C/N=text0/F=0/A=LT/X=108.46/Y=21.14 trace=coordinates
	AppendText "wavelength [nm]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 5,linecap= 2
	DrawLine 112.865102639296,0.0603585879208582,122.865102639296,0.0603585879208582
	SetDrawEnv xcoord= bottom,ycoord= prel,fsize= 16
	DrawText 111.828956051345,0.123062461745049,"10µm"
EndMacro

