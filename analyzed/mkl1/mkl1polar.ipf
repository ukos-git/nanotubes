#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include <Multi-peak fitting 2.0>
#include <InsertSubwindowInGraph>

Function export()
	saveWindow("combined", saveVector = 1)
End

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(492.75,72.5,901.5,295.25) as "combined"
	DefineGuide split={FL,0.5,FR}
	Display/W=(0.2,0.2,0.8,0.8)/FG=(FL,FT,split,FB)/HOST=#  :Packages:MultiPeakFit2:MPF_SetFolder_5:'Peak 1'
	AppendToGraph :Packages:MultiPeakFit2:MPF_SetFolder_5:'Peak 0',:Packages:MultiPeakFit2:MPF_SetFolder_3:'Peak 0'
	AppendToGraph fit_polar045,:Packages:MultiPeakFit2:MPF_SetFolder_3:fit_polar000,polar000
	AppendToGraph polar045,polar090,fit_polar090,:Packages:MultiPeakFit2:MPF_SetFolder_6:'Peak 0'
	ModifyGraph width=350,height={Aspect,0.5}
	ModifyGraph mode('Peak 1')=7,mode('Peak 0')=7,mode('Peak 0'#1)=7,mode(polar000)=3
	ModifyGraph mode(polar045)=2,mode(polar090)=2
	ModifyGraph marker(polar000)=62,marker(polar045)=60,marker(polar090)=62
	ModifyGraph rgb('Peak 1')=(0,0,0),rgb('Peak 0')=(0,0,0),rgb(fit_polar045)=(1,16019,65535)
	ModifyGraph rgb(polar045)=(1,16019,65535),rgb(polar090)=(0,0,0),rgb(fit_polar090)=(0,0,0)
	ModifyGraph rgb('Peak 0'#2)=(1,16019,65535)
	ModifyGraph msize(polar000)=0.1,msize(polar045)=0.1,msize(polar090)=0.1
	ModifyGraph hbFill('Peak 1')=5,hbFill('Peak 0')=5,hbFill('Peak 0'#1)=5
	ModifyGraph plusRGB('Peak 0'#1)=(65535,0,0,13107)
	ModifyGraph hideTrace(fit_polar045)=1,hideTrace(polar045)=1,hideTrace('Peak 0'#2)=1
	ModifyGraph offset(fit_polar045)={0,-7},offset(fit_polar000)={0,-7},offset(polar000)={0,-7}
	ModifyGraph offset(polar045)={0,-7},offset(polar090)={0,-12},offset(fit_polar090)={0,-12}
	ModifyGraph noLabel(left)=1
	ModifyGraph manTick(bottom)={0,50,0,0},manMinor(bottom)={4,50}
	Label left "PL intensity [a.u.]"
	Label bottom "wavelength [nm]"
	SetAxis left 0,50
	SetAxis bottom 890,1040
	Legend/C/N=text0/J/S=1/A=LT/X=0.00/Y=0.00 "polarisation\r\\s(fit_polar045) 45°\r\\s(fit_polar000) 0°\r\\s(fit_polar090) 90°"
	RenameWindow #,phononsideband
	SetActiveSubwindow ##
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Packages:MultiPeakFit2:MPF_SetFolder_6:mkl2air_silicon_example:data2:
	Display/W=(0.2,0.2,0.8,0.8)/FG=(split,FT,FR,FB)/HOST=#  ::data1:PLEM vs ::data1:xWavelength
	AppendToGraph ::PLEM vs ::xWavelength
	AppendToGraph PLEM vs xWavelength
	AppendToGraph mkl3spectra_222_norm,mkl3spectra_294_norm
	AppendToGraph ::data3:PLEM vs ::data3:xWavelength
	SetDataFolder fldrSav0
	ModifyGraph rgb(mkl3spectra_222_norm)=(0,0,0),rgb(mkl3spectra_294_norm)=(0,0,0)
	ModifyGraph rgb(PLEM#3)=(0,0,0)
	ModifyGraph offset(PLEM#1)={0,1},offset(PLEM#2)={0,2},offset(mkl3spectra_294_norm)={0,2}
	ModifyGraph offset(PLEM#3)={0,1}
	ModifyGraph noLabel(left)=1
	ModifyGraph standoff(bottom)=0
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={0,50}
	ModifyGraph manTick(bottom)={0,50,0,0},manMinor(bottom)={4,50}
	Label left "normalized intensity [a.u.]"
	Label bottom "wavelength [nm]"
	SetAxis left 0,3
	SetAxis bottom 890,1040
	Legend/C/N=legend/J/S=1/B=(65535,65535,65535,58982)/A=LT/X=0.00/Y=0.00 "treatment with\r\\s(PLEM) hydrogen\r\\s(PLEM#3) argon"
	RenameWindow #,mkl2air_silicon_example
	SetActiveSubwindow ##
EndMacro

