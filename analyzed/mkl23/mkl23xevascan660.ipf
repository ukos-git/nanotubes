#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "sma"
#include "utilities-images"

Function export()
	saveWindow("SMAgetCoordinatesfullImage", customName = IgorInfo(1) + "_fullImage", saveTiff = 1, saveImages = 0)
	//WAVE coordinates = root:coordinatesInGaAs
	//SMADuplicateRangeFromCoordinates(coordinates)
	WAVE coordinates = root:mkl23airInGaAs_coordinates
	SMADuplicateRangeFromCoordinates(coordinates)
End
