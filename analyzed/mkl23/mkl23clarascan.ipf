#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include <ZoomBrowser>
#include <GraphMagnifier>
#include <SaveGraph>
#include <ImageSlider>
#include <AxisSlider>
#include <Multi-peak fitting 2.0>
#include <InsertSubwindowInGraph>
#include <MovieFromStack>

#include "sma"
#include "utilities-images"

Function export()
	// SMAload()
	// SMAread()
	// SMAtasksLoadCamerascan()

	saveWindow("SMAgetCoordinatesfullImage", customName = "mkl23clarascan_fullImage", saveTiff = 1, saveImages = 0)
	saveWindow("magnification", customName = "cover")

	WAVE mkl23Siexactscan_coordinates = root:mkl23Siexactscan_coordinates
	SMADuplicateRangeFromCoordinates(mkl23Siexactscan_coordinates)
End

Function SliderProc(sa) : SliderControl
	STRUCT WMSliderAction &sa

	switch( sa.eventCode )
		case -1: // control being killed
			break
		default:
			if( sa.eventCode & 1 ) // value set
				Variable curval = sa.curval
				//WAVE wignerImage = root:WignerImage
				WAVE wignerImage = root:wignerProfileHor
				RemoveFromGraph/Z wigner;
				appendtograph/R wignerImage[][sa.curval]/TN=wigner
			endif
			break
	endswitch

	return 0
End

Function MakeGizmoMovie()
	Variable i

	DoWindow/F mkl23clarascan_wignerHorGizmo
	if(V_flag != 1)
		Abort "Window not Found"
	endif

	STRUCT WMSliderAction sa
	sa.eventCode = 1

	NewMovie/F=1/i/Z
	if(V_Flag == 0)
		for(i = 0; i < 26; i += 1)
			sa.curval = i
			SMASliderProcWignerHor(sa)
			DoUpdate
			AddMovieFrame
		endfor	
		CloseMovie
	endif
End

Function MakeMovie()
	Variable i

	DoWindow/F mkl23clarascan_wignerHor
	if(V_flag != 1)
		Abort "Window not Found"
	endif

	STRUCT WMSliderAction sa
	sa.eventCode = 1

	NewMovie/F=1/i/Z
	if(V_Flag == 0)
		for(i = 0; i < 26; i += 1)
			sa.curval = i
			SMASliderProcWignerHor(sa)
			DoUpdate
			AddMovieFrame
		endfor	
		CloseMovie
	endif
End