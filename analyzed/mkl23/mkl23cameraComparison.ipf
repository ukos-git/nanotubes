#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("zoom1", saveJSON=0)
	saveWindow("zoom2", saveJSON=0)
End
Window zoom1() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(24.75,134.75,489,270.5) as "zoom1"
	AppendImage/B=bottom_right/R ingaas
	ModifyImage ingaas ctab= {200,*,color_red,1}
	ModifyImage ingaas log= 1
	AppendImage/B=bottom_left silicon
	ModifyImage silicon ctab= {1000,30000,color_blue,1}
	ModifyImage silicon log= 1
	AppendImage/B=bottom_middle/L=lef2 ingaas
	ModifyImage ingaas#1 ctab= {200,*,color_red,1}
	ModifyImage ingaas#1 log= 1
	AppendImage/B=bottom_middle/R=right2 silicon
	ModifyImage silicon#1 ctab= {1000,30000,color_blue,1}
	ModifyImage silicon#1 log= 1
	ModifyGraph margin(left)=57,margin(bottom)=14,margin(top)=7,margin(right)=57,width=350
	ModifyGraph height={Aspect,0.33}
	ModifyGraph mirror(right)=0,mirror(left)=0
	ModifyGraph noLabel=1
	ModifyGraph axRGB=(0,0,0,0)
	ModifyGraph lblPos(right)=141,lblPos(left)=141
	ModifyGraph freePos(bottom_right)={0.05,kwFraction}
	ModifyGraph freePos(bottom_left)={0.05,kwFraction}
	ModifyGraph freePos(lef2)={0.33,kwFraction}
	ModifyGraph freePos(bottom_middle)={0.05,kwFraction}
	ModifyGraph freePos(right2)={0.34,kwFraction}
	ModifyGraph axisEnab(right)={0.05,0.95}
	ModifyGraph axisEnab(bottom_right)={0.7,1}
	ModifyGraph axisEnab(left)={0.05,0.95}
	ModifyGraph axisEnab(bottom_left)={0,0.3}
	ModifyGraph axisEnab(lef2)={0.05,0.95}
	ModifyGraph axisEnab(bottom_middle)={0.35,0.65}
	ModifyGraph axisEnab(right2)={0.05,0.95}
	ModifyGraph manTick(right)={0,10,0,0},manMinor(right)={0,50}
	ModifyGraph manTick(bottom_right)={0,10,0,0},manMinor(bottom_right)={0,50}
	ModifyGraph manTick(left)={0,10,0,0},manMinor(left)={0,50}
	ModifyGraph manTick(bottom_left)={0,10,0,0},manMinor(bottom_left)={0,50}
	ModifyGraph manTick(bottom_middle)={0,10,0,0},manMinor(bottom_middle)={0,50}
	SetAxis right 105,155
	SetAxis bottom_right 100,150
	SetAxis left 105,155
	SetAxis bottom_left 100,150
	SetAxis lef2 105,155
	SetAxis bottom_middle 100,150
	SetAxis right2 105,155
	ColorScale/C/N=text0/F=0/X=101.00/Y=0.00 image=ingaas, heightPct=60
	ColorScale/C/N=text0 axisRange={0,NaN,0}
	ColorScale/C/N=text1/F=0/A=RC/X=101.00/Y=0.00 image=silicon, side=2, heightPct=60
	SetDrawLayer UserFront
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.229554272194166,0.951360544217687,"silicon camera"
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.490800291724439,0.951360544217687,"overlay"
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.773857801296647,0.951360544217687,"InGaAs camera"
	SetDrawEnv xcoord= bottom_middle,ycoord= left,fillpat= 0
	DrawRect 100,105,150,155
	SetDrawEnv xcoord= bottom_middle,ycoord= prel,linethick= 2
	DrawLine 135,0.1,145,0.1
	SetDrawEnv fsize= 8
	DrawText 0.557730746852327,0.216506229445231,"10µm"
	SetDrawEnv xcoord= bottom_middle,ycoord= left,fillfgc= (65535,65535,65535,16384)
	DrawRect 113.5,122,117.5,118
EndMacro

Window zoom2() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(27,92,491.25,227.75) as "zoom2"
	AppendImage/B=bottom_right/R ingaas
	ModifyImage ingaas ctab= {300,1000,color_red,1}
	AppendImage/B=bottom_left silicon
	ModifyImage silicon ctab= {2000,32000,color_blue,1}
	ModifyImage silicon log= 1
	AppendImage/B=bottom_middle/L=lef2 ingaas
	ModifyImage ingaas#1 ctab= {300,1000,color_red,1}
	AppendImage/B=bottom_middle/R=right2 silicon
	ModifyImage silicon#1 ctab= {2000,30000,color_blue,1}
	ModifyImage silicon#1 log= 1
	ModifyGraph margin(left)=57,margin(bottom)=14,margin(top)=7,margin(right)=57,width=350
	ModifyGraph height={Aspect,0.33}
	ModifyGraph mirror(right)=0,mirror(left)=0
	ModifyGraph noLabel=1
	ModifyGraph axRGB=(0,0,0,0)
	ModifyGraph lblPos(right)=141,lblPos(left)=141
	ModifyGraph freePos(bottom_right)={0.05,kwFraction}
	ModifyGraph freePos(bottom_left)={0.05,kwFraction}
	ModifyGraph freePos(lef2)={0.33,kwFraction}
	ModifyGraph freePos(bottom_middle)={0.05,kwFraction}
	ModifyGraph freePos(right2)={0.34,kwFraction}
	ModifyGraph axisEnab(right)={0.05,0.95}
	ModifyGraph axisEnab(bottom_right)={0.7,1}
	ModifyGraph axisEnab(left)={0.05,0.95}
	ModifyGraph axisEnab(bottom_left)={0,0.3}
	ModifyGraph axisEnab(lef2)={0.05,0.95}
	ModifyGraph axisEnab(bottom_middle)={0.35,0.65}
	ModifyGraph axisEnab(right2)={0.05,0.95}
	ModifyGraph manTick(right)={0,10,0,0},manMinor(right)={0,50}
	ModifyGraph manTick(bottom_right)={0,10,0,0},manMinor(bottom_right)={0,50}
	ModifyGraph manTick(left)={0,10,0,0},manMinor(left)={0,50}
	ModifyGraph manTick(bottom_left)={0,10,0,0},manMinor(bottom_left)={0,50}
	ModifyGraph manTick(bottom_middle)={0,10,0,0},manMinor(bottom_middle)={0,50}
	SetAxis right 118,122
	SetAxis bottom_right 113.5,117.5
	SetAxis left 118,122
	SetAxis bottom_left 113.5,117.5
	SetAxis lef2 118,122
	SetAxis bottom_middle 113.5,117.5
	SetAxis right2 118,122
	Cursor/P/I A silicon 207,201
	ColorScale/C/N=text0/F=0/X=101.00/Y=0.00 image=ingaas, heightPct=60
	ColorScale/C/N=text0 axisRange={0,NaN,0}
	ColorScale/C/N=text1/F=0/A=RC/X=101.00/Y=0.00 image=silicon, side=2, heightPct=60
	SetDrawLayer UserFront
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.229554272194166,0.951360544217687,"silicon camera"
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.490800291724439,0.951360544217687,"overlay"
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.773857801296647,0.951360544217687,"InGaAs camera"
	SetDrawEnv xcoord= bottom_middle,ycoord= prel,linethick= 2
	DrawLine 116.492813141684,0.1,116.992813141684,0.1
	SetDrawEnv fsize= 8
	DrawText 0.563538586776542,0.225748373622681,"500nm"
	SetDrawEnv xcoord= bottom_middle,ycoord= left,fillpat= 0
	DrawRect 113.5,122,117.5,118
EndMacro

