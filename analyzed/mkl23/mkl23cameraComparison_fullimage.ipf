#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("zoom0")
End
Window zoom0() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(27,46.25,491.25,182) as "zoom0"
	AppendImage/B=bottom_right/R ingaas
	ModifyImage ingaas ctab= {0,775,color_red,1}
	AppendImage/B=bottom_left silicon
	ModifyImage silicon ctab= {0,20000,color_blue,1}
	AppendImage/B=bottom_middle/L=lef2 ingaas
	ModifyImage ingaas#1 ctab= {0,775,color_red,1}
	AppendImage/B=bottom_middle/R=right2 silicon
	ModifyImage silicon#1 ctab= {0,20000,color_blue,1}
	ModifyGraph margin(left)=57,margin(bottom)=14,margin(top)=7,margin(right)=57,width=350
	ModifyGraph height={Aspect,0.33}
	ModifyGraph mirror(right)=0,mirror(left)=0
	ModifyGraph noLabel=1
	ModifyGraph axRGB=(0,0,0,0)
	ModifyGraph lblPos(right)=141,lblPos(left)=141
	ModifyGraph freePos(bottom_right)={0.05,kwFraction}
	ModifyGraph freePos(bottom_left)={0.05,kwFraction}
	ModifyGraph freePos(lef2)={0.33,kwFraction}
	ModifyGraph freePos(bottom_middle)={0.05,kwFraction}
	ModifyGraph freePos(right2)={0.34,kwFraction}
	ModifyGraph axisEnab(right)={0.05,0.95}
	ModifyGraph axisEnab(bottom_right)={0.7,1}
	ModifyGraph axisEnab(left)={0.05,0.95}
	ModifyGraph axisEnab(bottom_left)={0,0.3}
	ModifyGraph axisEnab(lef2)={0.05,0.95}
	ModifyGraph axisEnab(bottom_middle)={0.35,0.65}
	ModifyGraph axisEnab(right2)={0.05,0.95}
	ModifyGraph manTick(right)={0,10,0,0},manMinor(right)={0,50}
	ModifyGraph manTick(bottom_right)={0,10,0,0},manMinor(bottom_right)={0,50}
	ModifyGraph manTick(left)={0,10,0,0},manMinor(left)={0,50}
	ModifyGraph manTick(bottom_left)={0,10,0,0},manMinor(bottom_left)={0,50}
	ModifyGraph manTick(bottom_middle)={0,10,0,0},manMinor(bottom_middle)={0,50}
	SetAxis right -5,305
	SetAxis bottom_right -5,305
	SetAxis left -5,305
	SetAxis bottom_left -5,305
	SetAxis lef2 -5,305
	SetAxis bottom_middle -5,305
	SetAxis right2 -5,305
	ColorScale/C/N=text0/F=0/X=101.00/Y=0.00 image=ingaas, heightPct=60
	ColorScale/C/N=text0 axisRange={0,NaN,0}
	ColorScale/C/N=text1/F=0/A=RC/X=101.00/Y=0.00 image=silicon, side=2, heightPct=60
	SetDrawLayer UserFront
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.234400798850063,0.951360544217687,"silicon camera"
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.490800291724439,0.951360544217687,"overlay"
	SetDrawEnv xcoord= rel,ycoord= rel,textxjust= 1,textyjust= 1
	DrawText 0.767395765755452,0.951360544217687,"InGaAs camera"
	SetDrawEnv xcoord= bottom_middle,ycoord= left,fillfgc= (65535,65535,65535,16384)
	DrawRect 100,105,150,155
	SetDrawEnv xcoord= bottom_middle,ycoord= prel,linethick= 2
	DrawLine 235.06160164271,0.1,285.061601642711,0.1
	SetDrawEnv xcoord= bottom_middle,ycoord= prel,fsize= 8
	DrawText 229.774200125242,0.18219026814221,"50µm"
EndMacro

