#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include "plem"
#include "sma"

Function export()
	saveWindow("heteroaggregate", saveImages = 1, saveVector = 0, saveJSON = 0)
	saveWindow("smawignerhor30", saveImages = 1, saveVector = 0, saveJSON = 0)
End
Window heteroaggregate() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl23aggregate:
	Display /W=(26.25,73.25,483.75,445.25)/T=intensity_left wavelength vs exactscan[0][*] as "heteroaggregate"
	AppendToGraph/T=intensity_right wavelength vs exactscan[10][*]
	AppendImage exactscan
	ModifyImage exactscan ctab= {0,4,Red,1}
	AppendImage/L=Lposition image_silicon
	ModifyImage image_silicon ctab= {*,*,YellowHot,0}
	AppendImage/L=Lposition_xeva image_ingaas
	ModifyImage image_ingaas ctab= {*,*,YellowHot,0}
	SetDataFolder fldrSav0
	ModifyGraph margin(bottom)=28,margin(top)=28,width={Aspect,1}
	ModifyGraph tick(intensity_left)=2,tick(intensity_right)=2
	ModifyGraph zero(intensity_left)=1,zero(intensity_right)=1
	ModifyGraph mirror(left)=3,mirror(bottom)=0
	ModifyGraph noLabel(intensity_left)=1,noLabel(intensity_right)=1,noLabel(bottom)=1
	ModifyGraph noLabel(Lposition)=1,noLabel(Lposition_xeva)=1
	ModifyGraph standoff=0
	ModifyGraph axThick(bottom)=0,axThick(Lposition)=0,axThick(Lposition_xeva)=0
	ModifyGraph lblPosMode(left)=4,lblPosMode(Lposition)=3,lblPosMode(Lposition_xeva)=3
	ModifyGraph lblPos(left)=50,lblPos(bottom)=35,lblPos(Lposition)=30,lblPos(Lposition_xeva)=30
	ModifyGraph freePos(intensity_left)={0,kwFraction}
	ModifyGraph freePos(intensity_right)={0,kwFraction}
	ModifyGraph freePos(Lposition)={0,kwFraction}
	ModifyGraph freePos(Lposition_xeva)={0,kwFraction}
	ModifyGraph axisEnab(left)={0.6,1}
	ModifyGraph axisEnab(intensity_left)={0,0.35}
	ModifyGraph axisEnab(intensity_right)={0.65,1}
	ModifyGraph axisEnab(Lposition)={0,0.25}
	ModifyGraph axisEnab(Lposition_xeva)={0.3,0.55}
	ModifyGraph gridEnab(intensity_left)={0,0.4}
	ModifyGraph gridEnab(intensity_right)={0,0.4}
	ModifyGraph manTick(left)={0,50,0,0},manMinor(left)={1,50}
	ModifyGraph manTick(intensity_left)={0,5,0,0},manMinor(intensity_left)={4,50}
	ModifyGraph manTick(intensity_right)={0,5,0,0},manMinor(intensity_right)={4,5}
	ModifyGraph manTick(bottom)={0,5,0,0},manMinor(bottom)={4,50}
	ModifyGraph manTick(Lposition)={0,5,0,0},manMinor(Lposition)={4,50}
	ModifyGraph manTick(Lposition_xeva)={0,5,0,0},manMinor(Lposition_xeva)={4,50}
	Label left "wavelength [nm]"
	Label bottom "position along the trench"
	Label Lposition "image\r(Si)"
	Label Lposition_xeva "image\r(InGaAs)"
	SetAxis left 900,1000
	SetAxis intensity_left 4,-1
	SetAxis intensity_right -1,4
	SetAxis bottom 103.508639263776,119.508639263776
	SetAxis Lposition 162,166
	Cursor/P/I A image_silicon 155,30
	ColorScale/C/N=text0/D={1,1,0}/F=0/B=1/A=LB/X=71.50/Y=100.00 image=exactscan
	ColorScale/C/N=text0 vert=0, widthPct=29, nticks=0
	ColorScale/C/N=text0_1/D={1,1,0}/F=0/B=1/A=LB/X=0.00/Y=100.00 image=exactscan
	ColorScale/C/N=text0_1 vert=0, widthPct=29, nticks=0, axisRange={0,4,1}
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= Lposition,linethick= 5,linefgc= (65535,65535,65535)
	DrawLine 103.867867867868,169.550089862211,105.867867867868,169.550089862211
	SetDrawEnv xcoord= bottom,ycoord= Lposition,textrgb= (65535,65535,65535)
	DrawText 104.071872874126,169.750089862211,"2µm"
EndMacro

Window smawignerhor30() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:smawignerhor:
	Display /W=(488.25,72.5,943.5,444.5)/L=left_LineProfileHor LineProfileHor as "smawignerhor30"
	AppendToGraph/L=left_WignerProfileSumHor WignerProfileSumHor
	AppendImage/L=left_WignerImageHor WignerImageHor
	ModifyImage WignerImageHor ctab= {-630957344.480194,630957344.480194,RedWhiteBlue,0}
	AppendImage WignerSource
	ModifyImage WignerSource ctab= {*,15000,YellowHot,0}
	ModifyImage WignerSource minRGB=0,maxRGB=NaN
	AppendImage/L=ingaas ::mkl23aggregate:image_ingaas
	ModifyImage image_ingaas ctab= {*,*,YellowHot,0}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=36,margin(bottom)=36,margin(top)=36,margin(right)=144,width={Plan,1,bottom,left}
	ModifyGraph grid(bottom)=1,grid(left_WignerImageHor)=1,grid(left)=1
	ModifyGraph mirror(bottom)=0,mirror(left)=0
	ModifyGraph nticks(left_LineProfileHor)=0,nticks(bottom)=0,nticks(left_WignerProfileSumHor)=0
	ModifyGraph nticks(left_WignerImageHor)=2,nticks(left)=2,nticks(ingaas)=0
	ModifyGraph minor(bottom)=1
	ModifyGraph noLabel(left_LineProfileHor)=2,noLabel(bottom)=2,noLabel(left_WignerImageHor)=2
	ModifyGraph noLabel(left)=2
	ModifyGraph standoff(left_LineProfileHor)=0,standoff(bottom)=0,standoff(left)=0
	ModifyGraph standoff(ingaas)=0
	ModifyGraph axOffset(bottom)=-0.75
	ModifyGraph axThick(left_LineProfileHor)=0,axThick(bottom)=0,axThick(left)=0,axThick(ingaas)=0
	ModifyGraph gridRGB(bottom)=(34952,34952,34952),gridRGB(left_WignerImageHor)=(34952,34952,34952)
	ModifyGraph gridRGB(left)=(34952,34952,34952)
	ModifyGraph axRGB(bottom)=(0,0,0,0),axRGB(left_WignerProfileSumHor)=(65535,65535,65535,0)
	ModifyGraph axRGB(left_WignerImageHor)=(0,0,0,0),axRGB(left)=(0,0,0,0)
	ModifyGraph lblPosMode(left_LineProfileHor)=1
	ModifyGraph lblPos(bottom)=49,lblPos(left)=67
	ModifyGraph lblLatPos(left_LineProfileHor)=57
	ModifyGraph freePos(left_LineProfileHor)={0,kwFraction}
	ModifyGraph freePos(left_WignerProfileSumHor)={0,kwFraction}
	ModifyGraph freePos(left_WignerImageHor)={0,kwFraction}
	ModifyGraph freePos(ingaas)=0
	ModifyGraph axisEnab(left_LineProfileHor)={0.45,0.6}
	ModifyGraph axisEnab(left_WignerProfileSumHor)={0.85,1}
	ModifyGraph axisEnab(left_WignerImageHor)={0.6,0.85}
	ModifyGraph axisEnab(left)={0.2,0.45}
	ModifyGraph axisEnab(ingaas)={0,0.2}
	ModifyGraph manTick(left_LineProfileHor)={0,0,0,2},manMinor(left_LineProfileHor)={0,50}
	ModifyGraph manTick(left_WignerProfileSumHor)={0,0,0,2},manMinor(left_WignerProfileSumHor)={0,50}
	ModifyGraph manTick(left_WignerImageHor)={0,2,0,0},manMinor(left_WignerImageHor)={0,50}
	ModifyGraph manTick(left)={0,2,0,0},manMinor(left)={0,0}
	Label left_LineProfileHor "spacial emission\rintensity [a.u]"
	SetAxis bottom 105,120
	ColorScale/C/N=WignerImageScaleBar/F=0/A=LB/X=74.40/Y=53.70/E=2
	ColorScale/C/N=WignerImageScaleBar image=WignerImageHor, heightPct=50, nticks=3
	ColorScale/C/N=WignerImageScaleBar highTrip=10, notation=1, prescaleExp=-8
	ColorScale/C/N=WignerImageScaleBar ZisZ=1
	AppendText "Wigner Probability |30⟩"
	TextBox/C/N=ingaas/S=1/A=LB/X=71.47/Y=16.98/E=2 "InGaAs Camera"
	TextBox/C/N=silicon/S=1/A=LB/X=71.73/Y=37.96/E=2 "Silicon Camera"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 3,linefgc= (65535,65535,65535)
	DrawLine 105.519700638345,0.664026646928201,107.519700638345,0.664026646928201
	SetDrawEnv xcoord= bottom,ycoord= prel,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 105.471429689229,0.648351472617448,"2µm"
EndMacro

