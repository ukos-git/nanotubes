#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=1	// Use modern global access method.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("extracted", saveJSON= 0, saveVector=0)
End
Window extracted() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(21,56.75,384.75,420.5) coordinates_modified[*][0]/TN=manual_from_image vs coordinates_modified[*][1] as "extracted"
	AppendToGraph/L=left_spectrum/B=bottom_spectrum :Packages:MultiPeakFit2:MPF_SetFolder_1:fit_spectrum
	AppendToGraph/L=left_profile/B=bottom2 lineprofile_hor
	AppendToGraph/L=left_spectrum/B=bottom_spectrum spectrum vs :SMAsourceGraph:wavelength
	AppendToGraph exactscan_full[*][0]/TN=full_exactscan vs exactscan_full[*][1]
	AppendImage imageRange6
	ModifyImage imageRange6 ctab= {5000,62000,YellowHot,0}
	ModifyImage imageRange6 log= 1
	AppendImage/B=bottom2/L=left2 imageRange6
	ModifyImage imageRange6#1 ctab= {5000,62000,YellowHot,0}
	ModifyImage imageRange6#1 log= 1
	ModifyGraph margin(left)=7,margin(bottom)=7,margin(top)=7,margin(right)=7,expand=-1
	ModifyGraph width=350,height={Aspect,1}
	ModifyGraph mode(fit_spectrum)=7,mode(lineprofile_hor)=7,mode(spectrum)=2,mode(full_exactscan)=3
	ModifyGraph marker(manual_from_image)=5,marker(spectrum)=19,marker(full_exactscan)=29
	ModifyGraph lSize(spectrum)=0.1
	ModifyGraph rgb(manual_from_image)=(65535,49151,49151),rgb(fit_spectrum)=(56797,56797,56797)
	ModifyGraph rgb(lineprofile_hor)=(56797,56797,56797),rgb(spectrum)=(0,0,0)
	ModifyGraph msize(manual_from_image)=7,msize(spectrum)=0.1
	ModifyGraph hbFill(fit_spectrum)=2,hbFill(lineprofile_hor)=2
	ModifyGraph usePlusRGB(fit_spectrum)=1,usePlusRGB(lineprofile_hor)=1
	ModifyGraph plusRGB(fit_spectrum)=(65535,32768,32768),plusRGB(lineprofile_hor)=(65535,32768,32768)
	ModifyGraph hideTrace(spectrum)=1,hideTrace(full_exactscan)=1
	ModifyGraph zmrkSize(full_exactscan)={:SMAsourceGraph:peakHeight,0,600,0,10}
	ModifyGraph zColor(full_exactscan)={:SMAsourceGraph:peakLocation,800,1300,dBZ14}
	ModifyGraph mirror(left)=1,mirror(bottom)=1,mirror(left2)=1
	ModifyGraph nticks(left)=0,nticks(bottom)=0,nticks(left_spectrum)=0,nticks(left_profile)=0
	ModifyGraph nticks(bottom2)=0,nticks(left2)=0
	ModifyGraph standoff(left)=0,standoff(bottom)=0,standoff(left_spectrum)=0,standoff(bottom_spectrum)=0
	ModifyGraph axRGB(left)=(65535,65535,65535,0),axRGB(bottom)=(65535,65535,65535,0)
	ModifyGraph axRGB(left_spectrum)=(65535,65535,65535,0),axRGB(bottom_spectrum)=(56797,56797,56797)
	ModifyGraph axRGB(left_profile)=(65535,65535,65535,0),axRGB(bottom2)=(56797,56797,56797)
	ModifyGraph axRGB(left2)=(56797,56797,56797)
	ModifyGraph tlblRGB(bottom_spectrum)=(61166,61166,61166),tlblRGB(bottom2)=(56797,56797,56797)
	ModifyGraph tlblRGB(left2)=(56797,56797,56797)
	ModifyGraph alblRGB(bottom_spectrum)=(61166,61166,61166),alblRGB(bottom2)=(56797,56797,56797)
	ModifyGraph alblRGB(left2)=(56797,56797,56797)
	ModifyGraph lblPosMode(bottom_spectrum)=3
	ModifyGraph lblPos(left)=88,lblPos(bottom)=66,lblPos(bottom_spectrum)=42
	ModifyGraph lblLatPos(bottom_spectrum)=-1
	ModifyGraph axisOnTop(left_spectrum)=1,axisOnTop(bottom_spectrum)=1,axisOnTop(bottom2)=1
	ModifyGraph axisOnTop(left2)=1
	ModifyGraph freePos(left_spectrum)={0.5,kwFraction}
	ModifyGraph freePos(bottom_spectrum)={0.75,kwFraction}
	ModifyGraph freePos(left_profile)={0.5,kwFraction}
	ModifyGraph freePos(bottom2)={0.3,kwFraction}
	ModifyGraph freePos(left2)={0.5,kwFraction}
	ModifyGraph axisEnab(left_spectrum)={0.75,1}
	ModifyGraph axisEnab(bottom_spectrum)={0.25,0.5}
	ModifyGraph axisEnab(left_profile)={0.55,0.75}
	ModifyGraph axisEnab(bottom2)={0.5,1}
	ModifyGraph axisEnab(left2)={0.3,0.55}
	ModifyGraph manTick(bottom_spectrum)={0,100,0,0},manMinor(bottom_spectrum)={3,2}
	ModifyGraph manTick(left_profile)={0,0,0,2},manMinor(left_profile)={0,50}
	Label bottom_spectrum "wavelength [nm]"
	SetAxis left 88,163
	SetAxis bottom 100,175
	SetAxis left_spectrum 0,*
	SetAxis bottom2 110.3,120.3
	SetAxis left2 117.5,122.5
	Cursor/P/I A imageRange6 757,427
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 5,linefgc= (65535,65535,65535),linejoin= 2,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.0383595706946045,0.127071974875503,"10µm"
	SetDrawEnv xcoord= bottom2,ycoord= left2,linethick= 5,linefgc= (65535,65535,65535),fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.538012177061051,0.547034310092075,"2µm"
	SetDrawEnv xcoord= bottom,ycoord= left,linefgc= (56797,56797,56797)
	DrawLine 110.3,122.5,137.553623999109,129.210901033818
	SetDrawEnv xcoord= bottom,ycoord= left,linefgc= (56797,56797,56797)
	DrawLine 110.3,117.5,137.459087241478,110.558506565839
	SetDrawEnv xcoord= bottom,ycoord= left,linefgc= (56797,56797,56797)
	DrawLine 120.3,122.5,137.506355620293,124.535997103869
	SetDrawEnv xcoord= bottom,ycoord= left,linefgc= (56797,56797,56797)
	DrawLine 120.206128973824,117.500030583036,137.506355620293,115.375074251241
	SetDrawEnv xcoord= bottom,ycoord= left,linefgc= (56797,56797,56797),linejoin= 2,fillfgc= (65535,65535,65535,16384)
	DrawRect 110.3,117.5,120.3,122.5
	SetDrawEnv fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.538012177061051,0.547034310092075,"2µm"
	SetDrawEnv xcoord= bottom2,ycoord= left2,linethick= 5,linefgc= (65535,65535,65535)
	DrawLine 111,121.939829323776,113,121.939829323776
	SetDrawEnv textrgb= (65535,65535,65535)
	DrawText 0.670710571923744,0.684575389948007,"⌀=0.757nm"
	SetWindow kwTopWin,hook=CursorMagKillGraphWindowHook,hookevents=2
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6-o!@;om93]d"
EndMacro

