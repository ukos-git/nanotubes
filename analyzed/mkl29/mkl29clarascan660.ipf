#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.


#include "sma"
#include "utilities-images"

Function export()
	WAVE coordinates = root:mkl29air_coordinates
	SMADuplicateRangeFromCoordinates(coordinates)
	// available: mkl29exactscan_coordinates
End