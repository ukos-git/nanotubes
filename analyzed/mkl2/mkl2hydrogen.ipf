#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("combined", saveVector = 1)
	SaveWindow("mkl2air_silicon_example", saveVector = 1)
	SaveWindow("nagatsu2010", saveVector = 1)
	SaveWindow("mkl2air_ediff", saveVector = 1)
End

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(51,52.25,509.25,273.5) as "combined"
	DefineGuide split={FL,0.5,FR}
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl2air_ingaas_ediff:
	Display/W=(0,0.2,0.8,0.8)/FG=($"",FT,split,FB)/HOST=#  ediff/TN=ingaas vs energy
	AppendToGraph ::mkl2air_silicon_ediff:ediff/TN=silicon vs ::mkl2air_silicon_ediff:energy
	AppendToGraph fit_ediff_ingaas,::mkl2air_silicon_ediff:fit_ediff
	SetDataFolder fldrSav0
	ModifyGraph mode(ingaas)=3,mode(silicon)=3
	ModifyGraph marker(ingaas)=19,marker(silicon)=19
	ModifyGraph rgb(silicon)=(1,16019,65535),rgb(fit_ediff)=(1,16019,65535)
	Label left "energy difference [eV]"
	Label bottom "emission from E₁₁ [eV]"
	SetAxis left 0,0.15
	Legend/C/N=text0/J/S=1/X=-10.00/Y=0.00 "\\s(ingaas) ingaas\r\\s(silicon) silicon"
	RenameWindow #,mkl2air_ediff
	SetActiveSubwindow ##
	String fldrSav1= GetDataFolder(1)
	SetDataFolder root:nagatsu2010:
	Display/W=(0.2,0.2,0.8,0.8)/FG=(split,FT,FR,FB)/HOST=#  fit_energy_difference,fit_nagatsu2010
	AppendToGraph nagatsu2010_ediff vs nagatsu2010_diameter
	AppendToGraph ::mkl2air_ingaas_diameter:mkl2air_ingaas_ediff[98,*]/TN=mkl2desorbed vs ::mkl2air_ingaas_diameter:mkl2air_ingaas_diameters[98,*]
	AppendToGraph ::mkl2air_silicon_diameter:mkl2air_silicon_ediff vs ::mkl2air_silicon_diameter:mkl2air_silicon_diameters
	AppendToGraph ::mkl2air_ingaas_diameter:mkl2air_ingaas_ediff[0,97]/TN=mkl2adsorbed vs ::mkl2air_ingaas_diameter:mkl2air_ingaas_diameters[0,97]
	AppendToGraph mkl3ediff vs diameter
	SetDataFolder fldrSav1
	ModifyGraph mode(nagatsu2010_ediff)=3,mode(mkl2desorbed)=3,mode(mkl2air_silicon_ediff)=3
	ModifyGraph mode(mkl2adsorbed)=3,mode(mkl3ediff)=3
	ModifyGraph marker(nagatsu2010_ediff)=19,marker(mkl2desorbed)=19,marker(mkl2air_silicon_ediff)=19
	ModifyGraph marker(mkl2adsorbed)=19,marker(mkl3ediff)=19
	ModifyGraph rgb(fit_energy_difference)=(0,0,0),rgb(mkl2desorbed)=(30583,30583,30583)
	ModifyGraph rgb(mkl2air_silicon_ediff)=(0,0,0),rgb(mkl2adsorbed)=(0,0,0),rgb(mkl3ediff)=(8738,8738,8738)
	ModifyGraph useMrkStrokeRGB(mkl2desorbed)=1
	ModifyGraph mrkStrokeRGB(mkl2desorbed)=(65535,0,0)
	Label left "energy difference [meV]"
	Label bottom "diameter [nm]"
	SetAxis left 0,150
	Legend/C/N=text0/J/S=1/B=(65535,65535,65535,32768)/A=LT/X=45.00/Y=1.32 "\\s(nagatsu2010_ediff) nagatsu2010\r\\s(mkl2desorbed) this work (des)"
	AppendText "\\s(mkl2adsorbed) this work (ads)"
	RenameWindow #,nagatsu2010
	SetActiveSubwindow ##
EndMacro

Window mkl2air_silicon_example() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl2air_silicon_example:data1:
	Display /W=(15,52.25,294,307.25) PLEM vs xWavelength as "mkl2air_silicon_example"
	AppendToGraph ::PLEM vs ::xWavelength
	AppendToGraph ::data2:PLEM vs ::data2:xWavelength
	AppendToGraph :::mkl3air_selection:mkl3spectra_222_norm,:::mkl3air_selection:mkl3spectra_294_norm
	AppendToGraph :::Graph2:data1:PLEM vs :::Graph2:data1:xWavelength
	SetDataFolder fldrSav0
	ModifyGraph rgb(mkl3spectra_222_norm)=(0,0,0),rgb(mkl3spectra_294_norm)=(0,0,0)
	ModifyGraph rgb(PLEM#3)=(0,0,0)
	ModifyGraph offset(PLEM#1)={0,1},offset(PLEM#2)={0,2},offset(mkl3spectra_294_norm)={0,2}
	ModifyGraph offset(PLEM#3)={0,1}
	ModifyGraph noLabel(left)=1
	ModifyGraph standoff=0
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={0,50}
	Label left "normalized intensity [a.u.]"
	Label bottom "wavelength [nm]"
	SetAxis left 0,3
	SetAxis bottom 890,1040
	Legend/C/N=legend/J/S=1/A=LT/X=0.00/Y=0.00 "treatment with\r\\s(PLEM) hydrogen\r\\s(PLEM#3) argon"
EndMacro

Window nagatsu2010() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:nagatsu2010:
	Display /W=(599.25,47,881.25,307.25) mkl3ediff vs diameter as "nagatsu2010"
	AppendToGraph fit_energy_difference,fit_nagatsu2010
	AppendToGraph nagatsu2010_ediff vs nagatsu2010_diameter
	AppendToGraph ::mkl2air_silicon_diameter:mkl2air_silicon_ediff vs ::mkl2air_silicon_diameter:mkl2air_silicon_diameters
	AppendToGraph ::fit_mkl3ediff,::mkl2air_ingaas_diameter:fit_mkl2air_ingaas_ediff
	AppendToGraph ::mkl2air_ingaas_diameter:mkl2air_ingaas_ediff[0,97]/TN=mkl2adsorbed vs ::mkl2air_ingaas_diameter:mkl2air_ingaas_diameters[0,97]
	AppendToGraph ::mkl2air_ingaas_diameter:mkl2air_ingaas_ediff[98,*]/TN=mkl2desorbed vs ::mkl2air_ingaas_diameter:mkl2air_ingaas_diameters[98,*]
	AppendToGraph ::mkl2air_ingaas_diameter:fit_dummyY
	SetDataFolder fldrSav0
	ModifyGraph mode(mkl3ediff)=3,mode(nagatsu2010_ediff)=3,mode(mkl2air_silicon_ediff)=3
	ModifyGraph mode(mkl2adsorbed)=3,mode(mkl2desorbed)=3
	ModifyGraph marker(mkl3ediff)=19,marker(nagatsu2010_ediff)=19,marker(mkl2air_silicon_ediff)=19
	ModifyGraph marker(mkl2adsorbed)=19,marker(mkl2desorbed)=19
	ModifyGraph lStyle(fit_dummyY)=3
	ModifyGraph rgb(mkl3ediff)=(30583,30583,30583),rgb(fit_energy_difference)=(0,0,0)
	ModifyGraph rgb(mkl2air_silicon_ediff)=(0,0,0),rgb(fit_mkl3ediff)=(0,0,0),rgb(fit_mkl2air_ingaas_ediff)=(0,0,0)
	ModifyGraph rgb(mkl2adsorbed)=(0,0,0),rgb(mkl2desorbed)=(30583,30583,30583),rgb(fit_dummyY)=(52428,1,1)
	ModifyGraph useMrkStrokeRGB(mkl2desorbed)=1
	ModifyGraph mrkStrokeRGB(mkl2desorbed)=(65535,0,0)
	Label left "energy difference [meV]"
	Label bottom "diameter [nm]"
	SetAxis left 0,150
	SetAxis bottom 0.68,1.25
	Legend/C/N=text0/J/S=1/B=(65535,65535,65535,32768)/X=-10.02/Y=-0.90 "\\s(fit_energy_difference) 30(4)/dₜ²\\s(mkl2adsorbed) this work"
	AppendText "\\s(fit_dummyY) 85(4)/dₜ²\\s(mkl2desorbed) this work\r\\s(fit_nagatsu2010) 68(2)/dₜ²\\s(nagatsu2010_ediff) nagatsu2010"
EndMacro

Window mkl2air_ediff() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mkl2air_ingaas_ediff:
	Display /W=(302.25,49.25,585,307.25) ediff/TN=ingaas vs energy as "mkl2air_ediff"
	AppendToGraph ::mkl2air_silicon_ediff:ediff/TN=silicon vs ::mkl2air_silicon_ediff:energy
	AppendToGraph fit_ediff_ingaas,::mkl2air_silicon_ediff:fit_ediff
	SetDataFolder fldrSav0
	ModifyGraph mode(ingaas)=3,mode(silicon)=3
	ModifyGraph marker(ingaas)=19,marker(silicon)=19
	ModifyGraph rgb(silicon)=(1,16019,65535),rgb(fit_ediff)=(1,16019,65535)
	Label left "energy difference [meV]"
	Label bottom "emission from E₁₁ [eV]"
	SetAxis left 0,0.15
	Legend/C/N=text0/J/S=1/X=-11.03/Y=-4.75 "\\s(ingaas) ingaas\r\\s(silicon) silicon"
EndMacro

