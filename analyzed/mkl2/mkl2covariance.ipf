#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	WAVE ingaaslevels
	ModifyContour/W=separated#covariance_ingaas covariance_sym manLevels=ingaaslevels
	WAVE siliconlevels
	ModifyContour/W=separated#covariance_silicon covariance_sym manLevels=root:siliconlevels
	DoUpdate/W=separated
	SaveWindow("contour", saveJSON=0, saveVector = 1)
	SaveWindow("separated", saveJSON=0, saveVector = 1)
End

Window contour() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(11.25,53,273,289.25) as "contour"
	AppendMatrixContour :Graph1:covariance_sym vs {:Graph1:wavelength,:Graph1:wavelength}
	ModifyContour covariance_sym manLevels={0,0,0},moreLevels={100000,200000,300000,400000}
	ModifyContour covariance_sym moreLevels={500000,600000,700000,800000,900000,1000000}
	ModifyContour covariance_sym moreLevels={2000000,3000000},rgbLines=(1,16019,65535)
	ModifyContour covariance_sym ctabFill={*,*,Grays,0},boundary=1,labels=0
	AppendMatrixContour :Graph0:covariance_sym vs {:Graph0:wavelength,:Graph0:wavelength}
	ModifyContour covariance_sym#1 autoLevels={30,130,5},rgbLines=(65535,0,0),boundary=1
	ModifyContour covariance_sym#1 labels=0
	ModifyGraph width={Plan,1,bottom,left}
	ModifyGraph rgb('covariance_sym=boundary')=(1,16019,65535)
	ModifyGraph usePlusRGB('covariance_sym#1=boundary')=1
	ModifyGraph plusRGB('covariance_sym#1=boundary')=(65535,49151,49151)
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	Label left "wavelength [nm]"
	Label bottom "wavelength [nm]"
	SetAxis left 800,1250
	SetAxis bottom 800,1250
EndMacro

Window separated() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(283.5,53.75,761.25,264.5) as "separated"
	DefineGuide split={FL,0.5,FR}
	Display/W=(0.2,0.2,0.8,0.8)/FG=(FL,FT,split,FB)/HOST=# 
	AppendMatrixContour :Graph1:covariance_sym vs {:Graph1:wavelength,:Graph1:wavelength}
	ModifyContour covariance_sym manLevels=siliconlevels,rgbLines=(17476,17476,17476)
	ModifyContour covariance_sym ctabFill={*,*,Grays,0},labels=0
	ModifyGraph width=350
	ModifyGraph hideTrace('covariance_sym=boundary')=1
	ModifyGraph mirror=0
	Label left "wavelength [nm]"
	Label bottom "wavelength [nm]"
	SetAxis left 830,1030
	SetAxis bottom 830,1030
	TextBox/C/N=text0/S=1/A=LT "Silicon"
	RenameWindow #,covariance_silicon
	SetActiveSubwindow ##
	Display/W=(0.2,0.2,0.8,0.8)/FG=(split,FT,FR,FB)/HOST=# 
	AppendMatrixContour :Graph0:covariance_sym vs {:Graph0:wavelength,:Graph0:wavelength}
	ModifyContour covariance_sym manLevels=ingaaslevels,rgbLines=(17476,17476,17476)
	ModifyContour covariance_sym labels=0
	ModifyGraph hideTrace('covariance_sym=boundary')=1
	ModifyGraph mirror=0
	Label left "wavelength [nm]"
	Label bottom "wavelength [nm]"
	SetAxis left 970,1170
	SetAxis bottom 970,1170
	TextBox/C/N=text0/S=1/A=LT "InGaAs"
	RenameWindow #,covariance_ingaas
	SetActiveSubwindow ##
EndMacro

