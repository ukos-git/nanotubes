#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("polymers", saveJSON = 1, saveImages = 1, saveVector = 1)
End

Window polymers() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(23.25,64.25,404.25,218.75) PFO,PFObpy as "polymers"
	AppendToGraph/B=bottom_left PFO,PFObpy
	ModifyGraph margin(left)=30,height={Aspect,0.33}
	ModifyGraph mode=7
	ModifyGraph lSize=1.2
	ModifyGraph rgb(PFObpy)=(1,16019,65535),rgb(PFObpy#1)=(1,16019,65535)
	ModifyGraph noLabel(left)=1
	ModifyGraph lblPosMode(left)=1,lblPosMode(bottom)=2,lblPosMode(bottom_left)=1
	ModifyGraph lblPos(bottom)=45
	ModifyGraph lblLatPos(bottom)=87
	ModifyGraph axisOnTop(bottom)=1,axisOnTop(bottom_left)=1
	ModifyGraph freePos(bottom_left)={0,kwFraction}
	ModifyGraph axisEnab(bottom)={0,0.45}
	ModifyGraph axisEnab(bottom_left)={0.55,1}
	ModifyGraph manTick(left)={0,0.2,0,1},manMinor(left)={1,50}
	ModifyGraph manTick(bottom)={0,200,0,0},manMinor(bottom)={1,5}
	ModifyGraph manTick(bottom_left)={0,200,0,0},manMinor(bottom_left)={1,50}
	Label left "OD"
	Label bottom "wavelength [nm]"
	SetAxis left -0.0293681527194805,0.462416932232467
	SetAxis bottom 450,800
	SetAxis bottom_left 950,1225
	TextBox/C/N=text0/F=0/A=LT/X=0.00/Y=0.00 "\\s(PFO) PFO\r\\s(PFObpy) PFObpy"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=11.85/Y=22.22 "(6,5)"
	TextBox/C/N=text2/F=0/B=1/A=MC/X=20.96/Y=49.31 "(7,5)"
	ShowTools/A
	SetDrawLayer UserFront
EndMacro

