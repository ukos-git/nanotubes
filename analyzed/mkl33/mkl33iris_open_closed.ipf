#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function CreateMovie()
	Variable i
	WAVE/T wv = ListToTextWave(WinList("mkl33exposure*", ";",""), ";")
	Sort/A=2 wv, wv

	DoWindow/F movie
	NewMovie/CF=0/Z/F=1/O/P=home as "mkl33exposure.mp4"
	For(i = 0; i < 4; i += 1)
		saveWindow(wv[i], saveJSON=0, saveImages=1, saveVector=0)
		WAVE/T images = ListtoTextWave(ImageNameList(wv[i], ";"), ";")
		Make/FREE/WAVE/N=(DimSize(images, 0)) waves = ImageNameToWaveRef(wv[i], images[p])
		NVAR numTime = $(GetWaveSDataFolder(waves[0], 1) + "INFO:gnumExposure")
		ReplaceWave/W=movie image=plem, waves[0]
		TextBox/C/N=time "\\JLt=" + num2str(numTime) + "s"
		DoUpdate/W=movie
		AddMovieFrame
	EndFor
	AddMovieFrame // repeat last frame
	CloseMovie
	saveWindow("movie", saveJSON=0, saveImages=1, saveVector=0)
End

Function export()
	Variable i
	WAVE/T wv = ListToTextWave(WinList("mkl33exposure*", ";",""), ";")
	Sort/A=2 wv, wv

	For(i=0; i < DimSize(wv, 0); i += 1)
		saveWindow(wv[i], saveJSON=0, saveImages=1, saveVector=0)
	EndFor
	saveWindow("movie", saveJSON=0, saveImages=1, saveVector=0)
End

Window mkl33exposure0_02s() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(636.75,370.25,913.5,597.5) as "mkl33exposure0.02s"
	AppendImage :PLEMd2:maps:mkl33830lpIrisOpen0_2s_0:PLEM
	ModifyImage PLEM ctab= {0,500,YellowHot256,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=70,expand=-1
	ModifyGraph width={Plan,1,bottom,left},height=200
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axRGB=(0,0,0,0)
	Label left "position / µm"
	Label bottom "position / µm"
	SetAxis left -36.0726696934039,-28.3517182110443
	SetAxis bottom -38.1277685575215,-30.6634914351047
	ColorScale/C/N=text0/F=0/Z=1/A=LB/X=105.00/Y=-2.50 image=PLEM
	AppendText "counts / s"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= rel,linethick= 4
	DrawLine -30.2573888662902,0.14,-28.2573888662902,0.14
	SetDrawEnv xcoord= bottom,ycoord= rel
	DrawText -29.8399478022582,0.102601156069364,"2µm"
	SetDrawEnv xcoord= rel,ycoord= rel
	DrawText 0.797826086956522,0.221635883905013,"t=0.02s"
EndMacro

Window mkl33exposure01s() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(930,371.75,1206.75,599) as "mkl33exposure01s"
	AppendImage :PLEMd2:maps:mkl33830lpIrisOpen1s_0:PLEM
	ModifyImage PLEM ctab= {0,500,YellowHot256,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=70,expand=-1
	ModifyGraph width={Plan,1,bottom,left},height=200
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axRGB=(0,0,0,0)
	Label left "position / µm"
	Label bottom "position / µm"
	SetAxis left -36.0726696934039,-28.3517182110443
	SetAxis bottom -38.1277685575215,-30.6634914351047
	ColorScale/C/N=text0/F=0/Z=1/A=LB/X=105.00/Y=-2.50 image=PLEM
	AppendText "counts / s"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= rel,linethick= 4
	DrawLine -30.2573888662902,0.14,-28.2573888662902,0.14
	SetDrawEnv xcoord= bottom,ycoord= rel
	DrawText -29.8399478022582,0.102601156069364,"2µm"
	SetDrawEnv xcoord= rel,ycoord= rel
	DrawText 0.797826086956522,0.221635883905013,"t=1s"
EndMacro

Window mkl33exposure10s() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(330.75,368.75,607.5,596) as "mkl33exposure10s"
	AppendImage :PLEMd2:maps:mkl33830lpIrisOpen10s_0:PLEM
	ModifyImage PLEM ctab= {0,500,YellowHot256,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=70,expand=-1
	ModifyGraph width={Plan,1,bottom,left},height=200
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axRGB=(0,0,0,0)
	Label left "position / µm"
	Label bottom "position / µm"
	SetAxis left -36.0726696934039,-28.3517182110443
	SetAxis bottom -38.1277685575215,-30.6634914351047
	ColorScale/C/N=text0/F=0/Z=1/A=LB/X=105.00/Y=-2.50 image=PLEM
	AppendText "counts / s"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= rel,linethick= 4
	DrawLine -30.2573888662902,0.14,-28.2573888662902,0.14
	SetDrawEnv xcoord= bottom,ycoord= rel
	DrawText -29.8399478022582,0.102601156069364,"2µm"
	SetDrawEnv xcoord= bottom,ycoord= rel,linethick= 4
	DrawLine -30.2573888662902,0.14,-28.2573888662902,0.14
	SetDrawEnv xcoord= bottom,ycoord= rel
	DrawText -29.8399478022582,0.102601156069364,"2µm"
	SetDrawEnv xcoord= rel,ycoord= rel
	DrawText 0.797826086956522,0.221635883905013,"t=10s"
EndMacro

Window mkl33exposure60s() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(30.75,370.25,307.5,597.5) as "mkl33exposure60s"
	AppendImage :PLEMd2:maps:mkl33830lpIrisOpen_0:PLEM
	ModifyImage PLEM ctab= {0,500,YellowHot256,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=70,expand=-1
	ModifyGraph width={Plan,1,bottom,left},height=200
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axRGB=(0,0,0,0)
	Label left "position / µm"
	Label bottom "position / µm"
	SetAxis left -36.0726696934039,-28.3517182110443
	SetAxis bottom -38.1277685575215,-30.6634914351047
	ColorScale/C/N=text0/F=0/Z=1/A=LB/X=105.00/Y=-2.50 image=PLEM
	AppendText "counts / s"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= rel,linethick= 4
	DrawLine -30.2573888662902,0.14,-28.2573888662902,0.14
	SetDrawEnv xcoord= bottom,ycoord= rel
	DrawText -29.8399478022582,0.102601156069364,"2µm"
	SetDrawEnv xcoord= rel,ycoord= rel
	DrawText 0.797826086956522,0.221635883905013,"t=60s"
EndMacro

Window movie() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:lineProfile:
	Display /W=(904.5,89,1286.25,315.5) MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX as "movie"
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendToGraph MIP_pathY/TN=wm_lineProfileTrace vs MIP_pathX
	AppendImage ::PLEMd2:maps:mkl33830lpIrisOpen_0:PLEM
	ModifyImage PLEM ctab= {0,500,YellowHot256,0}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=85,margin(right)=14,expand=-1
	ModifyGraph gfSize=16,width={Plan,1,bottom,left},height=283.465
	ModifyGraph zero=1
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axRGB=(0,0,0,0)
	Label left "position / µm"
	Label bottom "position / µm"
	SetAxis left -33.6750864663062,-31.006255540711
	SetAxis bottom -35.5340507075472,-33.6499115566038
	ColorScale/C/N=text0/F=0/A=LB/X=104.76/Y=-1.88 image=PLEM, fsize=10
	AppendText "intensity [cts/s]"
	TextBox/C/N=time/F=0/A=LB/X=103.66/Y=90.81 "\\JLt=60s"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine -33.8203278316422,-33.1844518909109,-33.8203278316422,-33.3844518909109
	SetDrawEnv xcoord= bottom,ycoord= left,fsize= 20,textrgb= (65535,65535,65535)
	DrawText -34.1332010677395,-33.5678627204754,"200nm"
	ModifyGraph swapXY=1
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6/7DG%kMK3r"
EndMacro

