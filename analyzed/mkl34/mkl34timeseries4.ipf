#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include <Split Axis>

Function export()
	SaveWindow("air")
End

Window air() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(16.5,50.75,473.25,331.25)/L=left2 peakHeight as "air"
	AppendToGraph/L=left3 peakLocation
	AppendImage source vs {*,wavelengthImage}
	ModifyImage source ctab= {0,500,Red,1}
	ModifyGraph margin(right)=85
	ModifyGraph mode=4
	ModifyGraph marker=8
	ModifyGraph msize=1
	ModifyGraph useMrkStrokeRGB=1
	ModifyGraph zColor(peakHeight)={peakHeight,0,500,Red,1}
	ModifyGraph mirror(bottom)=0,mirror(left)=0
	ModifyGraph standoff(left3)=0
	ModifyGraph lblPosMode(left2)=1,lblPosMode(left3)=1,lblPosMode(left)=1
	ModifyGraph lblPos(left)=57
	ModifyGraph freePos(left2)=0
	ModifyGraph freePos(left3)=0
	ModifyGraph axisEnab(left2)={0.5,0.75}
	ModifyGraph axisEnab(left3)={0.76,1}
	ModifyGraph axisEnab(left)={0,0.48}
	ModifyGraph manTick(left2)={0,100,0,0},manMinor(left2)={1,50}
	ModifyGraph manTick(bottom)={0,30,0,0},manMinor(bottom)={2,50}
	ModifyGraph manTick(left3)={0,2,0,0},manMinor(left3)={1,50}
	ModifyGraph manTick(left)={0,25,0,0},manMinor(left)={4,2}
	Label left2 "intensity\r[a.u.]"
	Label bottom "time [min]"
	Label left3 "wavelength\r[nm]"
	Label left "wavelength\r\n[nm]"
	SetAxis left2 0,250
	SetAxis left3 993,998
	SetAxis left 970,1020
	ErrorBars peakHeight Y,wave=(peakHeightErr,peakHeightErr)
	ErrorBars peakLocation Y,wave=(peakLocationErr,peakLocationErr)
	ColorScale/C/N=text0/F=0/A=MC/X=64.79/Y=-26.86 image=source, heightPct=50
	AppendText "intensity [a.u.]"
EndMacro

