#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("technique", saveJSON = 0)
	saveWindow("waterfall2", saveJSON = 0, saveVector = 1)
	saveWindow("overview", saveJSON = 1, saveVector = 1)
	saveWindow("measurementrange", saveJSON = 1, saveVector = 1)
End

Window overview() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:InGaAs:
	Display /W=(976.5,68,1417.5,431.75) coordinates[*][0]/TN=InGaAs vs coordinates[*][1] as "overview"
	AppendToGraph ::Silicon:coordinates[*][0]/TN=Silicon vs ::Silicon:coordinates[*][1]
	AppendImage ::borders
	ModifyImage borders explicit= 1
	ModifyImage borders eval={0,65535,65535,65535}
	ModifyImage borders eval={255,-1,-1,-1}
	ModifyImage borders eval={1,13107,13107,13107}
	AppendImage ::trenches
	ModifyImage trenches explicit= 1
	ModifyImage trenches eval={0,65535,65535,65535}
	ModifyImage trenches eval={255,-1,-1,-1}
	ModifyImage trenches eval={1,52428,52428,52428}
	AppendImage ::plotlydummy
	ModifyImage plotlydummy ctab= {*,1300,dBZ14,0}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=7,margin(bottom)=7,margin(top)=7,margin(right)=84,expand=-1
	ModifyGraph width=350,height={Plan,1,left,bottom}
	ModifyGraph mode=3
	ModifyGraph marker=29
	ModifyGraph mrkThick=0.1
	ModifyGraph gaps=0
	ModifyGraph mrkStrokeRGB=(0,0,0,6554)
	ModifyGraph zmrkSize(InGaAs)={:InGaAs:peakHeight,0,*,0,5},zmrkSize(Silicon)={:Silicon:peakHeight,0,*,0,5}
	ModifyGraph zColor(InGaAs)={:InGaAs:peakLocation,800,1300,dBZ14},zColor(Silicon)={:Silicon:peakLocation,800,1300,dBZ14}
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph standoff=0
	ModifyGraph axRGB(left)=(0,0,0,0),axRGB(bottom)=(65535,65535,65535,0)
	SetAxis left -5,300
	SetAxis bottom -5,300
	ColorScale/C/N=colorbar/F=0/B=1/A=LT/X=103.06/Y=20.79 image=plotlydummy
	ColorScale/C/N=colorbar heightPct=75
	AppendText "central emission wavelength [nm]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 5
	DrawLine 314.781514581876,287.5,364.781514581876,287.5
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawText 324.438914353392,268.957603938731,"50µm"
EndMacro

Window technique() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Silicon:
	Display /W=(520.5,67.25,962.25,341)/L=left1/B=bottom2 histLocationX vs histLocation as "technique"
	AppendToGraph/L=left2/T=bottom3 ::InGaAs:histLocationX vs ::InGaAs:histLocation
	AppendImage/B=bottom1/L=left1 source vs {*,wavelengthImage}
	ModifyImage source ctab= {0,*,Blue,1}
	AppendImage/T=bottom4/L=left2 ::InGaAs:source vs {*,::InGaAs:wavelengthImage}
	ModifyImage source#1 ctab= {0,*,Red,1}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=56,margin(bottom)=36,margin(top)=36
	ModifyGraph mode=6
	ModifyGraph lOptions(histLocationX)=2
	ModifyGraph rgb(histLocationX)=(0,0,65535)
	ModifyGraph gaps=0
	ModifyGraph lineJoin(histLocationX)={0,10}
	ModifyGraph mirror(bottom4)=1
	ModifyGraph nticks(bottom1)=0,nticks(bottom4)=0
	ModifyGraph noLabel(bottom4)=1
	ModifyGraph lblMargin(bottom4)=20
	ModifyGraph standoff=0
	ModifyGraph axRGB(left1)=(0,0,65535),axRGB(bottom2)=(0,0,65535),axRGB(left2)=(65535,16385,16385)
	ModifyGraph axRGB(bottom3)=(65535,16385,16385),axRGB(bottom1)=(0,0,0,0),axRGB(bottom4)=(0,0,0,0)
	ModifyGraph tlblRGB(left1)=(0,0,65535),tlblRGB(bottom2)=(0,0,65535),tlblRGB(left2)=(65535,16385,16385)
	ModifyGraph tlblRGB(bottom3)=(65535,16385,16385)
	ModifyGraph alblRGB(bottom2)=(0,0,65535),alblRGB(bottom3)=(65535,16385,16385)
	ModifyGraph lblPosMode(left1)=4,lblPosMode(bottom2)=3,lblPosMode(bottom3)=3,lblPosMode(bottom4)=1
	ModifyGraph lblPos(left1)=50,lblPos(bottom1)=23
	ModifyGraph lblLatPos(left1)=-55
	ModifyGraph freePos(left1)={0,bottom1}
	ModifyGraph freePos(bottom2)=-1
	ModifyGraph freePos(left2)={-5.08259212198221,bottom4}
	ModifyGraph freePos(bottom3)=0
	ModifyGraph freePos(bottom1)=0
	ModifyGraph freePos(bottom4)={0,left2}
	ModifyGraph axisEnab(left1)={0,0.475}
	ModifyGraph axisEnab(bottom2)={0.8,1}
	ModifyGraph axisEnab(left2)={0.525,1}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph axisEnab(bottom1)={0,0.8}
	ModifyGraph axisEnab(bottom4)={0,0.8}
	ModifyGraph manTick(bottom2)={0,10,0,0},manMinor(bottom2)={9,5}
	ModifyGraph manTick(bottom3)={0,100,0,0},manMinor(bottom3)={9,5}
	Label left1 "wavelength [nm]"
	Label bottom2 "Silicon detector"
	Label bottom3 "InGaAs detector"
	Label bottom1 "\\K(1,16019,65535)Silicon detector"
	Label bottom4 "\\K(65535,0,0)InGaAs detector"
	SetAxis left1 800,1050
	SetAxis left2 1050,1300
	SetAxis bottom1 772.361759425494,1272.36175942549
	SetAxis bottom4 11212.1122082585,11712.1122082585
	TextBox/C/N=text0/F=0/A=MC/X=43.09/Y=0.70 "\\JCnumber of SWNT\rin 0.09mm²"
	GroupBox CBSeparator0,pos={0.00,0.00},size={1659.00,4.00}
	SetWindow kwTopWin,userdata(exactscanInGaAs)=  "image_sliderLimits={*,*};"
EndMacro

Window waterfall2() : Graph
	PauseUpdate; Silent 1		// building window...
	NewWaterfall /W=(63.75,70.25,503.25,344) selection_mixed vs {wavelength,*} as "waterfall2"
	ModifyWaterfall angle=90, axlen= 0.9, hidden= 0
	ModifyGraph margin(left)=14,margin(bottom)=46,margin(top)=141,margin(right)=14
	ModifyGraph mode=7
	ModifyGraph hbFill=2
	ModifyGraph usePlusRGB=1
	ModifyGraph plusRGB=(65535,49151,49151)
	ModifyGraph negRGB=(0,0,65535)
	ModifyGraph noLabel(left)=2,noLabel(right)=1
	ModifyGraph fSize=12
	ModifyGraph standoff(left)=0,standoff(right)=0
	ModifyGraph gridRGB(right)=(30583,30583,30583)
	ModifyGraph axRGB(left)=(0,0,0,0),axRGB(right)=(0,0,0,0)
	ModifyGraph gridStyle(right)=3
	ModifyGraph gridHair(right)=3
	ModifyGraph lblPosMode(bottom)=4
	ModifyGraph lblPos(left)=42,lblPos(bottom)=35,lblPos(right)=424
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom)={0,100,0,0},manMinor(bottom)={3,2}
	Label bottom "wavelength [nm]"
	SetAxis left -0.1,*
	SetAxis bottom 800,1300
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linefgc= (30583,30583,30583)
	DrawLine 1063.72268907563,1,1063.72268907563,-8
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Silicon:
	Display/W=(0.112,0,0.525,0.502)/FG=(PL,$"",$"",$"")/HOST=#  histArea
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=54,margin(right)=7
	ModifyGraph mode=5
	ModifyGraph fSize=12
	ModifyGraph manTick(left)={0,10,0,0},manMinor(left)={1,50}
	Label left "number of SWNT\r(Si detector)"
	Label bottom "peak area [nm cts/s]"
	SetAxis bottom 0,*
	RenameWindow #,mkl34exactscanSi_histArea
	SetActiveSubwindow ##
	String fldrSav1= GetDataFolder(1)
	SetDataFolder root:InGaAs:
	Display/W=(0.525,0,1,0.507)/FG=($"",$"",PR,$"")/HOST=#  histArea
	SetDataFolder fldrSav1
	ModifyGraph margin(left)=54,margin(right)=7
	ModifyGraph mode=5
	ModifyGraph fSize=12
	ModifyGraph lblMargin(left)=4
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={9,5}
	ModifyGraph manTick(bottom)={0,50,0,0},manMinor(bottom)={0,0}
	Label left "number of SWNT\r\n(InGaAs detector)"
	Label bottom "peak area [nm cts/s]"
	SetAxis left 0,*
	SetAxis bottom 0,*
	RenameWindow #,mkl34exactscanInGaAs_histArea
	SetActiveSubwindow ##
EndMacro

Window measurementrange() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Silicon:
	Display /W=(62.25,233,504,503.75) ::fg830_abs vs ::fg830_wl as "measurementrange"
	AppendToGraph ::chroma760trans_toAbs/TN=chroma760abs vs ::chroma760trans_wl
	AppendToGraph ::InGaAs:selection[*][11]/TN=ingaas11 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][10]/TN=ingaas10 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][9]/TN=ingaas9 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][8]/TN=ingaas8 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][7]/TN=ingaas7 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][6]/TN=ingaas6 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][5]/TN=ingaas5 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][4]/TN=ingaas4 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][3]/TN=ingaas3 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][2]/TN=ingaas2 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][1]/TN=ingaas1 vs ::InGaAs:wavelength
	AppendToGraph ::InGaAs:selection[*][0]/TN=ingaas0 vs ::InGaAs:wavelength
	AppendToGraph selection[*][13]/TN=silicon13 vs wavelength
	AppendToGraph selection[*][12]/TN=silicon12 vs wavelength
	AppendToGraph selection[*][11]/TN=silicon11 vs wavelength
	AppendToGraph selection[*][10]/TN=silicon10 vs wavelength
	AppendToGraph selection[*][9]/TN=silicon9 vs wavelength
	AppendToGraph selection[*][8]/TN=silicon8 vs wavelength
	AppendToGraph selection[*][7]/TN=silicon7 vs wavelength
	AppendToGraph selection[*][6]/TN=silicon6 vs wavelength
	AppendToGraph selection[*][5]/TN=silicon5 vs wavelength
	AppendToGraph selection[*][4]/TN=silicon4 vs wavelength
	AppendToGraph selection[*][3]/TN=silicon3 vs wavelength
	AppendToGraph selection[*][2]/TN=silicon2 vs wavelength
	AppendToGraph selection[*][1]/TN=silicon1 vs wavelength
	AppendToGraph selection[*][0]/TN=silicon0 vs wavelength
	AppendToGraph/L=histogram ::InGaAs:histLocation,histLocation
	AppendToGraph ::qe_oe_m100 vs ::qe_oe_m100_wl
	AppendToGraph ::qe_ingaas_m90 vs ::qe_ingaas_m90_wl
	AppendToGraph ::grating1250 vs ::grating1250_wl
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=54,margin(right)=25
	ModifyGraph mode(fg830_abs)=7,mode(chroma760abs)=7,mode(ingaas11)=7,mode(ingaas10)=7
	ModifyGraph mode(ingaas9)=7,mode(ingaas8)=7,mode(ingaas7)=7,mode(ingaas6)=7,mode(ingaas5)=7
	ModifyGraph mode(ingaas4)=7,mode(ingaas3)=7,mode(ingaas2)=7,mode(ingaas1)=7,mode(ingaas0)=7
	ModifyGraph mode(silicon13)=7,mode(silicon12)=7,mode(silicon11)=7,mode(silicon10)=7
	ModifyGraph mode(silicon9)=7,mode(silicon8)=7,mode(silicon7)=7,mode(silicon6)=7
	ModifyGraph mode(silicon5)=7,mode(silicon4)=7,mode(silicon3)=7,mode(silicon2)=7
	ModifyGraph mode(silicon1)=7,mode(silicon0)=7,mode(histLocation)=5,mode(histLocation#1)=5
	ModifyGraph lStyle(fg830_abs)=3,lStyle(chroma760abs)=3,lStyle(qe_oe_m100)=3,lStyle(qe_ingaas_m90)=3
	ModifyGraph rgb(fg830_abs)=(13107,13107,13107),rgb(chroma760abs)=(0,0,0),rgb(silicon13)=(0,0,65535)
	ModifyGraph rgb(silicon12)=(0,0,65535),rgb(silicon11)=(0,0,65535),rgb(silicon10)=(0,0,65535)
	ModifyGraph rgb(silicon9)=(0,0,65535),rgb(silicon8)=(0,0,65535),rgb(silicon7)=(0,0,65535)
	ModifyGraph rgb(silicon6)=(0,0,65535),rgb(silicon5)=(0,0,65535),rgb(silicon4)=(0,0,65535)
	ModifyGraph rgb(silicon3)=(0,0,65535),rgb(silicon2)=(0,0,65535),rgb(silicon1)=(0,0,65535)
	ModifyGraph rgb(silicon0)=(0,0,65535),rgb(histLocation)=(0,0,0),rgb(histLocation#1)=(0,0,0)
	ModifyGraph rgb(qe_oe_m100)=(1,16019,65535),rgb(grating1250)=(52428,52425,1)
	ModifyGraph hbFill(fg830_abs)=2,hbFill(chroma760abs)=2,hbFill(ingaas11)=2,hbFill(ingaas10)=2
	ModifyGraph hbFill(ingaas9)=2,hbFill(ingaas8)=2,hbFill(ingaas7)=2,hbFill(ingaas6)=2
	ModifyGraph hbFill(ingaas5)=2,hbFill(ingaas4)=2,hbFill(ingaas3)=2,hbFill(ingaas2)=2
	ModifyGraph hbFill(ingaas1)=2,hbFill(ingaas0)=2,hbFill(silicon13)=2,hbFill(silicon12)=2
	ModifyGraph hbFill(silicon11)=2,hbFill(silicon10)=2,hbFill(silicon9)=2,hbFill(silicon8)=2
	ModifyGraph hbFill(silicon7)=2,hbFill(silicon6)=2,hbFill(silicon5)=2,hbFill(silicon4)=2
	ModifyGraph hbFill(silicon3)=2,hbFill(silicon2)=2,hbFill(silicon1)=2,hbFill(silicon0)=2
	ModifyGraph hbFill(qe_oe_m100)=2,hbFill(qe_ingaas_m90)=2
	ModifyGraph gaps(silicon0)=0
	ModifyGraph usePlusRGB(fg830_abs)=1,usePlusRGB(chroma760abs)=1,usePlusRGB(ingaas11)=1
	ModifyGraph usePlusRGB(ingaas10)=1,usePlusRGB(ingaas9)=1,usePlusRGB(ingaas8)=1,usePlusRGB(ingaas7)=1
	ModifyGraph usePlusRGB(ingaas6)=1,usePlusRGB(ingaas5)=1,usePlusRGB(ingaas4)=1,usePlusRGB(ingaas3)=1
	ModifyGraph usePlusRGB(ingaas2)=1,usePlusRGB(ingaas1)=1,usePlusRGB(ingaas0)=1,usePlusRGB(silicon13)=1
	ModifyGraph usePlusRGB(silicon12)=1,usePlusRGB(silicon11)=1,usePlusRGB(silicon10)=1
	ModifyGraph usePlusRGB(silicon9)=1,usePlusRGB(silicon8)=1,usePlusRGB(silicon7)=1
	ModifyGraph usePlusRGB(silicon6)=1,usePlusRGB(silicon5)=1,usePlusRGB(silicon4)=1
	ModifyGraph usePlusRGB(silicon3)=1,usePlusRGB(silicon2)=1,usePlusRGB(silicon1)=1
	ModifyGraph usePlusRGB(silicon0)=1,usePlusRGB(qe_oe_m100)=1,usePlusRGB(qe_ingaas_m90)=1
	ModifyGraph plusRGB(fg830_abs)=(30583,30583,30583,32768),plusRGB(chroma760abs)=(52428,52428,52428)
	ModifyGraph plusRGB(ingaas11)=(65535,49151,49151),plusRGB(ingaas10)=(65535,49151,49151)
	ModifyGraph plusRGB(ingaas9)=(65535,49151,49151),plusRGB(ingaas8)=(65535,49151,49151)
	ModifyGraph plusRGB(ingaas7)=(65535,49151,49151),plusRGB(ingaas6)=(65535,49151,49151)
	ModifyGraph plusRGB(ingaas5)=(65535,49151,49151),plusRGB(ingaas4)=(65535,49151,49151)
	ModifyGraph plusRGB(ingaas3)=(65535,49151,49151),plusRGB(ingaas2)=(65535,49151,49151)
	ModifyGraph plusRGB(ingaas1)=(65535,49151,49151),plusRGB(ingaas0)=(65535,49151,49151)
	ModifyGraph plusRGB(silicon13)=(49151,49152,65535),plusRGB(silicon12)=(49151,49152,65535)
	ModifyGraph plusRGB(silicon11)=(49151,49152,65535),plusRGB(silicon10)=(49151,49152,65535)
	ModifyGraph plusRGB(silicon9)=(49151,49152,65535),plusRGB(silicon8)=(49151,49152,65535)
	ModifyGraph plusRGB(silicon7)=(49151,49152,65535),plusRGB(silicon6)=(49151,49152,65535)
	ModifyGraph plusRGB(silicon5)=(49151,49152,65535),plusRGB(silicon4)=(49151,49152,65535)
	ModifyGraph plusRGB(silicon3)=(49151,49152,65535),plusRGB(silicon2)=(49151,49152,65535)
	ModifyGraph plusRGB(silicon1)=(49151,49152,65535),plusRGB(silicon0)=(49151,49152,65535)
	ModifyGraph plusRGB(histLocation)=(34952,34952,34952),plusRGB(histLocation#1)=(52428,52428,52428)
	ModifyGraph plusRGB(qe_oe_m100)=(32768,40777,65535,32768),plusRGB(qe_ingaas_m90)=(65535,49151,49151,32768)
	ModifyGraph hideTrace(grating1250)=1
	ModifyGraph useBarStrokeRGB(histLocation)=1,useBarStrokeRGB(histLocation#1)=1
	ModifyGraph grid(left)=2,grid(bottom)=1,grid(histogram)=1
	ModifyGraph log(histogram)=1
	ModifyGraph zero(left)=1
	ModifyGraph mirror(left)=3,mirror(bottom)=3,mirror(histogram)=1
	ModifyGraph noLabel(left)=1
	ModifyGraph standoff(left)=0,standoff(bottom)=0
	ModifyGraph gridRGB(left)=(0,0,0)
	ModifyGraph axRGB(left)=(4369,4369,4369)
	ModifyGraph gridStyle(left)=3
	ModifyGraph gridHair(left)=0
	ModifyGraph lblPosMode(left)=1,lblPosMode(bottom)=4,lblPosMode(histogram)=1
	ModifyGraph lblPos(left)=46,lblPos(bottom)=35
	ModifyGraph axisOnTop(left)=1,axisOnTop(histogram)=1
	ModifyGraph freePos(histogram)={0,bottom}
	ModifyGraph axisEnab(left)={0.25,1}
	ModifyGraph axisEnab(histogram)={0,0.25}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom)={0,100,0,0},manMinor(bottom)={3,2}
	Label left "norm. PL emission\rabsorption [%/100]"
	Label bottom "wavelength [nm]"
	Label histogram "amount of SWNT"
	SetAxis left 0,1
	SetAxis bottom 700,1400
EndMacro

