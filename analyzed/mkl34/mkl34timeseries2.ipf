#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("air")
End

Window air() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(11.25,58.25,468,339.5)/L=left3 source_maxPosition,peakLocation as "air"
	AppendToGraph/L=left2 peakHeight
	AppendImage source vs {*,wavelengthImage}
	ModifyImage source ctab= {0,500,Red,1}
	ModifyGraph margin(right)=85
	ModifyGraph mode(peakLocation)=4,mode(peakHeight)=4
	ModifyGraph marker(peakLocation)=19,marker(peakHeight)=19
	ModifyGraph msize(peakLocation)=1,msize(peakHeight)=0.5
	ModifyGraph useMrkStrokeRGB(peakLocation)=1,useMrkStrokeRGB(peakHeight)=1
	ModifyGraph zColor(peakHeight)={peakHeight,0,500,Red,1}
	ModifyGraph mirror(bottom)=0,mirror(left)=0
	ModifyGraph lblPosMode(left3)=1,lblPosMode(left2)=1,lblPosMode(left)=1
	ModifyGraph lblPos(left)=57
	ModifyGraph freePos(left3)=0
	ModifyGraph freePos(left2)=0
	ModifyGraph axisEnab(left3)={0.76,1}
	ModifyGraph axisEnab(left2)={0.5,0.75}
	ModifyGraph axisEnab(left)={0,0.48}
	ModifyGraph manTick(left3)={0,0.5,0,1},manMinor(left3)={4,50}
	ModifyGraph manTick(bottom)={0,30,0,0},manMinor(bottom)={2,50}
	ModifyGraph manTick(left2)={0,100,0,0},manMinor(left2)={1,50}
	ModifyGraph manTick(left)={0,25,0,0},manMinor(left)={4,2}
	Label left3 "wavelength\r[nm]"
	Label bottom "time [min]"
	Label left2 "intensity\r[a.u.]"
	Label left "wavelength\r\n[nm]"
	SetAxis left3 991.6,992.6
	SetAxis left2 200,450
	SetAxis left 970,1020
	ErrorBars peakLocation Y,wave=(peakLocationErr,peakLocationErr)
	ErrorBars peakHeight Y,wave=(peakHeightErr,peakHeightErr)
	ColorScale/C/N=text0/F=0/A=MC/X=64.43/Y=-27.10 image=source, heightPct=50
	AppendText "intensity [a.u.]"
EndMacro

