#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("main", saveJSON = 0)
	SaveWindow("plotly", customName = "main", saveImages = 0, saveJSON = 1)
	SaveWindow("zoom", saveJSON = 1, saveVector = 1)
End

Window plotly() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(500.25,42.5,960,323.75)/R/B=histogram_bottom histresultpfobpy/TN=pfobpy_hist as "plotly"
	AppendToGraph/R/B=histogram_bottom histresulttoluene/TN=toluene_hist
	AppendToGraph intensity_pfobpyfull/TN=pfobpy_int vs source_time[300,*]
	AppendToGraph intensity_toluene/TN=toluene_int vs source_time[0,299]
	AppendToGraph source_maxHeight vs source_time
	AppendToGraph/L=position source_maxPosition[0,299]/TN=toluene_wl vs source_time[0,299]
	AppendToGraph/L=position source_maxPosition[300,*]/TN=pfobpy_wl vs source_time[300,*]
	AppendToGraph/R/B=histogram_bottom :Packages:MultiPeakFit2:MPF_SetFolder_2:fit_histresultpfobpy/TN=pfobpy_fit
	AppendToGraph/R/B=histogram_bottom :Packages:MultiPeakFit2:MPF_SetFolder_3:fit_histresulttoluene/TN=toluene_fit
	ModifyGraph margin(right)=28
	ModifyGraph mode(pfobpy_hist)=5,mode(toluene_hist)=5
	ModifyGraph lSize(pfobpy_fit)=2,lSize(toluene_fit)=2
	ModifyGraph rgb(pfobpy_hist)=(0,0,0,16384),rgb(toluene_hist)=(0,0,0,16384),rgb(pfobpy_int)=(0,0,0)
	ModifyGraph rgb(pfobpy_wl)=(0,0,0),rgb(pfobpy_fit)=(0,0,0)
	ModifyGraph hbFill(pfobpy_hist)=2,hbFill(toluene_hist)=2
	ModifyGraph usePlusRGB(pfobpy_hist)=1,usePlusRGB(toluene_hist)=1
	ModifyGraph plusRGB(pfobpy_hist)=(0,0,0,32768),plusRGB(toluene_hist)=(65535,0,0,32768)
	ModifyGraph hideTrace(source_maxHeight)=1
	ModifyGraph nticks(right)=0
	ModifyGraph noLabel(right)=2
	ModifyGraph standoff(right)=0,standoff(bottom)=0
	ModifyGraph axThick(right)=0
	ModifyGraph lblPosMode(right)=2,lblPosMode(histogram_bottom)=1,lblPosMode(position)=1
	ModifyGraph lblPos(left)=72,lblPos(bottom)=43
	ModifyGraph freePos(histogram_bottom)=0
	ModifyGraph freePos(position)=0
	ModifyGraph axisEnab(histogram_bottom)={0.8,1}
	ModifyGraph axisEnab(left)={0,0.6}
	ModifyGraph axisEnab(bottom)={0,0.75}
	ModifyGraph axisEnab(position)={0.65,1}
	ModifyGraph manTick(position)={0,1,0,0},manMinor(position)={1,50}
	Label right "histogram of events"
	Label histogram_bottom "intensity fluctuation"
	Label left "pl intensity [a.u.]"
	Label bottom "time [hours]"
	Label position "wavelength [nm]"
	SetAxis position 995.5,998.5
EndMacro

Window main() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(54.75,42.5,514.5,325.25)/R/B=bottom2 histresultpfobpy,histresulttoluene as "main"
	AppendToGraph intensity_pfobpyfull vs source_time[300,*]
	AppendToGraph intensity_pfobpy/TN=pfobpy vs source_time[300,599]
	AppendToGraph intensity_toluene/TN=toluene vs source_time[0,299]
	AppendToGraph/L=left2/B=bottom3 sum1 vs wavelength
	AppendToGraph source_maxHeight vs source_time
	AppendToGraph/L=left3 source_maxPosition vs source_time
	AppendToGraph/R/B=bottom2 :Packages:MultiPeakFit2:MPF_SetFolder_2:fit_histresultpfobpy
	AppendToGraph/R/B=bottom2 :Packages:MultiPeakFit2:MPF_SetFolder_3:fit_histresulttoluene
	ModifyGraph mode(histresultpfobpy)=5,mode(histresulttoluene)=5,mode(sum1)=7
	ModifyGraph lSize(fit_histresultpfobpy)=2,lSize(fit_histresulttoluene)=2
	ModifyGraph rgb(histresultpfobpy)=(0,0,0),rgb(intensity_pfobpyfull)=(0,0,0),rgb(pfobpy)=(0,0,0)
	ModifyGraph rgb(fit_histresultpfobpy)=(0,0,0)
	ModifyGraph hbFill(histresultpfobpy)=2,hbFill(histresulttoluene)=2,hbFill(sum1)=2
	ModifyGraph usePlusRGB(histresultpfobpy)=1,usePlusRGB(histresulttoluene)=1,usePlusRGB(sum1)=1
	ModifyGraph plusRGB(histresultpfobpy)=(0,0,0,32768),plusRGB(histresulttoluene)=(65535,0,0,32768)
	ModifyGraph plusRGB(sum1)=(65535,49151,49151)
	ModifyGraph hideTrace(source_maxHeight)=1
	ModifyGraph nticks(right)=0,nticks(left2)=0,nticks(bottom3)=3
	ModifyGraph noLabel(right)=1,noLabel(left2)=1
	ModifyGraph lblPosMode(bottom2)=1,lblPosMode(left2)=3,lblPosMode(left3)=1
	ModifyGraph lblPos(left)=72,lblPos(bottom)=43,lblPos(left2)=20,lblPos(bottom3)=43
	ModifyGraph freePos(bottom2)=0
	ModifyGraph freePos(left2)={0.5,kwFraction}
	ModifyGraph freePos(bottom3)={0.55,kwFraction}
	ModifyGraph freePos(left3)=0
	ModifyGraph axisEnab(bottom2)={0.8,1}
	ModifyGraph axisEnab(left)={0,0.75}
	ModifyGraph axisEnab(bottom)={0,0.75}
	ModifyGraph axisEnab(left2)={0.55,0.9}
	ModifyGraph axisEnab(bottom3)={0.5,0.7}
	ModifyGraph axisEnab(left3)={0.75,1}
	ModifyGraph manTick(bottom3)={0,50,0,0},manMinor(bottom3)={1,50}
	Label right "sum of events\r(histogram)"
	Label bottom2 "intensity fluctuation"
	Label left "pl intensity [a.u.]"
	Label bottom "time [hours]"
	Label left2 "pl intensity [a.u.]"
	Label bottom3 "wavelength [nm]"
	Label left3 "wavelength [nm]"
	SetAxis bottom3 950,1050
	SetAxis left3 995.5,998.5
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,fillfgc= (65535,65535,0,32768),fillbgc= (65535,65535,0,32768)
	DrawOval 2.28209191759113,0.34984520123839,2.80507131537242,0.674922600619195
	SetDrawEnv xcoord= bottom,ycoord= prel
	DrawText 0.349495665288889,0.849866325839041,"\\JCinjection\rof PFO-Bpy"
EndMacro

Window zoom() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(90,282.5,549.75,563.75)/R/B=histogram_bottom histresultpfobpy/TN=pfobpy_hist as "zoom"
	AppendToGraph intensity_pfobpyfull/TN=pfobpy_int vs source_time[300,*]
	AppendToGraph intensity_toluene/TN=toluene_int vs source_time[0,299]
	AppendToGraph source_maxHeight vs source_time
	AppendToGraph/L=position source_maxPosition[0,299]/TN=toluene_wl vs source_time[0,299]
	AppendToGraph/L=position source_maxPosition[300,*]/TN=pfobpy_wl vs source_time[300,*]
	AppendToGraph/R/B=histogram_bottom :Packages:MultiPeakFit2:MPF_SetFolder_2:fit_histresultpfobpy/TN=pfobpy_fit
	ModifyGraph margin(right)=28
	ModifyGraph mode(pfobpy_hist)=5
	ModifyGraph lSize(pfobpy_fit)=2
	ModifyGraph rgb(pfobpy_hist)=(0,0,0,16384),rgb(pfobpy_int)=(0,0,0),rgb(pfobpy_wl)=(0,0,0)
	ModifyGraph rgb(pfobpy_fit)=(21845,0,32639)
	ModifyGraph hbFill(pfobpy_hist)=2
	ModifyGraph usePlusRGB(pfobpy_hist)=1
	ModifyGraph plusRGB(pfobpy_hist)=(0,0,0,32768)
	ModifyGraph hideTrace(source_maxHeight)=1
	ModifyGraph nticks(right)=0
	ModifyGraph noLabel(right)=2
	ModifyGraph standoff(right)=0,standoff(bottom)=0
	ModifyGraph axThick(right)=0
	ModifyGraph lblPosMode(right)=2,lblPosMode(histogram_bottom)=1,lblPosMode(position)=1
	ModifyGraph lblPos(left)=72,lblPos(bottom)=43
	ModifyGraph freePos(histogram_bottom)=0
	ModifyGraph freePos(position)=0
	ModifyGraph axisEnab(histogram_bottom)={0.8,1}
	ModifyGraph axisEnab(left)={0,0.6}
	ModifyGraph axisEnab(bottom)={0,0.75}
	ModifyGraph axisEnab(position)={0.65,1}
	ModifyGraph manTick(position)={0,1,0,0},manMinor(position)={1,50}
	Label right "histogram of events"
	Label histogram_bottom "intensity fluctuation"
	Label left "pl intensity [a.u.]"
	Label bottom "time [hours]"
	Label position "wavelength [nm]"
	SetAxis bottom 2.3,6
	SetAxis position 995.5,998.5
EndMacro

