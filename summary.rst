.. vim: set et ts=3 sts=3 sw=3 tw=79:
.. vim: :set fileencoding=utf-8:

*******
Summary
*******

**Nanoelectronics** is an essential technology for down-scaling beyond the
limit of silicon-based electronics. Single-Wall Carbon Nanotubes (SWNT) are
semiconducting components that exhibit a large variety of properties that make
them usable for sensing, telecommunication, or computational tasks.  Due to
their high surface to volume ratio, carbon nanotubes are strongly affected by
molecular adsorptions, and almost **all properties depend on surface
adsorption**.

SWNT with :doc:`smaller diameters <nitrogen>` (0.7-0.9nm) show a stronger
sensitivity to surface effects. An optimized :doc:`synthesis route <cvd>` was
developed to produce these nanotubes directly. They were produced with a clean
surface, high quality, and large lengths of 2µm. The results complement
previous studies on larger diameters (0.9-1.4nm). They allow performing
statistically significant assumptions for **a perfect nanotube**, which is
selected from a sub-set of nanotubes with good emission intensity, and high
mechanical durability.

The adsorption of molecules on the surface of carbon nanotubes influences the
motion and binding strength of charge-separated states in this system.  To gain
insight into the :doc:`adsorption processes <adsorption>` on the surface with a
minimum of concurrent overlapping effects, a :doc:`microscopic setup <images>`,
and a :doc:`measurement technique <spectra>` were developed.  The system was
estimated to exhibit **excellent properties** like long exciton diffusion
lengths (>350nm), and big exciton sizes (8.5(5)nm), which was substantiated by
a simulation.

We studied the adsorption processes at the surface of Single-Wall Carbon
Nanotubes for :doc:`molecules in the gas phase <gas>`, :doc:`solvent molecules
<solvent>`, and :doc:`surfactant molecules <molecular>`.  The experiments were
all carried out on suspended individualized carbon nanotubes on a silicon wafer
substrate.

The experiments in the **gas-phase** showed that the excitonic emission energy
and intensity experiences a rapid blue shift during observation. This shift was
associated with the spontaneous desorption of large clusters of gaseous
molecules caused by laser heat up. The measurement of this desorption was
essential for creating a reference to an initially clean surface and allows us
to perform a comparison with previous measurements on this topic. Furthermore,
the adsorption of hydrogen on the nanotube surface at high temperatures was
investigated.  It was found that a new emission mode arises slightly
red-shifted to the excitonic emission in these systems. The new signal is
almost equally strong as the main excitonic peak and was associated with the
brightening of dark excitons at sp³-defects through a K-phonon assisted
pathway. The finding is useful for the direct synthesis of spintronic devices
as these systems are known to act as single-photon emitters.

The suspended nanotubes were further studied to estimate the effect of
**solvent adsorption** on the excitonic states during nanotube dispersion for
each nanotube individually. A significant quantum yield loss is observable for
*hexane* and *acetonitrile*, while the emission intensity was found to be the
strongest in *toluene*.  The reference to a clean surface allowed us to
estimate the exact influence of the dielectric environment of adsorbing
solvents on the excitonic emission energy. Solvent adsorption was found to lead
to an energy shift that is almost twice as high as suggested in previous
studies.  The amount of this energy shift, however, was comparably similar for
all solvents, which suggests that the influence of the distinct dielectric
constant in the outer environment less significantly influences the energy
shift than previously thought.

An interesting phenomenon was found when using acetonitrile as a solvent, which
leads to greatly enhanced emission properties. The emission is more than twice
as high as in the same air-suspended nanotubes, which suggests a process that
depends on the laser intensity. In this study, it was reasonably explained how
an energy down-conversion is possible through the coupling of the excitonic
states with solvent vibrations. The strength of this coupling, however, also
suggests adsorptions to the inside of the tubular nanotube structure leading to
a coupled vibration of linear acetonitrile molecules that are **adsorbed to the
inner surface**. The findings are important for the field of nanofluidics and
provide an excellent system for efficient energy down-conversion in the
transmission window of biological tissue.

Having separated the pure effect of solvent adsorption allowed us to study the
undisturbed **molecular adsorption** of polymers in these systems. The addition
of polyfluorene polymer leads to a slow but stepwise intensity increase. The
intensity increase is overlapping with a concurrent process that leads to an
intensity decrease. Unfortunately, observing the stepwise process has a low
spacial resolution of only 100-250nm, which is in the range of the exciton
diffusion length in these systems and hinders detailed analysis. The two
competing and overlapping processes processes are considered to originate from
slow π-stacking and fast side-chain binding. Insights into this process are
essential for selecting suitably formed polymers.  However, the findings also
emphasize the importance of solvent selection during nanotube dispersion since
solvent effects were proven to be far more critical on the quantum yield in
these systems. These measurements can shed light on the ongoing debate on
polymers adsorption during nanotube individualization and allow us to direct
the discussion more towards the selection of suitable solvents.

This work provides **fundamental insights** into the adsorption of various
molecules on the surface of individually observed suspended Single-Wall Carbon
Nanotubes. It allows observing the adsorption of individual molecules below the
optical limit in the solid, liquid, and gas phases. Nanotubes are able to act
as sensing material for detecting changes in their direct surrounding. These
fundamental findings are also crucial for increasing the quantum yield of
solvent-dispersed nanotubes.  They can provide better light-harvesting systems
for microscopy in biological tissue and set the base for a more efficient
telecommunication infrastructure with nano-scale spintronics devices and lasing
components. The newly discovered solvent alignment in the nanotube surrounding
can potentially also be used for supercapacitors that are needed for caching
the calculation results in computational devices that use polymer wrapped
nanotubes as transistors. Although fundamental, these studies develop a
strategy to enlighten *this* room that is barely only visible at the bottom of
the nano-scale.

.. only:: latex

   Zusammenfassung
   ===============

   Nanoelektronik ist eine wichtige Technologie um das Größen-Limit
   gegenwärtiger Silizium-basierter Technologie zu überwinden. Einwandige
   Kohlenstoffnanoröhren sind halbleitende Moleküle, die eine Reihe von
   Eigenschaften dafür zur Verfügung stellen. Sie sind einsetzbar als Sensoren,
   in der Fernmeldetechnik und für elektronische Rechenoperationen. Aufgrund
   ihres hohen Verhältnisses von Oberfläche zu Volumen werden nahezu alle
   Eigenschaften von Kohlenstoffnanoröhren stark von Adsorption beeinflusst.

   Einwandige Kohlenstoffnanoröhren mit kleineren Durchmessern (0.7-0.9nm)
   zeigen einen stärkeren Einfluss auf Phänomene, die an der Oberfläche
   auftreten. Um speziell diese Nanoröhren genauer zu untersuchen wurde eine
   Synthese Strategie entwickelt, die Nanoröhren mit hoher Qualität und Länge
   herstellen kann und dabei eine saubere Oberfläche gewährleisten ohne ihre
   Emissions-Stärke durch Bündelung zu verlieren. Die erhaltenen Ergebnisse
   unterstützen Studien aus der Literatur, die zumeist an Röhren mit größeren
   Durchmessern durchgeführt wurden.  Die Größe des Datensatzes erlaubt es,
   Nanoröhren mit perfekten Emissions-Eigenschaften und großer mechanischer
   Stabilität auszuwählen.

   Adsorptionen beeinflussen die Bewegung und Bindungs-Stärke der Excitonen, da
   sie ein Coulomb Potential an der Außenseite der Röhre ausbilden.  Um die
   Adsorptionsprozess an der Oberfläche mit minimalen konkurrierenden Effekten
   zu untersuchen, wurde ein spezielles mikroskopisches Setup gewählt und eine
   Messmethode entwickelt um dieses System zu untersuchen. Das System wurde mit
   Hilfe von Bildern und Spektren charakterisiert. Über eine Simulation wurde
   außerdem gezeigt dass die untersuchten Nanoröhren große Diffusionslängen
   (>350nm) und Exciton Größen (<8.5nm) besitzen müssen.

   Der Adsorptions Prozess an Kohlenstoffnanoröhren wurde sowohl mit Molekülen
   in der Gas-Phase untersucht, also auch in Lösungsmitteln und mit
   Feststoffen. Alle Experimente wurde dabei an frei hängenden Röhren
   durchgeführt, die auf einem Silizium Wafer Substrat aufgebracht wurden.

   Die Experimente in der Gas Phase zeigten, dass die excitonische
   Emissions-Energie eine instantane und schnelle Blauverschiebung erfährt wenn
   die Nanoröhren mit einem Laser angeregt werden. Diese Verschiebung wurde auf
   die Desorption von Oberflächenverunreinigungen zurückgeführt, die an Luft
   inhärent die Messung beeinflussen. Durch die Annahme, nach der Untersuchung
   eine reine Oberfläche zu erhalten, konnte die Referenz der Vakkum-Emission
   erstellt werden, was es ermöglicht, den Einfluss der dielektrischen Umgebung
   genauer zu bestimmen. In einem weitern Experiment wurde die Adsorption von
   Wasserstoff getestet. In diesen Systemen bildet sich durch die Ausbildung
   von sp³-Defekten eine neue Emissionsbande aus. Solche Emissionen werden
   derzeit für die Anwendung als Einzelphotonenemitter diskutiert. Die hier
   vorgestellte Methode erlaubt die direkte Synthese solcher Systeme im CVD
   Ofen.

   Die frei hängenden Nanoröhren wurden weiter analysiert um den Effekt des
   Lösungsmittels auf die Emission detailiert zu untersuchen. Es wurde gezeigt,
   dass in Hexan und Acetonitril ein signifikant hoher Quantenausbeute-Verlust
   zu beobachten ist. Toluol hingegen zeigte sich hier am Besten. Die
   Energie-Verschiebungen waren insignifikant unterschiedlich zwischen den
   Lösungsmitteln. Ein Spezialfall war bei Acetonitril zu beobachten, in dem
   sich über den Zeitraum von 24h eine starke Emission herausbildet, die auf
   eine Kopplung mit Lösungsmittel-Schwingungen zurückgeführt wird. Die Stärke
   dieser Emission erlaubt die Vermutung, dass es sich um eine gekoppelte
   Schwingung von linear orientiertem Acetonitril in der Nanoröhre handelt.
   Eine solch starke Emission könnte zu Anwendungen in Zell-Gewebe führen, da
   weder Anregung noch Emission sich im Fenster der Blut- und Wasserabsorption
   befindet.

   Durch die eindeutige Identifizierung von Lösungsmitteleffekten auf die
   Dispergierung von Kohlenstoffnanoröhren war es möglich, den Prozess der
   Anlagerung von Polyfluorene Polymeren direkt zu beobachten. Das Hinzufügen
   von Polymer zur Lösung führt zu einem schrittweisen reversiblen Anstieg der
   Emissions Intensität. Dieser Anstieg wird von einem gleichzeitigen
   irreversiblen schrittweisen Abfall der Emissionsintensität begleitet. Leider
   ist das System nur geeignet, Adsorptionen bis maximal 100nm Länge
   aufzulösen.  Eine detaillierte Analyse ist daher schwer. Trotzdem wird
   vermutet, dass es sich bei dem langsamen Prozess um das Ausbilden von
   π-Stapeln handelt, wobei der schnelle Prozess mit der nicht-kovalenten
   Bindung der Polymer-Seitenketten an die Oberfläche assoziiert wird. Obwohl
   über die eigentliche Bindung des Polymers nur Vermutungen angestellt werden
   können, so wirft die Untersuchung doch einen Fokus auf die Wahl des
   Lösungsmittels, da diese Entscheidung einen viel größeren Effekt verursacht,
   als die Bindung des Polymers selbst.

   Diese Arbeit stellt fundamentale Betrachtungen zur Adsorption von
   verschiedenen Molekülen an Kohlenstoffnanoröhren auf. Die Betrachtungen
   wurden mit festen, flüssigen und gasförmigen Molekülen durchgeführt. Die
   Ergebnisse zeigen, dass Nanoröhren geeignet sind, als Molekül-Sensoren
   verwendet zu werden, da sie stark auf Änderungen in ihrer Umgebung reagieren
   können. Weiterhin wurden Lösungsmittel und Eigenschaften aufgezeigt, die die
   Quanteneffizienz signifikant beeinflussen. Eine Anwendung in der
   biologischen Mikroskopie ist denkbar, genauso wie für eine effizientere und
   sicherere Fernmeldeinfrastruktur. Weiterhin wurden Wege aufgezeigt,
   Super-Kondensatoren auf Nanorohr-Basis zu bauen, die als Anwendung in einem
   Kohlenstoffnanorohr-basierenden Computer von Interesse sein könnten. Obwohl
   die Erkenntnisse fundamental sind, zeigen diese Studien, dass es mit
   bestimmten Tricks möglich ist, den Raum am unteren Ende der Nanometerskala
   zu erforschen und zu entdecken.
