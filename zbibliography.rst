.. vim: set et ts=3 sts=3 sw=3 tw=79:

********
Appendix
********

.. only:: latex

   All sources are available online `<https://gitlab.com/ukos-git/nanotubes>`_.
   The built is deployed on `<https://ukos-git.gitlab.io/nanotubes>`_ with the
   most recent `pdf version
   <https://ukos-git.gitlab.io/nanotubes/_static/thesis.pdf>`_.

Built from git revision:

.. git_commit_detail::
    :branch:
    :commit:

Changes after submission of thesis:

.. git_changelog::
   :rev-list: submission..HEAD
   :hide_author: True
   :detailed-message-pre: True
   :commit:

.. bibliography:: citation/citation.bib
   :cited:
   :encoding: utf8
   :style: unsrt
