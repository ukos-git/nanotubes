.. vim: set et ts=3 sts=3 sw=3 tw=79:

*******
Methods
*******

.. figure:: collection/basics/geometric/graphen-sp2hybrid.*
   :align: center

   The hexagonal unit cell of graphitic carbon consists of a planar
   arrangement of sp² hybridized carbon with :math:`\mathrm{p_Z}` orbitals
   pointing out of the plane.

The correct synthesis and identification of new materials is the key to more
thorough analyses. In this chapter I will give a :ref:`historical introduction
<history>` to the synthesis of carbon nanotubes and in the further course
concentrate on the here chosen :doc:`synthesis technique <cvd>` for
:doc:`fabricating samples <samples>` with :doc:`small diameters <nitrogen>`. I
will then introduce the :doc:`microscopic setup <images>` that was
developed to perform a :doc:`spectroscopic investigation <spectra>` of
molecular :doc:`adsorption and desorption <adsorption>` from individual
single-wall carbon nanotubes (SWNT). The adsorption processes have been grouped
into :doc:`gas`, :doc:`solvent`, and :doc:`molecular`.

.. toctree::
   :maxdepth: 1

   history.rst
   cvd.rst
   samples.rst
   methods.rst
   nitrogen.rst
   images.rst
   spectra.rst
