#pragma TextEncoding = "Windows-1252"		// For details execute DisplayHelpTopic "The TextEncoding Pragma"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("background", saveVector = 1)
	saveWindow("kataura", saveVector = 1)
End

Window kataura() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:kataura_auswertung:
	Display /W=(482.25,77,939.75,359.75)/L=wavelength/B=bottom_right mk01[*][1]/TN=absorption_mk01 vs mk01[*][0] as "kataura"
	AppendToGraph/L=wavelength/B=bottom_left lambda11/TN=kataura1 vs diameter
	AppendToGraph/L=wavelength/B=bottom_left lambda22/TN=kataura2 vs diameter
	AppendToGraph/L=wavelength/B=bottom_left lambda11_display/TN=kataura3 vs diameter_display11
	AppendToGraph/L=wavelength/B=bottom_left lambda22_display/TN=kataura4 vs diameter_display22
	AppendToGraph/L=wavelength/B=bottom_right mk01peaks[*][0]/TN=peaks_mk01 vs mk01peaks[*][3]
	AppendToGraph/L=wavelength/B=bottom_left matching_lambda22/TN=matching1 vs matching_diameter
	AppendToGraph/L=wavelength/B=bottom_left matching_lambda22/TN=matching2 vs matching_diameter
	AppendToGraph/L=wavelength/B=bottom_left matching_lambda11/TN=matching3 vs matching_diameter
	AppendToGraph/L=wavelength/B=bottom_left matching_lambda11/TN=matching4 vs matching_diameter
	ModifyGraph userticks(wavelength)={peakAxis_tick,peakAxis_label}
	SetDataFolder fldrSav0
	ModifyGraph mode(kataura1)=3,mode(kataura2)=3,mode(kataura3)=3,mode(kataura4)=3
	ModifyGraph mode(peaks_mk01)=3,mode(matching1)=3,mode(matching2)=3,mode(matching3)=3
	ModifyGraph mode(matching4)=3
	ModifyGraph marker(kataura1)=1,marker(kataura2)=1,marker(peaks_mk01)=19,marker(matching1)=43
	ModifyGraph marker(matching3)=43
	ModifyGraph rgb(kataura2)=(0,0,0),rgb(kataura4)=(0,0,0),rgb(peaks_mk01)=(0,0,65535)
	ModifyGraph rgb(matching1)=(1,16019,65535),rgb(matching2)=(1,16019,65535),rgb(matching3)=(1,16019,65535)
	ModifyGraph rgb(matching4)=(1,16019,65535)
	ModifyGraph msize(matching1)=5,msize(matching3)=5
	ModifyGraph textMarker(kataura3)={:kataura_auswertung:nmindex,"default",0,0,5,0.00,10.00}
	ModifyGraph textMarker(kataura4)={:kataura_auswertung:nmindex,"default",0,0,5,0.00,10.00}
	ModifyGraph textMarker(matching2)={:kataura_auswertung:matching_nmindex,"default",0,0,5,0.00,10.00}
	ModifyGraph textMarker(matching4)={:kataura_auswertung:matching_nmindex,"default",0,0,5,0.00,10.00}
	ModifyGraph grid(wavelength)=1
	ModifyGraph lblPosMode=1
	ModifyGraph freePos(wavelength)=0
	ModifyGraph freePos(bottom_right)=0
	ModifyGraph freePos(bottom_left)=0
	ModifyGraph axisEnab(bottom_right)={0.8,1}
	ModifyGraph axisEnab(bottom_left)={0,0.79}
	Label wavelength "wavelength [nm]"
	Label bottom_right "optical density"
	Label bottom_left "SWNT diameter [nm]"
	SetAxis wavelength 1384,400
	SetAxis bottom_right 0.6,*
	SetAxis bottom_left 0.65,1.15
	Cursor/P A absorption_mk01 687
	Legend/C/N=text0/J/H={0,2,10}/B=(61166,61166,61166)/A=MC/X=-39.29/Y=-37.58 "\\Z14\\s(kataura1) E\\B11\\M\\Z14\r\\s(kataura2) E\\B22\\M"
	SetDrawLayer UserFront
	DrawPoly 1.2,1115,1,1,{1.2,1130,0.9,1123,0.9,648,1.2,648}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.57239507312781,1130.42397058824,1.0123,0.996988,{1.57239,1131,-2.2088,1131.87,-2.2088,650.406,1.43283,650.406}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 0.866583768310749,879.043379571248,0.935347,0.836066,{0.965783024099394,878.976470588235,-4.77720949963112,879.384615384616,-4.7676574015289,508.195475113122,1.49851895352407,508.195475113122}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.30289365678194,1113.4,0.990173,0.965054,{1.30024248528498,1132.21176470588,-2.9,1131.87,-2.9,594.393,1.325,594.393}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.19147359440348,1033.29,0.995769,1,{1.19047,1033.29,-3,1033.29,-3,650.406,1.43283,650.406}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.09379844727623,1258.73,0.987624,1,{1.093,1258.73,-1.4,1258.73,-1.4,680.196,1.15944,680.196}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.01643717049512,1184.76,0.995392,1,{1.01543,1184.76,-1.5,1184.76,-1.5,725.778,1.23346,725.778}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.05057728305417,1279.29,0.981221,1,{1.04708,1279.29,-0.8,1279.29,-0.8,725.778,1.23346,725.778}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 0.906031323285629,1064.2,3.28171,0.495979,{0.977885139964662,1334.8,0,1335.51,0,795.684,1.01223,795.684}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 1.10526520731411,985.712,0.992686,1,{1.10339,985.712,-3.8,985.712,-3.8,569.872,1.35938,569.872}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 0.912208583213981,962.905882352941,0.953186,0.556452,{1.72227504739756,1132.21176470588,-2.9,1131.87,-2.9,594.393,2.12573557341479,593.905882352941}
	SetDrawEnv xcoord= bottom_right,ycoord= wavelength,linefgc= (1,16019,65535),fillpat= 0
	DrawPoly 0.777686932345408,928.176470588235,1.00209,0.569444,{0.724349223818065,986.058823529412,-3.8,985.712,-3.8,569.872,1.07157771300384,569.305882352941}
EndMacro

Window background() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:derivative:
	Display /W=(16.5,76.25,474.75,332) zipfelmuetze03_range,zipfelmuetze03_range_bg/TN=background_zipfelmuetze03_range as "background"
	AppendToGraph/R=axisderivative2 zipfelmuetze03_rangedd/TN=derivative2
	AppendToGraph zipfelmuetze03_rangepeaks[*][3]/TN=peaks_zipfelmuetze03_range vs zipfelmuetze03_rangepeaks[*][0]
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=56,margin(bottom)=55,margin(top)=28,margin(right)=56,height={Aspect,0.5}
	ModifyGraph mode(background_zipfelmuetze03_range)=7,mode(derivative2)=7,mode(peaks_zipfelmuetze03_range)=3
	ModifyGraph marker(peaks_zipfelmuetze03_range)=19
	ModifyGraph rgb(background_zipfelmuetze03_range)=(0,0,0),rgb(derivative2)=(0,0,0)
	ModifyGraph rgb(peaks_zipfelmuetze03_range)=(0,0,65535)
	ModifyGraph hbFill(background_zipfelmuetze03_range)=2
	ModifyGraph useNegPat(derivative2)=1
	ModifyGraph usePlusRGB(background_zipfelmuetze03_range)=1
	ModifyGraph useNegRGB(derivative2)=1
	ModifyGraph hBarNegFill(derivative2)=2
	ModifyGraph plusRGB(background_zipfelmuetze03_range)=(65535,0,0,16384),plusRGB(derivative2)=(65535,0,0,32768)
	ModifyGraph negRGB(derivative2)=(65535,54607,32768)
	ModifyGraph grid(bottom)=1
	ModifyGraph nticks(axisderivative2)=0
	ModifyGraph minor(left)=1,minor(bottom)=1
	ModifyGraph noLabel(axisderivative2)=1
	ModifyGraph fSize=12
	ModifyGraph lblMargin(axisderivative2)=56
	ModifyGraph standoff(left)=0,standoff(bottom)=0
	ModifyGraph axThick(axisderivative2)=0
	ModifyGraph lblPosMode(axisderivative2)=3
	ModifyGraph lblPos(axisderivative2)=38
	ModifyGraph freePos(axisderivative2)=0
	ModifyGraph axisEnab(left)={0,0.75}
	ModifyGraph axisEnab(axisderivative2)={0.5,1}
	ModifyGraph manTick(bottom)={0,200,0,0},manMinor(bottom)={9,5}
	Label left "optical density"
	Label bottom "wavelength / nm"
	Label axisderivative2 " "
	SetAxis left 0.3,*
	TextBox/C/N=text0/F=0/A=MC/X=-24.38/Y=55.32 "\\Z122nd deviation"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2
	DrawLine 350,1.15,508,1.15
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2
	DrawLine 508,1.15,508,0.3
	SetDrawEnv xcoord= bottom,ycoord= left,fstyle= 1,textrgb= (65535,16385,16385),textxjust= 1,textyjust= 2
	DrawText 508,0.0179694643894311,"508"
	SetDrawEnv xcoord= bottom,ycoord= left,fstyle= 1,textrgb= (65535,16385,16385),textxjust= 2,textyjust= 1
	DrawText 313.575409097797,0.3,"0.3"
EndMacro

