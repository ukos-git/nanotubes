#pragma TextEncoding = "UTF-8"		// For details execute DisplayHelpTopic "The TextEncoding Pragma"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("range", saveVector = 1)
	SaveWindow("combined2", saveVector = 1)
	SaveWindow("combined3", saveVector = 1)
End

Window combined2() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Graph0:
	Display /W=(478.5,85.25,935.25,305.75)/L=left1/B=bottom1 ::dos65:data1:dos65energy vs ::dos65:data1:dos65 as "combined2"
	AppendToGraph/L=left2/B=bottom2 E11,E22 vs diameter
	AppendToGraph/L=left2/B=bottom2 E33,E44 vs diameter
	SetDataFolder fldrSav0
	ModifyGraph mode(E11)=3,mode(E22)=3,mode(E33)=3,mode(E44)=3
	ModifyGraph marker=19
	ModifyGraph lSize(dos65energy)=2
	ModifyGraph rgb(E11)=(0,0,0),rgb(E33)=(3,52428,1),rgb(E44)=(1,12815,52428)
	ModifyGraph msize=3
	ModifyGraph hbFill(dos65energy)=2
	ModifyGraph font(left1)="Arial",font(bottom1)="Arial"
	ModifyGraph fSize=12
	ModifyGraph lblMargin(left1)=6
	ModifyGraph lblPosMode(left1)=1,lblPosMode(bottom1)=1,lblPosMode(left2)=3,lblPosMode(bottom2)=1
	ModifyGraph lblPos(left2)=35
	ModifyGraph freePos(left1)={0.04,kwFraction}
	ModifyGraph freePos(bottom1)=0
	ModifyGraph freePos(left2)={0.4,kwFraction}
	ModifyGraph freePos(bottom2)=0
	ModifyGraph axisEnab(bottom1)={0.05,0.2}
	ModifyGraph axisEnab(bottom2)={0.4,0.85}
	ModifyGraph manTick(bottom1)={0,5,0,0},manMinor(bottom1)={4,50}
	Label left1 "energy [eV]"
	Label bottom1 "density of states (DOS)"
	Label left2 "energy [eV]"
	Label bottom2 "diameter [nm]"
	SetAxis left1 -2,2
	SetAxis bottom1 0,10
	SetAxis left2 0,5
	SetAxis bottom2*,1.2
	Legend/C/N=text1/J/F=0/A=MC/X=44.76/Y=-5.02 "\\Z12\\s(E44) E\\B44\\M\\Z12\r\\s(E33) E\\B33\\M\\Z12\r\\s(E22) E\\B22\\M\\Z12\r\\s(E11) E\\B11\\M\\Z12"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom1,ycoord= left1,arrow= 3
	DrawLine 5,-0.5,5,0.5
	SetDrawEnv xcoord= bottom1,ycoord= left1,arrow= 3
	DrawLine 8.80478087649402,-1.2,8.80478087649402,1.2
	SetDrawEnv xcoord= bottom1,ycoord= left1,textxjust= 1,textyjust= 1
	DrawText 2.63483526081245,0,"E\\B11"
	SetDrawEnv xcoord= bottom1,ycoord= left1,textxjust= 1,textyjust= 1
	DrawText 6.93080606531084,0,"E\\B22"
EndMacro

Window combined3() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Graph0:
	Display /W=(949.5,88.25,1407.75,309.5)/L=left1/B=bottom1 ::dos65:data1:dos65energy vs ::dos65:data1:dos65 as "combined3"
	AppendToGraph/L=left2/B=bottom2 E11,E22 vs diameter
	AppendToGraph/L=left2/B=bottom2 E33,E44 vs diameter
	AppendToGraph/L=left3/B=bottom3 ::Graph2:lambda22 vs ::Graph2:lambda11
	AppendToGraph/L=left3/B=bottom3 ::Graph2:lambda22 vs ::Graph2:lambda11
	SetDataFolder fldrSav0
	ModifyGraph mode(E11)=3,mode(E22)=3,mode(E33)=3,mode(E44)=3,mode(lambda22)=3,mode(lambda22#1)=3
	ModifyGraph marker(dos65energy)=19,marker(E11)=19,marker(E22)=19,marker(E33)=19
	ModifyGraph marker(E44)=19,marker(lambda22)=62
	ModifyGraph lSize(dos65energy)=2
	ModifyGraph rgb(E11)=(0,0,0),rgb(E33)=(3,52428,1),rgb(E44)=(1,12815,52428),rgb(lambda22#1)=(0,0,0)
	ModifyGraph msize(dos65energy)=3,msize(E11)=3,msize(E22)=3,msize(E33)=3,msize(E44)=3
	ModifyGraph msize(lambda22#1)=1.5
	ModifyGraph mrkThick(lambda22)=1
	ModifyGraph hbFill(dos65energy)=2
	ModifyGraph useMrkStrokeRGB(lambda22)=1
	ModifyGraph textMarker(lambda22#1)={:Graph0:nmindex,"default",0,0,5,0.00,5.00}
	ModifyGraph font(left1)="Arial",font(bottom1)="Arial"
	ModifyGraph fSize=12
	ModifyGraph lblPosMode(left1)=1,lblPosMode(bottom1)=1,lblPosMode(left2)=3,lblPosMode(bottom2)=1
	ModifyGraph lblPosMode(left3)=3,lblPosMode(bottom3)=3
	ModifyGraph lblPos(left2)=35,lblPos(left3)=53,lblPos(bottom3)=42
	ModifyGraph lblLatPos(bottom3)=4
	ModifyGraph axisOnTop(left3)=1
	ModifyGraph freePos(left1)=0
	ModifyGraph freePos(bottom1)=0
	ModifyGraph freePos(left2)={0.25,kwFraction}
	ModifyGraph freePos(bottom2)=0
	ModifyGraph freePos(left3)={0.8,kwFraction}
	ModifyGraph freePos(bottom3)=0
	ModifyGraph axisEnab(bottom1)={0,0.15}
	ModifyGraph axisEnab(bottom2)={0.25,0.6}
	ModifyGraph axisEnab(bottom3)={0.8,1}
	ModifyGraph manTick(bottom1)={0,5,0,0},manMinor(bottom1)={4,50}
	ModifyGraph manTick(left3)={500,100,0,0},manMinor(left3)={4,50}
	Label left1 "energy [eV]"
	Label bottom1 "density of states (DOS)"
	Label left2 "energy [eV]"
	Label bottom2 "diameter [nm]"
	Label left3 "Absorption in E\\B22\\M (λ in nm)"
	Label bottom3 "Emission from E\\B11\\M [nm]"
	SetAxis left1 -2,2
	SetAxis bottom1 0,10
	SetAxis left2 0,5
	SetAxis bottom2*,1.2
	SetAxis left3 450,800
	SetAxis bottom3 600,1500
	Legend/C/N=text1/J/S=1/A=MC/X=-16.37/Y=27.70 "\\Z12\\s(E44) E\\B44\\M\\Z12\r\\s(E33) E\\B33\\M\\Z12\r\\s(E22) E\\B22\\M\\Z12\r\\s(E11) E\\B11\\M\\Z12"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom1,ycoord= left1,arrow= 3
	DrawLine 5,-0.5,5,0.5
	SetDrawEnv xcoord= bottom1,ycoord= left1,arrow= 3
	DrawLine 8.80478087649402,-1.2,8.80478087649402,1.2
	SetDrawEnv xcoord= bottom1,ycoord= left1,textxjust= 1,textyjust= 1
	DrawText 2.63483526081245,0,"E\\B11"
	SetDrawEnv xcoord= bottom1,ycoord= left1,textxjust= 1,textyjust= 1
	DrawText 6.93080606531084,0,"E\\B22"
EndMacro

Window range() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:Graph2:
	Display /W=(5.25,83.75,462,364.25)/L=left1/B=bottom1 ::dos65:data1:dos65energy vs ::dos65:data1:dos65 as "range"
	AppendToGraph/L=left3/B=bottom3 lambda22 vs lambda11
	AppendToGraph/L=left3/B=bottom3 lambda22 vs lambda11
	SetDataFolder fldrSav0
	ModifyGraph margin(bottom)=56,margin(top)=28
	ModifyGraph mode(lambda22)=3,mode(lambda22#1)=3
	ModifyGraph marker(dos65energy)=19,marker(lambda22)=62
	ModifyGraph lSize(dos65energy)=2
	ModifyGraph rgb(lambda22#1)=(0,0,0)
	ModifyGraph msize(dos65energy)=3
	ModifyGraph mrkThick(lambda22)=1
	ModifyGraph hbFill(dos65energy)=2
	ModifyGraph useMrkStrokeRGB(lambda22)=1
	ModifyGraph offset(lambda22#1)={0,10}
	ModifyGraph textMarker(lambda22#1)={:Graph0:nmindex,"default",0,0,5,0.00,5.00}
	ModifyGraph font(left1)="Arial",font(bottom1)="Arial"
	ModifyGraph fSize=12
	ModifyGraph lblPosMode(left1)=1,lblPosMode(bottom1)=1,lblPosMode(left3)=3,lblPosMode(bottom3)=1
	ModifyGraph lblPos(left3)=53,lblPos(bottom3)=42
	ModifyGraph freePos(left1)=0
	ModifyGraph freePos(bottom1)=0
	ModifyGraph freePos(left3)={600,bottom3}
	ModifyGraph freePos(bottom3)=0
	ModifyGraph axisEnab(bottom1)={0,0.35}
	ModifyGraph axisEnab(bottom3)={0.65,1}
	ModifyGraph manTick(bottom1)={0,5,0,0},manMinor(bottom1)={4,50}
	ModifyGraph manTick(left3)={500,100,0,0},manMinor(left3)={4,50}
	Label left1 "energy [eV]"
	Label bottom1 "density of states (DOS)"
	Label left3 "absorption in E\\B22\\M (λ in nm)"
	Label bottom3 "emission from E\\B11\\M [nm]"
	SetAxis left1 -2,2
	SetAxis bottom1 0,10
	SetAxis left3 450,725
	SetAxis bottom3 830,1050
	Legend/C/N=text1/J/F=0/A=MC/X=-17.21/Y=33.56 ""
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom1,ycoord= left1,arrow= 3
	DrawLine 5,-0.5,5,0.5
	SetDrawEnv xcoord= bottom1,ycoord= left1,arrow= 3
	DrawLine 8.80478087649402,-1.2,8.80478087649402,1.2
	SetDrawEnv xcoord= bottom1,ycoord= left1,textxjust= 1,textyjust= 1
	DrawText 2.63483526081245,0,"E\\B11"
	SetDrawEnv xcoord= bottom1,ycoord= left1,textxjust= 1,textyjust= 1
	DrawText 6.93080606531084,0,"E\\B22"
EndMacro

