#pragma TextEncoding = "UTF-8"		// For details execute DisplayHelpTopic "The TextEncoding Pragma"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include <InsertSubwindowInGraph>

Function export()
	SaveWindow("PLEM", saveImages = 1, saveJSON = 0, saveVector = 0)
	SaveWindow("absorption", saveImages = 1, saveJSON = 0, saveVector = 0)
	SaveWindow("combined", saveImages = 1, saveJSON = 0, saveVector = 0)
End
Window PLEM() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(525.75,47,984.75,324.5) as "PLEM"
	AppendImage/B=bottomLeft :PLE_zipfelmuetze03_combined:Silicon
	ModifyImage Silicon ctab= {0,9e-10,colortable_plotly_earth,1}
	AppendImage/B=bottomRight :PLE_zipfelmuetze03_combined:InGaAs
	ModifyImage InGaAs ctab= {0,1e-11,colortable_plotly_earth,1}
	ModifyGraph margin(left)=86,margin(bottom)=42,margin(top)=14,margin(right)=86,height={Plan,1,left,bottomLeft}
	ModifyGraph mirror(left)=2,mirror(bottomLeft)=2,mirror(bottomRight)=2
	ModifyGraph fSize=12
	ModifyGraph standoff=0
	ModifyGraph lblPosMode(left)=3,lblPosMode(bottomLeft)=1,lblPosMode(bottomRight)=1
	ModifyGraph lblPos(left)=55
	ModifyGraph lblLatPos(bottomLeft)=70
	ModifyGraph freePos(bottomLeft)=0
	ModifyGraph freePos(bottomRight)=0
	ModifyGraph axisEnab(bottomLeft)={0,0.4}
	ModifyGraph axisEnab(bottomRight)={0.4,1}
	ModifyGraph manTick(left)={0,50,0,0},manMinor(left)={1,50}
	ModifyGraph manTick(bottomLeft)={0,50,0,0},manMinor(bottomLeft)={1,50}
	ModifyGraph manTick(bottomRight)={0,50,0,0},manMinor(bottomRight)={1,0}
	Label left "excitation in E\\B22\\M λ [nm]"
	Label bottomLeft "emission from E\\B11\\M λ [nm]"
	SetAxis/R left 490,760
	SetAxis bottomLeft 900,1040
	SetAxis bottomRight 1040,1250
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottomRight,ycoord= left,linethick= 4,linefgc= (65535,16385,16385),fillpat= 0,fillfgc= (65535,0,0,32768)
	DrawOval 1100.24740032527,577.049180327869,1135.53545907052,608.032786885246
	SetDrawEnv xcoord= bottomRight,ycoord= left,linethick= 4,linefgc= (65535,16385,16385),fillpat= 0,fillfgc= (65535,0,0,32768)
	DrawOval 1107.54373860332,627.213114754098,1147.49248435267,678.852459016393
	SetDrawEnv xcoord= bottomRight,ycoord= left,fstyle= 1
	DrawText 1112.28115516482,550.96992481203,"(8,4)"
	SetDrawEnv xcoord= bottomRight,ycoord= left,fstyle= 1
	DrawText 1114.05944102101,689.494206828547,"(7,6)"
	SetDrawEnv xcoord= bottomRight,ycoord= left,linethick= 4,linefgc= (65535,16385,16385),fillpat= 0,fillfgc= (65535,0,0,32768)
	DrawOval 1010,685,1046.81818181818,615.454545454545
	SetDrawEnv xcoord= bottomRight,ycoord= left,fstyle= 1
	DrawText 968.150350111919,644.494206828547,"(7,5)"
	SetWindow kwTopWin,userdata(PLE_map_zipfel03_3)=  "image_sliderLimits={-6.3109e-08,7.9185e-08};"
EndMacro

Window absorption() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:example_absorption:
	Display /W=(52.5,46.25,511.5,329.75) absorptionj as "absorption (zipfelmuetze03)"
	AppendToGraph/R=axisderivative2 absorptionjdd/TN=derivative2
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=42,margin(bottom)=42,margin(top)=14,margin(right)=14
	ModifyGraph mode(derivative2)=7
	ModifyGraph rgb(derivative2)=(0,0,0)
	ModifyGraph useNegPat(derivative2)=1
	ModifyGraph useNegRGB(derivative2)=1
	ModifyGraph hBarNegFill(derivative2)=2
	ModifyGraph plusRGB(derivative2)=(65535,0,0,32768)
	ModifyGraph negRGB(derivative2)=(65535,54607,32768)
	ModifyGraph mirror(axisderivative2)=1
	ModifyGraph nticks(axisderivative2)=0
	ModifyGraph noLabel(axisderivative2)=1
	ModifyGraph fSize=12
	ModifyGraph lblMargin(axisderivative2)=303
	ModifyGraph lblPosMode(left)=1,lblPosMode(axisderivative2)=3
	ModifyGraph lblPos(axisderivative2)=-171
	ModifyGraph lblLatPos(bottom)=-9,lblLatPos(axisderivative2)=26
	ModifyGraph lblRot(axisderivative2)=90
	ModifyGraph axisOnTop(axisderivative2)=1
	ModifyGraph freePos(axisderivative2)=0
	ModifyGraph axisEnab(left)={0,0.7}
	ModifyGraph axisEnab(axisderivative2)={0.75,1}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={1,50}
	Label left "optical density"
	Label bottom "wavelength λ [nm]"
	Label axisderivative2 "2nd derivative"
	SetAxis left 0,2
	SetAxis bottom 350,1350
	SetAxis/A=2 axisderivative2
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,fillpat= 0,fillfgc= (61166,61166,61166,32768),fillbgc= (0,0,0)
	DrawRect 899.418604651163,1.25806451612903,1341.27906976744,0.301075268817204
	SetDrawEnv xcoord= bottom,ycoord= left,fillpat= 0,fillfgc= (61166,61166,61166,32768),fillbgc= (0,0,0)
	DrawRect 403.891050583658,1.7,853.891050583657,0.3
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 650.26455026455,1.00604594921403,650.309468822171,0
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 1133.54716981132,1.2,1133.54716981132,0
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawText 900,0.03,"E\\B11"
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawText 400,0.03,"E\\B22"
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 592.677248677249,0,592.677248677249,0.887545344619105
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 1116.33962264151,1.15,1116.33962264151,0
	SetDrawEnv xcoord= bottom,ycoord= left,textxjust= 1
	DrawText 1126.21262428101,1.7160391849984,"(8,4) + (7,6)"
	SetDrawEnv xcoord= bottom,ycoord= left,textxjust= 1,textyjust= 1
	DrawText 645.8368892658,1.50287769784173,"(7,6) + (7,5)"
	SetDrawEnv xcoord= bottom,ycoord= left,textxjust= 1,textyjust= 1
	DrawText 566.332723903049,1.1,"(8,4)"
	SetDrawEnv arrow= 1
	DrawLine 0.710443864229765,0.286792452830189,0.760443864229765,0.186792452830189
	SetDrawEnv arrow= 1
	DrawLine 0.85,0.286792452830189,0.8,0.186792452830189
	SetDrawEnv arrow= 2
	DrawLine 0.800560143299533,0.535849056603774,0.850560143299532,0.435849056603774
	SetDrawEnv arrow= 2
	DrawLine 0.759883720930232,0.532075471698113,0.709883720930233,0.432075471698113
	SetDrawEnv arrow= 1
	DrawLine 0.299610894941634,0.515151515151515,0.299610894941634,0.631313131313131
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 1033,0.877697841726619,1033,0
	SetDrawEnv xcoord= prel,ycoord= left
	DrawText 0.505836575875486,1.7160391849984,"(7,5)"
	SetDrawEnv arrow= 2
	DrawLine 0.67704280155642,0.671717171717172,0.591439688715953,0.439393939393939
	SetDrawEnv arrow= 1
	DrawLine 0.599221789883268,0.328282828282828,0.669260700389105,0.222222222222222
EndMacro

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(582,148.25,1223.25,401.75) as "combined"
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:example_absorption:
	Display/W=(0,0,0.489,1)/HOST=#  absorptionj
	AppendToGraph/R=axisderivative2 absorptionjdd/TN=derivative2
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=42,margin(bottom)=42,margin(top)=14,margin(right)=14,width={Aspect,1.3}
	ModifyGraph height=198.425
	ModifyGraph mode(derivative2)=7
	ModifyGraph rgb(derivative2)=(0,0,0)
	ModifyGraph useNegPat(derivative2)=1
	ModifyGraph useNegRGB(derivative2)=1
	ModifyGraph hBarNegFill(derivative2)=2
	ModifyGraph plusRGB(derivative2)=(65535,0,0,32768)
	ModifyGraph negRGB(derivative2)=(65535,54607,32768)
	ModifyGraph mirror(axisderivative2)=1
	ModifyGraph nticks(axisderivative2)=0
	ModifyGraph noLabel(axisderivative2)=1
	ModifyGraph fSize=12
	ModifyGraph lblMargin(axisderivative2)=303
	ModifyGraph axThick(axisderivative2)=0
	ModifyGraph lblPosMode(left)=1,lblPosMode(axisderivative2)=3
	ModifyGraph lblPos(axisderivative2)=-112
	ModifyGraph lblLatPos(bottom)=-9,lblLatPos(axisderivative2)=-21
	ModifyGraph lblRot(axisderivative2)=90
	ModifyGraph axisOnTop(axisderivative2)=1
	ModifyGraph freePos(axisderivative2)=0
	ModifyGraph axisEnab(left)={0,0.7}
	ModifyGraph axisEnab(axisderivative2)={0.75,1}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={1,50}
	Label left "optical density"
	Label bottom "wavelength λ [nm]"
	Label axisderivative2 "2nd derivative"
	SetAxis left 0,2
	SetAxis bottom 350,1350
	SetAxis/A=2 axisderivative2
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,fillpat= 0,fillfgc= (61166,61166,61166,32768),fillbgc= (0,0,0)
	DrawRect 899.418604651163,1.25806451612903,1341.27906976744,0.301075268817204
	SetDrawEnv xcoord= bottom,ycoord= left,fillpat= 0,fillfgc= (61166,61166,61166,32768),fillbgc= (0,0,0)
	DrawRect 403.891050583658,1.7,853.891050583657,0.3
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 650.26455026455,1.00604594921403,650.309468822171,0
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 1133.54716981132,1.2,1133.54716981132,0
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawText 900,0.03,"E\\B11"
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawText 400,0.03,"E\\B22"
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 592.677248677249,0,592.677248677249,0.887545344619105
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 1116.33962264151,1.15,1116.33962264151,0
	SetDrawEnv xcoord= bottom,ycoord= left,textxjust= 1
	DrawText 1126.21262428101,1.7160391849984,"(8,4) + (7,6)"
	SetDrawEnv xcoord= bottom,ycoord= left,textxjust= 1,textyjust= 1
	DrawText 645.8368892658,1.50287769784173,"(7,6) + (7,5)"
	SetDrawEnv xcoord= bottom,ycoord= left,textxjust= 1,textyjust= 1
	DrawText 566.332723903049,1.1,"(8,4)"
	SetDrawEnv arrow= 1
	DrawLine 0.710443864229765,0.286792452830189,0.760443864229765,0.186792452830189
	SetDrawEnv arrow= 1
	DrawLine 0.85,0.286792452830189,0.8,0.186792452830189
	SetDrawEnv arrow= 2
	DrawLine 0.800560143299533,0.535849056603774,0.850560143299532,0.435849056603774
	SetDrawEnv arrow= 2
	DrawLine 0.759883720930232,0.532075471698113,0.709883720930233,0.432075471698113
	SetDrawEnv arrow= 1
	DrawLine 0.299610894941634,0.515151515151515,0.299610894941634,0.631313131313131
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawLine 1033,0.877697841726619,1033,0
	SetDrawEnv xcoord= prel,ycoord= left
	DrawText 0.505836575875486,1.7160391849984,"(7,5)"
	SetDrawEnv arrow= 2
	DrawLine 0.67704280155642,0.671717171717172,0.591439688715953,0.439393939393939
	SetDrawEnv arrow= 1
	DrawLine 0.599221789883268,0.328282828282828,0.669260700389105,0.222222222222222
	RenameWindow #,absorption
	SetActiveSubwindow ##
	Display/W=(0.484,0,0.994,1)/HOST=# 
	AppendImage/B=bottomLeft :PLE_zipfelmuetze03_combined:Silicon
	ModifyImage Silicon ctab= {0,9e-10,colortable_plotly_earth,1}
	AppendImage/B=bottomRight :PLE_zipfelmuetze03_combined:InGaAs
	ModifyImage InGaAs ctab= {0,1e-11,colortable_plotly_earth,1}
	ModifyGraph margin(left)=56,margin(bottom)=42,margin(top)=14,margin(right)=14,width={Aspect,1.3}
	ModifyGraph height=198.425
	ModifyGraph mirror(left)=2,mirror(bottomLeft)=2,mirror(bottomRight)=2
	ModifyGraph fSize=12
	ModifyGraph standoff=0
	ModifyGraph lblPosMode(bottomLeft)=1,lblPosMode(bottomRight)=1
	ModifyGraph lblLatPos(bottomLeft)=70
	ModifyGraph freePos(bottomLeft)=0
	ModifyGraph freePos(bottomRight)=0
	ModifyGraph axisEnab(bottomLeft)={0,0.4}
	ModifyGraph axisEnab(bottomRight)={0.4,1}
	ModifyGraph manTick(left)={0,50,0,0},manMinor(left)={1,50}
	ModifyGraph manTick(bottomLeft)={0,50,0,0},manMinor(bottomLeft)={1,50}
	ModifyGraph manTick(bottomRight)={0,50,0,0},manMinor(bottomRight)={1,0}
	Label left "excitation in E\\B22\\M λ [nm]"
	Label bottomLeft "emission from E\\B11\\M λ [nm]"
	SetAxis/R left 490,760
	SetAxis bottomLeft 900,1040
	SetAxis bottomRight 1040,1250
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottomRight,ycoord= left,linethick= 4,linefgc= (65535,16385,16385),fillpat= 0,fillfgc= (65535,0,0,32768)
	DrawOval 1100.24740032527,577.049180327869,1135.53545907052,608.032786885246
	SetDrawEnv xcoord= bottomRight,ycoord= left,linethick= 4,linefgc= (65535,16385,16385),fillpat= 0,fillfgc= (65535,0,0,32768)
	DrawOval 1107.54373860332,627.213114754098,1147.49248435267,678.852459016393
	SetDrawEnv xcoord= bottomRight,ycoord= left,fstyle= 1
	DrawText 1112.28115516482,550.96992481203,"(8,4)"
	SetDrawEnv xcoord= bottomRight,ycoord= left,fstyle= 1
	DrawText 1114.05944102101,689.494206828547,"(7,6)"
	SetDrawEnv xcoord= bottomRight,ycoord= left,linethick= 4,linefgc= (65535,16385,16385),fillpat= 0,fillfgc= (65535,0,0,32768)
	DrawOval 1010,685,1046.81818181818,615.454545454545
	SetDrawEnv xcoord= bottomRight,ycoord= left,fstyle= 1
	DrawText 968.150350111919,644.494206828547,"(7,5)"
	SetWindow kwTopWin,userdata(PLE_map_zipfel03_3)=  "image_sliderLimits={-6.3109e-08,7.9185e-08};"
	RenameWindow #,PLEM_plotlycolortable
	SetActiveSubwindow ##
EndMacro

