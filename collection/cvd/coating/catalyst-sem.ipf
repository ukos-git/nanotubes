#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("oswalt", saveJSON = 0, saveImages = 1, saveVector = 0)
End
Window oswalt() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(47.25,59.75,504.75,293) as "oswalt"
	AppendImage catalyst
	ModifyImage catalyst ctab= {29.5982142857143,255,Grays,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=14,width={Plan,1,bottom,left}
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph axThick=0
	SetAxis left 10,14
	SetAxis bottom 11.1944256156891,19.5493870560969
	Cursor/P/I A catalyst 448,494
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 2,linefgc= (65535,65535,65535)
	DrawLine 18.1806307989558,0.933006642292452,19.1806307989558,0.933006642292452
	SetDrawEnv fname= "Times New Roman",fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.847656436946639,0.907563025210084,"1µm"
EndMacro

