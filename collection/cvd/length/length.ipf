#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("comparison", saveJSON = 0, saveImages = 1, saveVector = 0)
End
Window comparison() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(33,77,493.5,268.25) as "comparison"
	AppendImage/T :L_BS_1_21:data1:'BS_1_21.png'
	ModifyImage 'BS_1_21.png' ctab= {0,1654.453125,YellowHot256,0}
	AppendImage/R :mk53plcam1_overview:data1:'MK53.1_3_99.png'
	ModifyImage 'MK53.1_3_99.png' ctab= {2194.25223214286,9800.99330357143,YellowHot,0}
	ModifyGraph margin(left)=36,margin(bottom)=14,margin(top)=14,margin(right)=36,width={Plan,1,top,left}
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph minor=1
	ModifyGraph noLabel=2
	ModifyGraph fSize=8
	ModifyGraph standoff=0
	ModifyGraph axThick=0
	ModifyGraph tkLblRot(left)=90
	ModifyGraph btLen=3
	ModifyGraph tlOffset(left)=-2,tlOffset(top)=-2
	ModifyGraph axisEnab(top)={0,0.5}
	ModifyGraph axisEnab(bottom)={0.5,1}
	SetAxis/R left 98.042846768337,87.6579520697168
	SetAxis top 159.313725490196,171.601307189542
	SetAxis/R right 85,105
	SetAxis bottom 25,55
	SetDrawLayer UserFront
	SetDrawEnv xcoord= top,ycoord= prel,linethick= 5,linefgc= (65535,65535,65535)
	DrawLine 160,0.0558659217877095,165,0.0558659217877095
	SetDrawEnv xcoord= top,ycoord= prel,fsize= 16,fstyle= 1,textrgb= (65535,65535,65535),textyjust= 2
	DrawText 161.435271759536,0.1,"1µm"
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 5,linefgc= (65535,65535,65535)
	DrawLine 43.191934754576,0.0558659217877095,53.1919347545761,0.0558659217877095
	SetDrawEnv xcoord= bottom,ycoord= prel,fsize= 16,fstyle= 1,textrgb= (65535,65535,65535),textyjust= 2
	DrawText 46.2269960142572,0.1,"2µm"
	SetDrawEnv xcoord= top,ycoord= prel,linethick= 5
	DrawLine 2.68398268398269,-0.0181847207709281,2.68398268398269,0.0426618400756324
EndMacro

