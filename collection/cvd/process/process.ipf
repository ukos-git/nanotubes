#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("suspended", saveImages = 1, saveJSON = 0, saveVector = 1)
	saveWindow("zeolith", saveImages = 1, saveJSON = 0, saveVector = 1)
End

Window suspended() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:suspended:
	Display /W=(16.5,53.75,474.75,263) cvd_temperature[0,*;60] vs cvd_time[0,*;60] as "suspended"
	AppendToGraph cvd_argon[0,*;60] vs cvd_time[0,*;60]
	AppendToGraph/R cvd_pressure[0,*;60] vs cvd_time[0,*;60]
	AppendToGraph cvd_ethanolEthanol vs cvd_timeEthanol
	SetDataFolder fldrSav0
	ModifyGraph height={Aspect,0.5}
	ModifyGraph lSize=2
	ModifyGraph rgb(cvd_temperature)=(0,12800,52224),rgb(cvd_argon)=(26112,52224,0)
	ModifyGraph rgb(cvd_pressure)=(52224,34816,0)
	ModifyGraph minor(left)=1
	ModifyGraph sep(left)=10
	ModifyGraph fSize=12
	ModifyGraph lblLatPos(left)=15
	Label left "temperatue [°C]\rargon flow [sccm]\rethanol flow [mg/min]"
	Label bottom "reaction progress [min]"
	Label right "pressure [mbar]"
	SetAxis left 0,1000
	SetAxis bottom 0,159.216833333333
	SetAxis right 0,1000
	ErrorBars cvd_temperature Y,wave=(:suspended:cvd_temperature_error,:suspended:cvd_temperature_error)
	ErrorBars cvd_argon Y,wave=(:suspended:cvd_argon_error,:suspended:cvd_argon_error)
	Legend/C/N=text0/J/F=0/X=65.12/Y=4.73 "\\Z12\\s(cvd_temperature) temperature\r\\s(cvd_argon) argon flow\r\\s(cvd_ethanolEthanol) ethanol flow"
	AppendText "\\s(cvd_pressure) pressure"
EndMacro

Window zeolith() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:zeolith:
	Display /W=(486,53,945,263) cvd_temperature[0,*;60] vs cvd_time[0,*;60] as "zeolith"
	AppendToGraph cvd_argon[0,*;60],cvd_etoh[0,*;60] vs cvd_time[0,*;60]
	AppendToGraph/R cvd_pressure[0,*;60] vs cvd_time[0,*;60]
	SetDataFolder fldrSav0
	ModifyGraph height={Aspect,0.5}
	ModifyGraph lSize=2
	ModifyGraph rgb(cvd_temperature)=(0,12800,52224),rgb(cvd_argon)=(26112,52224,0)
	ModifyGraph rgb(cvd_pressure)=(52224,34816,0)
	ModifyGraph fSize=12
	Label left "temperatue [°C]\rargon flow [sccm]\rethanol flow [mg/min]"
	Label bottom "reaction progress [min]"
	Label right "pressure [mbar]"
	SetAxis left 0,1000
	SetAxis bottom 30,90
	SetAxis right 0,1000
	ErrorBars cvd_temperature Y,wave=(:zeolith:cvd_temperature_error,:zeolith:cvd_temperature_error)
	ErrorBars cvd_pressure Y,wave=(:zeolith:cvd_pressure_error,:zeolith:cvd_pressure_error)
	Legend/C/N=text0/J/F=0/X=64.58/Y=1.74 "\\Z12\\s(cvd_temperature) temperature\r\\s(cvd_argon) argon-flow\r\\s(cvd_etoh) ethanol-flow\r\\s(cvd_pressure) pressure"
EndMacro

