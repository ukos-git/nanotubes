#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "plem"
#include "utilities-images"
#include <Color Table Control Panel>

Function export()
	SaveWindow("combined", saveJSON = 0, saveImages = 1, saveVector = 0)
End

Window mk109_15_graph() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:PLEMd2:maps:mk109_15:CHIRALITY:
	AppendToGraph/R=right850/B=bottom850 PLEMs2nm/TN=plem01850 vs PLEMs1nm
	AppendToGraph/R=right850/B=bottom850 PLEMs2nm/TN=plem02850 vs PLEMs1nm
	AppendToGraph/R=right850/B=bottom850 PLEMs2nm/TN=plem03850 vs PLEMs1nm
	AppendImage/R=right850/B=bottom850 ::PLEM
	ModifyImage PLEM ctab= {*,*,Terrain256,0}
	SetDataFolder fldrSav0
	ModifyGraph mode=3
	ModifyGraph marker(plem01850)=8,marker(plem02850)=1
	ModifyGraph rgb(plem02850)=(0,0,0),rgb(plem03850)=(65535,65535,65535)
	ModifyGraph msize(plem01850)=5
	ModifyGraph useMrkStrokeRGB(plem03850)=1
	ModifyGraph mrkStrokeRGB(plem03850)=(65535,65535,65535)
	ModifyGraph textMarker(plem03850)={:PLEMd2:maps:mk109_15:CHIRALITY:PLEMchirality,"default",0,0,5,0.00,10.00}
	ModifyGraph mirror=0
	ModifyGraph standoff=0
	Label right850 "excitation / nm"
	Label bottom850 "emission / nm"
	SetAxis right850 550,760
	SetAxis bottom850 900,1325
EndMacro

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:PLEMd2:maps:CoMoCat:CHIRALITY:
	Display /W=(23.25,63.5,479.25,309.5) PLEMs2nm/TN=plem01 vs PLEMs1nm as "combined"
	AppendToGraph PLEMs2nm/TN=plem02 vs PLEMs1nm
	AppendToGraph PLEMs2nm/TN=plem03 vs PLEMs1nm
	AppendToGraph/R/B=bottom2 :::mk108:CHIRALITY:PLEMs2nm/TN=plemr01 vs :::mk108:CHIRALITY:PLEMs1nm
	AppendToGraph/R/B=bottom2 :::mk108:CHIRALITY:PLEMs2nm/TN=plemr02 vs :::mk108:CHIRALITY:PLEMs1nm
	AppendToGraph/R/B=bottom2 :::mk108:CHIRALITY:PLEMs2nm/TN=plemr03 vs :::mk108:CHIRALITY:PLEMs1nm
	AppendToGraph/R=right850/B=bottom850 :::mk109_15:CHIRALITY:PLEMs2nm/TN=plem01850 vs :::mk109_15:CHIRALITY:PLEMs1nm
	AppendToGraph/R=right850/B=bottom850 :::mk109_15:CHIRALITY:PLEMs2nm/TN=plem02850 vs :::mk109_15:CHIRALITY:PLEMs1nm
	AppendToGraph/R=right850/B=bottom850 :::mk109_15:CHIRALITY:PLEMs2nm/TN=plem03850 vs :::mk109_15:CHIRALITY:PLEMs1nm
	AppendImage ::PLEM
	ModifyImage PLEM ctab= {0,*,:::::colortable_plotly_earth,1}
	AppendImage/B=bottom2/R :::mk108:PLEM
	ModifyImage PLEM#1 ctab= {0,*,:::::colortable_plotly_earth,1}
	AppendImage/B=bottom850/R=right850 :::mk109_15:PLEM
	ModifyImage PLEM#2 ctab= {0,*,:::::colortable_plotly_earth,1}
	AppendImage/L=nitrogen :::mk124v1ingaas_0:PLEM
	ModifyImage PLEM#3 ctab= {0,*,:::::colortable_plotly_earth,1}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=56,margin(bottom)=42,margin(top)=14,margin(right)=56,width={Plan,1,bottom2,right}
	ModifyGraph mode=3
	ModifyGraph marker(plem01)=8,marker(plem02)=1,marker(plemr01)=8,marker(plemr02)=1
	ModifyGraph marker(plem01850)=8,marker(plem02850)=1
	ModifyGraph rgb(plem02)=(0,0,0),rgb(plem03)=(61166,61166,61166),rgb(plemr02)=(0,0,0)
	ModifyGraph rgb(plemr03)=(65535,65535,65535),rgb(plem02850)=(0,0,0),rgb(plem03850)=(65535,65535,65535)
	ModifyGraph msize(plem01)=5,msize(plemr01)=5,msize(plem01850)=5
	ModifyGraph hideTrace(plem01)=1,hideTrace(plem02)=1,hideTrace(plemr01)=1,hideTrace(plemr02)=1
	ModifyGraph hideTrace(plem01850)=1,hideTrace(plem02850)=1
	ModifyGraph useMrkStrokeRGB(plem03)=1,useMrkStrokeRGB(plemr03)=1,useMrkStrokeRGB(plem03850)=1
	ModifyGraph mrkStrokeRGB(plem03)=(65535,65535,65535),mrkStrokeRGB(plemr03)=(65535,65535,65535)
	ModifyGraph mrkStrokeRGB(plem03850)=(65535,65535,65535)
	ModifyGraph offset(plem03)={0,10},offset(plemr03)={0,10}
	ModifyGraph textMarker(plem03)={:PLEMd2:maps:CoMoCat:CHIRALITY:PLEMchirality,"default",1,0,5,0.00,10.00}
	ModifyGraph textMarker(plemr03)={:PLEMd2:maps:mk108:CHIRALITY:PLEMchirality,"default",1,0,5,0.00,10.00}
	ModifyGraph textMarker(plem03850)={:PLEMd2:maps:mk109_15:CHIRALITY:PLEMchirality,"default",1,0,5,0.00,10.00}
	ModifyGraph mirror(left)=0,mirror(bottom)=0,mirror(right)=0
	ModifyGraph noLabel(bottom850)=2
	ModifyGraph standoff(left)=0,standoff(bottom)=0,standoff(right)=0,standoff(bottom2)=0
	ModifyGraph standoff(right850)=0,standoff(bottom850)=0
	ModifyGraph lblPosMode(bottom)=1,lblPosMode(right)=1,lblPosMode(bottom2)=1,lblPosMode(right850)=1
	ModifyGraph lblPosMode(bottom850)=1
	ModifyGraph lblPos(left)=55,lblPos(bottom)=46,lblPos(right)=55
	ModifyGraph lblLatPos(left)=50,lblLatPos(bottom)=100,lblLatPos(right)=-60
	ModifyGraph freePos(bottom2)={0,kwFraction}
	ModifyGraph freePos(right850)={0,kwFraction}
	ModifyGraph freePos(bottom850)={0.55,kwFraction}
	ModifyGraph freePos(nitrogen)={0,kwFraction}
	ModifyGraph axisEnab(left)={0.55,1}
	ModifyGraph axisEnab(bottom)={0,0.49}
	ModifyGraph axisEnab(right)={0,0.45}
	ModifyGraph axisEnab(bottom2)={0.51,1}
	ModifyGraph axisEnab(right850)={0.55,1}
	ModifyGraph axisEnab(bottom850)={0.51,1}
	ModifyGraph axisEnab(nitrogen)={0,0.45}
	ModifyGraph manTick(left)={0,100,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom)={0,100,0,0},manMinor(bottom)={3,2}
	ModifyGraph manTick(right)={0,100,0,0},manMinor(right)={3,2}
	ModifyGraph manTick(bottom2)={0,100,0,0},manMinor(bottom2)={3,2}
	ModifyGraph manTick(right850)={0,100,0,0},manMinor(right850)={3,2}
	ModifyGraph manTick(bottom850)={0,100,0,0},manMinor(bottom850)={3,2}
	ModifyGraph manTick(nitrogen)={0,100,0,0},manMinor(nitrogen)={3,2}
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	Label right "excitation wavelength [nm]"
	Label bottom850 "emission / nm"
	SetAxis left 500,700
	SetAxis bottom 925,1320
	SetAxis right 500,700
	SetAxis bottom2 925,1320
	SetAxis right850 500,700
	SetAxis bottom850 925,1320
	SetAxis nitrogen 500,700
	TextBox/C/N=text0/S=3/A=LT/X=50.50/Y=35.00 "850°C"
	TextBox/C/N=text1/S=3/A=LT/X=50.50/Y=90.00 "725°C"
	TextBox/C/N=text2/S=3/A=LT/X=0.50/Y=35.00 "CoMoCat"
	TextBox/C/N=text3/S=3/A=LT/X=0.50/Y=90.00 "750°C 1%AcN/EtOH"
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6-bo9lEC-FC/[$@;om@3r"
EndMacro

