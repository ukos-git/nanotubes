#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

static StrConstant GRAPH_NAME = "firstpassage"

static constant DEFAULT_OFFSET = 0

static Constant EMISSION_WAVELENGTH =  930e-9 // 927.52nm E11 emission at 660-670nm absorption
static Constant OBSERVER_DISTANCE = 5e-3      // 5mm high wd objective (Olympus LCPLN50XIR) with 1mm quartz glass window
static Constant EXCITON_SLITWIDTH = 8.5e-9    // exciton size <13 nm
static Constant CALC_REDUCTION = 1            // set to 2^N to reduce calc time

threadsafe Function airypattern(x, y)
	variable x, y

	variable argument 

	argument = (2 * pi / EMISSION_WAVELENGTH) * (EXCITON_SLITWIDTH / 2) / OBSERVER_DISTANCE 
	argument *= sqrt(x^2 + y^2)

	return besselj(1, argument) / argument
End

static Function StepsInit([offset])
	variable offset

	offset = ParamIsDefault(offset) ? DEFAULT_OFFSET : offset

	DoWindow $GRAPH_NAME
	if(V_flag)
		return 0
	endif
	Display/N=$GRAPH_NAME as "firstpassage"
	Make root:step
	Make root:emission

	Make root:excitation_energy/WAVE=excitation_energy
	Make root:excitation_intensity/WAVE=excitation_intensity

	SetScale/I x, offset - 1, offset + 1, excitation_energy, excitation_intensity
	Make/FREE/N=7 laser_coef = {21.0005,15333.7,0.0348989,0.532661,-0.0408297,0.517219,-0.0321556}
	WAVE excitation_intensity
	excitation_intensity = exp((-1 / (2 * (1 - laser_coef[6]^2))) * (((x - offset) / laser_coef[3])^2 ))
	excitation_energy = sqrt(excitation_intensity)

	AppendTograph/L=excitation excitation_intensity
	SetAxis excitation 0,*
	ModifyGraph nticks(excitation)=2,axisEnab(excitation)={0,0.2},freePos(excitation)={0,kwFraction}

	Label bottom "CNT length [nm]"
	Label left "diffusion steps"
	ModifyGraph axisEnab(left)={0.25,0.75}
	SetAxis bottom -0.125,2.125

	Make/N=1 root:histResult/WAVE=histResult = NaN
	Make/N=1 root:histResultC = NaN
	AppendToGraph/L=hist histResult/TN=histogram

	ModifyGraph axisEnab(hist)={0.8,1}, freePos(hist)={0,kwFraction}
	ModifyGraph mode(histogram)=5,hbFill(histogram)=2,rgb(histogram)=(65535,49151,49151),useBarStrokeRGB(histogram)=1
	Label hist "#"

	Make/U/B root:alive = 0
End

static Constant MAX_STEPS = 16384
Constant SEED_LED   = 1
Constant SEED_LASER = 0
Constant GEORGI_DIFFUSION_LENGTH = 0.105 // l_D = 105nm (georgi2008)
// anderson2013: lifetimes (350–750 ps), diffusion constants (130–350 cm2/s)

Function AddSteps(numTraces, numSteps, seedtype, diffusion_length, [offset, calcLineProfiles])
	variable numTraces, numSteps
	variable offset, seedtype, calcLineProfiles, diffusion_length

	variable previousTraces, previousSteps
	variable i, seed, err, status0, status1, normalization
	string name

	offset = ParamIsDefault(offset) ? DEFAULT_OFFSET : offset
	calcLineProfiles = ParamIsDefault(calcLineProfiles) ? 0 : calcLineProfiles

	numTraces = max(numTraces, 0)
	// WaveSizes fixed due to performance
	numSteps = min(max(numSteps, 1), MAX_STEPS)

	WAVE step = root:step
	previousSteps = DimSize(step, 0)
	Redimension/N=(numSteps) step
	step = p

	WAVE emission = root:emission
	previousTraces = DimSize(emission, 0)
	Redimension/N=(numTraces) emission

	WAVE alive = root:alive
	Redimension/N=(numTraces) alive

	WAVE excitation_intensity = root:excitation_intensity
	Make/FREE/N=7 laser_coef = {21.0005,15333.7,0.0348989,0.532661,-0.0408297,0.517219,-0.0321556}
	if(seedType == SEED_LASER)
		excitation_intensity = exp((-1 / (2 * (1 - laser_coef[6]^2))) * (((x - offset) / laser_coef[3])^2 )) // squared exponential
	elseif(seedType == SEED_LED)
		excitation_intensity = 0.5
	else
		Abort
	endif

	String traces = TraceNameList(GRAPH_NAME, ";", 1)
	for(i = 0; i < numTraces; i += 1)
		name = "diffusion" + num2istr(i)
		WAVE/Z wv = root:$name
		if(!WaveExists(wv) || DimSize(wv, 0) < MAX_STEPS)
			Make/O/N=(MAX_STEPS) root:$name/WAVE=wv = NaN
		endif
		if(WhichListItem(name, traces) == -1)
			//AppendToGraph/W=$GRAPH_NAME step/TN=$name vs wv
			alive[i] = 1  // all traces are born red
		endif
		if((i >= previousTraces) || (numSteps < 2))
			previousSteps = 0
			if(seedType == SEED_LASER)
				seed = sign(enoise(1)) * expNoise((1 - laser_coef[6]^2) * laser_coef[3]^2) + offset // laser excitation
			elseif(seedType == SEED_LED)
				seed = enoise(offset + 1)
			else
				seed = offset
			endif
			//ModifyGraph/W=$GRAPH_NAME rgb($name)=(65535,0,0, 13107) // excitons are red
			alive[i] = 1
		endif
		if(previousSteps < numSteps)
			wv[previousSteps, inf] = p < 1 ? seed : (abs(offset - wv[p - 1]) < 1 ? gnoise(diffusion_length / sqrt(numSteps - 1)) + wv[p - 1] : sign(wv[p - 1]) * inf)
			wv[previousSteps, inf] = (abs(offset - wv[p]) < 1 ? wv[p] : sign(wv[p]) * inf)
		endif
		wv[numSteps, inf] = NaN

		// mark exciton as dead or living
		if(!alive[i] && numType(wv[numSteps - 1]) == 0)
			//ModifyGraph/W=$GRAPH_NAME rgb($name)=(65535,0,0,13107)
			//ReorderTraces _front_, {$name}
			alive[i] = 1
		elseif(alive[i] && numType(wv[numSteps - 1]) != 0)
			//ModifyGraph/W=$GRAPH_NAME rgb($name)=(0,0,0,13107) // dead
			//ReorderTraces _back_, {$name}
			alive[i] = 0
		endif

		emission[i] = wv[numSteps - 1]
	endfor
	for(i = numTraces; i < ItemsInList(traces); i += 1)
		name = "diffusion" + num2istr(i)
		if(WhichListItem(name, traces) != -1)
			RemoveFromGraph/W=$GRAPH_NAME $name
		endif
	endfor

	Make/O/N=18 root:histResult/WAVE=histResult = NaN
	Make/O/N=18 root:histResultDead/WAVE=histDead = NaN
	if(DimSize(emission, 0) > 0)
		Histogram/B={offset - 1.125, 0.125, 18} emission, histResult
		SetScale/P x, offset - 1.125, 0.125, histDead
		Duplicate/FREE emission, emission_dead
		emission_dead = emission[p] < -1 ? 1 : 0
		histDead[0]  = sum(emission_dead)
		emission_dead = emission[p] > 1 ? 1 : 0
		histDead[17] = sum(emission_dead)
	endif

	SetAxis/W=$GRAPH_NAME histR 0, max(WaveMax(histResult), WaveMax(histDead))
	SetAxis/W=$GRAPH_NAME hist 0, max(WaveMax(histResult), WaveMax(histDead))

	// calc image
	WAVE image = root:cntimage
	calcImage(image, calcLineProfiles, 0)
End

Function calcImage(image, calcLineProfiles, swapXY)
	WAVE image
	Variable calcLineProfiles, swapXY

	Variable i, normalization, numTraces

	image = 0
	WAVE emission = root:emission
	numTraces = DimSize(emission, 0)

	Duplicate/FREE emission, emission_alive
	WaveTransForm zapInfs emission_alive
	printf "%d of %d excitons died due to end quenching\r" numTraces - DimSize(emission_alive, 0), numTraces
	NVAR landscape
	if(numType(landscape) == 0 && DimSize(emission_alive, 0) > 0)
		emission_alive = round(emission_alive[p] / landscape) * landscape
	endif
	for(i = 0; i < DimSize(emission_alive, 0)/CALC_REDUCTION; i += 1)
		if(swapXY)
			Multithread image[][] += airypattern(x, y - emission_alive[i])
		else
			Multithread image[][] += airypattern(x - emission_alive[i], y)
		endif
	endfor
	image[][] = image[p][q]^2 // intensity
	normalization = WaveMax(image)
	image /= normalization

	if(!calcLineProfiles)
		return NaN // No Line profiles
	endif

	Matrixop/O root:lineprofile0/WAVE=lineprofile0 = SumRows(image)
	Matrixop/O lineprofile0 = lineprofile0 / maxVal(lineprofile0)
	SetScale/P x, DimOffset(image, 0), DimDelta(image, 0), lineprofile0
	Matrixop/O root:lineprofile1/WAVE=lineprofile1 = Redimension(Sumcols(image), 768,0)
	Matrixop/O lineprofile1 = lineprofile1 / maxVal(lineprofile1)
	SetScale/P x, DimOffset(image, 1), DimDelta(image, 1), lineprofile1
	Make/O/N=(DimSize(image, 1)) lineprofile1_dummyx =  DimOffset(image, 1) + p * DimDelta(image, 1)
End

Function SliderProc(sa) : SliderControl
	STRUCT WMSliderAction &sa

	NVAR numTraces, numSteps, seedType, diffusionLength

	switch( sa.eventCode )
		case -3: // Control received keyboard focus
		case -2: // Control lost keyboard focus
		case -1: // Control being killed
			break
		default:
			if( sa.eventCode & 1 ) // value set
				AddSteps(numTraces, numSteps, seedType, diffusionLength)
			endif
			break
	endswitch

	return 0
End

Function ButtonProc(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	
	NVAR numTraces, numSteps, seedType, diffusionLength

	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			AddSteps(numTraces, numSteps, seedType, diffusionLength)
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Function SetVarProc(sva) : SetVariableControl
	STRUCT WMSetVariableAction &sva

	NVAR numTraces, numSteps, seedType, diffusionLength

	switch( sva.eventCode )
		case 1: // mouse up
		case 2: // Enter key
		case 3: // Live update
			AddSteps(numTraces, numSteps, seedType, diffusionLength)
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Function PopMenuProc(pa) : PopupMenuControl
	STRUCT WMPopupAction &pa

	NVAR numTraces, numSteps, seedType, diffusionLength

	switch( pa.eventCode )
		case 2: // mouse up
			seedType = pa.popNum - 1
			AddSteps(numTraces, numSteps, seedType, diffusionLength)
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Window firstpassage() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(147.75,384.5,552,892.25) step/TN=diffusion0 vs diffusion0 as "firstpassage"
	AppendToGraph/L=hist histResult/TN=histogram
	AppendToGraph/L=excitation excitation_intensity
	AppendToGraph/R=histR fit_histResultC
	AppendToGraph/L=hist histResultDead
	AppendImage/L=airyimage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	ModifyGraph height={Plan,1,airyimage,bottom}
	ModifyGraph mode(histogram)=5,mode(excitation_intensity)=7,mode(histResultDead)=5
	ModifyGraph rgb(excitation_intensity)=(65535,0,0,49151),rgb(fit_histResultC)=(65535,0,0,6554)
	ModifyGraph rgb(histResultDead)=(30583,30583,30583)
	ModifyGraph hbFill(histogram)=2,hbFill(excitation_intensity)=2,hbFill(histResultDead)=2
	ModifyGraph usePlusRGB(excitation_intensity)=1
	ModifyGraph plusRGB(excitation_intensity)=(65535,49151,49151)
	ModifyGraph useBarStrokeRGB(histogram)=1,useBarStrokeRGB(histResultDead)=1
	ModifyGraph tick(excitation)=1
	ModifyGraph mirror(left)=1,mirror(bottom)=0,mirror(excitation)=1
	ModifyGraph nticks(hist)=0,nticks(excitation)=2,nticks(histR)=0,nticks(airyimage)=0
	ModifyGraph noLabel(excitation)=1
	ModifyGraph standoff=0
	ModifyGraph axThick(hist)=0,axThick(histR)=0,axThick(airyimage)=0
	ModifyGraph lblPosMode(left)=1,lblPosMode(hist)=1,lblPosMode(excitation)=1
	ModifyGraph lblPos(left)=53
	ModifyGraph freePos(hist)={0,kwFraction}
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph freePos(histR)={0,kwFraction}
	ModifyGraph freePos(airyimage)={0,kwFraction}
	ModifyGraph axisEnab(left)={0.16,0.5}
	ModifyGraph axisEnab(hist)={0.5,0.6}
	ModifyGraph axisEnab(excitation)={0,0.15}
	ModifyGraph axisEnab(histR)={0.5,0.7}
	ModifyGraph axisEnab(airyimage)={0.61,1}
	ModifyGraph manTick(bottom)={0,1,0,0},manMinor(bottom)={1,50}
	Label left "diffusion steps"
	Label bottom "SWNT position [nm]"
	Label hist "emission"
	Label excitation "excitation"
	SetAxis bottom -1.25,1.25
	SetAxis excitation 0,1
	SetAxis histR 0,5329
	SetAxis airyimage 0,1.5
	TextBox/C/N=text0/F=0/A=MC/X=36.40/Y=-36.69 "\\s(excitation_intensity) excitation"
	TextBox/C/N=emission/F=0/B=(65535,65535,65535,32768)/A=MC/X=28.32/Y=7.77 "\\s(histogram) emission\r\\s(histResultDead) quenched"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=0.51/Y=-45.14 "SWNT"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= excitation,linejoin= 1,fillpat= 16,fillfgc= (30583,30583,30583),fillbgc= (65535,65535,65535,49151)
	DrawRect -1,0.2,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.764133113369009,1.32039504288164,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.794148425925069,1.29169274161713,0.994148425925068,1.29169274161713
	NewPanel/HOST=#/EXT=1/W=(111,0,0,579) 
	ModifyPanel fixedSize=0
	Slider slider0,pos={39.00,140.00},size={61.00,224.00},proc=SliderProc
	Slider slider0,limits={0,1023,1},variable= numSteps
	Button recalc,pos={22.00,434.00},size={50.00,20.00},proc=ButtonProc,title="calc"
	SetVariable setvar0,pos={5.00,41.00},size={100.00,18.00}
	SetVariable setvar0,limits={-inf,inf,0},value= landscape
	SetVariable setvar1,pos={6.00,379.00},size={100.00,18.00}
	SetVariable setvar1,limits={0,1024,1},value= numSteps
	RenameWindow #,P0
	SetActiveSubwindow ##
	NewPanel/HOST=#/EXT=2/W=(0,0,474,77) 
	ModifyPanel fixedSize=0
	Slider slider0,pos={96.00,10.00},size={377.00,49.00},proc=SliderProc
	Slider slider0,limits={1,2048,1},variable= numTraces,vert= 0
	SetVariable setvar0,pos={5.00,8.00},size={91.00,18.00},proc=SetVarProc,title="exctions"
	SetVariable setvar0,value= numTraces
	PopupMenu popup0,pos={3.00,34.00},size={89.00,36.00},proc=PopMenuProc,title="seed"
	PopupMenu popup0,mode=2,popvalue="LED",value= #"\"LASER;LED\""
	RenameWindow #,P1
	SetActiveSubwindow ##
EndMacro

Window airyimage() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(533.25,61.25,800.25,421.25)/B=bottomleft lineprofile1_dummyx vs lineprofile1 as "airyimage"
	AppendToGraph/L=topleft lineprofile0,lineprofile0
	AppendToGraph/B=bottomleft lineprofile1_dummyx vs lineprofile1
	AppendImage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	ModifyGraph margin(left)=7,margin(bottom)=7,margin(top)=7,margin(right)=7,width={Plan,1,bottom,left}
	ModifyGraph marker(lineprofile0)=19
	ModifyGraph lSize(lineprofile1_dummyx)=3,lSize(lineprofile0)=3
	ModifyGraph rgb(lineprofile1_dummyx)=(0,0,0),rgb(lineprofile0)=(0,0,0)
	ModifyGraph hbFill(lineprofile1_dummyx)=2
	ModifyGraph usePlusRGB(lineprofile1_dummyx)=1
	ModifyGraph plusRGB(lineprofile1_dummyx)=(65535,49151,49151)
	ModifyGraph zColor(lineprofile1_dummyx)={lineprofile1,0,1,YellowHot256},zColor(lineprofile0)={lineprofile0,0,1,YellowHot256}
	ModifyGraph mirror(left)=0,mirror(bottom)=0
	ModifyGraph nticks=0
	ModifyGraph standoff(bottomleft)=0
	ModifyGraph axThick=0
	ModifyGraph axRGB(left)=(65535,65535,65535),axRGB(bottom)=(65535,65535,65535)
	ModifyGraph lblPos(left)=27,lblPos(bottom)=27
	ModifyGraph freePos(bottomleft)={0,kwFraction}
	ModifyGraph freePos(topleft)={0.25,kwFraction}
	ModifyGraph axisEnab(left)={0,0.75}
	ModifyGraph axisEnab(bottomleft)={0,0.25}
	ModifyGraph axisEnab(topleft)={0.75,1}
	ModifyGraph axisEnab(bottom)={0.25,1}
	SetAxis left -1.5,1.5
	SetAxis bottomleft 1,0
	SetAxis topleft 0,1
	SetAxis bottom -1.1,1.1
	ColorScale/C/N=text0/F=0/B=1/A=MC/X=-34.53/Y=39.62 image=cntimage, vert=0, side=2
	ColorScale/C/N=text0 widthPct=30, nticks=2, fsize=12
	ColorScale/C/N=text0 userTicks={colorTicks,colorTicksT}, axisRange={0,1,0}
	AppendText "PL intensity [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.646122110135595,1.19324905888058,0.846122110135594,1.19324905888058
	SetDrawEnv xcoord= bottom,ycoord= left,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.551558215666837,1.24759822736988,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= left,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6-o!@;om83]d"
EndMacro

Window comparison() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:plcam:
	Display /W=(539.25,41.75,986.25,245)/L=left2 profile0 vs profile0_coord as "comparison"
	AppendToGraph/L=left3/B=bsimulation ::simulation_profile
	AppendImage fullimage_zoom
	ModifyImage fullimage_zoom ctab= {6500,30410.1477272727,YellowHot,0}
	ModifyImage fullimage_zoom log= 1
	AppendImage/B=bsimulation/L=lsimulation ::cntimage01
	ModifyImage cntimage01 ctab= {0.001,*,YellowHot,0}
	ModifyImage cntimage01 log= 1
	SetDataFolder fldrSav0
	ModifyGraph margin(bottom)=14,margin(top)=14,width={Plan,1,bottom,left}
	ModifyGraph mode=7
	ModifyGraph rgb(simulation_profile)=(1,16019,65535)
	ModifyGraph hbFill(profile0)=2,hbFill(simulation_profile)=5
	ModifyGraph usePlusRGB(profile0)=1
	ModifyGraph hBarNegFill(profile0)=2
	ModifyGraph plusRGB(profile0)=(65535,0,0,6554)
	ModifyGraph grid(bsimulation)=1
	ModifyGraph mirror(bottom)=0,mirror(left)=0
	ModifyGraph nticks(left2)=0,nticks(bottom)=0,nticks(bsimulation)=0,nticks(left)=0
	ModifyGraph nticks(lsimulation)=0
	ModifyGraph noLabel(left2)=2,noLabel(bottom)=2,noLabel(left)=2
	ModifyGraph fSize(left2)=12,fSize(bottom)=12,fSize(left)=12
	ModifyGraph standoff(left3)=0,standoff(bsimulation)=0,standoff(left)=0,standoff(lsimulation)=0
	ModifyGraph axThick(left2)=0,axThick(bottom)=0,axThick(bsimulation)=0,axThick(left)=0
	ModifyGraph axThick(lsimulation)=0
	ModifyGraph gridRGB(bsimulation)=(65535,65535,65535)
	ModifyGraph lblPosMode(left2)=1,lblPosMode(bottom)=1,lblPosMode(left)=1
	ModifyGraph lblPos(bottom)=39,lblPos(left3)=55,lblPos(left)=50
	ModifyGraph lblLatPos(left3)=-3
	ModifyGraph freePos(left2)={0,bottom}
	ModifyGraph freePos(left3)=7
	ModifyGraph freePos(bsimulation)={0,left2}
	ModifyGraph freePos(lsimulation)={0,kwFraction}
	ModifyGraph axisEnab(left2)={0.52,1}
	ModifyGraph axisEnab(bottom)={0,0.49}
	ModifyGraph axisEnab(left3)={0.52,1}
	ModifyGraph axisEnab(bsimulation)={0.51,1}
	ModifyGraph axisEnab(left)={0,0.5}
	ModifyGraph axisEnab(lsimulation)={0,0.5}
	Label left2 "PL intensity [a.u.]"
	Label bottom "position / µm"
	Label left3 "intensity [a.u.]"
	Label left "position / µm"
	SetAxis left2 0,*
	SetAxis bottom 110.3,120.3
	SetAxis left3 0,*
	SetAxis bsimulation -5.07,4.93
	ColorScale/C/N=text0/F=0/B=1/A=RB/X=-43.17/Y=22.87 image=fullimage_zoom
	ColorScale/C/N=text0 heightPct=50, fsize=12
	ColorScale/C/N=text0 userTicks={:plcam:colorticks,:plcam:colorticksT}
	ColorScale/C/N=text0 lblMargin=-10, tickUnit=1, axisRange={0,NaN,0}
	AppendText "PL intensity [a.u.]"
	Legend/C/N=text1/J/S=1/A=MT/X=0.00/Y=0.00 "\\s(profile0) experiment\r\\s(simulation_profile) simulation"
	ColorScale/C/N=text2/F=0/S=1/A=LB/X=-12.00/Y=0.00 image=cntimage01, side=2
	ColorScale/C/N=text2 heightPct=50
	AppendText "intensity (log) [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 3,linefgc= (65535,65535,65535)
	DrawLine 117.81643316636,0.625,119.81643316636,0.625
	SetDrawEnv fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.375709692237024,0.6,"2µm"
	SetDrawEnv xcoord= bsimulation,ycoord= prel,linethick= 3,linefgc= (65535,65535,65535)
	DrawLine 2.70882383691376,0.625,4.70882383691373,0.625
	SetDrawEnv fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.896976817494692,0.6,"2µm"
	SetWindow kwTopWin,userdata(WMZoomBrowser)=  "SMAgetCoordinatesfullImage"
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6/IPF)Y].A7]:YE,9#mD-pdkDfTB&DIIWuF'gX_3r"
	SetWindow kwTopWin,userdata(fullimage_zoom)=  "image_sliderLimits={0,37065};"
EndMacro
