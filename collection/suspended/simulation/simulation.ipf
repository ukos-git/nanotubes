#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include "sma"
#include "plem"

Function export()
	NVAR numTraces, numSteps, seedType, landscape

	landscape = 0.024 //24 nm
	numTraces = 2048
	numSteps = 1e4

	saveWindow("exciton1", saveImages = 1, saveVector = 0, saveJSON = 0)
	saveWindow("exciton16", saveImages = 1, saveVector = 0, saveJSON = 0)
	SaveWindow("combined", saveJSON = 0, saveVector = 0, saveImages = 1)
	SaveWindow("comparison", saveJSON = 0, saveVector = 0, saveImages = 1)
End

Function CreateMovie()
	Variable i
	Variable numTraces = 128
	Variable numSteps = 1

	AddSteps(0, 0, SEED_LASER, GEORGI_DIFFUSION_LENGTH)
	DoWindow/F firstpassage
	NewMovie/CF=0/Z/F=30/O/P=home as "simulation_laser.mp4"
	For(i = 0; i < numTraces; i += 1)
		AddSteps(i + 1, numSteps, SEED_LASER, GEORGI_DIFFUSION_LENGTH)
		DoUpdate/W=firstpassage
		AddMovieFrame
	EndFor
	CloseMovie

	AddSteps(0, 0, SEED_LASER, GEORGI_DIFFUSION_LENGTH)
	DoWindow/F firstpassage
	NewMovie/CF=0/Z/F=30/O/P=home as "simulation_led.mp4"
	For(i = 0; i < numTraces; i += 1)
		AddSteps(i + 1, numSteps, SEED_LED, GEORGI_DIFFUSION_LENGTH)
		DoUpdate/W=firstpassage
		AddMovieFrame
	EndFor
	CloseMovie
End
Window exciton1() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:laser_1exciton:
	Display /W=(959.25,43.25,1238.25,388.25)/L=hist histResult/TN=histogram as "exciton1"
	AppendToGraph/L=excitation excitation_intensity
	AppendToGraph/R=histR fit_histResultC
	AppendToGraph/L=hist histResultDead
	AppendToGraph step/TN=diffusion0 vs diffusion0
	AppendImage/L=airyimage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	SetDataFolder fldrSav0
	ModifyGraph height={Plan,1,airyimage,bottom}
	ModifyGraph mode(histogram)=5,mode(excitation_intensity)=7,mode(histResultDead)=5
	ModifyGraph rgb(excitation_intensity)=(1,16019,65535,49151),rgb(fit_histResultC)=(65535,0,0,6554)
	ModifyGraph rgb(histResultDead)=(30583,30583,30583),rgb(diffusion0)=(65535,0,0,13107)
	ModifyGraph hbFill(histogram)=2,hbFill(excitation_intensity)=5,hbFill(histResultDead)=2
	ModifyGraph plusRGB(excitation_intensity)=(65535,49151,49151)
	ModifyGraph useBarStrokeRGB(histogram)=1,useBarStrokeRGB(histResultDead)=1
	ModifyGraph tick(excitation)=1
	ModifyGraph mirror(bottom)=0,mirror(excitation)=1,mirror(left)=0
	ModifyGraph nticks(hist)=0,nticks(excitation)=2,nticks(histR)=0,nticks(airyimage)=0
	ModifyGraph noLabel(excitation)=1
	ModifyGraph standoff(hist)=0,standoff(bottom)=0,standoff(excitation)=0,standoff(histR)=0
	ModifyGraph standoff(airyimage)=0
	ModifyGraph axThick(hist)=0,axThick(histR)=0,axThick(airyimage)=0
	ModifyGraph lblPosMode(hist)=1,lblPosMode(excitation)=1
	ModifyGraph lblPos(left)=57
	ModifyGraph freePos(hist)={0,kwFraction}
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph freePos(histR)={0,kwFraction}
	ModifyGraph freePos(airyimage)={0,kwFraction}
	ModifyGraph axisEnab(hist)={0.5,0.6}
	ModifyGraph axisEnab(excitation)={0,0.15}
	ModifyGraph axisEnab(histR)={0.5,0.7}
	ModifyGraph axisEnab(left)={0.17,0.49}
	ModifyGraph axisEnab(airyimage)={0.61,1}
	ModifyGraph manTick(bottom)={0,1,0,0},manMinor(bottom)={1,50}
	Label hist "emission"
	Label bottom "SWNT position [nm]"
	Label excitation "excitation"
	Label left "diffusion steps"
	SetAxis bottom -1.25,1.25
	SetAxis excitation 0,1
	SetAxis histR 0,1
	SetAxis airyimage 0,1.5
	TextBox/C/N=text0/F=0/A=MC/X=36.40/Y=-36.69 "\\s(excitation_intensity) excitation"
	TextBox/C/N=emission/F=0/B=(65535,65535,65535,32768)/A=MC/X=28.32/Y=7.77 "\\s(histogram) emission"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=0.51/Y=-45.14 "SWNT"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= excitation,linejoin= 1,fillpat= 16,fillfgc= (30583,30583,30583),fillbgc= (65535,65535,65535,49151)
	DrawRect -1,0.2,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.617074289839597,1.2713754350385,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.745128818081932,1.24267313377399,0.945128818081931,1.24267313377399
EndMacro

Window exciton16() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(537,43.25,981.75,388.25) as "exciton16"
	ModifyGraph margin(left)=65,margin(right)=10
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:laser_16exciton:
	Display/W=(0,0,0.549,1)/HOST=# /L=hist histResult/TN=histogram
	AppendToGraph/L=excitation excitation_intensity
	AppendToGraph/R=histR fit_histResultC
	AppendToGraph/L=hist histResultDead
	AppendToGraph step/TN=diffusion0 vs diffusion0
	AppendToGraph step/TN=diffusion1 vs diffusion1
	AppendToGraph step/TN=diffusion2 vs diffusion2
	AppendToGraph step/TN=diffusion3 vs diffusion3
	AppendToGraph step/TN=diffusion4 vs diffusion4
	AppendToGraph step/TN=diffusion5 vs diffusion5
	AppendToGraph step/TN=diffusion6 vs diffusion6
	AppendToGraph step/TN=diffusion7 vs diffusion7
	AppendToGraph step/TN=diffusion8 vs diffusion8
	AppendToGraph step/TN=diffusion9 vs diffusion9
	AppendToGraph step/TN=diffusion10 vs diffusion10
	AppendToGraph step/TN=diffusion11 vs diffusion11
	AppendToGraph step/TN=diffusion12 vs diffusion12
	AppendToGraph step/TN=diffusion13 vs diffusion13
	AppendToGraph step/TN=diffusion14 vs diffusion14
	AppendToGraph step/TN=diffusion15 vs diffusion15
	AppendImage/L=airyimage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=42,margin(bottom)=42,margin(top)=14,margin(right)=14,height={Plan,1,airyimage,bottom}
	ModifyGraph mode(histogram)=5,mode(excitation_intensity)=7,mode(histResultDead)=5
	ModifyGraph rgb(excitation_intensity)=(1,16019,65535,49151),rgb(fit_histResultC)=(65535,0,0,6554)
	ModifyGraph rgb(histResultDead)=(30583,30583,30583),rgb(diffusion0)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion1)=(65535,0,0,13107),rgb(diffusion2)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion3)=(65535,0,0,13107),rgb(diffusion4)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion5)=(65535,0,0,13107),rgb(diffusion6)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion7)=(65535,0,0,13107),rgb(diffusion8)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion9)=(65535,0,0,13107),rgb(diffusion10)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion11)=(65535,0,0,13107),rgb(diffusion12)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion13)=(0,0,0,13107),rgb(diffusion14)=(65535,0,0,13107),rgb(diffusion15)=(65535,0,0,13107)
	ModifyGraph hbFill(histogram)=2,hbFill(excitation_intensity)=5,hbFill(histResultDead)=2
	ModifyGraph hBarNegFill(excitation_intensity)=5
	ModifyGraph plusRGB(excitation_intensity)=(65535,49151,49151)
	ModifyGraph useBarStrokeRGB(histogram)=1,useBarStrokeRGB(histResultDead)=1
	ModifyGraph tick(excitation)=1
	ModifyGraph mirror(bottom)=0,mirror(left)=0
	ModifyGraph nticks(hist)=0,nticks(excitation)=2,nticks(histR)=0,nticks(airyimage)=0
	ModifyGraph noLabel(excitation)=1
	ModifyGraph standoff(hist)=0,standoff(bottom)=0,standoff(excitation)=0,standoff(histR)=0
	ModifyGraph standoff(airyimage)=0
	ModifyGraph axThick(hist)=0,axThick(histR)=0,axThick(airyimage)=0
	ModifyGraph lblPosMode(hist)=1,lblPosMode(excitation)=1
	ModifyGraph lblPos(left)=57
	ModifyGraph freePos(hist)={0,kwFraction}
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph freePos(histR)={0,kwFraction}
	ModifyGraph freePos(airyimage)={0,kwFraction}
	ModifyGraph axisEnab(hist)={0.5,0.6}
	ModifyGraph axisEnab(excitation)={0,0.15}
	ModifyGraph axisEnab(histR)={0.5,0.7}
	ModifyGraph axisEnab(left)={0.17,0.49}
	ModifyGraph axisEnab(airyimage)={0.61,1}
	ModifyGraph manTick(bottom)={0,1,0,0},manMinor(bottom)={1,50}
	Label hist "emission"
	Label bottom "SWNT position [nm]"
	Label excitation "excitation"
	Label left "diffusion steps"
	SetAxis bottom -1.25,1.25
	SetAxis excitation 0,1
	SetAxis histR 0,9
	SetAxis airyimage 0,1.5
	TextBox/C/N=text0/F=0/A=MC/X=-31.84/Y=-35.71 "\\s(excitation_intensity) excitation"
	TextBox/C/N=emission/F=0/B=(65535,65535,65535,32768)/A=MC/X=-36.13/Y=6.23 "\\s(histogram) emission\r\\s(histResultDead) quenched"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=0.51/Y=-45.14 "SWNT"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= excitation,linejoin= 1,fillpat= 16,fillfgc= (30583,30583,30583),fillbgc= (65535,65535,65535,49151)
	DrawRect -1,0.2,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.568820613369009,1.2226751731748,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.696492175925069,1.19397287191029,0.896492175925068,1.19397287191029
	RenameWindow #,laser_16exciton
	SetActiveSubwindow ##
	String fldrSav1= GetDataFolder(1)
	SetDataFolder root:led_16exciton:
	Display/W=(0.427,0,1,1)/HOST=# /L=hist histResult/TN=histogram
	AppendToGraph/L=excitation excitation_intensity
	AppendToGraph/R=histR fit_histResultC
	AppendToGraph/L=hist histResultDead
	AppendToGraph step/TN=diffusion0 vs diffusion0
	AppendToGraph step/TN=diffusion1 vs diffusion1
	AppendToGraph step/TN=diffusion2 vs diffusion2
	AppendToGraph step/TN=diffusion3 vs diffusion3
	AppendToGraph step/TN=diffusion4 vs diffusion4
	AppendToGraph step/TN=diffusion5 vs diffusion5
	AppendToGraph step/TN=diffusion6 vs diffusion6
	AppendToGraph step/TN=diffusion7 vs diffusion7
	AppendToGraph step/TN=diffusion8 vs diffusion8
	AppendToGraph step/TN=diffusion9 vs diffusion9
	AppendToGraph step/TN=diffusion10 vs diffusion10
	AppendToGraph step/TN=diffusion11 vs diffusion11
	AppendToGraph step/TN=diffusion12 vs diffusion12
	AppendToGraph step/TN=diffusion13 vs diffusion13
	AppendToGraph step/TN=diffusion14 vs diffusion14
	AppendToGraph step/TN=diffusion15 vs diffusion15
	AppendImage/L=airyimage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	SetDataFolder fldrSav1
	ModifyGraph margin(left)=42,margin(bottom)=42,margin(top)=14,margin(right)=14
	ModifyGraph mode(histogram)=5,mode(excitation_intensity)=7,mode(histResultDead)=5
	ModifyGraph rgb(excitation_intensity)=(1,16019,65535,49151),rgb(fit_histResultC)=(65535,0,0,6554)
	ModifyGraph rgb(histResultDead)=(30583,30583,30583),rgb(diffusion0)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion1)=(65535,0,0,13107),rgb(diffusion2)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion3)=(65535,0,0,13107),rgb(diffusion4)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion5)=(65535,0,0,13107),rgb(diffusion6)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion7)=(65535,0,0,13107),rgb(diffusion8)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion9)=(65535,0,0,13107),rgb(diffusion10)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion11)=(0,0,0,13107),rgb(diffusion12)=(65535,0,0,13107),rgb(diffusion13)=(65535,0,0,13107)
	ModifyGraph rgb(diffusion14)=(0,0,0,13107),rgb(diffusion15)=(0,0,0,13107)
	ModifyGraph hbFill(histogram)=2,hbFill(excitation_intensity)=5,hbFill(histResultDead)=2
	ModifyGraph hBarNegFill(excitation_intensity)=5
	ModifyGraph plusRGB(excitation_intensity)=(65535,49151,49151)
	ModifyGraph useBarStrokeRGB(histogram)=1,useBarStrokeRGB(histResultDead)=1
	ModifyGraph tick(excitation)=1
	ModifyGraph mirror(bottom)=0,mirror(excitation)=1,mirror(left)=0
	ModifyGraph nticks(hist)=0,nticks(excitation)=2,nticks(histR)=0,nticks(airyimage)=0
	ModifyGraph noLabel(hist)=2,noLabel(excitation)=2,noLabel(left)=2
	ModifyGraph standoff(hist)=0,standoff(bottom)=0,standoff(excitation)=0,standoff(histR)=0
	ModifyGraph standoff(airyimage)=0
	ModifyGraph axOffset(bottom)=-0.0555556
	ModifyGraph axThick(hist)=0,axThick(excitation)=0,axThick(histR)=0,axThick(left)=0
	ModifyGraph axThick(airyimage)=0
	ModifyGraph lblPosMode(hist)=1,lblPosMode(excitation)=1
	ModifyGraph lblPos(left)=57
	ModifyGraph freePos(hist)={0,kwFraction}
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph freePos(histR)={0,kwFraction}
	ModifyGraph freePos(airyimage)={0,kwFraction}
	ModifyGraph axisEnab(hist)={0.5,0.6}
	ModifyGraph axisEnab(excitation)={0,0.15}
	ModifyGraph axisEnab(histR)={0.5,0.7}
	ModifyGraph axisEnab(left)={0.17,0.49}
	ModifyGraph axisEnab(airyimage)={0.61,1}
	ModifyGraph manTick(bottom)={0,1,0,0},manMinor(bottom)={1,50}
	Label hist "emission"
	Label bottom "SWNT position [nm]"
	Label excitation "excitation"
	Label left "diffusion steps"
	SetAxis bottom -1.25,1.25
	SetAxis excitation 0,1
	SetAxis histR 0,4
	SetAxis airyimage 0,1.5
	TextBox/C/N=text1/F=0/B=1/A=MC/X=0.51/Y=-45.14 "SWNT"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= excitation,linejoin= 1,fillpat= 16,fillfgc= (30583,30583,30583),fillbgc= (65535,65535,65535,49151)
	DrawRect -1,0.2,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.569580584186129,1.22299244547904,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.696872161333629,1.19429014421453,0.896872161333629,1.19429014421453
	RenameWindow #,led_16exciton
	SetActiveSubwindow ##
EndMacro

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(537,266.75,981.75,493.25) as "combined"
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:led_diff100nm:
	Display/W=(0,0,0.499,0.963)/HOST=# /L=hist histResult/TN=histogram
	AppendToGraph/L=excitation excitation_intensity
	AppendToGraph/R=histR fit_histResultC
	AppendToGraph/L=hist histResultDead
	AppendImage/L=airyimage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=14,margin(right)=14,height={Plan,1,airyimage,bottom}
	ModifyGraph mode(histogram)=5,mode(excitation_intensity)=7,mode(histResultDead)=5
	ModifyGraph rgb(excitation_intensity)=(1,16019,65535,49151),rgb(fit_histResultC)=(65535,0,0,6554)
	ModifyGraph rgb(histResultDead)=(30583,30583,30583)
	ModifyGraph hbFill(histogram)=2,hbFill(excitation_intensity)=5,hbFill(histResultDead)=2
	ModifyGraph hBarNegFill(excitation_intensity)=5
	ModifyGraph plusRGB(excitation_intensity)=(65535,49151,49151)
	ModifyGraph useBarStrokeRGB(histogram)=1,useBarStrokeRGB(histResultDead)=1
	ModifyGraph tick(excitation)=1
	ModifyGraph mirror(bottom)=0,mirror(excitation)=1
	ModifyGraph nticks(hist)=0,nticks(excitation)=2,nticks(histR)=0,nticks(airyimage)=0
	ModifyGraph noLabel(hist)=2,noLabel(excitation)=2
	ModifyGraph standoff=0
	ModifyGraph axThick(hist)=0,axThick(excitation)=0,axThick(histR)=0,axThick(airyimage)=0
	ModifyGraph lblPosMode(hist)=1,lblPosMode(excitation)=1
	ModifyGraph freePos(hist)={0,kwFraction}
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph freePos(histR)={0,kwFraction}
	ModifyGraph freePos(airyimage)={0,kwFraction}
	ModifyGraph axisEnab(hist)={0.17,0.45}
	ModifyGraph axisEnab(excitation)={0,0.15}
	ModifyGraph axisEnab(histR)={0.17,0.45}
	ModifyGraph axisEnab(airyimage)={0.5,1}
	ModifyGraph manTick(bottom)={0,1,0,0},manMinor(bottom)={1,50}
	Label hist "emission"
	Label bottom "SWNT position [nm]"
	Label excitation "excitation"
	SetAxis bottom -1.25,1.25
	SetAxis excitation 0,1
	SetAxis histR 0,5476
	SetAxis airyimage 0,1.1
	TextBox/C/N=text0/F=0/A=LT/X=65.00/Y=85.00 "\\s(excitation_intensity) excitation"
	TextBox/C/N=emission/F=0/B=(65535,65535,65535,32768)/A=LT/X=65.00/Y=50.00 "\\s(histogram) emission\r\\s(histResultDead) quenched"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=0.51/Y=-45.14 "SWNT"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= excitation,linejoin= 1,fillpat= 16,fillfgc= (30583,30583,30583),fillbgc= (65535,65535,65535,49151)
	DrawRect -1,0.2,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.764133113369009,1.32039504288164,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.794148425925069,1.29169274161713,0.994148425925068,1.29169274161713
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.695361022450166,0.85476582345214,0.895361022450165,0.85476582345214
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.568820613369009,0.88346812471665,"200nm"
	RenameWindow #,led_diff100nm
	SetActiveSubwindow ##
	String fldrSav1= GetDataFolder(1)
	SetDataFolder root:led_diff350nm:
	Display/W=(0.501,0,1,0.965)/HOST=# /L=hist histResult/TN=histogram
	AppendToGraph/L=excitation excitation_intensity
	AppendToGraph/R=histR fit_histResultC
	AppendToGraph/L=hist histResultDead
	AppendImage/L=airyimage cntimage
	ModifyImage cntimage ctab= {0.01,*,YellowHot,0}
	ModifyImage cntimage log= 1
	SetDataFolder fldrSav1
	ModifyGraph margin(left)=14,margin(right)=14,height={Plan,1,airyimage,bottom}
	ModifyGraph mode(histogram)=5,mode(excitation_intensity)=7,mode(histResultDead)=5
	ModifyGraph rgb(excitation_intensity)=(1,16019,65535,49151),rgb(fit_histResultC)=(65535,0,0,6554)
	ModifyGraph rgb(histResultDead)=(30583,30583,30583)
	ModifyGraph hbFill(histogram)=2,hbFill(excitation_intensity)=5,hbFill(histResultDead)=2
	ModifyGraph plusRGB(excitation_intensity)=(65535,49151,49151)
	ModifyGraph useBarStrokeRGB(histogram)=1,useBarStrokeRGB(histResultDead)=1
	ModifyGraph tick(excitation)=1
	ModifyGraph mirror(bottom)=0,mirror(excitation)=1
	ModifyGraph nticks(hist)=0,nticks(excitation)=2,nticks(histR)=0,nticks(airyimage)=0
	ModifyGraph noLabel(hist)=2,noLabel(excitation)=2
	ModifyGraph standoff=0
	ModifyGraph axThick(hist)=0,axThick(excitation)=0,axThick(histR)=0,axThick(airyimage)=0
	ModifyGraph lblPosMode(hist)=1,lblPosMode(excitation)=1
	ModifyGraph freePos(hist)={0,kwFraction}
	ModifyGraph freePos(excitation)={0,kwFraction}
	ModifyGraph freePos(histR)={0,kwFraction}
	ModifyGraph freePos(airyimage)={0,kwFraction}
	ModifyGraph axisEnab(hist)={0.17,0.45}
	ModifyGraph axisEnab(excitation)={0,0.15}
	ModifyGraph axisEnab(histR)={0.17,0.45}
	ModifyGraph axisEnab(airyimage)={0.5,1}
	ModifyGraph manTick(bottom)={0,1,0,0},manMinor(bottom)={1,50}
	Label hist "emission"
	Label bottom "SWNT position [nm]"
	Label excitation "excitation"
	SetAxis hist 0,288
	SetAxis bottom -1.25,1.25
	SetAxis excitation 0,1
	SetAxis histR 0,288
	SetAxis airyimage 0,1.1
	TextBox/C/N=text0/F=0/A=LT/X=65.00/Y=85.00 "\\s(excitation_intensity) excitation"
	TextBox/C/N=emission/F=0/B=(65535,65535,65535,32768)/A=LT/X=65.00/Y=50.00 "\\s(histogram) emission\r\\s(histResultDead) quenched"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=0.51/Y=-45.14 "SWNT"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= excitation,linejoin= 1,fillpat= 16,fillfgc= (30583,30583,30583),fillbgc= (65535,65535,65535,49151)
	DrawRect -1,0.2,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linefgc= (65535,65535,65535)
	DrawLine -1,0,1,0
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.764133113369009,1.32039504288164,"200nm"
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.794148425925069,1.29169274161713,0.994148425925068,1.29169274161713
	SetDrawEnv xcoord= bottom,ycoord= airyimage,linethick= 4,linefgc= (65535,65535,65535)
	DrawLine 0.695175039605223,0.856253573664676,0.895175039605222,0.856253573664676
	SetDrawEnv xcoord= bottom,ycoord= airyimage,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.568820613369009,0.884955874929186,"200nm"
	RenameWindow #,led_diff350nm
	SetActiveSubwindow ##
EndMacro

