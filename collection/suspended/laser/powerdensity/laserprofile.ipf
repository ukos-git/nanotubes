#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("esquare")
	saveWindow("combined")
	saveWindow("measure")
End

Function testing()
	String Yrange = "[*]"
	string expr="\[([[:digit:]\*]+)(?:,\s*([[:digit:]\*]+))?\](.*)"
	string pRangeStart, pRangeStop
	SplitString/E=(expr) Yrange, pRangeStart, pRangeStop, Yrange
	print pRangeStart, pRangeStop, Yrange
End
Window measure() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(85.5,92,336,342.5) as "measure"
	AppendImage laserprofile
	ModifyImage laserprofile ctab= {0,*,colortable_plotly_heatmap,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=14,width={Aspect,1}
	ModifyGraph tick=3
	ModifyGraph zero=1
	ModifyGraph mirror=1
	ModifyGraph noLabel=2
	ModifyGraph zeroThick=2
	SetDrawLayer UserFront
	DrawLine 0,1,1,1
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 5,linefgc= (65535,65535,65535)
	DrawLine 0.726609048989798,1.48942950262819,1.7266090489898,1.48942950262819
	DrawLine 0,1,1,1
	SetDrawEnv textrgb= (65535,65535,65535)
	DrawText 0.726470588235294,0.0852941176470588,"1µm"
EndMacro

Window esquare() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(641.25,92,891,341.75) fit_laserprofile[24][*],laserprofile[28][*] as "esquare"
	ModifyGraph margin(left)=28,margin(bottom)=42,margin(top)=14,margin(right)=28,width={Aspect,1}
	ModifyGraph mode(laserprofile)=3
	ModifyGraph marker(laserprofile)=19
	ModifyGraph lSize(laserprofile)=2
	ModifyGraph rgb(fit_laserprofile)=(0,0,0)
	ModifyGraph zColor(laserprofile)={laserprofile_lineprofile,*,*,ctableRGB,0,colortable_plotly_heatmap}
	ModifyGraph mirror(left)=1
	ModifyGraph nticks(left)=0
	ModifyGraph noLabel(left)=1
	ModifyGraph fSize=12
	Label left "laser intensity [a.u.]"
	Label bottom "sample position [µm]"
	SetAxis bottom -1.9,1.9
	SetDrawLayer UserFront
	DrawText 0.304231104565327,0.781990521327014,"1/e² ≈ 2.00µm"
EndMacro

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(35.25,41.75,492.75,251.75)/R=intensity/B=intensity_position fit_laserprofile[24][*] as "combined"
	AppendToGraph/R=intensity/B=intensity_position laserprofile[28][*]
	AppendImage laserprofile
	ModifyImage laserprofile ctab= {0,*,colortable_plotly_heatmap,0}
	ModifyGraph margin(left)=72,margin(top)=14,margin(right)=72,width={Plan,1,bottom,left}
	ModifyGraph mode(laserprofile)=3
	ModifyGraph marker(laserprofile)=19
	ModifyGraph rgb(fit_laserprofile)=(0,0,0)
	ModifyGraph zColor(laserprofile)={laserprofile_lineprofile,*,*,ctableRGB,0,colortable_plotly_heatmap}
	ModifyGraph tick(left)=3,tick(bottom)=3
	ModifyGraph mirror(left)=0,mirror(bottom)=0
	ModifyGraph nticks(intensity)=0
	ModifyGraph noLabel(intensity)=2,noLabel(left)=2,noLabel(bottom)=2
	ModifyGraph standoff(intensity)=0,standoff(left)=0,standoff(bottom)=0
	ModifyGraph axThick(intensity)=0,axThick(left)=0
	ModifyGraph zeroThick(left)=2,zeroThick(bottom)=2
	ModifyGraph lblPosMode(intensity)=1,lblPosMode(intensity_position)=1
	ModifyGraph lblPos(bottom)=12
	ModifyGraph freePos(intensity)={0,kwFraction}
	ModifyGraph freePos(intensity_position)={0,kwFraction}
	ModifyGraph axisEnab(intensity_position)={0.5,1}
	ModifyGraph axisEnab(bottom)={0,0.5}
	Label intensity "laser intensity [a.u.]"
	Label intensity_position "sample position [µm]"
	ColorScale/C/N=text0/F=0/B=1/A=MC/X=53.07/Y=-0.77 image=laserprofile
	ColorScale/C/N=text0 heightPct=110, nticks=0, lblMargin=20
	AppendText "intensity [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2
	DrawLine 0,-1.5,0,1.5
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 5,linefgc= (65535,65535,65535)
	DrawLine 0.726609048989798,1.17865107320269,1.7266090489898,1.17865107320269
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2
	DrawLine -1.5,0,1.5,0
	SetDrawEnv xcoord= bottom,ycoord= prel,fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.923095031230496,0.168944307761127,"1µm"
	DrawText 0.662468652458047,0.781990521327014,"1/e² ≈ 2.00µm"
EndMacro

