#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("powerrange_hor", saveVector = 1)
End

Window powerrange_hor() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(18,86,477,315.5) source_maxHeight vs :PLEMd2:power as "powerrange_hor"
	AppendToGraph fit_source_maxHeight
	AppendToGraph/R=wavelength/B=power_mirror source_maxPosition vs :PLEMd2:power
	ModifyGraph width={Aspect,2}
	ModifyGraph mode(source_maxHeight)=3,mode(source_maxPosition)=3
	ModifyGraph marker(source_maxHeight)=8,marker(source_maxPosition)=8
	ModifyGraph rgb(source_maxPosition)=(0,0,65535)
	ModifyGraph lblPosMode(wavelength)=1
	ModifyGraph lblMargin(wavelength)=1
	ModifyGraph lblPos(left)=52,lblPos(bottom)=36
	ModifyGraph lblLatPos(bottom)=90
	ModifyGraph freePos(wavelength)={0,kwFraction}
	ModifyGraph freePos(power_mirror)={0,kwFraction}
	ModifyGraph axisEnab(bottom)={0,0.49}
	ModifyGraph axisEnab(power_mirror)={0.51,1}
	ModifyGraph manTick(bottom)={0,25,0,0},manMinor(bottom)={4,0}
	ModifyGraph manTick(wavelength)={0,1,0,0},manMinor(wavelength)={1,50}
	ModifyGraph manTick(power_mirror)={0,25,0,0},manMinor(power_mirror)={4,0}
	Label left "max. intensity [cts]"
	Label bottom "power at sample [µW]"
	Label wavelength "wavelength [nm]"
	SetAxis left 0,600
	SetAxis bottom 0,60
	SetAxis power_mirror 0,60
	ErrorBars source_maxHeight XY,pct=5,wave=(source_maxHeight_err,source_maxHeight_err)
	ErrorBars source_maxPosition XY,pct=5,wave=(source_maxPosition_err,source_maxPosition_err)
	Legend/C/N=text0/J/F=0/B=(61166,61166,61166)/X=2.81/Y=-1.12 "\\s(source_maxHeight)\\s(source_maxPosition)  excitonic emission"
	AppendText "    \\s(fit_source_maxHeight)      exponential fit"
	SetDrawLayer UserBack
	SetDrawEnv xcoord= bottom,ycoord= left,fillfgc= (65535,57825,57825)
	DrawRRect 1.26086956521739,334.322033898305,24.2608695652174,13.9830508474576
	DrawText 0.227733872911543,0.765353888926107,"near-linear pl"
	DrawText 0.68749817369128,0.588101639211468,"desorbed state"
	SetDrawEnv xcoord= power_mirror,ycoord= wavelength,fillfgc= (58853,56540,65535)
	DrawRRect 12.4137931034483,975.248242167819,59.3103448275862,974.462609252484
	SetDrawLayer UserFront
EndMacro

