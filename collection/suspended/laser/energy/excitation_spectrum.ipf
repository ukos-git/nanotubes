#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <InsertSubwindowInGraph>

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("atsample", saveImages = 0, saveJSON = 1, saveVector = 1)
	SaveWindow("atsample_subwindow", customName = "atsample", saveImages = 1, saveJSON = 0, saveVector = 1)
	SaveWindow("powerrelation", saveVector = 1)
End


//Function powerExtraction(q_power)
//	Variable q_power // input q value in map that reflects the excitation power
//
//	Variable i, numMaps
//	String strMap, strMaps
//	String tracename
//
//	Struct PLEMd2stats stats
//
//	strMaps = GrepList(PLEMd2getStrMapsAvailable(), "powerAtsample.*")
//	WAVE/T wv = ListToTextWave(strMaps, ";")
//	numMaps = DimSize(wv, 0)
//
//	Make/FREE/N=(numMaps) lightsourceSample
//
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		lightsourceSample[i] = stats.numEmissionPower
//	endfor
//	Sort lightsourceSample, lightsourceSample, wv // emission percentage
//
//	Make/FREE/N=(numMaps) powerAtSample
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		powerAtSample[i] = stats.wavYpower[q_power]
//	endfor
//
//	strMaps = GrepList(PLEMd2getStrMapsAvailable(), "spectrumAtsample.*")
//	WAVE/T wv = ListToTextWave(strMaps, ";")
//	numMaps = DimSize(wv, 0)
//	
//	Make/FREE/N=(numMaps) lightsourceGlass
//
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		lightsourceGlass[i] = stats.numEmissionPower
//	endfor
//	Sort lightsourceGlass, lightsourceGlass, wv // assume same percentages (?)
//
//	Make/FREE/N=(numMaps) powerAtGlassPlate
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		powerAtGlassPlate[i] = stats.wavYpower[q_power]
//	endfor
//
//	// save waves
//	// CHECKEQUALWAVES(lightsourceSample, lightsourceGlass)
//	Duplicate/O powerAtGlassPlate root:powerAtGlassPlate
//	Duplicate/O powerAtSample root:powerAtSample
//End
//
//Function powerAnalysis()
//	Variable i, numMaps
//	String strMap, strMaps
//	String tracename
//
//	Struct PLEMd2stats stats
//
//	strMaps = GrepList(PLEMd2getStrMapsAvailable(), "powerAtsample.*")
//
//	WAVE/T wv = ListToTextWave(strMaps, ";")
//	numMaps = DimSize(wv, 0)
//
//	Make/O/N=(numMaps) root:powerPercent/WAVE=lightsource
//
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		lightsource[i] = stats.numEmissionPower
//	endfor
//
//	Sort lightsource, lightsource, wv
//
//	Make/O/N=(numMaps) root:powerAtSample/WAVE=power
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		power[i] = stats.wavYpower[0]
//	endfor
//	
//	Display power vs lightsource
//End
//
//Function SpectrumAnalysis()
//	Variable i, j, numMaps, forty
//	Variable central_excitation
//	String strMap, strMaps
//	String tracename
//
//	Struct PLEMd2stats stats
//
//	strMaps = GrepList(PLEMd2getStrMapsAvailable(), "spectrumAtsample.*")
//
//	WAVE/T wv = ListToTextWave(strMaps, ";")
//	numMaps = DimSize(wv, 0)
//
//	Make/FREE/N=(numMaps) lightsource
//
//	for(i = 0; i < numMaps; i += 1)
//		PLEMd2statsLoad(stats, wv[i])
//		lightsource[i] = stats.numEmissionPower
//	endfor
//
//	Sort/R lightsource, lightsource, wv
//
//	WAVE colortable = root:colors
//
//	DoWindow spectrumatsample
//	if(!!V_flag)
//		KillWindow spectrumatsample
//	endif
//	Display/N=spectrumatsample
//		for(i = 0; i < numMaps; i += 1)
//			PLEMd2statsLoad(stats, wv[i])
//			sprintf tracename, "lightsource center power %d%", lightsource[i]
//			AppendToGraph/W=spectrumatsample stats.wavPLEM/TN=$(tracename) vs stats.wavWavelength
//		endfor
//	WAVE chroma760em = root:chroma760em
//	WAVE chroma760em_wl = root:chroma760em_wl
//	AppendToGraph/W=spectrumatsample/R chroma760em/TN=chroma760 vs chroma760em_wl
//	ModifyGraph/W=spectrumatsample mode(chroma760)=7,rgb(chroma760)=(0,0,0)
//	ModifyGraph/W=spectrumatsample usePlusRGB(chroma760)=0,hbFill(chroma760)=5,plusRGB(chroma760)=(0,0,0,16384)
//	ModifyGraph/W=spectrumatsample hbFill(chroma760)=2
//	ModifyGraph/W=spectrumatsample rgb(chroma760)=(0,0,0)
//	ModifyGraph/W=spectrumatsample lStyle(chroma760)=7
//	ModifyGraph/W=spectrumatsample usePlusRGB(chroma760)=1,plusRGB(chroma760)=(65535,65535,65535)
//End

Window atsample() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(69,260,526.5,454.25) :PLEMd2:maps:spectrumAtsample_13:PLEM/TN='lightsource center power 100%' vs :PLEMd2:maps:spectrumAtsample_13:xWavelength as "atsample"
	AppendToGraph :PLEMd2:maps:spectrumAtsample_12:PLEM/TN='lightsource center power 90%' vs :PLEMd2:maps:spectrumAtsample_12:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_11:PLEM/TN='lightsource center power 80%' vs :PLEMd2:maps:spectrumAtsample_11:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_10:PLEM/TN='lightsource center power 70%' vs :PLEMd2:maps:spectrumAtsample_10:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_9:PLEM/TN='lightsource center power 60%' vs :PLEMd2:maps:spectrumAtsample_9:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_8:PLEM/TN='lightsource center power 50%' vs :PLEMd2:maps:spectrumAtsample_8:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_1:PLEM/TN='lightsource center power 35%' vs :PLEMd2:maps:spectrumAtsample_1:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_2:PLEM/TN='lightsource center power 30%' vs :PLEMd2:maps:spectrumAtsample_2:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_7:PLEM/TN='lightsource center power 25%' vs :PLEMd2:maps:spectrumAtsample_7:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_3:PLEM/TN='lightsource center power 20%' vs :PLEMd2:maps:spectrumAtsample_3:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_4:PLEM/TN='lightsource center power 15%' vs :PLEMd2:maps:spectrumAtsample_4:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_5:PLEM/TN='lightsource center power 10%' vs :PLEMd2:maps:spectrumAtsample_5:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_6:PLEM/TN='lightsource center power 6%' vs :PLEMd2:maps:spectrumAtsample_6:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_0:PLEM/TN='lightsource center power 40%' vs :PLEMd2:maps:spectrumAtsample_0:xWavelength
	AppendToGraph/R chroma760em/TN=chroma760 vs chroma760em_wl
	AppendToGraph/L=power/B=emissionout powerAtSample vs powerPercent
	AppendToGraph/L=power/B=emissionout fit_powerAtSample
	ModifyGraph margin(left)=28,margin(right)=28,expand=-1
	ModifyGraph mode('lightsource center power 100%')=7,mode('lightsource center power 90%')=7
	ModifyGraph mode('lightsource center power 80%')=7,mode('lightsource center power 70%')=7
	ModifyGraph mode('lightsource center power 60%')=7,mode('lightsource center power 50%')=7
	ModifyGraph mode('lightsource center power 35%')=7,mode('lightsource center power 30%')=7
	ModifyGraph mode('lightsource center power 25%')=7,mode('lightsource center power 20%')=7
	ModifyGraph mode('lightsource center power 15%')=7,mode('lightsource center power 10%')=7
	ModifyGraph mode('lightsource center power 6%')=7,mode(chroma760)=7,mode(powerAtSample)=3
	ModifyGraph marker(powerAtSample)=19
	ModifyGraph lSize('lightsource center power 40%')=2,lSize(fit_powerAtSample)=2
	ModifyGraph lStyle(chroma760)=7
	ModifyGraph rgb('lightsource center power 40%')=(0,0,0),rgb(chroma760)=(0,0,0),rgb(fit_powerAtSample)=(30583,30583,30583)
	ModifyGraph hbFill('lightsource center power 100%')=2,hbFill('lightsource center power 90%')=2
	ModifyGraph hbFill('lightsource center power 80%')=2,hbFill('lightsource center power 70%')=2
	ModifyGraph hbFill('lightsource center power 60%')=2,hbFill('lightsource center power 50%')=2
	ModifyGraph hbFill('lightsource center power 35%')=2,hbFill('lightsource center power 30%')=2
	ModifyGraph hbFill('lightsource center power 25%')=2,hbFill('lightsource center power 20%')=2
	ModifyGraph hbFill('lightsource center power 15%')=2,hbFill('lightsource center power 10%')=2
	ModifyGraph hbFill('lightsource center power 6%')=2,hbFill(chroma760)=2
	ModifyGraph useNegPat('lightsource center power 40%')=1
	ModifyGraph usePlusRGB('lightsource center power 100%')=1,usePlusRGB('lightsource center power 90%')=1
	ModifyGraph usePlusRGB('lightsource center power 80%')=1,usePlusRGB('lightsource center power 70%')=1
	ModifyGraph usePlusRGB('lightsource center power 60%')=1,usePlusRGB('lightsource center power 50%')=1
	ModifyGraph usePlusRGB('lightsource center power 35%')=1,usePlusRGB('lightsource center power 30%')=1
	ModifyGraph usePlusRGB('lightsource center power 25%')=1,usePlusRGB('lightsource center power 20%')=1
	ModifyGraph usePlusRGB('lightsource center power 15%')=1,usePlusRGB('lightsource center power 10%')=1
	ModifyGraph usePlusRGB('lightsource center power 6%')=1,usePlusRGB(chroma760)=1
	ModifyGraph hBarNegFill('lightsource center power 100%')=2,hBarNegFill('lightsource center power 90%')=2
	ModifyGraph hBarNegFill('lightsource center power 80%')=2,hBarNegFill('lightsource center power 70%')=2
	ModifyGraph hBarNegFill('lightsource center power 60%')=2,hBarNegFill('lightsource center power 50%')=2
	ModifyGraph hBarNegFill('lightsource center power 35%')=2,hBarNegFill('lightsource center power 30%')=2
	ModifyGraph hBarNegFill('lightsource center power 25%')=2,hBarNegFill('lightsource center power 20%')=2
	ModifyGraph hBarNegFill('lightsource center power 15%')=2,hBarNegFill('lightsource center power 10%')=2
	ModifyGraph hBarNegFill('lightsource center power 6%')=2,hBarNegFill('lightsource center power 40%')=5
	ModifyGraph plusRGB('lightsource center power 100%')=(65535,0,0,1638),plusRGB('lightsource center power 90%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 80%')=(65535,0,0,1638),plusRGB('lightsource center power 70%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 60%')=(65535,0,0,1638),plusRGB('lightsource center power 50%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 35%')=(65535,0,0,1638),plusRGB('lightsource center power 30%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 25%')=(65535,0,0,1638),plusRGB('lightsource center power 20%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 15%')=(65535,0,0,1638),plusRGB('lightsource center power 10%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 6%')=(65535,0,0,1638),plusRGB('lightsource center power 40%')=(65535,0,0,3277)
	ModifyGraph plusRGB(chroma760)=(65535,65535,65535)
	ModifyGraph nticks(left)=0,nticks(right)=3
	ModifyGraph noLabel(left)=1,noLabel(right)=2
	ModifyGraph standoff=0
	ModifyGraph axThick(right)=0
	ModifyGraph axRGB(left)=(0,0,0,0)
	ModifyGraph lblPosMode(power)=3,lblPosMode(emissionout)=3
	ModifyGraph lblPos(left)=27,lblPos(bottom)=38,lblPos(power)=50,lblPos(emissionout)=40
	ModifyGraph prescaleExp(power)=-3
	ModifyGraph freePos(power)=0
	ModifyGraph freePos(emissionout)={0,power}
	ModifyGraph axisEnab(power)={0.3,0.8}
	ModifyGraph axisEnab(emissionout)={0,0.4}
	ModifyGraph manTick(right)={0,1,0,0},manMinor(right)={9,5}
	ModifyGraph manTick(power)={0,1,0,0},manMinor(power)={1,5}
	Label left " "
	Label bottom "wavelength [nm]"
	Label right " "
	Label power "power at sample [mW]"
	Label emissionout "emission out in %"
	SetAxis bottom 525,750
	SetAxis right 0,1
	SetAxis power 0,1500
	SetAxis emissionout 0,100
	Legend/C/N=text0/J/F=0/A=MC/X=10.41/Y=37.66 "\\JCspectral weight at \r\\s('lightsource center power 40%') 40% emission"
	Legend/C/N=text1/J/F=0/A=MC/X=-31.62/Y=41.08 "\\s(powerAtSample) powerAtSample"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= emissionout,ycoord= power,linethick= 2
	DrawLine 40,0,40,653.29
	SetDrawEnv xcoord= emissionout,ycoord= power,linethick= 2
	DrawLine 0,653.29,40,653.29
EndMacro

Window atsample_subwindow() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(75,49.25,533.25,239)/R filterChroma760abs vs filterChroma760refl_wl as "atsample_subwindow"
	AppendToGraph :PLEMd2:maps:spectrumAtsample_13:PLEM/TN='lightsource center power 100%' vs :PLEMd2:maps:spectrumAtsample_13:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_12:PLEM/TN='lightsource center power 90%' vs :PLEMd2:maps:spectrumAtsample_12:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_11:PLEM/TN='lightsource center power 80%' vs :PLEMd2:maps:spectrumAtsample_11:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_10:PLEM/TN='lightsource center power 70%' vs :PLEMd2:maps:spectrumAtsample_10:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_9:PLEM/TN='lightsource center power 60%' vs :PLEMd2:maps:spectrumAtsample_9:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_8:PLEM/TN='lightsource center power 50%' vs :PLEMd2:maps:spectrumAtsample_8:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_1:PLEM/TN='lightsource center power 35%' vs :PLEMd2:maps:spectrumAtsample_1:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_2:PLEM/TN='lightsource center power 30%' vs :PLEMd2:maps:spectrumAtsample_2:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_7:PLEM/TN='lightsource center power 25%' vs :PLEMd2:maps:spectrumAtsample_7:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_3:PLEM/TN='lightsource center power 20%' vs :PLEMd2:maps:spectrumAtsample_3:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_4:PLEM/TN='lightsource center power 15%' vs :PLEMd2:maps:spectrumAtsample_4:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_5:PLEM/TN='lightsource center power 10%' vs :PLEMd2:maps:spectrumAtsample_5:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_6:PLEM/TN='lightsource center power 6%' vs :PLEMd2:maps:spectrumAtsample_6:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumAtsample_0:PLEM/TN='lightsource center power 40%' vs :PLEMd2:maps:spectrumAtsample_0:xWavelength
	AppendToGraph/R chroma760em/TN=chroma760 vs chroma760em_wl
	ModifyGraph margin(left)=28,margin(right)=28
	ModifyGraph mode(chroma760)=7
	ModifyGraph lSize('lightsource center power 40%')=2
	ModifyGraph lStyle(filterChroma760abs)=3,lStyle(chroma760)=7
	ModifyGraph rgb(filterChroma760abs)=(0,0,0),rgb('lightsource center power 90%')=(65535,6425,6425)
	ModifyGraph rgb('lightsource center power 80%')=(65535,11565,11565),rgb('lightsource center power 70%')=(65535,16705,16705)
	ModifyGraph rgb('lightsource center power 60%')=(65535,21845,21845),rgb('lightsource center power 50%')=(65535,26985,26985)
	ModifyGraph rgb('lightsource center power 35%')=(65535,29555,29555),rgb('lightsource center power 30%')=(65535,32125,32125)
	ModifyGraph rgb('lightsource center power 25%')=(65535,34695,34695),rgb('lightsource center power 20%')=(65535,37265,37265)
	ModifyGraph rgb('lightsource center power 15%')=(65535,39835,39835),rgb('lightsource center power 10%')=(65535,42405,42405)
	ModifyGraph rgb('lightsource center power 6%')=(65535,44975,44975),rgb('lightsource center power 40%')=(0,0,0)
	ModifyGraph rgb(chroma760)=(0,0,0)
	ModifyGraph hbFill('lightsource center power 100%')=2,hbFill('lightsource center power 90%')=2
	ModifyGraph hbFill('lightsource center power 80%')=2,hbFill('lightsource center power 70%')=2
	ModifyGraph hbFill('lightsource center power 60%')=2,hbFill('lightsource center power 50%')=2
	ModifyGraph hbFill('lightsource center power 35%')=2,hbFill('lightsource center power 30%')=2
	ModifyGraph hbFill('lightsource center power 25%')=2,hbFill('lightsource center power 20%')=2
	ModifyGraph hbFill('lightsource center power 15%')=2,hbFill('lightsource center power 10%')=2
	ModifyGraph hbFill('lightsource center power 6%')=2,hbFill(chroma760)=2
	ModifyGraph useNegPat('lightsource center power 40%')=1
	ModifyGraph usePlusRGB('lightsource center power 100%')=1,usePlusRGB('lightsource center power 90%')=1
	ModifyGraph usePlusRGB('lightsource center power 80%')=1,usePlusRGB('lightsource center power 70%')=1
	ModifyGraph usePlusRGB('lightsource center power 60%')=1,usePlusRGB('lightsource center power 50%')=1
	ModifyGraph usePlusRGB('lightsource center power 35%')=1,usePlusRGB('lightsource center power 30%')=1
	ModifyGraph usePlusRGB('lightsource center power 25%')=1,usePlusRGB('lightsource center power 20%')=1
	ModifyGraph usePlusRGB('lightsource center power 15%')=1,usePlusRGB('lightsource center power 10%')=1
	ModifyGraph usePlusRGB('lightsource center power 6%')=1,usePlusRGB(chroma760)=1
	ModifyGraph hBarNegFill('lightsource center power 100%')=2,hBarNegFill('lightsource center power 90%')=2
	ModifyGraph hBarNegFill('lightsource center power 80%')=2,hBarNegFill('lightsource center power 70%')=2
	ModifyGraph hBarNegFill('lightsource center power 60%')=2,hBarNegFill('lightsource center power 50%')=2
	ModifyGraph hBarNegFill('lightsource center power 35%')=2,hBarNegFill('lightsource center power 30%')=2
	ModifyGraph hBarNegFill('lightsource center power 25%')=2,hBarNegFill('lightsource center power 20%')=2
	ModifyGraph hBarNegFill('lightsource center power 15%')=2,hBarNegFill('lightsource center power 10%')=2
	ModifyGraph hBarNegFill('lightsource center power 6%')=2,hBarNegFill('lightsource center power 40%')=5
	ModifyGraph plusRGB('lightsource center power 100%')=(65535,0,0,1638),plusRGB('lightsource center power 90%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 80%')=(65535,0,0,1638),plusRGB('lightsource center power 70%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 60%')=(65535,0,0,1638),plusRGB('lightsource center power 50%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 35%')=(65535,0,0,1638),plusRGB('lightsource center power 30%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 25%')=(65535,0,0,1638),plusRGB('lightsource center power 20%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 15%')=(65535,0,0,1638),plusRGB('lightsource center power 10%')=(65535,0,0,1638)
	ModifyGraph plusRGB('lightsource center power 6%')=(65535,0,0,1638),plusRGB('lightsource center power 40%')=(65535,0,0,3277)
	ModifyGraph plusRGB(chroma760)=(65535,65535,65535)
	ModifyGraph nticks(right)=3,nticks(left)=0
	ModifyGraph noLabel(right)=2,noLabel(left)=1
	ModifyGraph axThick(right)=0
	ModifyGraph axRGB(left)=(0,0,0,0)
	ModifyGraph manTick(right)={0,1,0,0},manMinor(right)={9,5}
	Label right " "
	Label bottom "wavelength [nm]"
	Label left " "
	SetAxis right 0,1
	SetAxis bottom 525,750
	Legend/C/N=text0/J/F=0/A=MC/X=4.32/Y=17.86 "\\JCspectral weight at \r\\s('lightsource center power 40%') 40% emission"
	Display/W=(0.143,0,0.441,0.599)/HOST=#  powerAtSample vs powerPercent
	AppendToGraph fit_powerAtSample
	ModifyGraph width={Aspect,1}
	ModifyGraph mode(powerAtSample)=3
	ModifyGraph marker(powerAtSample)=19
	ModifyGraph rgb(fit_powerAtSample)=(0,0,0)
	ModifyGraph hideTrace(fit_powerAtSample)=1
	ModifyGraph lblMargin(left)=10
	ModifyGraph standoff=0
	ModifyGraph lblPosMode(left)=1
	ModifyGraph prescaleExp(left)=-3
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={1,5}
	Label left "power at sample \r[mW]"
	Label bottom "emission output [%]"
	SetAxis left 0,1500
	SetAxis bottom 0,100
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2
	DrawLine 40,0,40,653.29
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2
	DrawLine 0,653.29,40,653.29
	RenameWindow #,Graph0
	SetActiveSubwindow ##
EndMacro

Window powerrelation() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(543,50,767.25,241.25) fit_powerGlassSample as "powerrelation"
	AppendToGraph powerAtSample vs powerAtGlassPlate
	AppendToGraph powerAtSample vs powerAtGlassPlate
	ModifyGraph expand=-1,width=141.732,height={Aspect,1}
	ModifyGraph mode(powerAtSample)=3,mode(powerAtSample#1)=3
	ModifyGraph marker(powerAtSample)=8,marker(powerAtSample#1)=19
	ModifyGraph lStyle(fit_powerGlassSample)=3
	ModifyGraph msize(powerAtSample)=5
	ModifyGraph opaque(powerAtSample)=1
	ModifyGraph textMarker(powerAtSample#1)={powerPercent,"default",0,0,5,0.00,0.00}
	ModifyGraph manTick(left)={0,500,0,0},manMinor(left)={1,50}
	ModifyGraph manTick(bottom)={0,50,0,0},manMinor(bottom)={1,50}
	Label left "power at sample [µW]"
	Label bottom "power at glass plate [µW]"
	SetAxis left 0,*
	SetAxis bottom 0,*
EndMacro

