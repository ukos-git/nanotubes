#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include <InsertSubwindowInGraph>

Function recalc()
	//fitSolvents("all")
	findMedian("air")
	findMedian("toluene")
	findCoordinates("toluene")

	// update violin_boxbeutel plot
	cd root:
	Execute "Redimension/N=0 fwhm11_toluene; concatenate {root:fwhm11_a_toluene,root:fwhm11_b_toluene,root:fwhm11_c_toluene,root:fwhm11_d_toluene,root:fwhm11_e_toluene,root:fwhm11_f_toluene,root:fwhm11_g_toluene,root:fwhm11_i_toluene,root:fwhm11_j_toluene,root:fwhm11_k_toluene}, fwhm11_toluene"
	Execute "Redimension/N=0 fwhm11_air;concatenate {root:fwhm11_a_air,root:fwhm11_b_air,root:fwhm11_c_air,root:fwhm11_d_air,root:fwhm11_e_air,root:fwhm11_f_air,root:fwhm11_g_air,root:fwhm11_i_air,root:fwhm11_j_air,root:fwhm11_k_air}, fwhm11_air"
	Execute "Redimension/N=0 fwhm22_toluene; concatenate {root:fwhm22_a_toluene,root:fwhm22_b_toluene,root:fwhm22_c_toluene,root:fwhm22_d_toluene,root:fwhm22_e_toluene,root:fwhm22_f_toluene,root:fwhm22_g_toluene,root:fwhm22_i_toluene,root:fwhm22_j_toluene,root:fwhm22_k_toluene}, fwhm22_toluene"
	Execute "Redimension/N=0 fwhm22_air; concatenate {root:fwhm22_a_air,root:fwhm22_b_air,root:fwhm22_c_air,root:fwhm22_d_air,root:fwhm22_e_air,root:fwhm22_f_air,root:fwhm22_g_air,root:fwhm22_i_air,root:fwhm22_j_air,root:fwhm22_k_air}, fwhm22_air"

	WAVE fwhm11_air,fwhm11_toluene
	fwhm11_air[] = fwhm11_air[p] >= 40 ? NaN : fwhm11_air[p]
	fwhm11_air[] = fwhm11_air[p] < 0 ? NaN : fwhm11_air[p]
	fwhm11_toluene[] = fwhm11_toluene[p] < 0 ? NaN : fwhm11_toluene[p]

	String fwhm_air, fwhm_toluene
	StatsQuantiles/Q fwhm11_air
	sprintf fwhm_air, "%.1f(%d)meV", V_Median, V_MAD * 10
	sprintf fwhm_air, "%d(%d)meV", V_Median, V_MAD
	StatsQuantiles/Q fwhm11_toluene
	sprintf fwhm_toluene, "%.1f(%d)meV", V_Median, V_MAD * 10
	sprintf fwhm_toluene, "%d(%d)meV", V_Median, V_MAD
	TextBox/C/N=text0/W=violin_boxbeutel#violin "median: " + fwhm_air + " in air and " + fwhm_toluene + " in toluene"
End

// @brief changes after review of thesis
Function review()
	WAVE/T wv = root:wavelength_air_label
	wv[14] = "(10,3)"
	DoUpdate/W=airvariance
End

Function export()
	review()
	SaveTableCopy/O/W=wavelength_toluene_table/P=home/A=0/N=(0x00)/T=1 as "maps_toluene.csv"
	SaveTableCopy/O/W=wavelength_air_table/P=home/A=0/N=(0x00)/T=1 as "maps_air.csv"
	SaveWindow("airvariance")
	SaveWindow("airtoluene")
	SaveWindow("airtoluene")
	SaveWindow("toluenevariance")
	SaveWindow("SMAatlasFit_air_blue", saveVector=0) // pdf save bug for vector
	SaveWindow("violin_boxbeutel", saveVector=0) // pdf save bug for vector
	SaveWindow("violin_ediff")
	SaveWindow("violin_fwhm", saveVector=1)
	SaveWindow("maps_shift", saveVector=1)
	SaveWindow("maps_shift_combined", saveVector=1)
	SaveWindow("ediff_adsorption", saveVector=1)
	SaveWindow("maps_shift_air", saveVector=1)
End

Window wavelength_air_table() : Table
	PauseUpdate; Silent 1		// building window...
	Edit/W=(253.5,46.25,849.75,532.25) wavelength_airT,wavelength_air_label,wavelength_air as "wavelength_air_table"
	AppendToTable wavelength_airErr
	ModifyTable format(Point)=1,title(wavelength_airT)="label",title(wavelength_air_label)="chirality"
	ModifyTable title(wavelength_air)="wavelength [nm]",title(wavelength_airErr)="standard error [nm]"
EndMacro

Window airvariance() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(9.75,40.25,466.5,273.5) wavelength_air[*][0]/TN=coordinates vs wavelength_air[*][1] as "airvariance"
	AppendImage/B=bottom_left aircovariance_si
	ModifyImage aircovariance_si ctab= {0,*,colortable_plotly_earth,1}
	AppendImage/B=bottom_right aircovariance_ingaas
	ModifyImage aircovariance_ingaas ctab= {0,*,colortable_plotly_earth,1}
	ModifyGraph margin(left)=72,margin(bottom)=53,margin(top)=13,margin(right)=72,width={Plan,1,bottom,left}
	ModifyGraph mode=3
	ModifyGraph marker=1
	ModifyGraph textMarker(coordinates)={wavelength_air_label,"default",1,0,5,0.00,0.00}
	ModifyGraph grid(bottom)=1
	ModifyGraph tick(bottom)=2
	ModifyGraph mirror(left)=0,mirror(bottom)=1
	ModifyGraph noLabel(bottom)=1
	ModifyGraph lblMargin(bottom)=15
	ModifyGraph standoff(bottom)=0,standoff(bottom_left)=0,standoff(bottom_right)=0
	ModifyGraph gridRGB(bottom)=(65535,65535,65535)
	ModifyGraph axRGB(bottom)=(65535,65535,65535)
	ModifyGraph gridStyle(bottom)=5
	ModifyGraph gridHair(bottom)=0
	ModifyGraph lblPosMode(bottom)=1,lblPosMode(bottom_left)=1,lblPosMode(bottom_right)=1
	ModifyGraph lblPos(bottom)=45
	ModifyGraph freePos(bottom_left)={0,kwFraction}
	ModifyGraph freePos(bottom_right)={0,kwFraction}
	ModifyGraph axisEnab(bottom_left)={0,0.5}
	ModifyGraph axisEnab(bottom_right)={0.5,1}
	ModifyGraph manTick(bottom)={0,1020,0,0},manMinor(bottom)={0,50}
	ModifyGraph manTick(bottom_right)={0,50,0,0},manMinor(bottom_right)={0,50}
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	Label bottom_left "Silicon Detector"
	Label bottom_right "InGaAs Detector"
	SetAxis left 537.5,752
	SetAxis bottom 820,1220
	SetAxis bottom_left 820,1020
	SetAxis bottom_right 1020,1220
	TextBox/C/N=text0/S=1/A=LT/X=7.50/Y=0.00 "air"
EndMacro

Window airtoluene() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(12,298.25,468.75,535.25) as "airtoluene"
	AppendImage/B=bottom_left aircovariance_si
	ModifyImage aircovariance_si ctab= {2.33893725415073e-06,0.000719025669642857,colortable_plotly_earth,1}
	AppendImage/B=bottom_right toluenecovariance_si
	ModifyImage toluenecovariance_si ctab= {0,4.58915625e-05,colortable_plotly_earth,1}
	ModifyGraph expand=-1,width={Plan,1,bottom_left,left}
	ModifyGraph mirror(left)=0
	ModifyGraph lblPosMode(bottom_left)=1,lblPosMode(bottom_right)=1
	ModifyGraph freePos(bottom_left)={0,kwFraction}
	ModifyGraph freePos(bottom_right)={0,kwFraction}
	ModifyGraph axisEnab(bottom_left)={0,0.5}
	ModifyGraph axisEnab(bottom_right)={0.5,1}
	Label left "excitation wavelength [nm]"
	Label bottom_left "emission wavelength [nm]\rin air"
	Label bottom_right "emission wavelength [nm]\rin toluene"
	SetAxis left 540,750
	SetAxis bottom_left 820,1049
	SetAxis bottom_right 820,1049
	TextBox/C/N=airbox/S=1/A=LT/X=7.50/Y=0.00 "air"
	TextBox/C/N=phmebox/S=1/A=MT/Y=0.00 "PhMe"
	SetWindow kwTopWin,userdata(toluenecovariance_si)=  "image_sliderLimits={0,0.00038073};"
	SetWindow kwTopWin,userdata(aircovariance_si)=  "image_sliderLimits={0,0.00072225};"
EndMacro

Window toluenevariance() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:dielectric_toluene:
	Display /W=(481.5,42.5,936.75,265.25) ::wavelength_toluene[*][0]/TN=coordinates vs ::wavelength_toluene[*][1] as "toluenevariance"
	AppendToGraph mkl22_008_000_0_b_e22/TN=mkl22_008_000_0_b_ vs mkl22_008_000_0_b_e11
	AppendToGraph mkl22_010_002_0_b_e22/TN=mkl22_010_002_0_b_ vs mkl22_010_002_0_b_e11
	AppendToGraph mkl22_013_005_0_b_e22/TN=mkl22_013_005_0_b_ vs mkl22_013_005_0_b_e11
	AppendToGraph mkl22_015_007_0_b_e22/TN=mkl22_015_007_0_b_ vs mkl22_015_007_0_b_e11
	AppendToGraph mkl23_100_069_0_g_e22/TN=mkl23_100_069_0_g_ vs mkl23_100_069_0_g_e11
	AppendToGraph mkl23_103_043_0_i_e22/TN=mkl23_103_043_0_i_ vs mkl23_103_043_0_i_e11
	AppendToGraph mkl23_104_016_0_d_e22/TN=mkl23_104_016_0_d_ vs mkl23_104_016_0_d_e11
	AppendToGraph mkl23_106_017_0_b_e22/TN=mkl23_106_017_0_b_ vs mkl23_106_017_0_b_e11
	AppendToGraph mkl23_106_018_0_b_e22/TN=mkl23_106_018_0_b_ vs mkl23_106_018_0_b_e11
	AppendToGraph mkl23_107_017_0_b_e22/TN=mkl23_107_017_0_b_ vs mkl23_107_017_0_b_e11
	AppendToGraph mkl23_107_018_0_b_e22/TN=mkl23_107_018_0_b_ vs mkl23_107_018_0_b_e11
	AppendToGraph mkl23_108_019_0_b_e22/TN=mkl23_108_019_0_b_ vs mkl23_108_019_0_b_e11
	AppendToGraph mkl23_109_020_0_b_e22/TN=mkl23_109_020_0_b_ vs mkl23_109_020_0_b_e11
	AppendToGraph mkl23_112_023_0_b_e22/TN=mkl23_112_023_0_b_ vs mkl23_112_023_0_b_e11
	AppendToGraph mkl23_113_024_0_d_e22/TN=mkl23_113_024_0_d_ vs mkl23_113_024_0_d_e11
	AppendToGraph mkl23_117_035_0_g_e22/TN=mkl23_117_035_0_g_ vs mkl23_117_035_0_g_e11
	AppendToGraph mkl23_118_036_0_g_e22/TN=mkl23_118_036_0_g_ vs mkl23_118_036_0_g_e11
	AppendToGraph mkl23_119_037_0_g_e22/TN=mkl23_119_037_0_g_ vs mkl23_119_037_0_g_e11
	AppendToGraph mkl23_124_045_0_g_e22/TN=mkl23_124_045_0_g_ vs mkl23_124_045_0_g_e11
	AppendToGraph mkl23_125_048_0_i_e22/TN=mkl23_125_048_0_i_ vs mkl23_125_048_0_i_e11
	AppendToGraph mkl23_126_050_0_i_e22/TN=mkl23_126_050_0_i_ vs mkl23_126_050_0_i_e11
	AppendToGraph mkl23_127_051_0_i_e22/TN=mkl23_127_051_0_i_ vs mkl23_127_051_0_i_e11
	AppendToGraph mkl23_129_057_0_i_e22/TN=mkl23_129_057_0_i_ vs mkl23_129_057_0_i_e11
	AppendToGraph mkl23_133_069_0_g_e22/TN=mkl23_133_069_0_g_ vs mkl23_133_069_0_g_e11
	AppendToGraph mkl23_134_071_0_e_e22/TN=mkl23_134_071_0_e_ vs mkl23_134_071_0_e_e11
	AppendToGraph mkl23_135_072_0_d_e22/TN=mkl23_135_072_0_d_ vs mkl23_135_072_0_d_e11
	AppendToGraph mkl23_136_081_0_i_e22/TN=mkl23_136_081_0_i_ vs mkl23_136_081_0_i_e11
	AppendToGraph mkl23_137_083_0_g_e22/TN=mkl23_137_083_0_g_ vs mkl23_137_083_0_g_e11
	AppendToGraph mkl23_141_087_0_g_e22/TN=mkl23_141_087_0_g_ vs mkl23_141_087_0_g_e11
	AppendToGraph mkl24_223_173_0_k_e22/TN=mkl24_223_173_0_k_ vs mkl24_223_173_0_k_e11
	AppendToGraph mkl24_234_191_0_k_e22/TN=mkl24_234_191_0_k_ vs mkl24_234_191_0_k_e11
	AppendToGraph mkl24_239_196_0_f_e22/TN=mkl24_239_196_0_f_ vs mkl24_239_196_0_f_e11
	AppendToGraph mkl24_242_199_0_f_e22/TN=mkl24_242_199_0_f_ vs mkl24_242_199_0_f_e11
	AppendToGraph mkl24_248_149_0_d_e22/TN=mkl24_248_149_0_d_ vs mkl24_248_149_0_d_e11
	AppendToGraph mkl24_256_157_0_d_e22/TN=mkl24_256_157_0_d_ vs mkl24_256_157_0_d_e11
	AppendToGraph mkl24_261_148_0_d_e22/TN=mkl24_261_148_0_d_ vs mkl24_261_148_0_d_e11
	AppendToGraph mkl24_264_165_0_d_e22/TN=mkl24_264_165_0_d_ vs mkl24_264_165_0_d_e11
	AppendToGraph mkl24_266_167_0_b_e22/TN=mkl24_266_167_0_b_ vs mkl24_266_167_0_b_e11
	AppendToGraph mkl24_267_168_0_d_e22/TN=mkl24_267_168_0_d_ vs mkl24_267_168_0_d_e11
	AppendToGraph mkl24_267_169_0_d_e22/TN=mkl24_267_169_0_d_ vs mkl24_267_169_0_d_e11
	AppendToGraph mkl29_292_272_0_b_e22/TN=mkl29_292_272_0_b_ vs mkl29_292_272_0_b_e11
	AppendToGraph mkl29_293_274_0_b_e22/TN=mkl29_293_274_0_b_ vs mkl29_293_274_0_b_e11
	AppendToGraph mkl29_294_275_0_d_e22/TN=mkl29_294_275_0_d_ vs mkl29_294_275_0_d_e11
	AppendToGraph mkl29_295_280_0_b_e22/TN=mkl29_295_280_0_b_ vs mkl29_295_280_0_b_e11
	AppendToGraph mkl29_296_284_0_d_e22/TN=mkl29_296_284_0_d_ vs mkl29_296_284_0_d_e11
	AppendToGraph mkl34_421_353_0_c_e22/TN=mkl34_421_353_0_c_ vs mkl34_421_353_0_c_e11
	AppendToGraph mkl34_422_353_0_c_e22/TN=mkl34_422_353_0_c_ vs mkl34_422_353_0_c_e11
	AppendToGraph mkl34_427_360_0_b_e22/TN=mkl34_427_360_0_b_ vs mkl34_427_360_0_b_e11
	AppendToGraph mkl34_430_365_0_a_e22/TN=mkl34_430_365_0_a_ vs mkl34_430_365_0_a_e11
	AppendToGraph mkl34_436_375_0_a_e22/TN=mkl34_436_375_0_a_ vs mkl34_436_375_0_a_e11
	AppendToGraph mkl34_442_401_0_b_e22/TN=mkl34_442_401_0_b_ vs mkl34_442_401_0_b_e11
	AppendImage/B=bottom_left ::toluenecovariance_si
	ModifyImage toluenecovariance_si ctab= {0,2.7195e-05,::colortable_plotly_earth,1}
	AppendImage/B=bottom_right ::toluenecovariance_ingaas
	ModifyImage toluenecovariance_ingaas ctab= {3.80185267857143e-05,0.000241529464285714,::colortable_plotly_earth,1}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=72,margin(bottom)=56,margin(top)=14,margin(right)=72,width={Plan,1,bottom_left,left}
	ModifyGraph mode(coordinates)=3,mode(mkl22_008_000_0_b_)=4,mode(mkl22_010_002_0_b_)=4
	ModifyGraph mode(mkl22_013_005_0_b_)=4,mode(mkl22_015_007_0_b_)=4,mode(mkl23_100_069_0_g_)=4
	ModifyGraph mode(mkl23_103_043_0_i_)=4,mode(mkl23_104_016_0_d_)=4,mode(mkl23_106_017_0_b_)=4
	ModifyGraph mode(mkl23_106_018_0_b_)=4,mode(mkl23_107_017_0_b_)=4,mode(mkl23_107_018_0_b_)=4
	ModifyGraph mode(mkl23_108_019_0_b_)=4,mode(mkl23_109_020_0_b_)=4,mode(mkl23_112_023_0_b_)=4
	ModifyGraph mode(mkl23_113_024_0_d_)=4,mode(mkl23_117_035_0_g_)=4,mode(mkl23_118_036_0_g_)=4
	ModifyGraph mode(mkl23_119_037_0_g_)=4,mode(mkl23_124_045_0_g_)=4,mode(mkl23_125_048_0_i_)=4
	ModifyGraph mode(mkl23_126_050_0_i_)=4,mode(mkl23_127_051_0_i_)=4,mode(mkl23_129_057_0_i_)=4
	ModifyGraph mode(mkl23_133_069_0_g_)=4,mode(mkl23_134_071_0_e_)=4,mode(mkl23_135_072_0_d_)=4
	ModifyGraph mode(mkl23_136_081_0_i_)=4,mode(mkl23_137_083_0_g_)=4,mode(mkl23_141_087_0_g_)=4
	ModifyGraph mode(mkl24_223_173_0_k_)=4,mode(mkl24_234_191_0_k_)=4,mode(mkl24_239_196_0_f_)=4
	ModifyGraph mode(mkl24_242_199_0_f_)=4,mode(mkl24_248_149_0_d_)=4,mode(mkl24_256_157_0_d_)=4
	ModifyGraph mode(mkl24_261_148_0_d_)=4,mode(mkl24_264_165_0_d_)=4,mode(mkl24_266_167_0_b_)=4
	ModifyGraph mode(mkl24_267_168_0_d_)=4,mode(mkl24_267_169_0_d_)=4,mode(mkl29_292_272_0_b_)=4
	ModifyGraph mode(mkl29_293_274_0_b_)=4,mode(mkl29_294_275_0_d_)=4,mode(mkl29_295_280_0_b_)=4
	ModifyGraph mode(mkl29_296_284_0_d_)=4,mode(mkl34_421_353_0_c_)=4,mode(mkl34_422_353_0_c_)=4
	ModifyGraph mode(mkl34_427_360_0_b_)=4,mode(mkl34_430_365_0_a_)=4,mode(mkl34_436_375_0_a_)=4
	ModifyGraph mode(mkl34_442_401_0_b_)=4
	ModifyGraph marker(coordinates)=19
	ModifyGraph rgb(mkl22_008_000_0_b_)=(65535,49151,49151,58982),rgb(mkl22_013_005_0_b_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl22_015_007_0_b_)=(65535,49151,49151,58982),rgb(mkl23_100_069_0_g_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_103_043_0_i_)=(65535,49151,49151,58982),rgb(mkl23_104_016_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_108_019_0_b_)=(65535,49151,49151,58982),rgb(mkl23_109_020_0_b_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_112_023_0_b_)=(65535,49151,49151,58982),rgb(mkl23_113_024_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_117_035_0_g_)=(65535,49151,49151,58982),rgb(mkl23_118_036_0_g_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_119_037_0_g_)=(65535,49151,49151,58982),rgb(mkl23_124_045_0_g_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_125_048_0_i_)=(65535,49151,49151,58982),rgb(mkl23_126_050_0_i_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_127_051_0_i_)=(65535,49151,49151,58982),rgb(mkl23_129_057_0_i_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_133_069_0_g_)=(65535,49151,49151,58982),rgb(mkl23_134_071_0_e_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_136_081_0_i_)=(65535,49151,49151,58982),rgb(mkl23_137_083_0_g_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl23_141_087_0_g_)=(65535,49151,49151,58982),rgb(mkl24_234_191_0_k_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl24_239_196_0_f_)=(65535,49151,49151,58982),rgb(mkl24_242_199_0_f_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl24_248_149_0_d_)=(65535,49151,49151,58982),rgb(mkl24_256_157_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl24_261_148_0_d_)=(65535,49151,49151,58982),rgb(mkl24_264_165_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl24_266_167_0_b_)=(65535,49151,49151,58982),rgb(mkl24_267_168_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl24_267_169_0_d_)=(65535,49151,49151,58982),rgb(mkl29_292_272_0_b_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl29_293_274_0_b_)=(65535,49151,49151,58982),rgb(mkl29_294_275_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl29_295_280_0_b_)=(65535,49151,49151,58982),rgb(mkl29_296_284_0_d_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl34_421_353_0_c_)=(65535,49151,49151,58982),rgb(mkl34_422_353_0_c_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl34_427_360_0_b_)=(65535,49151,49151,58982),rgb(mkl34_430_365_0_a_)=(65535,49151,49151,58982)
	ModifyGraph rgb(mkl34_442_401_0_b_)=(65535,49151,49151,58982)
	ModifyGraph zmrkSize(mkl22_008_000_0_b_)={:dielectric_toluene:mkl22_008_000_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl22_010_002_0_b_)={:dielectric_toluene:mkl22_010_002_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl22_013_005_0_b_)={:dielectric_toluene:mkl22_013_005_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl22_015_007_0_b_)={:dielectric_toluene:mkl22_015_007_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_100_069_0_g_)={:dielectric_toluene:mkl23_100_069_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_103_043_0_i_)={:dielectric_toluene:mkl23_103_043_0_i_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_104_016_0_d_)={:dielectric_toluene:mkl23_104_016_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_106_017_0_b_)={:dielectric_toluene:mkl23_106_017_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_106_018_0_b_)={:dielectric_toluene:mkl23_106_018_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_107_017_0_b_)={:dielectric_toluene:mkl23_107_017_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_107_018_0_b_)={:dielectric_toluene:mkl23_107_018_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_108_019_0_b_)={:dielectric_toluene:mkl23_108_019_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_109_020_0_b_)={:dielectric_toluene:mkl23_109_020_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_112_023_0_b_)={:dielectric_toluene:mkl23_112_023_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_113_024_0_d_)={:dielectric_toluene:mkl23_113_024_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_117_035_0_g_)={:dielectric_toluene:mkl23_117_035_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_118_036_0_g_)={:dielectric_toluene:mkl23_118_036_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_119_037_0_g_)={:dielectric_toluene:mkl23_119_037_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_124_045_0_g_)={:dielectric_toluene:mkl23_124_045_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_125_048_0_i_)={:dielectric_toluene:mkl23_125_048_0_i_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_126_050_0_i_)={:dielectric_toluene:mkl23_126_050_0_i_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_127_051_0_i_)={:dielectric_toluene:mkl23_127_051_0_i_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_129_057_0_i_)={:dielectric_toluene:mkl23_129_057_0_i_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_133_069_0_g_)={:dielectric_toluene:mkl23_133_069_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_134_071_0_e_)={:dielectric_toluene:mkl23_134_071_0_e_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_135_072_0_d_)={:dielectric_toluene:mkl23_135_072_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_136_081_0_i_)={:dielectric_toluene:mkl23_136_081_0_i_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_137_083_0_g_)={:dielectric_toluene:mkl23_137_083_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl23_141_087_0_g_)={:dielectric_toluene:mkl23_141_087_0_g_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_223_173_0_k_)={:dielectric_toluene:mkl24_223_173_0_k_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_234_191_0_k_)={:dielectric_toluene:mkl24_234_191_0_k_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_239_196_0_f_)={:dielectric_toluene:mkl24_239_196_0_f_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_242_199_0_f_)={:dielectric_toluene:mkl24_242_199_0_f_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_248_149_0_d_)={:dielectric_toluene:mkl24_248_149_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_256_157_0_d_)={:dielectric_toluene:mkl24_256_157_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_261_148_0_d_)={:dielectric_toluene:mkl24_261_148_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_264_165_0_d_)={:dielectric_toluene:mkl24_264_165_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_266_167_0_b_)={:dielectric_toluene:mkl24_266_167_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_267_168_0_d_)={:dielectric_toluene:mkl24_267_168_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl24_267_169_0_d_)={:dielectric_toluene:mkl24_267_169_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl29_292_272_0_b_)={:dielectric_toluene:mkl29_292_272_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl29_293_274_0_b_)={:dielectric_toluene:mkl29_293_274_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl29_294_275_0_d_)={:dielectric_toluene:mkl29_294_275_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl29_295_280_0_b_)={:dielectric_toluene:mkl29_295_280_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl29_296_284_0_d_)={:dielectric_toluene:mkl29_296_284_0_d_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl34_421_353_0_c_)={:dielectric_toluene:mkl34_421_353_0_c_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl34_422_353_0_c_)={:dielectric_toluene:mkl34_422_353_0_c_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl34_427_360_0_b_)={:dielectric_toluene:mkl34_427_360_0_b_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl34_430_365_0_a_)={:dielectric_toluene:mkl34_430_365_0_a_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl34_436_375_0_a_)={:dielectric_toluene:mkl34_436_375_0_a_intensity,0,*,0,10}
	ModifyGraph zmrkSize(mkl34_442_401_0_b_)={:dielectric_toluene:mkl34_442_401_0_b_intensity,0,*,0,10}
	ModifyGraph textMarker(coordinates)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	ModifyGraph arrowMarker(mkl22_008_000_0_b_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl22_010_002_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl22_013_005_0_b_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl22_015_007_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_100_069_0_g_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_103_043_0_i_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_104_016_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_106_017_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_106_018_0_b_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_107_017_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_107_018_0_b_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_108_019_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_109_020_0_b_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_112_023_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_113_024_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_117_035_0_g_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_118_036_0_g_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_119_037_0_g_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_124_045_0_g_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_125_048_0_i_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_126_050_0_i_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_127_051_0_i_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_129_057_0_i_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_133_069_0_g_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_134_071_0_e_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_135_072_0_d_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_136_081_0_i_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl23_137_083_0_g_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl23_141_087_0_g_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl24_223_173_0_k_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl24_234_191_0_k_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl24_239_196_0_f_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl24_242_199_0_f_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl24_248_149_0_d_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl24_256_157_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl24_261_148_0_d_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl24_264_165_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl24_266_167_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl24_267_168_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl24_267_169_0_d_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl29_292_272_0_b_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl29_293_274_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl29_294_275_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl29_295_280_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl29_296_284_0_d_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl34_421_353_0_c_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl34_422_353_0_c_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl34_427_360_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl34_430_365_0_a_)={_inline_,0.75,7,0.5,2},arrowMarker(mkl34_436_375_0_a_)={_inline_,0.75,7,0.5,2}
	ModifyGraph arrowMarker(mkl34_442_401_0_b_)={_inline_,0.75,7,0.5,2}
	ModifyGraph grid(bottom)=2
	ModifyGraph tick(bottom)=2
	ModifyGraph mirror(left)=0,mirror(bottom)=1
	ModifyGraph noLabel(bottom)=1
	ModifyGraph lblMargin(bottom)=15
	ModifyGraph standoff(bottom)=0,standoff(bottom_left)=0,standoff(bottom_right)=0
	ModifyGraph gridRGB(bottom)=(65535,65535,65535)
	ModifyGraph axRGB(bottom)=(65535,65535,65535)
	ModifyGraph gridStyle(bottom)=5
	ModifyGraph gridHair(bottom)=0
	ModifyGraph lblPosMode(bottom)=1,lblPosMode(bottom_left)=1,lblPosMode(bottom_right)=1
	ModifyGraph lblPos(bottom)=45
	ModifyGraph freePos(bottom_left)={0,kwFraction}
	ModifyGraph freePos(bottom_right)={0,kwFraction}
	ModifyGraph axisEnab(bottom_left)={0,0.5}
	ModifyGraph axisEnab(bottom_right)={0.5,1}
	ModifyGraph manTick(bottom)={0,1035,0,0},manMinor(bottom)={0,50}
	ModifyGraph manTick(bottom_left)={0,50,0,0},manMinor(bottom_left)={0,50}
	ModifyGraph manTick(bottom_right)={0,50,0,0},manMinor(bottom_right)={0,5}
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	Label bottom_left "Silicon Detector"
	Label bottom_right "InGaAs Detector"
	SetAxis left 540,752
	SetAxis bottom 820,1250
	SetAxis bottom_left 820,1035
	SetAxis bottom_right 1035,1250
	TextBox/C/N=text0/S=1/A=LT/X=0.00/Y=0.00 "PhMe"
	SetWindow kwTopWin,userdata(toluenecovariance_si)=  "image_sliderLimits={0,0.00038073};"
	SetWindow kwTopWin,userdata(toluenecovariance_ingaas)=  "image_sliderLimits={0,0.00050095};"
EndMacro

Window SMAatlasFit_air_blue() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:PLEMd2:maps:mkl22map_0:CHIRALITY:
	Display /W=(527.25,384.5,983.25,648.5) PLEMs2nm/TN=mkl22map_0 vs PLEMs1nm as "SMAatlasFit_air_blue"
	AppendToGraph :::mkl22map_1:CHIRALITY:PLEMs2nm/TN=mkl22map_1 vs :::mkl22map_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl22map_2:CHIRALITY:PLEMs2nm/TN=mkl22map_2 vs :::mkl22map_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl22map_3:CHIRALITY:PLEMs2nm/TN=mkl22map_3 vs :::mkl22map_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl22map_4:CHIRALITY:PLEMs2nm/TN=mkl22map_4 vs :::mkl22map_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl22map_5:CHIRALITY:PLEMs2nm/TN=mkl22map_5 vs :::mkl22map_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl22map_6:CHIRALITY:PLEMs2nm/TN=mkl22map_6 vs :::mkl22map_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl22map_7:CHIRALITY:PLEMs2nm/TN=mkl22map_7 vs :::mkl22map_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi:CHIRALITY:PLEMs2nm/TN=mkl23mapSi vs :::mkl23mapSi:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_0:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_0 vs :::mkl23mapSi_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_1:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_1 vs :::mkl23mapSi_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_2:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_2 vs :::mkl23mapSi_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_3:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_3 vs :::mkl23mapSi_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_4:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_4 vs :::mkl23mapSi_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_5:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_5 vs :::mkl23mapSi_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_6:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_6 vs :::mkl23mapSi_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_7:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_7 vs :::mkl23mapSi_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapSi_8:CHIRALITY:PLEMs2nm/TN=mkl23mapSi_8 vs :::mkl23mapSi_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas vs :::mkl23mapIngaas:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_0:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_0 vs :::mkl23mapIngaas_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_1:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_1 vs :::mkl23mapIngaas_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_2:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_2 vs :::mkl23mapIngaas_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_3:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_3 vs :::mkl23mapIngaas_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_4:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_4 vs :::mkl23mapIngaas_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_5:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_5 vs :::mkl23mapIngaas_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_6:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_6 vs :::mkl23mapIngaas_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_7:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_7 vs :::mkl23mapIngaas_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_8:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_8 vs :::mkl23mapIngaas_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_9:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_9 vs :::mkl23mapIngaas_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_10:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_10 vs :::mkl23mapIngaas_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_10_2:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_10_2 vs :::mkl23mapIngaas_10_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_11:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_11 vs :::mkl23mapIngaas_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_12:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_12 vs :::mkl23mapIngaas_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_13:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_13 vs :::mkl23mapIngaas_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_14:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_14 vs :::mkl23mapIngaas_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_15:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_15 vs :::mkl23mapIngaas_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_16:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_16 vs :::mkl23mapIngaas_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_17:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_17 vs :::mkl23mapIngaas_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_18:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_18 vs :::mkl23mapIngaas_18:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_19:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_19 vs :::mkl23mapIngaas_19:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_20:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_20 vs :::mkl23mapIngaas_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_21:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_21 vs :::mkl23mapIngaas_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_22:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_22 vs :::mkl23mapIngaas_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_23:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_23 vs :::mkl23mapIngaas_23:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_24:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_24 vs :::mkl23mapIngaas_24:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_25:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_25 vs :::mkl23mapIngaas_25:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_26:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_26 vs :::mkl23mapIngaas_26:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_27:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_27 vs :::mkl23mapIngaas_27:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_28:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_28 vs :::mkl23mapIngaas_28:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_29:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_29 vs :::mkl23mapIngaas_29:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_30:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_30 vs :::mkl23mapIngaas_30:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_31:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_31 vs :::mkl23mapIngaas_31:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_32:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_32 vs :::mkl23mapIngaas_32:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_33:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_33 vs :::mkl23mapIngaas_33:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_34:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_34 vs :::mkl23mapIngaas_34:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_35:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_35 vs :::mkl23mapIngaas_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_36:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_36 vs :::mkl23mapIngaas_36:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_37:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_37 vs :::mkl23mapIngaas_37:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_38:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_38 vs :::mkl23mapIngaas_38:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_39:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_39 vs :::mkl23mapIngaas_39:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_40:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_40 vs :::mkl23mapIngaas_40:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_41:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_41 vs :::mkl23mapIngaas_41:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_42:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_42 vs :::mkl23mapIngaas_42:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_43:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_43 vs :::mkl23mapIngaas_43:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_44:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_44 vs :::mkl23mapIngaas_44:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_45:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_45 vs :::mkl23mapIngaas_45:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_46:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_46 vs :::mkl23mapIngaas_46:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_47:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_47 vs :::mkl23mapIngaas_47:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_48:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_48 vs :::mkl23mapIngaas_48:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_49:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_49 vs :::mkl23mapIngaas_49:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_50:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_50 vs :::mkl23mapIngaas_50:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_51:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_51 vs :::mkl23mapIngaas_51:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_52:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_52 vs :::mkl23mapIngaas_52:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_53:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_53 vs :::mkl23mapIngaas_53:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_54:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_54 vs :::mkl23mapIngaas_54:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_55:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_55 vs :::mkl23mapIngaas_55:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_56:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_56 vs :::mkl23mapIngaas_56:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_57:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_57 vs :::mkl23mapIngaas_57:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_58:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_58 vs :::mkl23mapIngaas_58:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_59:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_59 vs :::mkl23mapIngaas_59:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_60:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_60 vs :::mkl23mapIngaas_60:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_61:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_61 vs :::mkl23mapIngaas_61:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_62:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_62 vs :::mkl23mapIngaas_62:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_63:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_63 vs :::mkl23mapIngaas_63:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_64:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_64 vs :::mkl23mapIngaas_64:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_65:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_65 vs :::mkl23mapIngaas_65:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_66:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_66 vs :::mkl23mapIngaas_66:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_67:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_67 vs :::mkl23mapIngaas_67:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_68:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_68 vs :::mkl23mapIngaas_68:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_69:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_69 vs :::mkl23mapIngaas_69:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl23mapIngaas_70:CHIRALITY:PLEMs2nm/TN=mkl23mapIngaas_70 vs :::mkl23mapIngaas_70:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_0:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_0 vs :::mkl24mapSi_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_1:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_1 vs :::mkl24mapSi_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_2:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_2 vs :::mkl24mapSi_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_3:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_3 vs :::mkl24mapSi_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_4:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_4 vs :::mkl24mapSi_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_5:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_5 vs :::mkl24mapSi_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_6:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_6 vs :::mkl24mapSi_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_7:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_7 vs :::mkl24mapSi_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_8:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_8 vs :::mkl24mapSi_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_9:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_9 vs :::mkl24mapSi_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_10:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_10 vs :::mkl24mapSi_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_11:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_11 vs :::mkl24mapSi_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_12:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_12 vs :::mkl24mapSi_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_13:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_13 vs :::mkl24mapSi_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_14:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_14 vs :::mkl24mapSi_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_15:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_15 vs :::mkl24mapSi_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_16:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_16 vs :::mkl24mapSi_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_17:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_17 vs :::mkl24mapSi_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_18:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_18 vs :::mkl24mapSi_18:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_20:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_20 vs :::mkl24mapSi_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_21:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_21 vs :::mkl24mapSi_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_22:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_22 vs :::mkl24mapSi_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_23:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_23 vs :::mkl24mapSi_23:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_24:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_24 vs :::mkl24mapSi_24:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_25:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_25 vs :::mkl24mapSi_25:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_26:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_26 vs :::mkl24mapSi_26:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_27:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_27 vs :::mkl24mapSi_27:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapSi_28:CHIRALITY:PLEMs2nm/TN=mkl24mapSi_28 vs :::mkl24mapSi_28:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_31:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_31 vs :::mkl24mapInGaAs_31:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_32:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_32 vs :::mkl24mapInGaAs_32:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_34:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_34 vs :::mkl24mapInGaAs_34:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_35:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_35 vs :::mkl24mapInGaAs_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_36:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_36 vs :::mkl24mapInGaAs_36:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_38:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_38 vs :::mkl24mapInGaAs_38:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_39:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_39 vs :::mkl24mapInGaAs_39:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_40:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_40 vs :::mkl24mapInGaAs_40:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_41:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_41 vs :::mkl24mapInGaAs_41:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_42:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_42 vs :::mkl24mapInGaAs_42:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_43:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_43 vs :::mkl24mapInGaAs_43:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_44:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_44 vs :::mkl24mapInGaAs_44:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_45:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_45 vs :::mkl24mapInGaAs_45:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_46:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_46 vs :::mkl24mapInGaAs_46:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_47:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_47 vs :::mkl24mapInGaAs_47:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_48:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_48 vs :::mkl24mapInGaAs_48:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_49:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_49 vs :::mkl24mapInGaAs_49:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_50:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_50 vs :::mkl24mapInGaAs_50:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_52:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_52 vs :::mkl24mapInGaAs_52:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_53:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_53 vs :::mkl24mapInGaAs_53:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_54:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_54 vs :::mkl24mapInGaAs_54:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_55:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_55 vs :::mkl24mapInGaAs_55:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_56:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_56 vs :::mkl24mapInGaAs_56:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_57:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_57 vs :::mkl24mapInGaAs_57:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_58:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_58 vs :::mkl24mapInGaAs_58:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_59:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_59 vs :::mkl24mapInGaAs_59:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_60:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_60 vs :::mkl24mapInGaAs_60:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl24mapInGaAs_61:CHIRALITY:PLEMs2nm/TN=mkl24mapInGaAs_61 vs :::mkl24mapInGaAs_61:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_0:CHIRALITY:PLEMs2nm/TN=mkl29map_0 vs :::mkl29map_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_1:CHIRALITY:PLEMs2nm/TN=mkl29map_1 vs :::mkl29map_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_2:CHIRALITY:PLEMs2nm/TN=mkl29map_2 vs :::mkl29map_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_3:CHIRALITY:PLEMs2nm/TN=mkl29map_3 vs :::mkl29map_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_4:CHIRALITY:PLEMs2nm/TN=mkl29map_4 vs :::mkl29map_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_5:CHIRALITY:PLEMs2nm/TN=mkl29map_5 vs :::mkl29map_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_7:CHIRALITY:PLEMs2nm/TN=mkl29map_7 vs :::mkl29map_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_8:CHIRALITY:PLEMs2nm/TN=mkl29map_8 vs :::mkl29map_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_9:CHIRALITY:PLEMs2nm/TN=mkl29map_9 vs :::mkl29map_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_10:CHIRALITY:PLEMs2nm/TN=mkl29map_10 vs :::mkl29map_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_11:CHIRALITY:PLEMs2nm/TN=mkl29map_11 vs :::mkl29map_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_12:CHIRALITY:PLEMs2nm/TN=mkl29map_12 vs :::mkl29map_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_13:CHIRALITY:PLEMs2nm/TN=mkl29map_13 vs :::mkl29map_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_14:CHIRALITY:PLEMs2nm/TN=mkl29map_14 vs :::mkl29map_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_15:CHIRALITY:PLEMs2nm/TN=mkl29map_15 vs :::mkl29map_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_16:CHIRALITY:PLEMs2nm/TN=mkl29map_16 vs :::mkl29map_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_17:CHIRALITY:PLEMs2nm/TN=mkl29map_17 vs :::mkl29map_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_18:CHIRALITY:PLEMs2nm/TN=mkl29map_18 vs :::mkl29map_18:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_20:CHIRALITY:PLEMs2nm/TN=mkl29map_20 vs :::mkl29map_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_21:CHIRALITY:PLEMs2nm/TN=mkl29map_21 vs :::mkl29map_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl29map_22:CHIRALITY:PLEMs2nm/TN=mkl29map_22 vs :::mkl29map_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_0:CHIRALITY:PLEMs2nm/TN=mkl33map_air_0 vs :::mkl33map_air_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_1:CHIRALITY:PLEMs2nm/TN=mkl33map_air_1 vs :::mkl33map_air_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_2:CHIRALITY:PLEMs2nm/TN=mkl33map_air_2 vs :::mkl33map_air_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_3:CHIRALITY:PLEMs2nm/TN=mkl33map_air_3 vs :::mkl33map_air_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_4:CHIRALITY:PLEMs2nm/TN=mkl33map_air_4 vs :::mkl33map_air_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_5:CHIRALITY:PLEMs2nm/TN=mkl33map_air_5 vs :::mkl33map_air_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_6:CHIRALITY:PLEMs2nm/TN=mkl33map_air_6 vs :::mkl33map_air_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_7:CHIRALITY:PLEMs2nm/TN=mkl33map_air_7 vs :::mkl33map_air_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl33map_air_8:CHIRALITY:PLEMs2nm/TN=mkl33map_air_8 vs :::mkl33map_air_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_0:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_0 vs :::mkl34mapInGaAs_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_2:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_2 vs :::mkl34mapInGaAs_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_3:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_3 vs :::mkl34mapInGaAs_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_4:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_4 vs :::mkl34mapInGaAs_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_5:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_5 vs :::mkl34mapInGaAs_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_6:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_6 vs :::mkl34mapInGaAs_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_7:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_7 vs :::mkl34mapInGaAs_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_8:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_8 vs :::mkl34mapInGaAs_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_9:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_9 vs :::mkl34mapInGaAs_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_10:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_10 vs :::mkl34mapInGaAs_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_11:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_11 vs :::mkl34mapInGaAs_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_12:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_12 vs :::mkl34mapInGaAs_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_13:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_13 vs :::mkl34mapInGaAs_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_14:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_14 vs :::mkl34mapInGaAs_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_15:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_15 vs :::mkl34mapInGaAs_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_16:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_16 vs :::mkl34mapInGaAs_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_17:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_17 vs :::mkl34mapInGaAs_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_18:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_18 vs :::mkl34mapInGaAs_18:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_19:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_19 vs :::mkl34mapInGaAs_19:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_20:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_20 vs :::mkl34mapInGaAs_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_21:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_21 vs :::mkl34mapInGaAs_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_22:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_22 vs :::mkl34mapInGaAs_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_23:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_23 vs :::mkl34mapInGaAs_23:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_24:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_24 vs :::mkl34mapInGaAs_24:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_25:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_25 vs :::mkl34mapInGaAs_25:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_26:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_26 vs :::mkl34mapInGaAs_26:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_27:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_27 vs :::mkl34mapInGaAs_27:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_28:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_28 vs :::mkl34mapInGaAs_28:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_29:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_29 vs :::mkl34mapInGaAs_29:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_30:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_30 vs :::mkl34mapInGaAs_30:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_31:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_31 vs :::mkl34mapInGaAs_31:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_32:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_32 vs :::mkl34mapInGaAs_32:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_33:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_33 vs :::mkl34mapInGaAs_33:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_34:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_34 vs :::mkl34mapInGaAs_34:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_35:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_35 vs :::mkl34mapInGaAs_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_36:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_36 vs :::mkl34mapInGaAs_36:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_37:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_37 vs :::mkl34mapInGaAs_37:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_38:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_38 vs :::mkl34mapInGaAs_38:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_39:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_39 vs :::mkl34mapInGaAs_39:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_40:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_40 vs :::mkl34mapInGaAs_40:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_42:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_42 vs :::mkl34mapInGaAs_42:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapInGaAs_43:CHIRALITY:PLEMs2nm/TN=mkl34mapInGaAs_43 vs :::mkl34mapInGaAs_43:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_1:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_1 vs :::mkl34mapSi_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_2:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_2 vs :::mkl34mapSi_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_3:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_3 vs :::mkl34mapSi_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_4:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_4 vs :::mkl34mapSi_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_5:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_5 vs :::mkl34mapSi_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_6:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_6 vs :::mkl34mapSi_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_7:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_7 vs :::mkl34mapSi_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_8:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_8 vs :::mkl34mapSi_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_9:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_9 vs :::mkl34mapSi_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_10:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_10 vs :::mkl34mapSi_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_11:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_11 vs :::mkl34mapSi_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_12:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_12 vs :::mkl34mapSi_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_13:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_13 vs :::mkl34mapSi_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_14:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_14 vs :::mkl34mapSi_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_15:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_15 vs :::mkl34mapSi_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_16:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_16 vs :::mkl34mapSi_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_17:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_17 vs :::mkl34mapSi_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_18:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_18 vs :::mkl34mapSi_18:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_19:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_19 vs :::mkl34mapSi_19:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_20:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_20 vs :::mkl34mapSi_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_21:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_21 vs :::mkl34mapSi_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_22:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_22 vs :::mkl34mapSi_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_23:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_23 vs :::mkl34mapSi_23:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_24:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_24 vs :::mkl34mapSi_24:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_25:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_25 vs :::mkl34mapSi_25:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_26:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_26 vs :::mkl34mapSi_26:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_27:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_27 vs :::mkl34mapSi_27:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_28:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_28 vs :::mkl34mapSi_28:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_29:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_29 vs :::mkl34mapSi_29:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_30:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_30 vs :::mkl34mapSi_30:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_31:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_31 vs :::mkl34mapSi_31:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_32:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_32 vs :::mkl34mapSi_32:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_33:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_33 vs :::mkl34mapSi_33:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_34:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_34 vs :::mkl34mapSi_34:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_35:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_35 vs :::mkl34mapSi_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_36:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_36 vs :::mkl34mapSi_36:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_37:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_37 vs :::mkl34mapSi_37:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_38:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_38 vs :::mkl34mapSi_38:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_39:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_39 vs :::mkl34mapSi_39:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_40:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_40 vs :::mkl34mapSi_40:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_41:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_41 vs :::mkl34mapSi_41:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_42:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_42 vs :::mkl34mapSi_42:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_43:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_43 vs :::mkl34mapSi_43:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_44:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_44 vs :::mkl34mapSi_44:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_45:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_45 vs :::mkl34mapSi_45:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_46:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_46 vs :::mkl34mapSi_46:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_47:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_47 vs :::mkl34mapSi_47:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_49:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_49 vs :::mkl34mapSi_49:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_50:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_50 vs :::mkl34mapSi_50:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_51:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_51 vs :::mkl34mapSi_51:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_52:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_52 vs :::mkl34mapSi_52:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_53:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_53 vs :::mkl34mapSi_53:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_54:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_54 vs :::mkl34mapSi_54:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_55:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_55 vs :::mkl34mapSi_55:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_56:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_56 vs :::mkl34mapSi_56:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_57:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_57 vs :::mkl34mapSi_57:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_58:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_58 vs :::mkl34mapSi_58:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_59:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_59 vs :::mkl34mapSi_59:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_60:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_60 vs :::mkl34mapSi_60:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_61:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_61 vs :::mkl34mapSi_61:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_62:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_62 vs :::mkl34mapSi_62:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_63:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_63 vs :::mkl34mapSi_63:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_64:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_64 vs :::mkl34mapSi_64:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_65:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_65 vs :::mkl34mapSi_65:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_66:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_66 vs :::mkl34mapSi_66:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_67:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_67 vs :::mkl34mapSi_67:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_68:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_68 vs :::mkl34mapSi_68:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_69:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_69 vs :::mkl34mapSi_69:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_70:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_70 vs :::mkl34mapSi_70:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_71:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_71 vs :::mkl34mapSi_71:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_72:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_72 vs :::mkl34mapSi_72:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl34mapSi_73:CHIRALITY:PLEMs2nm/TN=mkl34mapSi_73 vs :::mkl34mapSi_73:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_0:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_0 vs :::mkl44TRSimap_air_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_1:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_1 vs :::mkl44TRSimap_air_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_2:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_2 vs :::mkl44TRSimap_air_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_3:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_3 vs :::mkl44TRSimap_air_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_4:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_4 vs :::mkl44TRSimap_air_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_5:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_5 vs :::mkl44TRSimap_air_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_6:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_6 vs :::mkl44TRSimap_air_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_7:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_7 vs :::mkl44TRSimap_air_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_8:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_8 vs :::mkl44TRSimap_air_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_9:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_9 vs :::mkl44TRSimap_air_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_11:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_11 vs :::mkl44TRSimap_air_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_12:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_12 vs :::mkl44TRSimap_air_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_13:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_13 vs :::mkl44TRSimap_air_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_14:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_14 vs :::mkl44TRSimap_air_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_15:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_15 vs :::mkl44TRSimap_air_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_16:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_16 vs :::mkl44TRSimap_air_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_17:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_17 vs :::mkl44TRSimap_air_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_18:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_18 vs :::mkl44TRSimap_air_18:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_19:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_19 vs :::mkl44TRSimap_air_19:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_20:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_20 vs :::mkl44TRSimap_air_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_21:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_21 vs :::mkl44TRSimap_air_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_22:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_22 vs :::mkl44TRSimap_air_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_23:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_23 vs :::mkl44TRSimap_air_23:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_24:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_24 vs :::mkl44TRSimap_air_24:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_25:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_25 vs :::mkl44TRSimap_air_25:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_26:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_26 vs :::mkl44TRSimap_air_26:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_27:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_27 vs :::mkl44TRSimap_air_27:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_29:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_29 vs :::mkl44TRSimap_air_29:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_30:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_30 vs :::mkl44TRSimap_air_30:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_31:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_31 vs :::mkl44TRSimap_air_31:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_32:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_32 vs :::mkl44TRSimap_air_32:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_33:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_33 vs :::mkl44TRSimap_air_33:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_34:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_34 vs :::mkl44TRSimap_air_34:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_35:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_35 vs :::mkl44TRSimap_air_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_36:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_36 vs :::mkl44TRSimap_air_36:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_37:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_37 vs :::mkl44TRSimap_air_37:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_38:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_38 vs :::mkl44TRSimap_air_38:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_39:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_39 vs :::mkl44TRSimap_air_39:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_40:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_40 vs :::mkl44TRSimap_air_40:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_41:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_41 vs :::mkl44TRSimap_air_41:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_42:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_42 vs :::mkl44TRSimap_air_42:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_43:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_43 vs :::mkl44TRSimap_air_43:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_44:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_44 vs :::mkl44TRSimap_air_44:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_45:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_45 vs :::mkl44TRSimap_air_45:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_46:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_46 vs :::mkl44TRSimap_air_46:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_47:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_47 vs :::mkl44TRSimap_air_47:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_48:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_48 vs :::mkl44TRSimap_air_48:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_49:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_49 vs :::mkl44TRSimap_air_49:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_50:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_50 vs :::mkl44TRSimap_air_50:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_51:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_51 vs :::mkl44TRSimap_air_51:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_52:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_52 vs :::mkl44TRSimap_air_52:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRSimap_air_54:CHIRALITY:PLEMs2nm/TN=mkl44TRSimap_air_54 vs :::mkl44TRSimap_air_54:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_0:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_0 vs :::mkl44TRInGaAsmap_air_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_1:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_1 vs :::mkl44TRInGaAsmap_air_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_2:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_2 vs :::mkl44TRInGaAsmap_air_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_3:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_3 vs :::mkl44TRInGaAsmap_air_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_4:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_4 vs :::mkl44TRInGaAsmap_air_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_5:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_5 vs :::mkl44TRInGaAsmap_air_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_6:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_6 vs :::mkl44TRInGaAsmap_air_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_7:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_7 vs :::mkl44TRInGaAsmap_air_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_8:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_8 vs :::mkl44TRInGaAsmap_air_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_9:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_9 vs :::mkl44TRInGaAsmap_air_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_10:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_10 vs :::mkl44TRInGaAsmap_air_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_11:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_11 vs :::mkl44TRInGaAsmap_air_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_12:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_12 vs :::mkl44TRInGaAsmap_air_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_13:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_13 vs :::mkl44TRInGaAsmap_air_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_14:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_14 vs :::mkl44TRInGaAsmap_air_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_15:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_15 vs :::mkl44TRInGaAsmap_air_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_16:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_16 vs :::mkl44TRInGaAsmap_air_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_17:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_17 vs :::mkl44TRInGaAsmap_air_17:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_19:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_19 vs :::mkl44TRInGaAsmap_air_19:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_20:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_20 vs :::mkl44TRInGaAsmap_air_20:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_21:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_21 vs :::mkl44TRInGaAsmap_air_21:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_22:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_22 vs :::mkl44TRInGaAsmap_air_22:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_23:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_23 vs :::mkl44TRInGaAsmap_air_23:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_24:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_24 vs :::mkl44TRInGaAsmap_air_24:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_25:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_25 vs :::mkl44TRInGaAsmap_air_25:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_26:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_26 vs :::mkl44TRInGaAsmap_air_26:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_27:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_27 vs :::mkl44TRInGaAsmap_air_27:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_28:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_28 vs :::mkl44TRInGaAsmap_air_28:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_29:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_29 vs :::mkl44TRInGaAsmap_air_29:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_30:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_30 vs :::mkl44TRInGaAsmap_air_30:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_31:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_31 vs :::mkl44TRInGaAsmap_air_31:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_32:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_32 vs :::mkl44TRInGaAsmap_air_32:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_33:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_33 vs :::mkl44TRInGaAsmap_air_33:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_34:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_34 vs :::mkl44TRInGaAsmap_air_34:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_35:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_35 vs :::mkl44TRInGaAsmap_air_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_36:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_36 vs :::mkl44TRInGaAsmap_air_36:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_37:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_37 vs :::mkl44TRInGaAsmap_air_37:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_38:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_38 vs :::mkl44TRInGaAsmap_air_38:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_39:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_39 vs :::mkl44TRInGaAsmap_air_39:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_40:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_40 vs :::mkl44TRInGaAsmap_air_40:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_41:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_41 vs :::mkl44TRInGaAsmap_air_41:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_42:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_42 vs :::mkl44TRInGaAsmap_air_42:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl44TRInGaAsmap_air_43:CHIRALITY:PLEMs2nm/TN=mkl44TRInGaAsmap_air_43 vs :::mkl44TRInGaAsmap_air_43:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_0:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_0 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_1:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_1 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_2:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_2 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_3:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_3 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_4:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_4 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_5:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_5 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_6:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_6 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_7:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_7 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_8:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_8 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_9:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_9 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_10:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_10 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_10:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_11:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_11 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_11:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_12:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_12 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_13:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_13 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_13:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_14:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_14 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_14:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_15:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_15 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_15:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl47exactcoormkl47TRexactcoordinates_map_air_16:CHIRALITY:PLEMs2nm/TN=mkl47exactcoormkl47TRexactcoordinates_map_air_16 vs :::mkl47exactcoormkl47TRexactcoordinates_map_air_16:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl50Lexactcoordinates_map_2:CHIRALITY:PLEMs2nm/TN=mkl50Lexactcoordinates_map_2 vs :::mkl50Lexactcoordinates_map_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl50Lexactcoordinates_map_12:CHIRALITY:PLEMs2nm/TN=mkl50Lexactcoordinates_map_12 vs :::mkl50Lexactcoordinates_map_12:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl50Lexactcoordinates_map_30:CHIRALITY:PLEMs2nm/TN=mkl50Lexactcoordinates_map_30 vs :::mkl50Lexactcoordinates_map_30:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl50Lexactcoordinates_map_35:CHIRALITY:PLEMs2nm/TN=mkl50Lexactcoordinates_map_35 vs :::mkl50Lexactcoordinates_map_35:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_0:CHIRALITY:PLEMs2nm/TN=mkl51map_0 vs :::mkl51map_0:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_1:CHIRALITY:PLEMs2nm/TN=mkl51map_1 vs :::mkl51map_1:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_2:CHIRALITY:PLEMs2nm/TN=mkl51map_2 vs :::mkl51map_2:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_3:CHIRALITY:PLEMs2nm/TN=mkl51map_3 vs :::mkl51map_3:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_4:CHIRALITY:PLEMs2nm/TN=mkl51map_4 vs :::mkl51map_4:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_5:CHIRALITY:PLEMs2nm/TN=mkl51map_5 vs :::mkl51map_5:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_6:CHIRALITY:PLEMs2nm/TN=mkl51map_6 vs :::mkl51map_6:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_7:CHIRALITY:PLEMs2nm/TN=mkl51map_7 vs :::mkl51map_7:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_8:CHIRALITY:PLEMs2nm/TN=mkl51map_8 vs :::mkl51map_8:CHIRALITY:PLEMs1nm
	AppendToGraph :::mkl51map_9:CHIRALITY:PLEMs2nm/TN=mkl51map_9 vs :::mkl51map_9:CHIRALITY:PLEMs1nm
	AppendToGraph :::::wavelength_air[*][0]/TN=coordinates vs :::::wavelength_air[*][1]
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=72,margin(bottom)=56,margin(top)=14,margin(right)=72,width={Plan,1,bottom,left}
	ModifyGraph gbRGB=(61166,61166,61166)
	ModifyGraph mode=3
	ModifyGraph marker(mkl22map_0)=19,marker(mkl22map_1)=19,marker(mkl22map_2)=19,marker(mkl22map_3)=19
	ModifyGraph marker(mkl22map_4)=19,marker(mkl22map_5)=19,marker(mkl22map_6)=19,marker(mkl22map_7)=19
	ModifyGraph marker(mkl23mapSi)=19,marker(mkl23mapSi_0)=19,marker(mkl23mapSi_1)=19
	ModifyGraph marker(mkl23mapSi_2)=19,marker(mkl23mapSi_3)=19,marker(mkl23mapSi_4)=19
	ModifyGraph marker(mkl23mapSi_5)=19,marker(mkl23mapSi_6)=19,marker(mkl23mapSi_7)=19
	ModifyGraph marker(mkl23mapSi_8)=19,marker(mkl23mapIngaas)=19,marker(mkl23mapIngaas_0)=19
	ModifyGraph marker(mkl23mapIngaas_1)=19,marker(mkl23mapIngaas_2)=19,marker(mkl23mapIngaas_3)=19
	ModifyGraph marker(mkl23mapIngaas_4)=19,marker(mkl23mapIngaas_5)=19,marker(mkl23mapIngaas_6)=19
	ModifyGraph marker(mkl23mapIngaas_7)=19,marker(mkl23mapIngaas_8)=19,marker(mkl23mapIngaas_9)=19
	ModifyGraph marker(mkl23mapIngaas_10)=19,marker(mkl23mapIngaas_10_2)=19,marker(mkl23mapIngaas_11)=19
	ModifyGraph marker(mkl23mapIngaas_12)=19,marker(mkl23mapIngaas_13)=19,marker(mkl23mapIngaas_14)=19
	ModifyGraph marker(mkl23mapIngaas_15)=19,marker(mkl23mapIngaas_16)=19,marker(mkl23mapIngaas_17)=19
	ModifyGraph marker(mkl23mapIngaas_18)=19,marker(mkl23mapIngaas_19)=19,marker(mkl23mapIngaas_20)=19
	ModifyGraph marker(mkl23mapIngaas_21)=19,marker(mkl23mapIngaas_22)=19,marker(mkl23mapIngaas_23)=19
	ModifyGraph marker(mkl23mapIngaas_24)=19,marker(mkl23mapIngaas_25)=19,marker(mkl23mapIngaas_26)=19
	ModifyGraph marker(mkl23mapIngaas_27)=19,marker(mkl23mapIngaas_28)=19,marker(mkl23mapIngaas_29)=19
	ModifyGraph marker(mkl23mapIngaas_30)=19,marker(mkl23mapIngaas_31)=19,marker(mkl23mapIngaas_32)=19
	ModifyGraph marker(mkl23mapIngaas_33)=19,marker(mkl23mapIngaas_34)=19,marker(mkl23mapIngaas_35)=19
	ModifyGraph marker(mkl23mapIngaas_36)=19,marker(mkl23mapIngaas_37)=19,marker(mkl23mapIngaas_38)=19
	ModifyGraph marker(mkl23mapIngaas_39)=19,marker(mkl23mapIngaas_40)=19,marker(mkl23mapIngaas_41)=19
	ModifyGraph marker(mkl23mapIngaas_42)=19,marker(mkl23mapIngaas_43)=19,marker(mkl23mapIngaas_44)=19
	ModifyGraph marker(mkl23mapIngaas_45)=19,marker(mkl23mapIngaas_46)=19,marker(mkl23mapIngaas_47)=19
	ModifyGraph marker(mkl23mapIngaas_48)=19,marker(mkl23mapIngaas_49)=19,marker(mkl23mapIngaas_50)=19
	ModifyGraph marker(mkl23mapIngaas_51)=19,marker(mkl23mapIngaas_52)=19,marker(mkl23mapIngaas_53)=19
	ModifyGraph marker(mkl23mapIngaas_54)=19,marker(mkl23mapIngaas_55)=19,marker(mkl23mapIngaas_56)=19
	ModifyGraph marker(mkl23mapIngaas_57)=19,marker(mkl23mapIngaas_58)=19,marker(mkl23mapIngaas_59)=19
	ModifyGraph marker(mkl23mapIngaas_60)=19,marker(mkl23mapIngaas_61)=19,marker(mkl23mapIngaas_62)=19
	ModifyGraph marker(mkl23mapIngaas_63)=19,marker(mkl23mapIngaas_64)=19,marker(mkl23mapIngaas_65)=19
	ModifyGraph marker(mkl23mapIngaas_66)=19,marker(mkl23mapIngaas_67)=19,marker(mkl23mapIngaas_68)=19
	ModifyGraph marker(mkl23mapIngaas_69)=19,marker(mkl23mapIngaas_70)=19,marker(mkl24mapSi_0)=19
	ModifyGraph marker(mkl24mapSi_1)=19,marker(mkl24mapSi_2)=19,marker(mkl24mapSi_3)=19
	ModifyGraph marker(mkl24mapSi_4)=19,marker(mkl24mapSi_5)=19,marker(mkl24mapSi_6)=19
	ModifyGraph marker(mkl24mapSi_7)=19,marker(mkl24mapSi_8)=19,marker(mkl24mapSi_9)=19
	ModifyGraph marker(mkl24mapSi_10)=19,marker(mkl24mapSi_11)=19,marker(mkl24mapSi_12)=19
	ModifyGraph marker(mkl24mapSi_13)=19,marker(mkl24mapSi_14)=19,marker(mkl24mapSi_15)=19
	ModifyGraph marker(mkl24mapSi_16)=19,marker(mkl24mapSi_17)=19,marker(mkl24mapSi_18)=19
	ModifyGraph marker(mkl24mapSi_20)=19,marker(mkl24mapSi_21)=19,marker(mkl24mapSi_22)=19
	ModifyGraph marker(mkl24mapSi_23)=19,marker(mkl24mapSi_24)=19,marker(mkl24mapSi_25)=19
	ModifyGraph marker(mkl24mapSi_26)=19,marker(mkl24mapSi_27)=19,marker(mkl24mapSi_28)=19
	ModifyGraph marker(mkl24mapInGaAs_31)=19,marker(mkl24mapInGaAs_32)=19,marker(mkl24mapInGaAs_34)=19
	ModifyGraph marker(mkl24mapInGaAs_35)=19,marker(mkl24mapInGaAs_36)=19,marker(mkl24mapInGaAs_38)=19
	ModifyGraph marker(mkl24mapInGaAs_39)=19,marker(mkl24mapInGaAs_40)=19,marker(mkl24mapInGaAs_41)=19
	ModifyGraph marker(mkl24mapInGaAs_42)=19,marker(mkl24mapInGaAs_43)=19,marker(mkl24mapInGaAs_44)=19
	ModifyGraph marker(mkl24mapInGaAs_45)=19,marker(mkl24mapInGaAs_46)=19,marker(mkl24mapInGaAs_47)=19
	ModifyGraph marker(mkl24mapInGaAs_48)=19,marker(mkl24mapInGaAs_49)=19,marker(mkl24mapInGaAs_50)=19
	ModifyGraph marker(mkl24mapInGaAs_52)=19,marker(mkl24mapInGaAs_53)=19,marker(mkl24mapInGaAs_54)=19
	ModifyGraph marker(mkl24mapInGaAs_55)=19,marker(mkl24mapInGaAs_56)=19,marker(mkl24mapInGaAs_57)=19
	ModifyGraph marker(mkl24mapInGaAs_58)=19,marker(mkl24mapInGaAs_59)=19,marker(mkl24mapInGaAs_60)=19
	ModifyGraph marker(mkl24mapInGaAs_61)=19,marker(mkl29map_0)=19,marker(mkl29map_1)=19
	ModifyGraph marker(mkl29map_2)=19,marker(mkl29map_3)=19,marker(mkl29map_4)=19,marker(mkl29map_5)=19
	ModifyGraph marker(mkl29map_7)=19,marker(mkl29map_8)=19,marker(mkl29map_9)=19,marker(mkl29map_10)=19
	ModifyGraph marker(mkl29map_11)=19,marker(mkl29map_12)=19,marker(mkl29map_13)=19
	ModifyGraph marker(mkl29map_14)=19,marker(mkl29map_15)=19,marker(mkl29map_16)=19
	ModifyGraph marker(mkl29map_17)=19,marker(mkl29map_18)=19,marker(mkl29map_20)=19
	ModifyGraph marker(mkl29map_21)=19,marker(mkl29map_22)=19,marker(mkl33map_air_0)=19
	ModifyGraph marker(mkl33map_air_1)=19,marker(mkl33map_air_2)=19,marker(mkl33map_air_3)=19
	ModifyGraph marker(mkl33map_air_4)=19,marker(mkl33map_air_5)=19,marker(mkl33map_air_6)=19
	ModifyGraph marker(mkl33map_air_7)=19,marker(mkl33map_air_8)=19,marker(mkl34mapInGaAs_0)=19
	ModifyGraph marker(mkl34mapInGaAs_2)=19,marker(mkl34mapInGaAs_3)=19,marker(mkl34mapInGaAs_4)=19
	ModifyGraph marker(mkl34mapInGaAs_5)=19,marker(mkl34mapInGaAs_6)=19,marker(mkl34mapInGaAs_7)=19
	ModifyGraph marker(mkl34mapInGaAs_8)=19,marker(mkl34mapInGaAs_9)=19,marker(mkl34mapInGaAs_10)=19
	ModifyGraph marker(mkl34mapInGaAs_11)=19,marker(mkl34mapInGaAs_12)=19,marker(mkl34mapInGaAs_13)=19
	ModifyGraph marker(mkl34mapInGaAs_14)=19,marker(mkl34mapInGaAs_15)=19,marker(mkl34mapInGaAs_16)=19
	ModifyGraph marker(mkl34mapInGaAs_17)=19,marker(mkl34mapInGaAs_18)=19,marker(mkl34mapInGaAs_19)=19
	ModifyGraph marker(mkl34mapInGaAs_20)=19,marker(mkl34mapInGaAs_21)=19,marker(mkl34mapInGaAs_22)=19
	ModifyGraph marker(mkl34mapInGaAs_23)=19,marker(mkl34mapInGaAs_24)=19,marker(mkl34mapInGaAs_25)=19
	ModifyGraph marker(mkl34mapInGaAs_26)=19,marker(mkl34mapInGaAs_27)=19,marker(mkl34mapInGaAs_28)=19
	ModifyGraph marker(mkl34mapInGaAs_29)=19,marker(mkl34mapInGaAs_30)=19,marker(mkl34mapInGaAs_31)=19
	ModifyGraph marker(mkl34mapInGaAs_32)=19,marker(mkl34mapInGaAs_33)=19,marker(mkl34mapInGaAs_34)=19
	ModifyGraph marker(mkl34mapInGaAs_35)=19,marker(mkl34mapInGaAs_36)=19,marker(mkl34mapInGaAs_37)=19
	ModifyGraph marker(mkl34mapInGaAs_38)=19,marker(mkl34mapInGaAs_39)=19,marker(mkl34mapInGaAs_40)=19
	ModifyGraph marker(mkl34mapInGaAs_42)=19,marker(mkl34mapInGaAs_43)=19,marker(mkl34mapSi_1)=19
	ModifyGraph marker(mkl34mapSi_2)=19,marker(mkl34mapSi_3)=19,marker(mkl34mapSi_4)=19
	ModifyGraph marker(mkl34mapSi_5)=19,marker(mkl34mapSi_6)=19,marker(mkl34mapSi_7)=19
	ModifyGraph marker(mkl34mapSi_8)=19,marker(mkl34mapSi_9)=19,marker(mkl34mapSi_10)=19
	ModifyGraph marker(mkl34mapSi_11)=19,marker(mkl34mapSi_12)=19,marker(mkl34mapSi_13)=19
	ModifyGraph marker(mkl34mapSi_14)=19,marker(mkl34mapSi_15)=19,marker(mkl34mapSi_16)=19
	ModifyGraph marker(mkl34mapSi_17)=19,marker(mkl34mapSi_18)=19,marker(mkl34mapSi_19)=19
	ModifyGraph marker(mkl34mapSi_20)=19,marker(mkl34mapSi_21)=19,marker(mkl34mapSi_22)=19
	ModifyGraph marker(mkl34mapSi_23)=19,marker(mkl34mapSi_24)=19,marker(mkl34mapSi_25)=19
	ModifyGraph marker(mkl34mapSi_26)=19,marker(mkl34mapSi_27)=19,marker(mkl34mapSi_28)=19
	ModifyGraph marker(mkl34mapSi_29)=19,marker(mkl34mapSi_30)=19,marker(mkl34mapSi_31)=19
	ModifyGraph marker(mkl34mapSi_32)=19,marker(mkl34mapSi_33)=19,marker(mkl34mapSi_34)=19
	ModifyGraph marker(mkl34mapSi_35)=19,marker(mkl34mapSi_36)=19,marker(mkl34mapSi_37)=19
	ModifyGraph marker(mkl34mapSi_38)=19,marker(mkl34mapSi_39)=19,marker(mkl34mapSi_40)=19
	ModifyGraph marker(mkl34mapSi_41)=19,marker(mkl34mapSi_42)=19,marker(mkl34mapSi_43)=19
	ModifyGraph marker(mkl34mapSi_44)=19,marker(mkl34mapSi_45)=19,marker(mkl34mapSi_46)=19
	ModifyGraph marker(mkl34mapSi_47)=19,marker(mkl34mapSi_49)=19,marker(mkl34mapSi_50)=19
	ModifyGraph marker(mkl34mapSi_51)=19,marker(mkl34mapSi_52)=19,marker(mkl34mapSi_53)=19
	ModifyGraph marker(mkl34mapSi_54)=19,marker(mkl34mapSi_55)=19,marker(mkl34mapSi_56)=19
	ModifyGraph marker(mkl34mapSi_57)=19,marker(mkl34mapSi_58)=19,marker(mkl34mapSi_59)=19
	ModifyGraph marker(mkl34mapSi_60)=19,marker(mkl34mapSi_61)=19,marker(mkl34mapSi_62)=19
	ModifyGraph marker(mkl34mapSi_63)=19,marker(mkl34mapSi_64)=19,marker(mkl34mapSi_65)=19
	ModifyGraph marker(mkl34mapSi_66)=19,marker(mkl34mapSi_67)=19,marker(mkl34mapSi_68)=19
	ModifyGraph marker(mkl34mapSi_69)=19,marker(mkl34mapSi_70)=19,marker(mkl34mapSi_71)=19
	ModifyGraph marker(mkl34mapSi_72)=19,marker(mkl34mapSi_73)=19,marker(mkl44TRSimap_air_0)=19
	ModifyGraph marker(mkl44TRSimap_air_1)=19,marker(mkl44TRSimap_air_2)=19,marker(mkl44TRSimap_air_3)=19
	ModifyGraph marker(mkl44TRSimap_air_4)=19,marker(mkl44TRSimap_air_5)=19,marker(mkl44TRSimap_air_6)=19
	ModifyGraph marker(mkl44TRSimap_air_7)=19,marker(mkl44TRSimap_air_8)=19,marker(mkl44TRSimap_air_9)=19
	ModifyGraph marker(mkl44TRSimap_air_11)=19,marker(mkl44TRSimap_air_12)=19,marker(mkl44TRSimap_air_13)=19
	ModifyGraph marker(mkl44TRSimap_air_14)=19,marker(mkl44TRSimap_air_15)=19,marker(mkl44TRSimap_air_16)=19
	ModifyGraph marker(mkl44TRSimap_air_17)=19,marker(mkl44TRSimap_air_18)=19,marker(mkl44TRSimap_air_19)=19
	ModifyGraph marker(mkl44TRSimap_air_20)=19,marker(mkl44TRSimap_air_21)=19,marker(mkl44TRSimap_air_22)=19
	ModifyGraph marker(mkl44TRSimap_air_23)=19,marker(mkl44TRSimap_air_24)=19,marker(mkl44TRSimap_air_25)=19
	ModifyGraph marker(mkl44TRSimap_air_26)=19,marker(mkl44TRSimap_air_27)=19,marker(mkl44TRSimap_air_29)=19
	ModifyGraph marker(mkl44TRSimap_air_30)=19,marker(mkl44TRSimap_air_31)=19,marker(mkl44TRSimap_air_32)=19
	ModifyGraph marker(mkl44TRSimap_air_33)=19,marker(mkl44TRSimap_air_34)=19,marker(mkl44TRSimap_air_35)=19
	ModifyGraph marker(mkl44TRSimap_air_36)=19,marker(mkl44TRSimap_air_37)=19,marker(mkl44TRSimap_air_38)=19
	ModifyGraph marker(mkl44TRSimap_air_39)=19,marker(mkl44TRSimap_air_40)=19,marker(mkl44TRSimap_air_41)=19
	ModifyGraph marker(mkl44TRSimap_air_42)=19,marker(mkl44TRSimap_air_43)=19,marker(mkl44TRSimap_air_44)=19
	ModifyGraph marker(mkl44TRSimap_air_45)=19,marker(mkl44TRSimap_air_46)=19,marker(mkl44TRSimap_air_47)=19
	ModifyGraph marker(mkl44TRSimap_air_48)=19,marker(mkl44TRSimap_air_49)=19,marker(mkl44TRSimap_air_50)=19
	ModifyGraph marker(mkl44TRSimap_air_51)=19,marker(mkl44TRSimap_air_52)=19,marker(mkl44TRSimap_air_54)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_0)=19,marker(mkl44TRInGaAsmap_air_1)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_2)=19,marker(mkl44TRInGaAsmap_air_3)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_4)=19,marker(mkl44TRInGaAsmap_air_5)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_6)=19,marker(mkl44TRInGaAsmap_air_7)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_8)=19,marker(mkl44TRInGaAsmap_air_9)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_10)=19,marker(mkl44TRInGaAsmap_air_11)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_12)=19,marker(mkl44TRInGaAsmap_air_13)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_14)=19,marker(mkl44TRInGaAsmap_air_15)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_16)=19,marker(mkl44TRInGaAsmap_air_17)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_19)=19,marker(mkl44TRInGaAsmap_air_20)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_21)=19,marker(mkl44TRInGaAsmap_air_22)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_23)=19,marker(mkl44TRInGaAsmap_air_24)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_25)=19,marker(mkl44TRInGaAsmap_air_26)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_27)=19,marker(mkl44TRInGaAsmap_air_28)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_29)=19,marker(mkl44TRInGaAsmap_air_30)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_31)=19,marker(mkl44TRInGaAsmap_air_32)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_33)=19,marker(mkl44TRInGaAsmap_air_34)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_35)=19,marker(mkl44TRInGaAsmap_air_36)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_37)=19,marker(mkl44TRInGaAsmap_air_38)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_39)=19,marker(mkl44TRInGaAsmap_air_40)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_41)=19,marker(mkl44TRInGaAsmap_air_42)=19
	ModifyGraph marker(mkl44TRInGaAsmap_air_43)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_0)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_1)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_2)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_3)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_4)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_5)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_6)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_7)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_8)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_9)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_10)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_11)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_12)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_13)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_14)=19
	ModifyGraph marker(mkl47exactcoormkl47TRexactcoordinates_map_air_15)=19,marker(mkl47exactcoormkl47TRexactcoordinates_map_air_16)=19
	ModifyGraph marker(mkl50Lexactcoordinates_map_2)=19,marker(mkl50Lexactcoordinates_map_12)=19
	ModifyGraph marker(mkl50Lexactcoordinates_map_30)=19,marker(mkl50Lexactcoordinates_map_35)=19
	ModifyGraph marker(mkl51map_0)=19,marker(mkl51map_1)=19,marker(mkl51map_2)=19,marker(mkl51map_3)=19
	ModifyGraph marker(mkl51map_4)=19,marker(mkl51map_5)=19,marker(mkl51map_6)=19,marker(mkl51map_7)=19
	ModifyGraph marker(mkl51map_8)=19,marker(mkl51map_9)=19,marker(coordinates)=8
	ModifyGraph lSize=1.2
	ModifyGraph rgb(mkl22map_0)=(49151,53155,65535),rgb(mkl22map_1)=(49151,53155,65535)
	ModifyGraph rgb(mkl22map_2)=(49151,53155,65535),rgb(mkl22map_3)=(49151,53155,65535)
	ModifyGraph rgb(mkl22map_4)=(49151,53155,65535),rgb(mkl22map_5)=(49151,53155,65535)
	ModifyGraph rgb(mkl22map_6)=(49151,53155,65535),rgb(mkl22map_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapSi)=(49151,53155,65535),rgb(mkl23mapSi_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapSi_1)=(49151,53155,65535),rgb(mkl23mapSi_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapSi_3)=(49151,53155,65535),rgb(mkl23mapSi_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapSi_5)=(49151,53155,65535),rgb(mkl23mapSi_6)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapSi_7)=(49151,53155,65535),rgb(mkl23mapSi_8)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas)=(49151,53155,65535),rgb(mkl23mapIngaas_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_1)=(49151,53155,65535),rgb(mkl23mapIngaas_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_3)=(49151,53155,65535),rgb(mkl23mapIngaas_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_5)=(49151,53155,65535),rgb(mkl23mapIngaas_6)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_7)=(49151,53155,65535),rgb(mkl23mapIngaas_8)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_9)=(49151,53155,65535),rgb(mkl23mapIngaas_10)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_10_2)=(49151,53155,65535),rgb(mkl23mapIngaas_11)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_12)=(49151,53155,65535),rgb(mkl23mapIngaas_13)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_14)=(49151,53155,65535),rgb(mkl23mapIngaas_15)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_16)=(49151,53155,65535),rgb(mkl23mapIngaas_17)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_18)=(49151,53155,65535),rgb(mkl23mapIngaas_19)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_20)=(49151,53155,65535),rgb(mkl23mapIngaas_21)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_22)=(49151,53155,65535),rgb(mkl23mapIngaas_23)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_24)=(49151,53155,65535),rgb(mkl23mapIngaas_25)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_26)=(49151,53155,65535),rgb(mkl23mapIngaas_27)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_28)=(49151,53155,65535),rgb(mkl23mapIngaas_29)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_30)=(49151,53155,65535),rgb(mkl23mapIngaas_31)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_32)=(49151,53155,65535),rgb(mkl23mapIngaas_33)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_34)=(49151,53155,65535),rgb(mkl23mapIngaas_35)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_36)=(49151,53155,65535),rgb(mkl23mapIngaas_37)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_38)=(49151,53155,65535),rgb(mkl23mapIngaas_39)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_40)=(49151,53155,65535),rgb(mkl23mapIngaas_41)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_42)=(49151,53155,65535),rgb(mkl23mapIngaas_43)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_44)=(49151,53155,65535),rgb(mkl23mapIngaas_45)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_46)=(49151,53155,65535),rgb(mkl23mapIngaas_47)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_48)=(49151,53155,65535),rgb(mkl23mapIngaas_49)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_50)=(49151,53155,65535),rgb(mkl23mapIngaas_51)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_52)=(49151,53155,65535),rgb(mkl23mapIngaas_53)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_54)=(49151,53155,65535),rgb(mkl23mapIngaas_55)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_56)=(49151,53155,65535),rgb(mkl23mapIngaas_57)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_58)=(49151,53155,65535),rgb(mkl23mapIngaas_59)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_60)=(49151,53155,65535),rgb(mkl23mapIngaas_61)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_62)=(49151,53155,65535),rgb(mkl23mapIngaas_63)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_64)=(49151,53155,65535),rgb(mkl23mapIngaas_65)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_66)=(49151,53155,65535),rgb(mkl23mapIngaas_67)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_68)=(49151,53155,65535),rgb(mkl23mapIngaas_69)=(49151,53155,65535)
	ModifyGraph rgb(mkl23mapIngaas_70)=(49151,53155,65535),rgb(mkl24mapSi_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_1)=(49151,53155,65535),rgb(mkl24mapSi_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_3)=(49151,53155,65535),rgb(mkl24mapSi_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_5)=(49151,53155,65535),rgb(mkl24mapSi_6)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_7)=(49151,53155,65535),rgb(mkl24mapSi_8)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_9)=(49151,53155,65535),rgb(mkl24mapSi_10)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_11)=(49151,53155,65535),rgb(mkl24mapSi_12)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_13)=(49151,53155,65535),rgb(mkl24mapSi_14)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_15)=(49151,53155,65535),rgb(mkl24mapSi_16)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_17)=(49151,53155,65535),rgb(mkl24mapSi_18)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_20)=(49151,53155,65535),rgb(mkl24mapSi_21)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_22)=(49151,53155,65535),rgb(mkl24mapSi_23)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_24)=(49151,53155,65535),rgb(mkl24mapSi_25)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_26)=(49151,53155,65535),rgb(mkl24mapSi_27)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapSi_28)=(49151,53155,65535),rgb(mkl24mapInGaAs_31)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_32)=(49151,53155,65535),rgb(mkl24mapInGaAs_34)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_35)=(49151,53155,65535),rgb(mkl24mapInGaAs_36)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_38)=(49151,53155,65535),rgb(mkl24mapInGaAs_39)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_40)=(49151,53155,65535),rgb(mkl24mapInGaAs_41)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_42)=(49151,53155,65535),rgb(mkl24mapInGaAs_43)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_44)=(49151,53155,65535),rgb(mkl24mapInGaAs_45)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_46)=(49151,53155,65535),rgb(mkl24mapInGaAs_47)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_48)=(49151,53155,65535),rgb(mkl24mapInGaAs_49)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_50)=(49151,53155,65535),rgb(mkl24mapInGaAs_52)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_53)=(49151,53155,65535),rgb(mkl24mapInGaAs_54)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_55)=(49151,53155,65535),rgb(mkl24mapInGaAs_56)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_57)=(49151,53155,65535),rgb(mkl24mapInGaAs_58)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_59)=(49151,53155,65535),rgb(mkl24mapInGaAs_60)=(49151,53155,65535)
	ModifyGraph rgb(mkl24mapInGaAs_61)=(49151,53155,65535),rgb(mkl29map_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_1)=(49151,53155,65535),rgb(mkl29map_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_3)=(49151,53155,65535),rgb(mkl29map_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_5)=(49151,53155,65535),rgb(mkl29map_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_8)=(49151,53155,65535),rgb(mkl29map_9)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_10)=(49151,53155,65535),rgb(mkl29map_11)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_12)=(49151,53155,65535),rgb(mkl29map_13)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_14)=(49151,53155,65535),rgb(mkl29map_15)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_16)=(49151,53155,65535),rgb(mkl29map_17)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_18)=(49151,53155,65535),rgb(mkl29map_20)=(49151,53155,65535)
	ModifyGraph rgb(mkl29map_21)=(49151,53155,65535),rgb(mkl29map_22)=(49151,53155,65535)
	ModifyGraph rgb(mkl33map_air_0)=(49151,53155,65535),rgb(mkl33map_air_1)=(49151,53155,65535)
	ModifyGraph rgb(mkl33map_air_2)=(49151,53155,65535),rgb(mkl33map_air_3)=(49151,53155,65535)
	ModifyGraph rgb(mkl33map_air_4)=(49151,53155,65535),rgb(mkl33map_air_5)=(49151,53155,65535)
	ModifyGraph rgb(mkl33map_air_6)=(49151,53155,65535),rgb(mkl33map_air_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl33map_air_8)=(49151,53155,65535),rgb(mkl34mapInGaAs_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_2)=(49151,53155,65535),rgb(mkl34mapInGaAs_3)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_4)=(49151,53155,65535),rgb(mkl34mapInGaAs_5)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_6)=(49151,53155,65535),rgb(mkl34mapInGaAs_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_8)=(49151,53155,65535),rgb(mkl34mapInGaAs_9)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_10)=(49151,53155,65535),rgb(mkl34mapInGaAs_11)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_12)=(49151,53155,65535),rgb(mkl34mapInGaAs_13)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_14)=(49151,53155,65535),rgb(mkl34mapInGaAs_15)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_16)=(49151,53155,65535),rgb(mkl34mapInGaAs_17)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_18)=(49151,53155,65535),rgb(mkl34mapInGaAs_19)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_20)=(49151,53155,65535),rgb(mkl34mapInGaAs_21)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_22)=(49151,53155,65535),rgb(mkl34mapInGaAs_23)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_24)=(49151,53155,65535),rgb(mkl34mapInGaAs_25)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_26)=(49151,53155,65535),rgb(mkl34mapInGaAs_27)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_28)=(49151,53155,65535),rgb(mkl34mapInGaAs_29)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_30)=(49151,53155,65535),rgb(mkl34mapInGaAs_31)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_32)=(49151,53155,65535),rgb(mkl34mapInGaAs_33)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_34)=(49151,53155,65535),rgb(mkl34mapInGaAs_35)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_36)=(49151,53155,65535),rgb(mkl34mapInGaAs_37)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_38)=(49151,53155,65535),rgb(mkl34mapInGaAs_39)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_40)=(49151,53155,65535),rgb(mkl34mapInGaAs_42)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapInGaAs_43)=(49151,53155,65535),rgb(mkl34mapSi_1)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_2)=(49151,53155,65535),rgb(mkl34mapSi_3)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_4)=(49151,53155,65535),rgb(mkl34mapSi_5)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_6)=(49151,53155,65535),rgb(mkl34mapSi_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_8)=(49151,53155,65535),rgb(mkl34mapSi_9)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_10)=(49151,53155,65535),rgb(mkl34mapSi_11)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_12)=(49151,53155,65535),rgb(mkl34mapSi_13)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_14)=(49151,53155,65535),rgb(mkl34mapSi_15)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_16)=(49151,53155,65535),rgb(mkl34mapSi_17)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_18)=(49151,53155,65535),rgb(mkl34mapSi_19)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_20)=(49151,53155,65535),rgb(mkl34mapSi_21)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_22)=(49151,53155,65535),rgb(mkl34mapSi_23)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_24)=(49151,53155,65535),rgb(mkl34mapSi_25)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_26)=(49151,53155,65535),rgb(mkl34mapSi_27)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_28)=(49151,53155,65535),rgb(mkl34mapSi_29)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_30)=(49151,53155,65535),rgb(mkl34mapSi_31)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_32)=(49151,53155,65535),rgb(mkl34mapSi_33)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_34)=(49151,53155,65535),rgb(mkl34mapSi_35)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_36)=(49151,53155,65535),rgb(mkl34mapSi_37)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_38)=(49151,53155,65535),rgb(mkl34mapSi_39)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_40)=(49151,53155,65535),rgb(mkl34mapSi_41)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_42)=(49151,53155,65535),rgb(mkl34mapSi_43)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_44)=(49151,53155,65535),rgb(mkl34mapSi_45)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_46)=(49151,53155,65535),rgb(mkl34mapSi_47)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_49)=(49151,53155,65535),rgb(mkl34mapSi_50)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_51)=(49151,53155,65535),rgb(mkl34mapSi_52)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_53)=(49151,53155,65535),rgb(mkl34mapSi_54)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_55)=(49151,53155,65535),rgb(mkl34mapSi_56)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_57)=(49151,53155,65535),rgb(mkl34mapSi_58)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_59)=(49151,53155,65535),rgb(mkl34mapSi_60)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_61)=(49151,53155,65535),rgb(mkl34mapSi_62)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_63)=(49151,53155,65535),rgb(mkl34mapSi_64)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_65)=(49151,53155,65535),rgb(mkl34mapSi_66)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_67)=(49151,53155,65535),rgb(mkl34mapSi_68)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_69)=(49151,53155,65535),rgb(mkl34mapSi_70)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_71)=(49151,53155,65535),rgb(mkl34mapSi_72)=(49151,53155,65535)
	ModifyGraph rgb(mkl34mapSi_73)=(49151,53155,65535),rgb(mkl44TRSimap_air_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_1)=(49151,53155,65535),rgb(mkl44TRSimap_air_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_3)=(49151,53155,65535),rgb(mkl44TRSimap_air_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_5)=(49151,53155,65535),rgb(mkl44TRSimap_air_6)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_7)=(49151,53155,65535),rgb(mkl44TRSimap_air_8)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_9)=(49151,53155,65535),rgb(mkl44TRSimap_air_11)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_12)=(49151,53155,65535),rgb(mkl44TRSimap_air_13)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_14)=(49151,53155,65535),rgb(mkl44TRSimap_air_15)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_16)=(49151,53155,65535),rgb(mkl44TRSimap_air_17)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_18)=(49151,53155,65535),rgb(mkl44TRSimap_air_19)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_20)=(49151,53155,65535),rgb(mkl44TRSimap_air_21)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_22)=(49151,53155,65535),rgb(mkl44TRSimap_air_23)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_24)=(49151,53155,65535),rgb(mkl44TRSimap_air_25)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_26)=(49151,53155,65535),rgb(mkl44TRSimap_air_27)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_29)=(49151,53155,65535),rgb(mkl44TRSimap_air_30)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_31)=(49151,53155,65535),rgb(mkl44TRSimap_air_32)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_33)=(49151,53155,65535),rgb(mkl44TRSimap_air_34)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_35)=(49151,53155,65535),rgb(mkl44TRSimap_air_36)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_37)=(49151,53155,65535),rgb(mkl44TRSimap_air_38)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_39)=(49151,53155,65535),rgb(mkl44TRSimap_air_40)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_41)=(49151,53155,65535),rgb(mkl44TRSimap_air_42)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_43)=(49151,53155,65535),rgb(mkl44TRSimap_air_44)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_45)=(49151,53155,65535),rgb(mkl44TRSimap_air_46)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_47)=(49151,53155,65535),rgb(mkl44TRSimap_air_48)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_49)=(49151,53155,65535),rgb(mkl44TRSimap_air_50)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_51)=(49151,53155,65535),rgb(mkl44TRSimap_air_52)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRSimap_air_54)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_1)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_3)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_5)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_6)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_7)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_8)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_9)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_10)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_11)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_12)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_13)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_14)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_15)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_16)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_17)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_19)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_20)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_21)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_22)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_23)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_24)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_25)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_26)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_27)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_28)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_29)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_30)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_31)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_32)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_33)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_34)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_35)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_36)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_37)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_38)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_39)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_40)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_41)=(49151,53155,65535)
	ModifyGraph rgb(mkl44TRInGaAsmap_air_42)=(49151,53155,65535),rgb(mkl44TRInGaAsmap_air_43)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_0)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_1)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_2)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_3)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_4)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_5)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_6)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_8)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_9)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_10)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_11)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_12)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_13)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_14)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_15)=(49151,53155,65535)
	ModifyGraph rgb(mkl47exactcoormkl47TRexactcoordinates_map_air_16)=(49151,53155,65535)
	ModifyGraph rgb(mkl50Lexactcoordinates_map_2)=(49151,53155,65535),rgb(mkl50Lexactcoordinates_map_12)=(49151,53155,65535)
	ModifyGraph rgb(mkl50Lexactcoordinates_map_30)=(49151,53155,65535),rgb(mkl50Lexactcoordinates_map_35)=(49151,53155,65535)
	ModifyGraph rgb(mkl51map_0)=(49151,53155,65535),rgb(mkl51map_1)=(49151,53155,65535)
	ModifyGraph rgb(mkl51map_2)=(49151,53155,65535),rgb(mkl51map_3)=(49151,53155,65535)
	ModifyGraph rgb(mkl51map_4)=(49151,53155,65535),rgb(mkl51map_5)=(49151,53155,65535)
	ModifyGraph rgb(mkl51map_6)=(49151,53155,65535),rgb(mkl51map_7)=(49151,53155,65535)
	ModifyGraph rgb(mkl51map_8)=(49151,53155,65535),rgb(mkl51map_9)=(49151,53155,65535)
	ModifyGraph msize(coordinates)=10
	ModifyGraph mrkThick(coordinates)=2
	ModifyGraph gaps=0
	ModifyGraph useMrkStrokeRGB=1
	ModifyGraph mrkStrokeRGB(mkl22map_0)=(16385,28398,65535),mrkStrokeRGB(mkl22map_1)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl22map_2)=(16385,28398,65535),mrkStrokeRGB(mkl22map_3)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl22map_4)=(16385,28398,65535),mrkStrokeRGB(mkl22map_5)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl22map_6)=(16385,28398,65535),mrkStrokeRGB(mkl22map_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapSi)=(16385,28398,65535),mrkStrokeRGB(mkl23mapSi_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapSi_1)=(16385,28398,65535),mrkStrokeRGB(mkl23mapSi_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapSi_3)=(16385,28398,65535),mrkStrokeRGB(mkl23mapSi_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapSi_5)=(16385,28398,65535),mrkStrokeRGB(mkl23mapSi_6)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapSi_7)=(16385,28398,65535),mrkStrokeRGB(mkl23mapSi_8)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_1)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_3)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_5)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_6)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_7)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_8)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_9)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_10)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_10_2)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_11)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_12)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_13)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_14)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_15)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_16)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_17)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_18)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_19)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_20)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_21)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_22)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_23)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_24)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_25)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_26)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_27)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_28)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_29)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_30)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_31)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_32)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_33)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_34)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_35)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_36)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_37)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_38)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_39)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_40)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_41)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_42)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_43)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_44)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_45)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_46)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_47)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_48)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_49)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_50)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_51)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_52)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_53)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_54)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_55)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_56)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_57)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_58)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_59)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_60)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_61)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_62)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_63)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_64)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_65)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_66)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_67)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_68)=(16385,28398,65535),mrkStrokeRGB(mkl23mapIngaas_69)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl23mapIngaas_70)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_1)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_3)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_5)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_6)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_7)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_8)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_9)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_10)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_11)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_12)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_13)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_14)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_15)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_16)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_17)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_18)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_20)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_21)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_22)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_23)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_24)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_25)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_26)=(16385,28398,65535),mrkStrokeRGB(mkl24mapSi_27)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapSi_28)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_31)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_32)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_34)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_35)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_36)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_38)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_39)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_40)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_41)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_42)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_43)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_44)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_45)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_46)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_47)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_48)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_49)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_50)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_52)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_53)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_54)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_55)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_56)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_57)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_58)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_59)=(16385,28398,65535),mrkStrokeRGB(mkl24mapInGaAs_60)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl24mapInGaAs_61)=(16385,28398,65535),mrkStrokeRGB(mkl29map_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_1)=(16385,28398,65535),mrkStrokeRGB(mkl29map_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_3)=(16385,28398,65535),mrkStrokeRGB(mkl29map_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_5)=(16385,28398,65535),mrkStrokeRGB(mkl29map_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_8)=(16385,28398,65535),mrkStrokeRGB(mkl29map_9)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_10)=(16385,28398,65535),mrkStrokeRGB(mkl29map_11)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_12)=(16385,28398,65535),mrkStrokeRGB(mkl29map_13)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_14)=(16385,28398,65535),mrkStrokeRGB(mkl29map_15)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_16)=(16385,28398,65535),mrkStrokeRGB(mkl29map_17)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_18)=(16385,28398,65535),mrkStrokeRGB(mkl29map_20)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl29map_21)=(16385,28398,65535),mrkStrokeRGB(mkl29map_22)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl33map_air_0)=(16385,28398,65535),mrkStrokeRGB(mkl33map_air_1)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl33map_air_2)=(16385,28398,65535),mrkStrokeRGB(mkl33map_air_3)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl33map_air_4)=(16385,28398,65535),mrkStrokeRGB(mkl33map_air_5)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl33map_air_6)=(16385,28398,65535),mrkStrokeRGB(mkl33map_air_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl33map_air_8)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_2)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_3)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_4)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_5)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_6)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_8)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_9)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_10)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_11)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_12)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_13)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_14)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_15)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_16)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_17)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_18)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_19)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_20)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_21)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_22)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_23)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_24)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_25)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_26)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_27)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_28)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_29)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_30)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_31)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_32)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_33)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_34)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_35)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_36)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_37)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_38)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_39)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_40)=(16385,28398,65535),mrkStrokeRGB(mkl34mapInGaAs_42)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapInGaAs_43)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_1)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_2)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_3)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_4)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_5)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_6)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_8)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_9)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_10)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_11)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_12)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_13)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_14)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_15)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_16)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_17)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_18)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_19)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_20)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_21)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_22)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_23)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_24)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_25)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_26)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_27)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_28)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_29)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_30)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_31)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_32)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_33)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_34)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_35)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_36)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_37)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_38)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_39)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_40)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_41)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_42)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_43)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_44)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_45)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_46)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_47)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_49)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_50)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_51)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_52)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_53)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_54)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_55)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_56)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_57)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_58)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_59)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_60)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_61)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_62)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_63)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_64)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_65)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_66)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_67)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_68)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_69)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_70)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_71)=(16385,28398,65535),mrkStrokeRGB(mkl34mapSi_72)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl34mapSi_73)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_1)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_3)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_5)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_6)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_7)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_8)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_9)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_11)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_12)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_13)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_14)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_15)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_16)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_17)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_18)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_19)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_20)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_21)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_22)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_23)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_24)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_25)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_26)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_27)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_29)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_30)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_31)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_32)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_33)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_34)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_35)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_36)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_37)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_38)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_39)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_40)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_41)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_42)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_43)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_44)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_45)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_46)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_47)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_48)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_49)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_50)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_51)=(16385,28398,65535),mrkStrokeRGB(mkl44TRSimap_air_52)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRSimap_air_54)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_1)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_3)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_5)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_6)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_7)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_8)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_9)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_10)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_11)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_12)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_13)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_14)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_15)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_16)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_17)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_19)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_20)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_21)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_22)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_23)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_24)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_25)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_26)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_27)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_28)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_29)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_30)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_31)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_32)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_33)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_34)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_35)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_36)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_37)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_38)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_39)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_40)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_41)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl44TRInGaAsmap_air_42)=(16385,28398,65535),mrkStrokeRGB(mkl44TRInGaAsmap_air_43)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_0)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_1)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_2)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_3)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_4)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_5)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_6)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_8)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_9)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_10)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_11)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_12)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_13)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_14)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_15)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl47exactcoormkl47TRexactcoordinates_map_air_16)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl50Lexactcoordinates_map_2)=(16385,28398,65535),mrkStrokeRGB(mkl50Lexactcoordinates_map_12)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl50Lexactcoordinates_map_30)=(16385,28398,65535),mrkStrokeRGB(mkl50Lexactcoordinates_map_35)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl51map_0)=(16385,28398,65535),mrkStrokeRGB(mkl51map_1)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl51map_2)=(16385,28398,65535),mrkStrokeRGB(mkl51map_3)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl51map_4)=(16385,28398,65535),mrkStrokeRGB(mkl51map_5)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl51map_6)=(16385,28398,65535),mrkStrokeRGB(mkl51map_7)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(mkl51map_8)=(16385,28398,65535),mrkStrokeRGB(mkl51map_9)=(16385,28398,65535)
	ModifyGraph mrkStrokeRGB(coordinates)=(65535,0,0)
	ModifyGraph zmrkSize(mkl22map_0)={:PLEMd2:maps:mkl22map_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_1)={:PLEMd2:maps:mkl22map_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_2)={:PLEMd2:maps:mkl22map_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_3)={:PLEMd2:maps:mkl22map_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_4)={:PLEMd2:maps:mkl22map_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_5)={:PLEMd2:maps:mkl22map_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_6)={:PLEMd2:maps:mkl22map_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl22map_7)={:PLEMd2:maps:mkl22map_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi)={:PLEMd2:maps:mkl23mapSi:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_0)={:PLEMd2:maps:mkl23mapSi_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_1)={:PLEMd2:maps:mkl23mapSi_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_2)={:PLEMd2:maps:mkl23mapSi_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_3)={:PLEMd2:maps:mkl23mapSi_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_4)={:PLEMd2:maps:mkl23mapSi_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_5)={:PLEMd2:maps:mkl23mapSi_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_6)={:PLEMd2:maps:mkl23mapSi_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_7)={:PLEMd2:maps:mkl23mapSi_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapSi_8)={:PLEMd2:maps:mkl23mapSi_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas)={:PLEMd2:maps:mkl23mapIngaas:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_0)={:PLEMd2:maps:mkl23mapIngaas_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_1)={:PLEMd2:maps:mkl23mapIngaas_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_2)={:PLEMd2:maps:mkl23mapIngaas_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_3)={:PLEMd2:maps:mkl23mapIngaas_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_4)={:PLEMd2:maps:mkl23mapIngaas_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_5)={:PLEMd2:maps:mkl23mapIngaas_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_6)={:PLEMd2:maps:mkl23mapIngaas_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_7)={:PLEMd2:maps:mkl23mapIngaas_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_8)={:PLEMd2:maps:mkl23mapIngaas_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_9)={:PLEMd2:maps:mkl23mapIngaas_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_10)={:PLEMd2:maps:mkl23mapIngaas_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_10_2)={:PLEMd2:maps:mkl23mapIngaas_10_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_11)={:PLEMd2:maps:mkl23mapIngaas_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_12)={:PLEMd2:maps:mkl23mapIngaas_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_13)={:PLEMd2:maps:mkl23mapIngaas_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_14)={:PLEMd2:maps:mkl23mapIngaas_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_15)={:PLEMd2:maps:mkl23mapIngaas_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_16)={:PLEMd2:maps:mkl23mapIngaas_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_17)={:PLEMd2:maps:mkl23mapIngaas_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_18)={:PLEMd2:maps:mkl23mapIngaas_18:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_19)={:PLEMd2:maps:mkl23mapIngaas_19:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_20)={:PLEMd2:maps:mkl23mapIngaas_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_21)={:PLEMd2:maps:mkl23mapIngaas_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_22)={:PLEMd2:maps:mkl23mapIngaas_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_23)={:PLEMd2:maps:mkl23mapIngaas_23:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_24)={:PLEMd2:maps:mkl23mapIngaas_24:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_25)={:PLEMd2:maps:mkl23mapIngaas_25:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_26)={:PLEMd2:maps:mkl23mapIngaas_26:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_27)={:PLEMd2:maps:mkl23mapIngaas_27:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_28)={:PLEMd2:maps:mkl23mapIngaas_28:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_29)={:PLEMd2:maps:mkl23mapIngaas_29:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_30)={:PLEMd2:maps:mkl23mapIngaas_30:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_31)={:PLEMd2:maps:mkl23mapIngaas_31:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_32)={:PLEMd2:maps:mkl23mapIngaas_32:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_33)={:PLEMd2:maps:mkl23mapIngaas_33:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_34)={:PLEMd2:maps:mkl23mapIngaas_34:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_35)={:PLEMd2:maps:mkl23mapIngaas_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_36)={:PLEMd2:maps:mkl23mapIngaas_36:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_37)={:PLEMd2:maps:mkl23mapIngaas_37:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_38)={:PLEMd2:maps:mkl23mapIngaas_38:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_39)={:PLEMd2:maps:mkl23mapIngaas_39:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_40)={:PLEMd2:maps:mkl23mapIngaas_40:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_41)={:PLEMd2:maps:mkl23mapIngaas_41:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_42)={:PLEMd2:maps:mkl23mapIngaas_42:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_43)={:PLEMd2:maps:mkl23mapIngaas_43:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_44)={:PLEMd2:maps:mkl23mapIngaas_44:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_45)={:PLEMd2:maps:mkl23mapIngaas_45:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_46)={:PLEMd2:maps:mkl23mapIngaas_46:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_47)={:PLEMd2:maps:mkl23mapIngaas_47:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_48)={:PLEMd2:maps:mkl23mapIngaas_48:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_49)={:PLEMd2:maps:mkl23mapIngaas_49:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_50)={:PLEMd2:maps:mkl23mapIngaas_50:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_51)={:PLEMd2:maps:mkl23mapIngaas_51:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_52)={:PLEMd2:maps:mkl23mapIngaas_52:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_53)={:PLEMd2:maps:mkl23mapIngaas_53:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_54)={:PLEMd2:maps:mkl23mapIngaas_54:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_55)={:PLEMd2:maps:mkl23mapIngaas_55:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_56)={:PLEMd2:maps:mkl23mapIngaas_56:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_57)={:PLEMd2:maps:mkl23mapIngaas_57:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_58)={:PLEMd2:maps:mkl23mapIngaas_58:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_59)={:PLEMd2:maps:mkl23mapIngaas_59:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_60)={:PLEMd2:maps:mkl23mapIngaas_60:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_61)={:PLEMd2:maps:mkl23mapIngaas_61:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_62)={:PLEMd2:maps:mkl23mapIngaas_62:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_63)={:PLEMd2:maps:mkl23mapIngaas_63:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_64)={:PLEMd2:maps:mkl23mapIngaas_64:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_65)={:PLEMd2:maps:mkl23mapIngaas_65:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_66)={:PLEMd2:maps:mkl23mapIngaas_66:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_67)={:PLEMd2:maps:mkl23mapIngaas_67:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_68)={:PLEMd2:maps:mkl23mapIngaas_68:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_69)={:PLEMd2:maps:mkl23mapIngaas_69:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl23mapIngaas_70)={:PLEMd2:maps:mkl23mapIngaas_70:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_0)={:PLEMd2:maps:mkl24mapSi_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_1)={:PLEMd2:maps:mkl24mapSi_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_2)={:PLEMd2:maps:mkl24mapSi_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_3)={:PLEMd2:maps:mkl24mapSi_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_4)={:PLEMd2:maps:mkl24mapSi_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_5)={:PLEMd2:maps:mkl24mapSi_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_6)={:PLEMd2:maps:mkl24mapSi_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_7)={:PLEMd2:maps:mkl24mapSi_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_8)={:PLEMd2:maps:mkl24mapSi_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_9)={:PLEMd2:maps:mkl24mapSi_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_10)={:PLEMd2:maps:mkl24mapSi_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_11)={:PLEMd2:maps:mkl24mapSi_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_12)={:PLEMd2:maps:mkl24mapSi_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_13)={:PLEMd2:maps:mkl24mapSi_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_14)={:PLEMd2:maps:mkl24mapSi_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_15)={:PLEMd2:maps:mkl24mapSi_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_16)={:PLEMd2:maps:mkl24mapSi_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_17)={:PLEMd2:maps:mkl24mapSi_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_18)={:PLEMd2:maps:mkl24mapSi_18:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_20)={:PLEMd2:maps:mkl24mapSi_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_21)={:PLEMd2:maps:mkl24mapSi_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_22)={:PLEMd2:maps:mkl24mapSi_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_23)={:PLEMd2:maps:mkl24mapSi_23:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_24)={:PLEMd2:maps:mkl24mapSi_24:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_25)={:PLEMd2:maps:mkl24mapSi_25:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_26)={:PLEMd2:maps:mkl24mapSi_26:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_27)={:PLEMd2:maps:mkl24mapSi_27:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapSi_28)={:PLEMd2:maps:mkl24mapSi_28:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_31)={:PLEMd2:maps:mkl24mapInGaAs_31:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_32)={:PLEMd2:maps:mkl24mapInGaAs_32:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_34)={:PLEMd2:maps:mkl24mapInGaAs_34:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_35)={:PLEMd2:maps:mkl24mapInGaAs_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_36)={:PLEMd2:maps:mkl24mapInGaAs_36:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_38)={:PLEMd2:maps:mkl24mapInGaAs_38:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_39)={:PLEMd2:maps:mkl24mapInGaAs_39:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_40)={:PLEMd2:maps:mkl24mapInGaAs_40:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_41)={:PLEMd2:maps:mkl24mapInGaAs_41:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_42)={:PLEMd2:maps:mkl24mapInGaAs_42:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_43)={:PLEMd2:maps:mkl24mapInGaAs_43:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_44)={:PLEMd2:maps:mkl24mapInGaAs_44:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_45)={:PLEMd2:maps:mkl24mapInGaAs_45:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_46)={:PLEMd2:maps:mkl24mapInGaAs_46:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_47)={:PLEMd2:maps:mkl24mapInGaAs_47:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_48)={:PLEMd2:maps:mkl24mapInGaAs_48:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_49)={:PLEMd2:maps:mkl24mapInGaAs_49:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_50)={:PLEMd2:maps:mkl24mapInGaAs_50:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_52)={:PLEMd2:maps:mkl24mapInGaAs_52:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_53)={:PLEMd2:maps:mkl24mapInGaAs_53:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_54)={:PLEMd2:maps:mkl24mapInGaAs_54:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_55)={:PLEMd2:maps:mkl24mapInGaAs_55:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_56)={:PLEMd2:maps:mkl24mapInGaAs_56:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_57)={:PLEMd2:maps:mkl24mapInGaAs_57:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_58)={:PLEMd2:maps:mkl24mapInGaAs_58:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_59)={:PLEMd2:maps:mkl24mapInGaAs_59:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_60)={:PLEMd2:maps:mkl24mapInGaAs_60:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl24mapInGaAs_61)={:PLEMd2:maps:mkl24mapInGaAs_61:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_0)={:PLEMd2:maps:mkl29map_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_1)={:PLEMd2:maps:mkl29map_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_2)={:PLEMd2:maps:mkl29map_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_3)={:PLEMd2:maps:mkl29map_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_4)={:PLEMd2:maps:mkl29map_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_5)={:PLEMd2:maps:mkl29map_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_7)={:PLEMd2:maps:mkl29map_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_8)={:PLEMd2:maps:mkl29map_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_9)={:PLEMd2:maps:mkl29map_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_10)={:PLEMd2:maps:mkl29map_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_11)={:PLEMd2:maps:mkl29map_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_12)={:PLEMd2:maps:mkl29map_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_13)={:PLEMd2:maps:mkl29map_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_14)={:PLEMd2:maps:mkl29map_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_15)={:PLEMd2:maps:mkl29map_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_16)={:PLEMd2:maps:mkl29map_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_17)={:PLEMd2:maps:mkl29map_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_18)={:PLEMd2:maps:mkl29map_18:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_20)={:PLEMd2:maps:mkl29map_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_21)={:PLEMd2:maps:mkl29map_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl29map_22)={:PLEMd2:maps:mkl29map_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_0)={:PLEMd2:maps:mkl33map_air_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_1)={:PLEMd2:maps:mkl33map_air_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_2)={:PLEMd2:maps:mkl33map_air_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_3)={:PLEMd2:maps:mkl33map_air_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_4)={:PLEMd2:maps:mkl33map_air_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_5)={:PLEMd2:maps:mkl33map_air_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_6)={:PLEMd2:maps:mkl33map_air_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_7)={:PLEMd2:maps:mkl33map_air_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl33map_air_8)={:PLEMd2:maps:mkl33map_air_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_0)={:PLEMd2:maps:mkl34mapInGaAs_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_2)={:PLEMd2:maps:mkl34mapInGaAs_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_3)={:PLEMd2:maps:mkl34mapInGaAs_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_4)={:PLEMd2:maps:mkl34mapInGaAs_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_5)={:PLEMd2:maps:mkl34mapInGaAs_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_6)={:PLEMd2:maps:mkl34mapInGaAs_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_7)={:PLEMd2:maps:mkl34mapInGaAs_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_8)={:PLEMd2:maps:mkl34mapInGaAs_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_9)={:PLEMd2:maps:mkl34mapInGaAs_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_10)={:PLEMd2:maps:mkl34mapInGaAs_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_11)={:PLEMd2:maps:mkl34mapInGaAs_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_12)={:PLEMd2:maps:mkl34mapInGaAs_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_13)={:PLEMd2:maps:mkl34mapInGaAs_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_14)={:PLEMd2:maps:mkl34mapInGaAs_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_15)={:PLEMd2:maps:mkl34mapInGaAs_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_16)={:PLEMd2:maps:mkl34mapInGaAs_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_17)={:PLEMd2:maps:mkl34mapInGaAs_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_18)={:PLEMd2:maps:mkl34mapInGaAs_18:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_19)={:PLEMd2:maps:mkl34mapInGaAs_19:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_20)={:PLEMd2:maps:mkl34mapInGaAs_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_21)={:PLEMd2:maps:mkl34mapInGaAs_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_22)={:PLEMd2:maps:mkl34mapInGaAs_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_23)={:PLEMd2:maps:mkl34mapInGaAs_23:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_24)={:PLEMd2:maps:mkl34mapInGaAs_24:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_25)={:PLEMd2:maps:mkl34mapInGaAs_25:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_26)={:PLEMd2:maps:mkl34mapInGaAs_26:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_27)={:PLEMd2:maps:mkl34mapInGaAs_27:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_28)={:PLEMd2:maps:mkl34mapInGaAs_28:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_29)={:PLEMd2:maps:mkl34mapInGaAs_29:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_30)={:PLEMd2:maps:mkl34mapInGaAs_30:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_31)={:PLEMd2:maps:mkl34mapInGaAs_31:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_32)={:PLEMd2:maps:mkl34mapInGaAs_32:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_33)={:PLEMd2:maps:mkl34mapInGaAs_33:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_34)={:PLEMd2:maps:mkl34mapInGaAs_34:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_35)={:PLEMd2:maps:mkl34mapInGaAs_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_36)={:PLEMd2:maps:mkl34mapInGaAs_36:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_37)={:PLEMd2:maps:mkl34mapInGaAs_37:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_38)={:PLEMd2:maps:mkl34mapInGaAs_38:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_39)={:PLEMd2:maps:mkl34mapInGaAs_39:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_40)={:PLEMd2:maps:mkl34mapInGaAs_40:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_42)={:PLEMd2:maps:mkl34mapInGaAs_42:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapInGaAs_43)={:PLEMd2:maps:mkl34mapInGaAs_43:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_1)={:PLEMd2:maps:mkl34mapSi_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_2)={:PLEMd2:maps:mkl34mapSi_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_3)={:PLEMd2:maps:mkl34mapSi_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_4)={:PLEMd2:maps:mkl34mapSi_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_5)={:PLEMd2:maps:mkl34mapSi_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_6)={:PLEMd2:maps:mkl34mapSi_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_7)={:PLEMd2:maps:mkl34mapSi_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_8)={:PLEMd2:maps:mkl34mapSi_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_9)={:PLEMd2:maps:mkl34mapSi_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_10)={:PLEMd2:maps:mkl34mapSi_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_11)={:PLEMd2:maps:mkl34mapSi_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_12)={:PLEMd2:maps:mkl34mapSi_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_13)={:PLEMd2:maps:mkl34mapSi_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_14)={:PLEMd2:maps:mkl34mapSi_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_15)={:PLEMd2:maps:mkl34mapSi_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_16)={:PLEMd2:maps:mkl34mapSi_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_17)={:PLEMd2:maps:mkl34mapSi_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_18)={:PLEMd2:maps:mkl34mapSi_18:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_19)={:PLEMd2:maps:mkl34mapSi_19:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_20)={:PLEMd2:maps:mkl34mapSi_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_21)={:PLEMd2:maps:mkl34mapSi_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_22)={:PLEMd2:maps:mkl34mapSi_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_23)={:PLEMd2:maps:mkl34mapSi_23:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_24)={:PLEMd2:maps:mkl34mapSi_24:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_25)={:PLEMd2:maps:mkl34mapSi_25:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_26)={:PLEMd2:maps:mkl34mapSi_26:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_27)={:PLEMd2:maps:mkl34mapSi_27:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_28)={:PLEMd2:maps:mkl34mapSi_28:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_29)={:PLEMd2:maps:mkl34mapSi_29:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_30)={:PLEMd2:maps:mkl34mapSi_30:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_31)={:PLEMd2:maps:mkl34mapSi_31:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_32)={:PLEMd2:maps:mkl34mapSi_32:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_33)={:PLEMd2:maps:mkl34mapSi_33:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_34)={:PLEMd2:maps:mkl34mapSi_34:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_35)={:PLEMd2:maps:mkl34mapSi_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_36)={:PLEMd2:maps:mkl34mapSi_36:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_37)={:PLEMd2:maps:mkl34mapSi_37:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_38)={:PLEMd2:maps:mkl34mapSi_38:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_39)={:PLEMd2:maps:mkl34mapSi_39:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_40)={:PLEMd2:maps:mkl34mapSi_40:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_41)={:PLEMd2:maps:mkl34mapSi_41:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_42)={:PLEMd2:maps:mkl34mapSi_42:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_43)={:PLEMd2:maps:mkl34mapSi_43:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_44)={:PLEMd2:maps:mkl34mapSi_44:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_45)={:PLEMd2:maps:mkl34mapSi_45:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_46)={:PLEMd2:maps:mkl34mapSi_46:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_47)={:PLEMd2:maps:mkl34mapSi_47:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_49)={:PLEMd2:maps:mkl34mapSi_49:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_50)={:PLEMd2:maps:mkl34mapSi_50:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_51)={:PLEMd2:maps:mkl34mapSi_51:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_52)={:PLEMd2:maps:mkl34mapSi_52:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_53)={:PLEMd2:maps:mkl34mapSi_53:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_54)={:PLEMd2:maps:mkl34mapSi_54:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_55)={:PLEMd2:maps:mkl34mapSi_55:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_56)={:PLEMd2:maps:mkl34mapSi_56:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_57)={:PLEMd2:maps:mkl34mapSi_57:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_58)={:PLEMd2:maps:mkl34mapSi_58:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_59)={:PLEMd2:maps:mkl34mapSi_59:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_60)={:PLEMd2:maps:mkl34mapSi_60:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_61)={:PLEMd2:maps:mkl34mapSi_61:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_62)={:PLEMd2:maps:mkl34mapSi_62:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_63)={:PLEMd2:maps:mkl34mapSi_63:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_64)={:PLEMd2:maps:mkl34mapSi_64:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_65)={:PLEMd2:maps:mkl34mapSi_65:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_66)={:PLEMd2:maps:mkl34mapSi_66:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_67)={:PLEMd2:maps:mkl34mapSi_67:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_68)={:PLEMd2:maps:mkl34mapSi_68:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_69)={:PLEMd2:maps:mkl34mapSi_69:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_70)={:PLEMd2:maps:mkl34mapSi_70:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_71)={:PLEMd2:maps:mkl34mapSi_71:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_72)={:PLEMd2:maps:mkl34mapSi_72:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl34mapSi_73)={:PLEMd2:maps:mkl34mapSi_73:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_0)={:PLEMd2:maps:mkl44TRSimap_air_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_1)={:PLEMd2:maps:mkl44TRSimap_air_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_2)={:PLEMd2:maps:mkl44TRSimap_air_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_3)={:PLEMd2:maps:mkl44TRSimap_air_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_4)={:PLEMd2:maps:mkl44TRSimap_air_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_5)={:PLEMd2:maps:mkl44TRSimap_air_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_6)={:PLEMd2:maps:mkl44TRSimap_air_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_7)={:PLEMd2:maps:mkl44TRSimap_air_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_8)={:PLEMd2:maps:mkl44TRSimap_air_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_9)={:PLEMd2:maps:mkl44TRSimap_air_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_11)={:PLEMd2:maps:mkl44TRSimap_air_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_12)={:PLEMd2:maps:mkl44TRSimap_air_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_13)={:PLEMd2:maps:mkl44TRSimap_air_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_14)={:PLEMd2:maps:mkl44TRSimap_air_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_15)={:PLEMd2:maps:mkl44TRSimap_air_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_16)={:PLEMd2:maps:mkl44TRSimap_air_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_17)={:PLEMd2:maps:mkl44TRSimap_air_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_18)={:PLEMd2:maps:mkl44TRSimap_air_18:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_19)={:PLEMd2:maps:mkl44TRSimap_air_19:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_20)={:PLEMd2:maps:mkl44TRSimap_air_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_21)={:PLEMd2:maps:mkl44TRSimap_air_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_22)={:PLEMd2:maps:mkl44TRSimap_air_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_23)={:PLEMd2:maps:mkl44TRSimap_air_23:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_24)={:PLEMd2:maps:mkl44TRSimap_air_24:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_25)={:PLEMd2:maps:mkl44TRSimap_air_25:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_26)={:PLEMd2:maps:mkl44TRSimap_air_26:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_27)={:PLEMd2:maps:mkl44TRSimap_air_27:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_29)={:PLEMd2:maps:mkl44TRSimap_air_29:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_30)={:PLEMd2:maps:mkl44TRSimap_air_30:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_31)={:PLEMd2:maps:mkl44TRSimap_air_31:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_32)={:PLEMd2:maps:mkl44TRSimap_air_32:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_33)={:PLEMd2:maps:mkl44TRSimap_air_33:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_34)={:PLEMd2:maps:mkl44TRSimap_air_34:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_35)={:PLEMd2:maps:mkl44TRSimap_air_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_36)={:PLEMd2:maps:mkl44TRSimap_air_36:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_37)={:PLEMd2:maps:mkl44TRSimap_air_37:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_38)={:PLEMd2:maps:mkl44TRSimap_air_38:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_39)={:PLEMd2:maps:mkl44TRSimap_air_39:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_40)={:PLEMd2:maps:mkl44TRSimap_air_40:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_41)={:PLEMd2:maps:mkl44TRSimap_air_41:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_42)={:PLEMd2:maps:mkl44TRSimap_air_42:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_43)={:PLEMd2:maps:mkl44TRSimap_air_43:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_44)={:PLEMd2:maps:mkl44TRSimap_air_44:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_45)={:PLEMd2:maps:mkl44TRSimap_air_45:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_46)={:PLEMd2:maps:mkl44TRSimap_air_46:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_47)={:PLEMd2:maps:mkl44TRSimap_air_47:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_48)={:PLEMd2:maps:mkl44TRSimap_air_48:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_49)={:PLEMd2:maps:mkl44TRSimap_air_49:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_50)={:PLEMd2:maps:mkl44TRSimap_air_50:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_51)={:PLEMd2:maps:mkl44TRSimap_air_51:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_52)={:PLEMd2:maps:mkl44TRSimap_air_52:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRSimap_air_54)={:PLEMd2:maps:mkl44TRSimap_air_54:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_0)={:PLEMd2:maps:mkl44TRInGaAsmap_air_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_1)={:PLEMd2:maps:mkl44TRInGaAsmap_air_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_2)={:PLEMd2:maps:mkl44TRInGaAsmap_air_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_3)={:PLEMd2:maps:mkl44TRInGaAsmap_air_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_4)={:PLEMd2:maps:mkl44TRInGaAsmap_air_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_5)={:PLEMd2:maps:mkl44TRInGaAsmap_air_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_6)={:PLEMd2:maps:mkl44TRInGaAsmap_air_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_7)={:PLEMd2:maps:mkl44TRInGaAsmap_air_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_8)={:PLEMd2:maps:mkl44TRInGaAsmap_air_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_9)={:PLEMd2:maps:mkl44TRInGaAsmap_air_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_10)={:PLEMd2:maps:mkl44TRInGaAsmap_air_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_11)={:PLEMd2:maps:mkl44TRInGaAsmap_air_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_12)={:PLEMd2:maps:mkl44TRInGaAsmap_air_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_13)={:PLEMd2:maps:mkl44TRInGaAsmap_air_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_14)={:PLEMd2:maps:mkl44TRInGaAsmap_air_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_15)={:PLEMd2:maps:mkl44TRInGaAsmap_air_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_16)={:PLEMd2:maps:mkl44TRInGaAsmap_air_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_17)={:PLEMd2:maps:mkl44TRInGaAsmap_air_17:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_19)={:PLEMd2:maps:mkl44TRInGaAsmap_air_19:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_20)={:PLEMd2:maps:mkl44TRInGaAsmap_air_20:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_21)={:PLEMd2:maps:mkl44TRInGaAsmap_air_21:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_22)={:PLEMd2:maps:mkl44TRInGaAsmap_air_22:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_23)={:PLEMd2:maps:mkl44TRInGaAsmap_air_23:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_24)={:PLEMd2:maps:mkl44TRInGaAsmap_air_24:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_25)={:PLEMd2:maps:mkl44TRInGaAsmap_air_25:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_26)={:PLEMd2:maps:mkl44TRInGaAsmap_air_26:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_27)={:PLEMd2:maps:mkl44TRInGaAsmap_air_27:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_28)={:PLEMd2:maps:mkl44TRInGaAsmap_air_28:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_29)={:PLEMd2:maps:mkl44TRInGaAsmap_air_29:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_30)={:PLEMd2:maps:mkl44TRInGaAsmap_air_30:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_31)={:PLEMd2:maps:mkl44TRInGaAsmap_air_31:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_32)={:PLEMd2:maps:mkl44TRInGaAsmap_air_32:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_33)={:PLEMd2:maps:mkl44TRInGaAsmap_air_33:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_34)={:PLEMd2:maps:mkl44TRInGaAsmap_air_34:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_35)={:PLEMd2:maps:mkl44TRInGaAsmap_air_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_36)={:PLEMd2:maps:mkl44TRInGaAsmap_air_36:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_37)={:PLEMd2:maps:mkl44TRInGaAsmap_air_37:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_38)={:PLEMd2:maps:mkl44TRInGaAsmap_air_38:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_39)={:PLEMd2:maps:mkl44TRInGaAsmap_air_39:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_40)={:PLEMd2:maps:mkl44TRInGaAsmap_air_40:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_41)={:PLEMd2:maps:mkl44TRInGaAsmap_air_41:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_42)={:PLEMd2:maps:mkl44TRInGaAsmap_air_42:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl44TRInGaAsmap_air_43)={:PLEMd2:maps:mkl44TRInGaAsmap_air_43:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_0)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_1)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_2)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_3)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_4)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_5)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_6)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_7)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_8)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_9)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_10)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_10:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_11)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_11:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_12)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_13)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_13:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_14)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_14:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_15)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_15:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl47exactcoormkl47TRexactcoordinates_map_air_16)={:PLEMd2:maps:mkl47exactcoormkl47TRexactcoordinates_map_air_16:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl50Lexactcoordinates_map_2)={:PLEMd2:maps:mkl50Lexactcoordinates_map_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl50Lexactcoordinates_map_12)={:PLEMd2:maps:mkl50Lexactcoordinates_map_12:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl50Lexactcoordinates_map_30)={:PLEMd2:maps:mkl50Lexactcoordinates_map_30:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl50Lexactcoordinates_map_35)={:PLEMd2:maps:mkl50Lexactcoordinates_map_35:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_0)={:PLEMd2:maps:mkl51map_0:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_1)={:PLEMd2:maps:mkl51map_1:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_2)={:PLEMd2:maps:mkl51map_2:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_3)={:PLEMd2:maps:mkl51map_3:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_4)={:PLEMd2:maps:mkl51map_4:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_5)={:PLEMd2:maps:mkl51map_5:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_6)={:PLEMd2:maps:mkl51map_6:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_7)={:PLEMd2:maps:mkl51map_7:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_8)={:PLEMd2:maps:mkl51map_8:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph zmrkSize(mkl51map_9)={:PLEMd2:maps:mkl51map_9:CHIRALITY:fit1D,0,*,0,2}
	ModifyGraph manTick(bottom)={0,50,0,0},manMinor(bottom)={0,50}
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	SetAxis left 500,800
	SetAxis bottom 800,1280
EndMacro

Window violin_boxbeutel() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(946.5,44.75,1455.75,293) as "violin_boxbeutel"
	Display/W=(0,0,0.452,1)/HOST=# 
	AppendViolinPlot/TN=e11 fwhm11_air,fwhm11_toluene vs experiment
	ModifyGraph margin(bottom)=28
	ModifyGraph mode=4
	ModifyGraph rgb=(65535,16385,16385)
	ModifyGraph lineJoin(e11)={0,10}
	Label left "FWHM of E₁₁ [meV]"
	SetAxis left 0,30
	ModifyViolinPlot trace=e11,MarkerSize=1,ShowMean,ShowMedian,Jitter=0.9
	TextBox/C/N=text0/S=1/A=LT/X=0.00/Y=0.00 "15(1)meV in air and 22(1)meV in toluene"
	RenameWindow #,violin
	SetActiveSubwindow ##
	Display/W=(0.722,0,1,1)/HOST=# 
	AppendViolinPlot/TN=intensity_drop_toluene pleIntensity1D_decrease_toluene
	ModifyGraph margin(left)=113,margin(bottom)=28,margin(right)=7
	ModifyGraph mode=4
	ModifyGraph lineJoin(intensity_drop_toluene)={0,10}
	ModifyGraph nticks(bottom)=0
	ModifyGraph lblMargin(left)=20
	ModifyGraph lblPosMode(left)=1,lblPosMode(bottom)=3
	ModifyGraph lblPos(bottom)=15
	ModifyGraph lblLatPos(bottom)=-20
	ModifyGraph manTick(left)={0,25,-2,0},manMinor(left)={4,2}
	Label left "intensity in toluene [%]"
	Label bottom "toluene / air"
	SetAxis left 0,1
	SetAxis bottom 0,1.5
	ModifyViolinPlot trace=intensity_drop_toluene,MarkerSize=1,ShowMean,ShowMedian
	TextBox/C/N=text0/S=1/A=LT/X=-190.00/Y=0.71 "\\JC85% (median)\rintensity drop"
	RenameWindow #,boxbeutel
	SetActiveSubwindow ##
	Display/W=(0.414,0,0.787,1)/HOST=# 
	AppendViolinPlot pleDiff11_toluene,pleDiff22_toluene vs energylevels
	ModifyGraph margin(bottom)=28
	ModifyGraph mode=4
	ModifyGraph lineJoin(pleDiff11_toluene)={0,10}
	Label left "ΔE (air, tolune) [meV]"
	SetAxis left 0,180
	ModifyViolinPlot trace=pleDiff11_toluene,ShowMean,ShowMedian
	TextBox/C/N=text0/S=1/A=LT/X=0.00/Y=0.00 "shift: 75meV"
	RenameWindow #,shift
	SetActiveSubwindow ##
EndMacro

Window violin_ediff() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(959.25,556.25,1406.25,770.75) as "violin_ediff"
	AppendViolinPlot/L=e11/TN=toluene11 e11_a_toluene,e11_b_toluene,e11_c_toluene vs alpXabet
	AddWavesToViolinPlot e11_d_toluene,e11_e_toluene,e11_f_toluene,e11_g_toluene,e11_i_toluene
	AppendViolinPlot/L=e22/TN=toluene22 e22_a_toluene,e22_b_toluene,e22_c_toluene vs alpXabet
	AddWavesToViolinPlot e22_d_toluene,e22_e_toluene,e22_f_toluene,e22_g_toluene,e22_i_toluene
	ModifyGraph margin(left)=55,margin(bottom)=28
	ModifyGraph mode=4
	ModifyGraph rgb=(1,16019,65535)
	ModifyGraph lineJoin(toluene11)={0,10},lineJoin(toluene22)={0,10}
	ModifyGraph grid(bottom)=1
	ModifyGraph zero(e22)=1
	ModifyGraph standoff(e11)=0,standoff(bottom)=0
	ModifyGraph gridRGB(bottom)=(0,0,0)
	ModifyGraph gridStyle(bottom)=5
	ModifyGraph gridHair(bottom)=0
	ModifyGraph lblPosMode(e11)=1,lblPosMode(e22)=1
	ModifyGraph freePos(e11)={0,kwFraction}
	ModifyGraph freePos(e22)={0,kwFraction}
	ModifyGraph axisEnab(e11)={0,0.45}
	ModifyGraph axisEnab(e22)={0.51,1}
	ModifyGraph manTick(e11)={0,50,0,0},manMinor(e11)={4,50}
	ModifyGraph manTick(e22)={0,50,0,0},manMinor(e22)={4,50}
	Label e11 "E₁₁ diff [meV]"
	Label e22 "E₂₂ diff [meV]"
	SetAxis e11 0,150
	SetAxis e22 0,150
	ModifyViolinPlot trace=toluene11,DataMarker=19,LineColor=(0,0,0,0),LineThickness=0
	ModifyViolinPlot trace=toluene11,LineStyle=0
	ModifyViolinPlot trace=toluene22,DataMarker=19,LineColor=(0,0,0,0),LineThickness=0
	ModifyViolinPlot trace=toluene22,LineStyle=0
EndMacro

Window violin_fwhm() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(442.5,308.75,891,524) as "violin_fwhm"
	AppendViolinPlot/L=e11/TN=air11 fwhm11_a_air,fwhm11_b_air,fwhm11_c_air vs alpXabet
	AddWavesToViolinPlot fwhm11_d_air,fwhm11_e_air,fwhm11_f_air,fwhm11_g_air,fwhm11_i_air
	AddWavesToViolinPlot fwhm11_j_air,fwhm11_k_air
	AppendViolinPlot/L=e11/TN=toluene11 fwhm11_a_toluene,fwhm11_b_toluene vs alpXabet
	AddWavesToViolinPlot fwhm11_c_toluene,fwhm11_d_toluene,fwhm11_e_toluene,fwhm11_f_toluene
	AddWavesToViolinPlot fwhm11_g_toluene,fwhm11_i_toluene,fwhm11_j_toluene,fwhm11_k_toluene
	AppendViolinPlot/L=e22/TN=air22 fwhm22_a_air,fwhm22_b_air,fwhm22_c_air vs alpXabet
	AddWavesToViolinPlot fwhm22_d_air,fwhm22_e_air,fwhm22_f_air,fwhm22_g_air,fwhm22_i_air
	AddWavesToViolinPlot fwhm22_j_air,fwhm22_k_air
	AppendViolinPlot/L=e22/TN=toluene22 fwhm22_a_toluene,fwhm22_b_toluene vs alpXabet
	AddWavesToViolinPlot fwhm22_c_toluene,fwhm22_d_toluene,fwhm22_e_toluene,fwhm22_f_toluene
	AddWavesToViolinPlot fwhm22_g_toluene,fwhm22_i_toluene,fwhm22_j_toluene,fwhm22_k_toluene
	ModifyGraph margin(left)=55,margin(bottom)=28
	ModifyGraph mode=4
	ModifyGraph rgb(toluene11)=(1,16019,65535),rgb(toluene22)=(1,16019,65535)
	ModifyGraph lineJoin(air11)={0,10},lineJoin(toluene11)={0,10},lineJoin(air22)={0,10}
	ModifyGraph lineJoin(toluene22)={0,10}
	ModifyGraph grid(bottom)=1
	ModifyGraph zero(e22)=1
	ModifyGraph standoff(e11)=0,standoff(bottom)=0
	ModifyGraph gridRGB(bottom)=(0,0,0)
	ModifyGraph gridStyle(bottom)=5
	ModifyGraph gridHair(bottom)=0
	ModifyGraph lblPosMode(e11)=1,lblPosMode(e22)=1
	ModifyGraph freePos(e11)={0,kwFraction}
	ModifyGraph freePos(e22)={0,kwFraction}
	ModifyGraph axisEnab(e11)={0,0.45}
	ModifyGraph axisEnab(e22)={0.51,1}
	ModifyGraph manTick(e11)={0,25,0,0},manMinor(e11)={4,50}
	ModifyGraph manTick(e22)={0,50,0,0},manMinor(e22)={0,50}
	Label e11 "E₁₁ emission\rFWHM [meV]"
	Label e22 "E₂₂ excitation\rFWHM [meV]"
	SetAxis e11 0,30
	SetAxis e22 0,200
EndMacro

Window maps_shift() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(492.75,213.5,941.25,461) :dielectric_acn:mkl50_570_589_0_d_e22/TN=mkl50_570_589_0_d_ vs :dielectric_acn:mkl50_570_589_0_d_e11 as "maps_shift"
	AppendToGraph :dielectric_acn:mkl50_569_600_0_b_e22/TN=mkl50_569_600_0_b_ vs :dielectric_acn:mkl50_569_600_0_b_e11
	AppendToGraph wavelength_a_e22 vs wavelength_a_e11
	AppendToGraph wavelength_b_e22 vs wavelength_b_e11
	AppendToGraph wavelength_c_e22 vs wavelength_c_e11
	AppendToGraph wavelength_d_e22 vs wavelength_d_e11
	AppendToGraph wavelength_e_e22 vs wavelength_e_e11
	AppendToGraph wavelength_g_e22 vs wavelength_g_e11
	AppendToGraph wavelength_f_e22 vs wavelength_f_e11
	AppendToGraph wavelength_i_e22 vs wavelength_i_e11
	AppendToGraph wavelength_k_e22 vs wavelength_k_e11
	AppendToGraph wavelength_m_e22 vs wavelength_m_e11
	AppendToGraph wavelength_toluene[*][0]/TN=coordinates vs wavelength_toluene[*][1]
	ModifyGraph margin(left)=72,margin(right)=72,expand=-1,width={Plan,1,bottom,left}
	ModifyGraph mode(mkl50_570_589_0_d_)=4,mode(mkl50_569_600_0_b_)=4,mode(wavelength_a_e22)=4
	ModifyGraph mode(wavelength_b_e22)=4,mode(wavelength_c_e22)=4,mode(wavelength_d_e22)=4
	ModifyGraph mode(wavelength_e_e22)=4,mode(wavelength_g_e22)=4,mode(wavelength_f_e22)=4
	ModifyGraph mode(wavelength_i_e22)=4,mode(wavelength_k_e22)=4,mode(wavelength_m_e22)=4
	ModifyGraph mode(coordinates)=3
	ModifyGraph marker(mkl50_570_589_0_d_)=19,marker(mkl50_569_600_0_b_)=19,marker(wavelength_a_e22)=19
	ModifyGraph marker(wavelength_b_e22)=19,marker(wavelength_c_e22)=19,marker(wavelength_d_e22)=19
	ModifyGraph marker(wavelength_e_e22)=19,marker(wavelength_g_e22)=19,marker(wavelength_f_e22)=19
	ModifyGraph marker(wavelength_i_e22)=19,marker(wavelength_k_e22)=19,marker(wavelength_m_e22)=19
	ModifyGraph rgb(coordinates)=(4369,4369,4369)
	ModifyGraph gaps=0
	ModifyGraph useMrkStrokeRGB(wavelength_a_e22)=1,useMrkStrokeRGB(wavelength_b_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_c_e22)=1,useMrkStrokeRGB(wavelength_d_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_e_e22)=1,useMrkStrokeRGB(wavelength_g_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_f_e22)=1,useMrkStrokeRGB(wavelength_i_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_k_e22)=1,useMrkStrokeRGB(wavelength_m_e22)=1
	ModifyGraph offset(coordinates)={-30,5}
	ModifyGraph zColor(mkl50_570_589_0_d_)={twodigits,*,*,cindexRGB,0,colors},zColor(mkl50_569_600_0_b_)={twodigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_a_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_b_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_c_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_d_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_e_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_g_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_f_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_i_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_k_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_m_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph textMarker(coordinates)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	ModifyGraph axOffset(bottom)=0.533333
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	SetAxis left 500,800
	SetAxis bottom 800,1280
	ErrorBars wavelength_a_e22 XY,wave=(wavelength_a_e11err,wavelength_a_e11err),wave=(wavelength_a_e22err,wavelength_a_e22err)
	ErrorBars wavelength_b_e22 XY,wave=(wavelength_b_e11err,wavelength_b_e11err),wave=(wavelength_b_e22err,wavelength_b_e22err)
	ErrorBars wavelength_c_e22 XY,wave=(wavelength_c_e11err,wavelength_c_e11err),wave=(wavelength_c_e22err,wavelength_c_e22err)
	ErrorBars wavelength_d_e22 XY,wave=(wavelength_d_e11err,wavelength_d_e11err),wave=(wavelength_d_e22err,wavelength_d_e22err)
	ErrorBars wavelength_e_e22 XY,wave=(wavelength_e_e11err,wavelength_e_e11err),wave=(wavelength_e_e22err,wavelength_e_e22err)
	ErrorBars wavelength_g_e22 XY,wave=(wavelength_g_e11err,wavelength_g_e11err),wave=(wavelength_g_e22err,wavelength_g_e22err)
	ErrorBars wavelength_f_e22 XY,wave=(wavelength_f_e11err,wavelength_f_e11err),wave=(wavelength_f_e22err,wavelength_f_e22err)
	ErrorBars wavelength_i_e22 XY,wave=(wavelength_i_e11err,wavelength_i_e11err),wave=(wavelength_i_e22err,wavelength_i_e22err)
	ErrorBars wavelength_k_e22 XY,wave=(wavelength_k_e11err,wavelength_k_e11err),wave=(wavelength_k_e22err,wavelength_k_e22err)
	ErrorBars wavelength_m_e22 XY,wave=(wavelength_m_e11err,wavelength_m_e11err),wave=(wavelength_m_e22err,wavelength_m_e22err)
	TextBox/C/N=text0/S=3/A=LT/X=0.00/Y=0.00 "\\K(65535,1685,1685)\\W5019\\K(0,0,0) desorbed\r\\K(52428,52428,52428)\\W5019\\K(0,0,0) air\r\\K(0,0,0)\\W5019\\K(0,0,0) AcN"
	AppendText "\\K(1,16019,65535)\\W5019\\K(0,0,0) PhMe"
EndMacro

Window maps_shift_combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(837,368.75,1295.25,651.5) as "maps_shift_combined"
	DefineGuide twothird={FL,0.66,FR}
	Display/W=(0.2,0.2,0.8,0.8)/FG=(FL,FT,twothird,FB)/HOST=#  :dielectric_acn:mkl50_570_589_0_d_e22/TN=mkl50_570_589_0_d_ vs :dielectric_acn:mkl50_570_589_0_d_e11
	AppendToGraph :dielectric_acn:mkl50_569_600_0_b_e22/TN=mkl50_569_600_0_b_ vs :dielectric_acn:mkl50_569_600_0_b_e11
	AppendToGraph wavelength_a_e22 vs wavelength_a_e11
	AppendToGraph wavelength_b_e22 vs wavelength_b_e11
	AppendToGraph wavelength_c_e22 vs wavelength_c_e11
	AppendToGraph wavelength_d_e22 vs wavelength_d_e11
	AppendToGraph wavelength_e_e22 vs wavelength_e_e11
	AppendToGraph wavelength_g_e22 vs wavelength_g_e11
	AppendToGraph wavelength_f_e22 vs wavelength_f_e11
	AppendToGraph wavelength_i_e22 vs wavelength_i_e11
	AppendToGraph wavelength_k_e22 vs wavelength_k_e11
	AppendToGraph wavelength_m_e22 vs wavelength_m_e11
	AppendToGraph wavelength_toluene[*][0]/TN=coordinates vs wavelength_toluene[*][1]
	ModifyGraph expand=1,width={Plan,1,bottom,left}
	ModifyGraph mode(mkl50_570_589_0_d_)=4,mode(mkl50_569_600_0_b_)=4,mode(wavelength_a_e22)=4
	ModifyGraph mode(wavelength_b_e22)=4,mode(wavelength_c_e22)=4,mode(wavelength_d_e22)=4
	ModifyGraph mode(wavelength_e_e22)=4,mode(wavelength_g_e22)=4,mode(wavelength_f_e22)=4
	ModifyGraph mode(wavelength_i_e22)=4,mode(wavelength_k_e22)=4,mode(wavelength_m_e22)=4
	ModifyGraph mode(coordinates)=3
	ModifyGraph marker(mkl50_570_589_0_d_)=19,marker(mkl50_569_600_0_b_)=19,marker(wavelength_a_e22)=19
	ModifyGraph marker(wavelength_b_e22)=19,marker(wavelength_c_e22)=19,marker(wavelength_d_e22)=19
	ModifyGraph marker(wavelength_e_e22)=19,marker(wavelength_g_e22)=19,marker(wavelength_f_e22)=19
	ModifyGraph marker(wavelength_i_e22)=19,marker(wavelength_k_e22)=19,marker(wavelength_m_e22)=19
	ModifyGraph rgb(coordinates)=(4369,4369,4369)
	ModifyGraph gaps=0
	ModifyGraph useMrkStrokeRGB(wavelength_a_e22)=1,useMrkStrokeRGB(wavelength_b_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_c_e22)=1,useMrkStrokeRGB(wavelength_d_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_e_e22)=1,useMrkStrokeRGB(wavelength_g_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_f_e22)=1,useMrkStrokeRGB(wavelength_i_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_k_e22)=1,useMrkStrokeRGB(wavelength_m_e22)=1
	ModifyGraph offset(coordinates)={-30,5}
	ModifyGraph zColor(mkl50_570_589_0_d_)={twodigits,*,*,cindexRGB,0,colors},zColor(mkl50_569_600_0_b_)={twodigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_a_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_b_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_c_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_d_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_e_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_g_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_f_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_i_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_k_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_m_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph textMarker(coordinates)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	ModifyGraph lblMargin(bottom)=5
	ModifyGraph lblPosMode(bottom)=1
	ModifyGraph lblLatPos(bottom)=-6
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	SetAxis left 500,800
	SetAxis bottom 800,1280
	ErrorBars wavelength_a_e22 XY,wave=(wavelength_a_e11err,wavelength_a_e11err),wave=(wavelength_a_e22err,wavelength_a_e22err)
	ErrorBars wavelength_b_e22 XY,wave=(wavelength_b_e11err,wavelength_b_e11err),wave=(wavelength_b_e22err,wavelength_b_e22err)
	ErrorBars wavelength_c_e22 XY,wave=(wavelength_c_e11err,wavelength_c_e11err),wave=(wavelength_c_e22err,wavelength_c_e22err)
	ErrorBars wavelength_d_e22 XY,wave=(wavelength_d_e11err,wavelength_d_e11err),wave=(wavelength_d_e22err,wavelength_d_e22err)
	ErrorBars wavelength_e_e22 XY,wave=(wavelength_e_e11err,wavelength_e_e11err),wave=(wavelength_e_e22err,wavelength_e_e22err)
	ErrorBars wavelength_g_e22 XY,wave=(wavelength_g_e11err,wavelength_g_e11err),wave=(wavelength_g_e22err,wavelength_g_e22err)
	ErrorBars wavelength_f_e22 XY,wave=(wavelength_f_e11err,wavelength_f_e11err),wave=(wavelength_f_e22err,wavelength_f_e22err)
	ErrorBars wavelength_i_e22 XY,wave=(wavelength_i_e11err,wavelength_i_e11err),wave=(wavelength_i_e22err,wavelength_i_e22err)
	ErrorBars wavelength_k_e22 XY,wave=(wavelength_k_e11err,wavelength_k_e11err),wave=(wavelength_k_e22err,wavelength_k_e22err)
	ErrorBars wavelength_m_e22 XY,wave=(wavelength_m_e11err,wavelength_m_e11err),wave=(wavelength_m_e22err,wavelength_m_e22err)
	TextBox/C/N=text0/S=3/A=LT/X=0.00/Y=0.00 "\\K(65535,1685,1685)\\W5019\\K(0,0,0) desorbed\r\\K(52428,52428,52428)\\W5019\\K(0,0,0) air\r\\K(0,0,0)\\W5019\\K(0,0,0) AcN"
	AppendText "\\K(1,16019,65535)\\W5019\\K(0,0,0) PhMe"
	RenameWindow #,maps_shift
	SetActiveSubwindow ##
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:adsorption:
	Display/W=(0.2,0.2,0.8,0.8)/FG=(twothird,FT,FR,FB)/HOST=#  ediff_toluene,ediff_toluene vs diameter
	AppendToGraph ediff_waterlayer vs diameter
	AppendToGraph ediff_waterlayer vs diameter
	AppendToGraph ediff22_toluene,ediff22_waterlayer vs diameter
	AppendToGraph fit_ediff_waterlayer,fit_ediff_toluene
	SetDataFolder fldrSav0
	ModifyGraph margin(top)=56
	ModifyGraph mode(ediff_toluene)=3,mode(ediff_toluene#1)=3,mode(ediff_waterlayer)=3
	ModifyGraph mode(ediff_waterlayer#1)=3,mode(ediff22_toluene)=3,mode(ediff22_waterlayer)=3
	ModifyGraph marker(ediff_toluene)=19,marker(ediff_toluene#1)=19,marker(ediff_waterlayer)=19
	ModifyGraph marker(ediff_waterlayer#1)=19,marker(ediff22_toluene)=8,marker(ediff22_waterlayer)=8
	ModifyGraph rgb(ediff_toluene)=(0,15934,65535),rgb(ediff_toluene#1)=(0,15934,65535)
	ModifyGraph rgb(ediff_waterlayer)=(52428,52428,52428),rgb(ediff_waterlayer#1)=(4369,4369,4369)
	ModifyGraph rgb(ediff22_toluene)=(0,15934,65535),rgb(ediff22_waterlayer)=(4369,4369,4369)
	ModifyGraph rgb(fit_ediff_waterlayer)=(4369,4369,4369),rgb(fit_ediff_toluene)=(0,15934,65535)
	ModifyGraph useMrkStrokeRGB(ediff_waterlayer)=1
	ModifyGraph offset(ediff_toluene#1)={-0.02,-3},offset(ediff_waterlayer#1)={0.02,-3}
	ModifyGraph textMarker(ediff_toluene#1)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	ModifyGraph textMarker(ediff_waterlayer#1)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	Label left "energy difference [meV]"
	Label bottom "diameter [nm]"
	SetAxis left 0,110
	SetAxis bottom 0.625,1.025
	TextBox/C/N=text0/S=3/A=LT/X=-20.54/Y=-17.79 "air\tE\\B11\\M\\s(ediff_waterlayer) E\\B22\\M\\s(ediff22_waterlayer)"
	AppendText "PhMe\tE\\B11\\M\\s(ediff_toluene) E\\B22\\M\\s(ediff22_toluene)"
	RenameWindow #,ediff_adsorption
	SetActiveSubwindow ##
EndMacro

Window ediff_adsorption() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:adsorption:
	Display /W=(959.25,311,1416.75,528.5) ediff_toluene,ediff_toluene,ediff_waterlayer vs diameter as "ediff_adsorption"
	AppendToGraph ediff_waterlayer vs diameter
	AppendToGraph ::fit_ediff_waterlayer,::fit_ediff_toluene
	AppendToGraph ediff22_toluene,ediff22_waterlayer vs diameter
	SetDataFolder fldrSav0
	ModifyGraph margin(top)=56
	ModifyGraph mode(ediff_toluene)=3,mode(ediff_toluene#1)=3,mode(ediff_waterlayer)=3
	ModifyGraph mode(ediff_waterlayer#1)=3,mode(ediff22_toluene)=3,mode(ediff22_waterlayer)=3
	ModifyGraph marker(ediff_toluene)=19,marker(ediff_toluene#1)=19,marker(ediff_waterlayer)=19
	ModifyGraph marker(ediff_waterlayer#1)=19,marker(ediff22_toluene)=8,marker(ediff22_waterlayer)=8
	ModifyGraph rgb(ediff_toluene)=(4369,4369,4369),rgb(ediff_toluene#1)=(4369,4369,4369)
	ModifyGraph rgb(fit_ediff_toluene)=(4369,4369,4369),rgb(ediff22_toluene)=(4369,4369,4369)
	ModifyGraph offset(ediff_toluene#1)={-0.02,3},offset(ediff_waterlayer#1)={-0.02,3}
	ModifyGraph textMarker(ediff_toluene#1)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	ModifyGraph textMarker(ediff_waterlayer#1)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	Label left "energy difference [meV]"
	Label bottom "diameter [nm]"
	SetAxis left 20,110
	SetAxis bottom 0.625,1.025
	Legend/C/N=text0/J/S=1/A=LT/X=-2.85/Y=-31.15 "toluene \t\tE\\B11\\M\\s(ediff_toluene) E\\B22\\M\\s(ediff22_toluene)"
	AppendText "water layer\tE\\B11\\M\\s(ediff_waterlayer) E\\B22\\M\\s(ediff22_waterlayer)"
EndMacro

Window maps_shift_air() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(959.25,225.5,1407.75,473) wavelength_a_e22[0,1] vs wavelength_a_e11 as "maps_shift_air"
	AppendToGraph wavelength_b_e22[0,1] vs wavelength_b_e11[0,1]
	AppendToGraph wavelength_c_e22[0,1] vs wavelength_c_e11[0,1]
	AppendToGraph wavelength_d_e22[0,1] vs wavelength_d_e11[0,1]
	AppendToGraph wavelength_e_e22[0,1] vs wavelength_e_e11[0,1]
	AppendToGraph wavelength_g_e22[0,1] vs wavelength_g_e11[0,1]
	AppendToGraph wavelength_f_e22[0,1] vs wavelength_f_e11[0,1]
	AppendToGraph wavelength_i_e22[0,1] vs wavelength_i_e11[0,1]
	AppendToGraph wavelength_k_e22[0,1] vs wavelength_k_e11[0,1]
	AppendToGraph wavelength_m_e22[0,1] vs wavelength_m_e11[0,1]
	AppendToGraph wavelength_toluene[*][0]/TN=coordinates vs wavelength_toluene[*][1]
	ModifyGraph margin(left)=72,margin(right)=72,expand=-1,width={Plan,1,bottom,left}
	ModifyGraph mode(wavelength_a_e22)=4,mode(wavelength_b_e22)=4,mode(wavelength_c_e22)=4
	ModifyGraph mode(wavelength_d_e22)=4,mode(wavelength_e_e22)=4,mode(wavelength_g_e22)=4
	ModifyGraph mode(wavelength_f_e22)=4,mode(wavelength_i_e22)=4,mode(wavelength_k_e22)=4
	ModifyGraph mode(wavelength_m_e22)=4,mode(coordinates)=3
	ModifyGraph marker(wavelength_a_e22)=19,marker(wavelength_b_e22)=19,marker(wavelength_c_e22)=19
	ModifyGraph marker(wavelength_d_e22)=19,marker(wavelength_e_e22)=19,marker(wavelength_g_e22)=19
	ModifyGraph marker(wavelength_f_e22)=19,marker(wavelength_i_e22)=19,marker(wavelength_k_e22)=19
	ModifyGraph marker(wavelength_m_e22)=19
	ModifyGraph rgb(coordinates)=(4369,4369,4369)
	ModifyGraph gaps=0
	ModifyGraph useMrkStrokeRGB(wavelength_a_e22)=1,useMrkStrokeRGB(wavelength_b_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_c_e22)=1,useMrkStrokeRGB(wavelength_d_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_e_e22)=1,useMrkStrokeRGB(wavelength_g_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_f_e22)=1,useMrkStrokeRGB(wavelength_i_e22)=1
	ModifyGraph useMrkStrokeRGB(wavelength_k_e22)=1,useMrkStrokeRGB(wavelength_m_e22)=1
	ModifyGraph offset(coordinates)={-75,-5}
	ModifyGraph zColor(wavelength_a_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_b_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_c_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_d_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_e_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_g_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_f_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_i_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph zColor(wavelength_k_e22)={threedigits,*,*,cindexRGB,0,colors},zColor(wavelength_m_e22)={threedigits,*,*,cindexRGB,0,colors}
	ModifyGraph textMarker(coordinates)={wavelength_toluene_label,"default",0,0,5,0.00,0.00}
	ModifyGraph axOffset(bottom)=0.533333
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"
	SetAxis left 500,800
	SetAxis bottom 800,1280
	ErrorBars wavelength_a_e22 XY,wave=(wavelength_a_e11err,wavelength_a_e11err),wave=(wavelength_a_e22err,wavelength_a_e22err)
	ErrorBars wavelength_b_e22 XY,wave=(wavelength_b_e11err,wavelength_b_e11err),wave=(wavelength_b_e22err,wavelength_b_e22err)
	ErrorBars wavelength_c_e22 XY,wave=(wavelength_c_e11err,wavelength_c_e11err),wave=(wavelength_c_e22err,wavelength_c_e22err)
	ErrorBars wavelength_d_e22 XY,wave=(wavelength_d_e11err,wavelength_d_e11err),wave=(wavelength_d_e22err,wavelength_d_e22err)
	ErrorBars wavelength_e_e22 XY,wave=(wavelength_e_e11err,wavelength_e_e11err),wave=(wavelength_e_e22err,wavelength_e_e22err)
	ErrorBars wavelength_g_e22 XY,wave=(wavelength_g_e11err,wavelength_g_e11err),wave=(wavelength_g_e22err,wavelength_g_e22err)
	ErrorBars wavelength_f_e22 XY,wave=(wavelength_f_e11err,wavelength_f_e11err),wave=(wavelength_f_e22err,wavelength_f_e22err)
	ErrorBars wavelength_i_e22 XY,wave=(wavelength_i_e11err,wavelength_i_e11err),wave=(wavelength_i_e22err,wavelength_i_e22err)
	ErrorBars wavelength_k_e22 XY,wave=(wavelength_k_e11err,wavelength_k_e11err),wave=(wavelength_k_e22err,wavelength_k_e22err)
	ErrorBars wavelength_m_e22 XY,wave=(wavelength_m_e11err,wavelength_m_e11err),wave=(wavelength_m_e22err,wavelength_m_e22err)
	TextBox/C/N=text0/S=3/A=LT/X=0.00/Y=0.00 "\\K(65535,1685,1685)\\W5019\\K(0,0,0) desorbed\r\\K(52428,52428,52428)\\W5019\\K(0,0,0) air"
EndMacro

