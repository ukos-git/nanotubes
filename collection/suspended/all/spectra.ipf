#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

#include "sma"
#include "plem"

Function export()
	saveWindow("autocorrelation", saveVector = 1)
	saveWindow("diff", saveJSON=0, saveVector = 1)
	saveWindow("intensity_pfobpy", saveJSON=0, saveVector = 1)
	saveWindow("intensity_solvent", saveJSON=0, saveVector = 1)
End

Function cnt(cw, xw) : FitFunc
	WAVE cw; Variable xw

	return cw[2] * exp( -((xw - cw[0])/cw[1])^2 ) + cw[2]/cw[5] * exp( -((xw - 1240/(1240/cw[0]-cw[4]))/cw[1])^2 ) + cw[3]
End

Function createPeakCoef(numPeak)
	Variable numPeak

	WAVE fwhm = root:peakFWHM
	WAVE peakHeight = root:peakHeight
	WAVE peakLocation = root:peakLocation

	WAVE coef = root:coef
	WAVE limits = root:limits

	coef[0] = peakLocation[numPeak]
	limits[0][0] = coef[0] - 5
	limits[0][1] = coef[0] + 5

	coef[1] = 13 / (2*sqrt(ln(2)))
	limits[1][0] = 10  / (2*sqrt(ln(2)))
	limits[1][1] = 40  / (2*sqrt(ln(2)))

	coef[2] = peakHeight[numPeak]
	coef[3] = 0
	coef[4] = 60e-3
	limits[4][0] = 15e-3
	limits[4][1] = 150e-3
	coef[5] = coef[2]/5
	limits[5][0] = 1
	limits[5][1] = 50
End

Function analyzeSideBands()
	Struct PLEMd2stats stats
	Variable tolerance
	Variable i, dim0

	WAVE wavelength = root:wavelength_air
	WAVE diameter = root:diameter

	WAVE/U/I indices = getIndices("air.*")
	dim0 = DimSize(indices, 0)

	Make/O/N=(dim0) root:ediff/WAVE=ediff = NaN
	Make/O/N=(dim0) root:energy/WAVE=energy = NaN
	Make/O/N=(dim0) root:fwhm/WAVE=fwhm = NaN
	Make/O/N=(dim0) root:diameters/WAVE=diameters = NaN

	Make/O/N=(dim0) root:ediff_err/WAVE=ediff_err = NaN
	Make/O/N=(dim0) root:energy_err/WAVE=energy_err = NaN

	for(i = 1; i < dim0; i += 1)
		PLEMd2statsLoad(stats, PLEMd2strPLEM(indices[i]))
		WAVE corrected = RemoveSpikes(stats.wavPLEM)

		createPeakCoef(indices[i])
		Wave coef = root:coef
		WAVE limits = root:limits
		//Gencurvefit/MAT/X={stats.wavWavelength} cnt, corrected, coef,"000000",limits
		Abort "Install GenCurveFit"
		WAVE M_Covar
		if(stats.numDetector == PLEMd2detectorNewton && 1240/(1240/coef[0]-coef[4]) > 1050)
			continue // out of detectable range
		endif
		if(coef[4] < 0.02 || coef[4] > 0.145)
			continue // band in range
		endif
		if(coef[5] > 30)
			continue // band not detectable
		endif
		if((coef[1] * 2 * sqrt(ln(2))) > 30)
			continue // fwhm too big
		endif

		ediff[i] = coef[4]
		energy[i] = coef[0]
		ediff_err[i] = sqrt(M_Covar[4][4])
		energy_err[i] = sqrt(M_Covar[0][0])
		fwhm[i] = coef[1] * (2*sqrt(ln(2)))

		tolerance = 0.5
		do
			FindValue/Z/V=(energy[i])/T=(tolerance) wavelength
			tolerance += 0.5
		while(V_Value == -1)
		diameters[i] = diameter[V_Value]
	endfor
End


Function searchBestSpectrum()
	Struct PLEMd2stats stats
	Variable i, dim0, maxVal, j

	WAVE peakLocation = root:peakLocation
	WAVE peakHeight = root:peakHeight

	WAVE/U/I indices = getIndices("air.*")
	dim0 = DimSize(indices, 0)

	for(i = 1; i < dim0; i += 1)
		PLEMd2statsLoad(stats, PLEMd2strPLEM(indices[i]))
		WAVE corrected = RemoveSpikes(stats.wavPLEM)
		if(peakLocation[indices[i]] > 850)
			continue // search (6,4)
		endif

		if(peakHeight[indices[i]] > maxVal)
			j = indices[i]
			maxVal = peakHeight[indices[i]]
		endif
	endfor
	PLEMd2displaybynum(j)
End



Window intensity_full() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(424.5,56.375,820.5,264.875) as "intensity_full"
	AppendViolinPlot hexan_decrease,acn_decrease,toluene_decrease vs solventsT
	ModifyGraph margin(left)=55
	ModifyGraph mode=4
	ModifyGraph manTick(left)={0,25,-2,0},manMinor(left)={0,50}
	Label left "intensity in solvent\rnormalized to air [%]"
	SetAxis left 1e-06,1
	ModifyViolinPlot trace=hexan_decrease,LineThickness=0,ShowMedian
EndMacro

Window autocorrelation() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(15.75,284.75,474,493.25) acncovariance_si,pfobpycovariance_si,toluenecovariance_si as "autocorrelation"
	AppendToGraph hexancovariance_si,aircovariance_si
	ModifyGraph mode=7
	ModifyGraph rgb(acncovariance_si)=(1,16019,65535),rgb(pfobpycovariance_si)=(65535,43690,0)
	ModifyGraph rgb(hexancovariance_si)=(0,65535,0),rgb(aircovariance_si)=(0,0,0)
	ModifyGraph hbFill=5
	ModifyGraph hBarNegFill(toluenecovariance_si)=5
	ModifyGraph offset(acncovariance_si)={0,0.0133},offset(pfobpycovariance_si)={0,0.01}
	ModifyGraph offset(toluenecovariance_si)={0,0.0066},offset(hexancovariance_si)={0,0.0033}
	ModifyGraph muloffset(pfobpycovariance_si)={0,2},muloffset(hexancovariance_si)={0,30}
	ModifyGraph nticks(left)=0
	ModifyGraph noLabel(left)=2
	ModifyGraph axThick(left)=0
	Label bottom "wavelength [nm]"
	SetAxis bottom 820,1039
	Legend/C/N=text0/J/F=0/B=(65535,65535,65535,32768)/A=MC/X=-36.92/Y=34.00 "\\s(acncovariance_si) AcN 37.5\r\\s(pfobpycovariance_si) PFO-Bpy in PhMe"
	AppendText "\\s(toluenecovariance_si) PhMe 2.38\r\\s(hexancovariance_si) n-Hexane 1.88\r\\s(aircovariance_si) Air 0"
EndMacro

Window diff() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(15.75,58.25,472.5,265.25) toluene_ediff11 vs toluene_e11air as "diff"
	AppendToGraph hexan_ediff11 vs hexan_e11air
	AppendToGraph acn_ediff11 vs acn_e11air
	ModifyGraph expand=-1
	ModifyGraph mode=3
	ModifyGraph marker=19
	ModifyGraph rgb(toluene_ediff11)=(65535,32768,32768),rgb(hexan_ediff11)=(40969,65535,16385)
	ModifyGraph rgb(acn_ediff11)=(32768,40777,65535)
	ModifyGraph useMrkStrokeRGB(hexan_ediff11)=1
	ModifyGraph manTick(left)={0,25,0,0},manMinor(left)={0,50}
	Label left "energy difference\rin solvent [meV]"
	Label bottom "emission in air [nm]"
	SetAxis left 0,125
	SetAxis bottom 820,1039
	Legend/C/N=text0/J/S=1/A=LT/X=0.00/Y=0.00 "\\s(acn_ediff11) acetonitrile\r\\s(toluene_ediff11) toluene\r\\s(hexan_ediff11) hexane"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= prel,ycoord= left,dash= 3
	DrawLine 0.05,75,0.95,75
	SetDrawEnv xcoord= prel,ycoord= left,dash= 3
	DrawLine 0.05,40,0.95,40
	SetDrawEnv xcoord= bottom,ycoord= left,fillfgc= (65535,65535,65535,32768)
	DrawPoly 928.752074000332,127.843721978988,1.30137,1.2,{950.965227402323,141.357710659416,1025.0782396088,38.780487804878,1024.99500836732,141.206926582215,951.156025806872,141.357710659416}
	SetDrawEnv xcoord= bottom,ycoord= left
	DrawText 957.365069473433,107.646016653871,"end of silicon QE"
EndMacro

Window intensity_pfobpy() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(483.75,286.25,940.5,494.75) as "intensity_pfobpy"
	AppendViolinPlot toluene_decrease,pfobpy_decrease vs solventsT3
	ModifyGraph mode=4
	ModifyGraph lineJoin(toluene_decrease)={0,10}
	ModifyGraph manTick(left)={0,25,-2,0},manMinor(left)={0,50}
	Label left "intensity in solvent\rnormalized to air [%]"
	SetAxis left 0,0.75
	ModifyViolinPlot trace=toluene_decrease,ShowMedian
EndMacro

Window intensity_solvent() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(482.25,59.75,940.5,266.75) as "intensity_solvent"
	AppendViolinPlot hexan_decrease,acn_decrease,toluene_decrease vs solventsT
	ModifyGraph margin(left)=55
	ModifyGraph mode=4
	ModifyGraph lineJoin(hexan_decrease)={0,10}
	ModifyGraph manTick(left)={0,25,-2,0},manMinor(left)={0,50}
	Label left "intensity in solvent\rnormalized to air [%]"
	SetAxis left 1e-06,0.75
	ModifyViolinPlot trace=hexan_decrease,LineThickness=0,ShowMedian
EndMacro

