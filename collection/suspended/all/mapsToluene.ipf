#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "plem"
#include "utilities-images"

Function export()
	SaveWindow("combined", saveJSON = 0)
	SaveWindow("acetonitrile", saveJSON = 0)
End

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(263.25,49.25,720.75,306.5) as "combined"
	AppendImage :PLEMd2:combined:data1:PLEM
	ModifyImage PLEM ctab= {1e-12,1.5e-10,Terrain,0}
	ModifyImage PLEM log= 1
	AppendImage/L=ltop :PLEMd2:combined:data1:PLEM
	ModifyImage PLEM#1 ctab= {0,5e-11,Terrain,0}
	AppendImage/B=middle :PLEMd2:combined:PLEM
	ModifyImage PLEM#2 ctab= {1e-12,1.5e-10,Terrain,0}
	ModifyImage PLEM#2 log= 1
	AppendImage/B=middle/L=ltop :PLEMd2:combined:PLEM
	ModifyImage PLEM#3 ctab= {0,1.5e-10,Terrain,0}
	AppendImage/B=bright/L=ltop :PLEMd2:combined:data2:PLEM
	ModifyImage PLEM#4 ctab= {0,2e-10,Terrain,0}
	AppendImage/B=bright :PLEMd2:combined:data2:PLEM
	ModifyImage PLEM#5 ctab= {1e-12,2e-10,Terrain,0}
	ModifyImage PLEM#5 log= 1
	ModifyGraph margin(right)=56,width={Plan,1,middle,left}
	ModifyGraph mirror(left)=0,mirror(bottom)=0
	ModifyGraph standoff(left)=0,standoff(bottom)=0,standoff(ltop)=0,standoff(middle)=0
	ModifyGraph lblPos(left)=43,lblPos(bottom)=37,lblPos(middle)=37
	ModifyGraph lblLatPos(left)=-52,lblLatPos(middle)=7
	ModifyGraph freePos(ltop)=0
	ModifyGraph freePos(middle)=0
	ModifyGraph freePos(bright)=0
	ModifyGraph axisEnab(left)={0,0.45}
	ModifyGraph axisEnab(bottom)={0,0.25}
	ModifyGraph axisEnab(ltop)={0.55,1}
	ModifyGraph axisEnab(middle)={0.3,0.55}
	ModifyGraph axisEnab(bright)={0.6,0.9}
	ModifyGraph manTick(bright)={0,100,0,0},manMinor(bright)={1,50}
	Label left "excitation [nm]"
	Label middle "emission [nm]"
	SetAxis left 510,*
	SetAxis bottom 810,1050
	SetAxis ltop 510,*
	SetAxis middle 810,1050
	ColorScale/C/N=text0/S=3/A=LT/X=90.00/Y=53.00 image=PLEM, heightPct=40
	ColorScale/C/N=text1/S=3/A=LT/X=90.00/Y=0.00 image=PLEM#1, heightPct=40
EndMacro

Window acetonitrile() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(727.5,54.5,1186.5,251) as "acetonitrile"
	AppendImage :PLEMd2:maps:mkl50Lacnbest_map_0:PLEM
	ModifyImage PLEM ctab= {2e-12,2e-11,:PLEMd2:colortable_plotly_earth,1}
	AppendImage/B=bright :PLEMd2:maps:mkl34survivorsSilicon_5:PLEM
	ModifyImage PLEM#1 ctab= {5e-13,1.5e-10,:PLEMd2:colortable_plotly_earth,1}
	ModifyGraph width={Plan,1,bottom,left}
	ModifyGraph grid=1
	ModifyGraph mirror(left)=0,mirror(bottom)=0
	ModifyGraph standoff(left)=0,standoff(bottom)=0
	ModifyGraph gridRGB=(65535,0,0)
	ModifyGraph gridHair=1
	ModifyGraph lblPosMode(bottom)=1
	ModifyGraph lblPos(bottom)=37
	ModifyGraph lblLatPos(bottom)=100
	ModifyGraph freePos(bright)={0,kwFraction}
	ModifyGraph axisEnab(bottom)={0,0.49}
	ModifyGraph axisEnab(bright)={0.51,1}
	Label left "excitation [nm]"
	Label bottom "emission [nm]"
	SetAxis left 550,740
	SetAxis bottom 810,1050
	SetAxis bright 810,1050
	TextBox/C/N=text0/S=1/A=LT/X=1.00/Y=1.00 "AcN"
	TextBox/C/N=text1/S=1/A=LT/X=52.00/Y=1.00 "PhMe"
EndMacro

