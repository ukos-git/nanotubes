#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "plem"
#include "sma"

// @get all relevant coordinate Waves
Function/S getWaveList()
	String list

	SetDataFolder root:data
	list = GrepList(WaveList("*", ";", ""), "mkl[0-9]+\w+[_](?:originals|coordinates)")
	SetDataFolder root:

	return list
End

Function analyseExperimentName(name, sample, experiment, dataType)
	String name
	String &sample, &experiment, &dataType

	SplitString/E="(mkl[0-9]+)(\w+)(?:_(originals|coordinates))?" name, sample, experiment, dataType
	if(V_flag != 3)
		return 1
	endif

	return 0
End

// @brief get all experiments for a given @p sample
Function/WAVE getExperiments(sample)
	String sample

	Variable i, numWaves
	String mklxx, experiment, dataType
	String list = ""

	WAVE/T waves = ListToTextWave(getWaveList(), ";")
	numWaves = DimSize(waves, 0)
	for(i = 0; i < numWaves; i += 1)
		if(analyseExperimentName(waves[i], mklxx, experiment, dataType))
			continue
		endif
		if(!!cmpstr(dataType, "originals"))
			continue
		endif
		if(!!cmpstr(mklxx, sample))
			continue
		endif

		list = AddListItem(experiment, list)
	endfor

	WAVE/T dummy = ListToTextWave(list, ";")
	FindDuplicates/FREE/RT=experiments dummy

	return experiments
End

Function/WAVE getSamples()
	Variable i, numWaves
	String sample, experiment, dataType
	String list = ""

	WAVE/T waves = ListToTextWave(getWaveList(), ";")
	numWaves = DimSize(waves, 0)
	for(i = 0; i < numWaves; i += 1)
		if(analyseExperimentName(waves[i], sample, experiment, dataType))
			continue
		endif
		if(!!cmpstr(dataType, "originals"))
			continue
		endif

		list = AddListItem(sample, list)
	endfor

	WAVE/T dummy = ListToTextWave(list, ";")
	if(DimSize(dummy, 0) == 0)
		return dummy
	endif

	Sort/A dummy, dummy
	FindDuplicates/FREE/RT=samples dummy
	return samples
End

// set filter to PLEMd2FILTER_FEHL0750
Function setFilter()
	Variable i
	string strPLEM, filters
	Struct Plemd2stats stats

	// set filter to PLEMd2FILTER_FEHL0750
	String FELHmaps = "mkl50Lacnbest_map_0;mkl50Lacnbest_map_1;"
	for(i = 0; i < ItemsInList(FELHmaps); i += 1)
		strPLEM = StringFromList(i, FELHmaps)
		PLEMd2statsload(stats, strPLEM)

		filters = "filterFELH0750;filterChroma760trans;reflSilver;reflSilver;reflSilver"
		PLEMd2SetCorrection(filters, stats.wavFilterEmi, stats.wavWavelength)
		filters = PLEMd2getFilterExcString(PLEMd2getSystem(stats.strUser), stats.numDetector)
		PLEMd2SetCorrection(filters, stats.wavFilterExc, stats.wavExcitation)

		PLEMd2BuildMaps(strPLEM)
	endfor
End

Function cleanUp()
	Variable i, j, k, numSamples, numExperiments

	WAVE/T samples = getSamples()
	numSamples = DimSize(samples, 0)
	for(i = 0; i < numSamples; i += 1)
		WAVE/T experiments = getExperiments(samples[i])
		numExperiments = DimSize(experiments, 0)
		for(j = 0; j < numExperiments; j += 1)
			WAVE coordinates = $("root:data:" + samples[i] + experiments[j] + "_coordinates")
			WAVE originals = $("root:data:" + samples[i] + experiments[j] + "_originals")
			if(!WaveExists(coordinates) || !WaveExists(originals))
				Abort "cleanup: invalid call"
			endif
			SMAdeleteDuplicateCoordinates(coordinates, coordinates, companion = originals)
		endfor
	endfor

	WAVE reference = root:data:mkl34airInGaAsBest_coordinates
	WAVE coordinates = root:data:mkl34airInGaAs_coordinates
	WAVE originals = root:data:mkl34airInGaAs_originals
	do
	while(SMAdeleteDuplicateCoordinates(coordinates, reference, companion = originals) > 0)
End

Function loadSpectra()
	STRUCT FILO#experiment filos
	FILO#structureLoad(filos)

	Struct SMAprefs prefs
	SMAloadPackagePrefs(prefs)

	cleanUp()

	filos.strFolder = prefs.strBasePath
	filos.strFileList = extractWaves("!*Maps*", ".*")

	FILO#structureSave(filos)
	printf "%d spectra\r", ItemsInList(filos.strFileList)

	testDuplicates()
End

Function loadMaps()
	STRUCT FILO#experiment filos
	FILO#structureLoad(filos)

	Struct SMAprefs prefs
	SMAloadPackagePrefs(prefs)

	cleanUp()

	filos.strFolder = prefs.strBasePath
	filos.strFileList = extractWaves("*", ".*Maps.*")

	FILO#structureSave(filos)
	printf "%d maps\r", ItemsInList(filos.strFileList)

	testDuplicates()
End

Function/S testDuplicates()
	String list

	STRUCT FILO#experiment filos
	FILO#structureLoad(filos)

	WAVE/T files = ListToTextWave(filos.strFileList, ";")
	files[] = CleanupName(ParseFilePath(3, files[p], ":", 0, 0), 0)
	FindDuplicates/FREE/DT=duplicates files

	if(DimSize(duplicates, 0) > 0)
		wfprintf list, "%s;", duplicates
		print "these filenames are duplicates:", duplicates
	endif
End

Function/S extractWaves(filter, keyword)
	String filter, keyword

	String list
	Variable i, numItems

	SetDataFolder root:data
	list = GrepList(WaveList(filter, ";", ""), ".*" + keyword + "_originals")
	if(ItemsInList(list) == 0)
		return ""
	endif
	Concatenate/FREE/T list, wv
	wfprintf list, "%s;", wv
	SetDataFolder root:

	return list
End

Function correlateExperiment(keyword)
	String keyword

	Variable xSampling, ySampling
	WAVE/U/I indices = getIndices(keyword + ".*")

	if(SMAgetCovarianceForDifferentSetups(indices))
		return 1
	endif

	// autocorrelation
	WAVE si = root:covariance_si_diag
	Duplicate/O si $("root:" + keyword + "covariance_si")
	WAVE ingaas = root:covariance_ingaas_diag
	Duplicate/O ingaas $("root:" + keyword + "covariance_ingaas")
	KillWaves/Z ingaas, si

	// full covariance matrix
	WAVE/Z si = root:covariance_si
	if(WaveExists(si))
		Duplicate/O si $("root:" + keyword + "covariance_si_full")
	endif
	WAVE/Z ingaas = root:covariance_ingaas
	if(WaveExists(ingaas))
		Duplicate/O ingaas $("root:" + keyword + "covariance_ingaasg_full")
	endif
	KillWaves/Z ingaas, si
End

Function correlateSolvents()
	NVAR/Z downsample = root:downsample
	if(!NVAR_EXISTS(downsample))
		Variable/G root:downsample
		NVAR downsample = root:downsample
	endif

	String experiment, experiments
	Variable i, numExperiments, timerRefNum
	String status

	experiments = "hexan;acn;pfobpy;recl;air;toluene;"
	numExperiments = ItemsInList(experiments)
	downsample = 2^0
	timerRefNum = StartMSTimer
	do
		downsample /= 2
		for(i = 0; i < numExperiments; i += 1)
			experiment = StringFromList(i, experiments)
			correlateExperiment(experiment)
			DoUpdate
			sprintf status, "experiment: %s \tsamplesize: %02dnm", experiment[0,2], downsample
			lap(timerRefNum, status)
		endfor
	while(downsample > 0.5)

	timerRefNum = StopMSTimer(timerRefNum)
End

Function fitSolvents(experiments)
	String experiments

	String experiment
	Variable i, numExperiments, timerRefNum
	String status
	String all = "hexan;acn;toluene;pfobpy;recl;air;"

	WAVE colors = root:colors
	if(!cmpstr(experiments, "all"))
		experiments = all
	endif

	numExperiments = ItemsInList(experiments)
	timerRefNum = StartMSTimer
	for(i = 0; i < numExperiments; i += 1)
		experiment = StringFromList(i, experiments)
		WAVE/U/I indices = getIndices(experiment + ".*")
		WAVE/Z init = $("root:wavelength_" + experiment)
		if(!WaveExists(init))
			WAVE init = root:wavelength_toluene
		endif
		WAVE/Z/T initT = $("root:wavelength_" + experiment + "T")
		if(!WaveExists(initT))
			WAVE/T initT = root:wavelength_tolueneT
		endif
		SMAatlasFit(indices, init, initT, verbose = 0)

		Duplicate/FREE/U/I/R=[WhichListItem(experiment, all)][] colors color
		SMAdisplayAtlasFit(indices, color)

		sprintf status, "experiment: %s \tspectra: %03d", experiment[0,2], DimSize(indices, 0)
		lap(timerRefNum, status)
	endfor

	timerRefNum = StopMSTimer(timerRefNum)
End

Window solvents() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(35.25,63.5,429.75,272) pfobpy_si,toluene_si,acn_si,hexan_si
	ModifyGraph rgb(hexan_si)=(0,0,0)
	ModifyGraph muloffset(hexan_si)={0,25}
	SetAxis left*,0.0169156478
EndMacro

Function/WAVE getIndices(keyword)
	String keyword

	Variable i, numMaps, numFound

	WAVE/T maps = ListToTextWave(PLEMd2getStrMapsAvailable(), ";")
	WAVE/T mapsSelection = ListToTextWave(extractWaves("*", keyword), ";")
	mapsSelection[] = CleanupName(ParseFilePath(3, mapsSelection[p], ":", 0, 0), 0)

	numMaps = Dimsize(mapsSelection, 0)
	Make/N=(numMaps)/U/I/FREE indices
	for(i = 0; i < numMaps; i += 1)
		FindValue/TEXT=(mapsSelection[i]) maps
		if(V_StartPos != 0 || V_Value < 0)
			continue
		endif

		indices[numFound] = V_Value
		numFound += 1
	endfor
	Redimension/N=(numFound) indices
	if(numFound < 2)
		return indices
	endif

	FindDuplicates/FREE/RN=dupsfree indices
	Sort dupsfree, dupsfree
	return dupsfree
End

Function findCoordinates(experiment)
	String experiment

	Variable i, numToluene, j, numMatches, k, numPeaks, numResults, peakID, numIndices
	String sample, dummy, dataType, name, win
	Struct Plemd2stats toluene
	Struct Plemd2stats air

	//win = "win_" + experiment
	win = experiment + "variance"

	DoWindow/F $win
	if(V_flag)
		WAVE/T traces = ListToTextWave(ListMatch(TraceNameList(win, ";", 1), "mk*"), ";")
		numMatches = DimSize(traces, 0)
		for(i = 0; i < numMatches; i += 1)
			RemoveFromGraph $traces[i]
		endfor
	else
		Display/N=$win
		win = S_name
	endif

	if(!DataFolderExists("root:dielectric_" + experiment))
		NewDataFolder $("root:dielectric_" + experiment)
	endif
	DFREF dfr = $("root:dielectric_" + experiment)

	WAVE original = $("root:wavelength_" + experiment)
	WAVE/T originalT = $("root:wavelength_" + experiment + "T")

	WAVE/T strPLEM = PLEMd2getAllstrPLEM()
	WAVE coordinates = PLEMd2getCoordinates()

	WAVE/U/I indices_toluene = getIndices(experiment + ".*")
	Make/N=(DimSize(indices_toluene, 0), 3)/FREE coordinates_toluene = coordinates[indices_toluene[p]][q]

	Make/O/N=(DimSize(indices_toluene, 0)  * 10) dfr:pleIntensity1D_decrease/WAVE=decrease
	Make/O/N=(DimSize(indices_toluene, 0)  * 10) dfr:pleIntensity2D_decrease/WAVE=decrease2
	Make/O/N=(DimSize(indices_toluene, 0)  * 10) dfr:pleDiff11/WAVE=ediff11
	Make/O/N=(DimSize(indices_toluene, 0)  * 10) dfr:pleDiff22/WAVE=ediff22

	Make/WAVE/FREE/N=(DimSize(originalT, 0)) e11diff, e22diff

	numToluene = DimSize(indices_toluene, 0)
	for(i = 0; i < numToluene; i += 1)
		PLEMd2statsload(toluene, strPLEM[indices_toluene[i]])
		numPeaks = DimSize(toluene.wavchiralityText, 0)
		if(numPeaks == 0)
			continue
		endif
		analyseExperimentName(strPLEM[indices_toluene[i]], sample, dummy, dataType)
		WAVE/U/I indices_air = getIndices(sample + "air.*")
		Make/N=(DimSize(indices_air, 0), 3)/FREE coordinates_air = coordinates[indices_air[p]][q]
		WAVE matches = CoordinateFinderXYZ(coordinates_air, coordinates_toluene[i][0], coordinates_toluene[i][1], coordinates_toluene[i][2], verbose = 0, accuracy = 10)
		if(!WaveExists(matches))
			continue
		endif
		numMatches = DimSize(matches, 0)
		if(numMatches == 0)
			continue
		endif
		matches[] = indices_air[matches[p]]
		for(j = 0; j < numMatches; j += 1)
			PLEMd2statsload(air, strPLEM[matches[j]])
			if(DimSize(air.wavchiralityText, 0) == 0)
				continue
			endif
			for(k = 0; k < numPeaks; k += 1)
				FindValue/TEXT=(toluene.wavchiralityText[k]) air.wavchiralityText
				if(V_Value != -1)
					peakID = V_Value

					// also matches arrow prefixes (→b) and suffixes (b↓)
					sprintf name, "%s_%03d_%03d_%d_%s_", sample, indices_toluene[i], matches[j], k, "" + toluene.wavchiralityText[k]
					Make/O/N=2 dfr:$(name + "e11")/WAVE=e11 = {air.wavEnergyS1[peakID], toluene.wavEnergyS1[k]}
					Make/O/N=2 dfr:$(name + "e22")/WAVE=e22 = {air.wavEnergyS2[peakID], toluene.wavEnergyS2[k]}
					Make/O/N=2 dfr:$(name + "intensity")/WAVE=intensity = {air.wav1Dfit[peakID], toluene.wav1Dfit[k]}
					if((e22[1] - e22[0]) < 0 || (e22[1] - e22[0]) > 75)
						continue // do not add missmatching peaks to graph
					endif
					AppendToGraph/W=$win e22/TN=$name vs e11
					ModifyGraph/W=$win zmrkSize($name)={intensity,0,*,0,10}
					ModifyGraph mode($name)=4
					if((e11[1] - e11[0]) < 40)
						ModifyGraph rgb($name)=(65535,0,0)
					else
						ModifyGraph rgb($name)=(65535,49151,49151,58982)
					endif

					ModifyGraph arrowMarker($name)={_inline_,0.75,7,0.5,2}
					decrease[numResults] = toluene.wav1Dfit[k] / air.wav1Dfit[peakID]
					decrease2[numResults] = toluene.wav2Dfit[k] / air.wav2Dfit[peakID]
					ediff11[numResults] = (1240 / air.wavEnergyS1[peakID] - 1240 / toluene.wavEnergyS1[k]) * 1e3
					ediff22[numResults] = (1240 / air.wavEnergyS2[peakID] - 1240 / toluene.wavEnergyS2[k]) * 1e3

					FindValue/TEXT=(toluene.wavchiralityText[k]) originalT
					if(V_Value != -1)
						peakID = V_Value
						WAVE peak = e11diff[peakID]
						if(!WaveExists(peak))
							Make/N=0/FREE peak
							e11diff[peakID] = peak
						endif
						Redimension/N=(DimSize(peak, 0) + 1) peak
						peak[DimSize(peak, 0) - 1] = ediff11[numResults]
	
						WAVE peak = e22diff[peakID]
						if(!WaveExists(peak))
							Make/N=0/FREE peak
							e22diff[peakID] = peak
						endif
						Redimension/N=(DimSize(peak, 0) + 1) peak
						peak[DimSize(peak, 0) - 1] = ediff22[numResults]
					endif

					numResults += 1	
				endif
			endfor
		endfor
	endfor

	numIndices = DimSize(original, 0)
	for(peakID = 0; peakID < numIndices; peakID += 1)
		WAVE/Z peak = e11diff[peakID]
		if(!WaveExists(peak))
			continue
		endif
		if(strlen(originalT[peakID]) > 1)
			continue // side peak
		endif
		Duplicate/O peak $("root:e11_" + originalT[peakID] + "_" + experiment)/WAVE=wv
		if(DimSize(wv, 0) == 1)
			Redimension/N=2 wv
			wv[1] = NaN
		endif
		wv[] = wv[p] >= 25 && wv[p] < 150 ? wv[p] : NaN

		WAVE peak = e22diff[peakID]
		Duplicate/O peak $("root:e22_" + originalT[peakID] + "_" + experiment)/WAVE=wv
		if(DimSize(wv, 0) == 1)
			Redimension/N=2 wv
			wv[1] = NaN
		endif
		wv[] = wv[p] >= 25 && wv[p] < 150 ? wv[p] : NaN
	endfor

	SetAxis bottom 820,1250
	SetAxis left 540,752
	Label left "excitation wavelength [nm]"
	Label bottom "emission wavelength [nm]"

	Redimension/N=(numResults) decrease, decrease2, ediff11, ediff22
End

Function peakAnalysisFindCoordinates(experiment)
	String experiment

	Variable i, numToluene, j, numMatches
	Variable diff, intensity
	Variable locAir, locTol, intAir, intTol
	String sample, dummy, dataType

	WAVE peakLocation = root:peakLocation
	WAVE peakFWHM = root:peakFWHM
	WAVE peakHeight = root:peakHeight


	WAVE/T strPLEM = PLEMd2getAllstrPLEM()
	WAVE coordinates = PLEMd2getCoordinates()

	WAVE/U/I indices_toluene = getIndices(experiment + ".*")
	Make/N=(DimSize(indices_toluene, 0), 3)/FREE coordinates_toluene = coordinates[indices_toluene[p]][q]

	Make/O/N=(DimSize(indices_toluene, 0)) $("root:" + experiment + "_decrease")/WAVE=decrease = NaN
	Make/O/N=(DimSize(indices_toluene, 0)) $("root:" + experiment + "_e11air")/WAVE=e11air = NaN
	Make/O/N=(DimSize(indices_toluene, 0)) $("root:" + experiment + "_e11")/WAVE=e11solvent = NaN
	Make/O/N=(DimSize(indices_toluene, 0)) $("root:" + experiment + "_ediff11")/WAVE=ediff11 = NaN

	numToluene = DimSize(indices_toluene, 0)
	for(i = 0; i < numToluene; i += 1)
		analyseExperimentName(strPLEM[indices_toluene[i]], sample, dummy, dataType)
		WAVE/U/I indices_air = getIndices(sample + "air.*")
		Make/N=(DimSize(indices_air, 0), 3)/FREE coordinates_air = coordinates[indices_air[p]][q]
		WAVE matches = CoordinateFinderXYZ(coordinates_air, coordinates_toluene[i][0], coordinates_toluene[i][1], coordinates_toluene[i][2], verbose = 0, accuracy = 10)
		if(!WaveExists(matches))
			continue
		endif
		numMatches = DimSize(matches, 0)
		if(numMatches > 1 || numMatches == 0)
			continue
		endif
		matches[] = indices_air[matches[p]]
		for(j = 0; j < numMatches; j += 1)
			locAir = peakLocation[matches[j]]
			locTol = peakLocation[indices_toluene[i]]
			intAir = peakHeight[matches[j]]
			intTol = peakHeight[indices_toluene[i]]

			diff = (1240/locAir - 1240/locTol) * 1e3
			intensity = intTol / intAir

			if(numtype(e11air[i]) && ediff11[i] < intensity)
				continue
			endif
			if(diff < 0 || diff > 150) // energy between 0 and 150meV
				continue
			endif

			e11air[i] = locAir
			e11solvent[i] = locTol
			decrease[i] = intensity
			ediff11[i] = diff
		endfor
	endfor
End

Function findMedian(experiment)
	String experiment

	Variable i, numIndices, j, numMatches, k, numPeaks, peakID
	String sample, dummy, dataType, name
	Struct Plemd2stats stats

	WAVE/T strPLEM = PLEMd2getAllstrPLEM()
	WAVE coordinates = PLEMd2getCoordinates()

	WAVE/U/I indices = getIndices(experiment + ".*")
	WAVE original = $("root:wavelength_" + experiment)
	WAVE/T originalT = $("root:wavelength_" + experiment + "T")

	Make/WAVE/FREE/N=(DimSize(originalT, 0)) e11, e22, e11fwhm, e22fwhm, pleIntensity

	numIndices = DimSize(indices, 0)
	for(i = 0; i < numIndices; i += 1)
		PLEMd2statsload(stats, strPLEM[indices[i]])

		numPeaks = DimSize(stats.wavchiralityText, 0)
		for(k = 0; k < numPeaks; k += 1)
			FindValue/TEXT=(stats.wavchiralityText[k]) originalT
			if(V_Value != -1 && V_startPos == 0 && !cmpstr(originalT[V_Value], stats.wavchiralityText[k]))
				peakID = V_Value
				WAVE peak = e11[peakID]
				if(!WaveExists(peak))
					Make/N=0/FREE peak
					e11[peakID] = peak
				endif
				Redimension/N=(DimSize(peak, 0) + 1) peak
				peak[DimSize(peak, 0) - 1] = stats.wavEnergyS1[k]

				WAVE peak = e22[peakID]
				if(!WaveExists(peak))
					Make/N=0/FREE peak
					e22[peakID] = peak
				endif
				Redimension/N=(DimSize(peak, 0) + 1) peak
				peak[DimSize(peak, 0) - 1] = stats.wavEnergyS2[k]

				WAVE peak = e11fwhm[peakID]
				if(!WaveExists(peak))
					Make/N=0/FREE peak
					e11fwhm[peakID] = peak
				endif
				Redimension/N=(DimSize(peak, 0) + 1) peak
				peak[DimSize(peak, 0) - 1] = stats.wavFWHMS1[k] > 4 && stats.wavFWHMS1[k] < 30 ? stats.wavFWHMS1[k] : NaN
				peak[DimSize(peak, 0) - 1] = 1240e3/stats.wavEnergyS1[k] - 1240e3/(peak[DimSize(peak, 0) - 1] + stats.wavEnergyS1[k])

				WAVE peak = e22fwhm[peakID]
				if(!WaveExists(peak))
					Make/N=0/FREE peak
					e22fwhm[peakID] = peak
				endif
				Redimension/N=(DimSize(peak, 0) + 1) peak
				peak[DimSize(peak, 0) - 1] = stats.wavFWHMS2[k]
				peak[DimSize(peak, 0) - 1] = 1240e3/stats.wavEnergyS2[k] - 1240e3/(peak[DimSize(peak, 0) - 1] + stats.wavEnergyS2[k])
			endif
		endfor
	endfor

	Duplicate/FREE original medians, variances
	numIndices = DimSize(original, 0)
	for(peakID = 0; peakID < numIndices; peakID += 1)
		WAVE peak = e11[peakID]
		if(!WaveExists(peak))
			continue
		endif
		medians[peakID][1] = median(peak)
		variances[peakID][1] = variance(peak)

		WAVE peak = e22[peakID]
		if(!WaveExists(peak))
			continue
		endif
		medians[peakID][0] = median(peak)
		variances[peakID][0] = variance(peak)

		WAVE peak = e11fwhm[peakID]
		if(strlen(originalT[peakID]) > 1)
			continue // side peak
		endif
		Duplicate/O peak $("root:fwhm11_" + originalT[peakID] + "_" + experiment)/WAVE=wv
		if(DimSize(wv, 0) == 1)
			Redimension/N=2 wv
			wv[1] = NaN
		endif
		WAVE peak = e22fwhm[peakID]
		Duplicate/O peak $("root:fwhm22_" + originalT[peakID] + "_" + experiment)/WAVE=wv
		if(DimSize(wv, 0) == 1)
			Redimension/N=2 wv
			wv[1] = NaN
		endif
	endfor

	variances[][] = round(sqrt(variances[p][q]) / 0.5) * 0.5
	print variance(variances)
	Duplicate/O variances $("root:wavelength_" + experiment + "Err")
	//return 0
	original[][] = round(medians[p][q] / 0.5) * 0.5
End

