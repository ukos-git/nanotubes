#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	saveWindow("excitation_660vs565", saveVector=1)
End

Window excitation_660vs565() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(35.25,41.75,493.5,239)/R trans_dc660 as "excitation_660vs565"
	AppendToGraph :PLEMd2:maps:spectrumLED570_0:PLEM/TN=led565 vs :PLEMd2:maps:spectrumLED570_0:xWavelength
	AppendToGraph :PLEMd2:maps:spectrumLED660_0:PLEM/TN=led660 vs :PLEMd2:maps:spectrumLED660_0:xWavelength
	AppendToGraph :led660nm:led660fullspectrum/TN=led660 vs :led660nm:xWavelength
	AppendToGraph/R trans_filter570bp
	ModifyGraph mode(trans_dc660)=7,mode(led660)=7,mode(trans_filter570bp)=7
	ModifyGraph rgb(trans_dc660)=(0,0,0),rgb(led565)=(3,52428,1),rgb(led660)=(52428,1,1)
	ModifyGraph rgb(led660#1)=(52428,1,1),rgb(trans_filter570bp)=(3,52428,1)
	ModifyGraph hbFill(trans_dc660)=2,hbFill(led565)=2,hbFill(led660)=2,hbFill(trans_filter570bp)=2
	ModifyGraph usePlusRGB(trans_dc660)=1,usePlusRGB(led660)=1,usePlusRGB(trans_filter570bp)=1
	ModifyGraph plusRGB(trans_dc660)=(0,0,0,13107),plusRGB(led565)=(0,65535,0,13107)
	ModifyGraph plusRGB(led660)=(52428,1,1,49151),plusRGB(trans_filter570bp)=(1,65535,33232,49151)
	ModifyGraph manTick(right)={0,25,0,0},manMinor(right)={0,50}
	ModifyGraph manTick(left)={0,0.5,0,1},manMinor(left)={1,50}
	Label right "transmission [%]"
	Label bottom "wavelength [nm]"
	Label left "spectral intensity [a.u.]"
	SetAxis right 0,100
	SetAxis bottom 525,725
	SetAxis left 0,1
	Legend/C/N=text0/J/S=3/A=MC/X=37.15/Y=38.96 "\\s(led660) 660nm led\r\\s(trans_filter570bp) 565nm led\r\\s(trans_dc660) dc660"
EndMacro

