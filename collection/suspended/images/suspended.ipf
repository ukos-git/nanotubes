#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"
#include <SaveGraph>

Function export()
	saveWindow("plcam", saveJSON = 1)
	saveWindow("mkc3sem")
	saveWindow("mkc3sem_single")
End

Window plcam() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:suspended_plcam:
	Display /W=(213.75,44.75,672.75,442.25)/L=left2 profile0 vs profile0_coord as "plcam"
	AppendImage fullimage_zoom
	ModifyImage fullimage_zoom ctab= {5559.75,30410.1477272727,YellowHot,0}
	SetDataFolder fldrSav0
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=85,width={Plan,1,bottom,left}
	ModifyGraph mode=7
	ModifyGraph hbFill=2
	ModifyGraph usePlusRGB=1
	ModifyGraph hBarNegFill=2
	ModifyGraph plusRGB=(65535,0,0,6554)
	ModifyGraph mirror(bottom)=0,mirror(left)=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph fSize=12
	ModifyGraph axThick=0
	ModifyGraph lblPosMode=1
	ModifyGraph lblPos(bottom)=39,lblPos(left)=50
	ModifyGraph freePos(left2)={0,bottom}
	ModifyGraph axisEnab(left2)={0.52,1}
	ModifyGraph axisEnab(left)={0,0.5}
	Label left2 "PL intensity [a.u.]"
	Label bottom "position / µm"
	Label left "position / µm"
	SetAxis left2 0,*
	SetAxis bottom 110.3,120.3
	ColorScale/C/N=text0/F=0/B=1/A=RB/X=-19.54/Y=0.20 image=fullimage_zoom
	ColorScale/C/N=text0 heightPct=50, fsize=12
	ColorScale/C/N=text0 userTicks={:suspended_plcam:colorticks,:suspended_plcam:colorticksT}
	ColorScale/C/N=text0 lblMargin=-10, tickUnit=1, axisRange={0,NaN,0}
	AppendText "PL intensity [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 3
	DrawLine 117.037037037037,0.562962962962963,119.037037037037,0.562962962962963
	SetDrawEnv fsize= 16
	DrawText 0.714814814814815,0.644444444444444,"2µm"
	SetWindow kwTopWin,userdata(WMZoomBrowser)=  "SMAgetCoordinatesfullImage"
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6/IPF)Y].A7]:YE,9#mD-pdkDfTB&DIIWuF'gX_3r"
	SetWindow kwTopWin,userdata(fullimage_zoom)=  "image_sliderLimits={0,37065};"
EndMacro

Window mkc3sem() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(401.25,305.75,858.75,440.75) as "mkc3sem"
	AppendImage :trenches_sem3:image1
	ModifyImage image1 ctab= {0,254,YellowHot,1}
	AppendImage/B=bottom2/R=right1 :trenches_sem3:image0
	ModifyImage image0 ctab= {0,254,YellowHot,1}
	AppendImage/B=bottom3/R=right2 :trenches_sem3:image0
	ModifyImage image0#1 ctab= {0,254,YellowHot,1}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=85,gfSize=12
	ModifyGraph width={Plan,1,bottom,left}
	ModifyGraph mirror(left)=0,mirror(bottom)=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph fSize(left)=12,fSize(bottom)=12,fSize(right1)=12,fSize(bottom2)=12
	ModifyGraph standoff=0
	ModifyGraph axThick=0
	ModifyGraph lblPosMode(bottom)=1,lblPosMode(right2)=4
	ModifyGraph lblPos(left)=48,lblPos(bottom)=43,lblPos(right1)=84,lblPos(right2)=45
	ModifyGraph lblLatPos(bottom)=180
	ModifyGraph freePos(right1)={1.2,bottom2}
	ModifyGraph freePos(bottom2)={-4,left}
	ModifyGraph freePos(right2)={0,bottom3}
	ModifyGraph freePos(bottom3)={-4,left}
	ModifyGraph axisEnab(bottom)={0,0.3}
	ModifyGraph axisEnab(bottom2)={0.33,0.63}
	ModifyGraph axisEnab(bottom3)={0.66,0.96}
	ModifyGraph manTick(left)={0,2,0,0},manMinor(left)={1,2}
	ModifyGraph manTick(bottom)={0,2,0,0},manMinor(bottom)={1,2}
	ModifyGraph manTick(right1)={0,1,0,0},manMinor(right1)={1,50}
	ModifyGraph manTick(bottom2)={0,1,0,0},manMinor(bottom2)={1,50}
	ModifyGraph manTick(right2)={0,0.1,0,1},manMinor(right2)={1,50}
	ModifyGraph manTick(bottom3)={0,0.1,0,1},manMinor(bottom3)={1,50}
	SetAxis left -4,5
	SetAxis bottom -5,4
	SetAxis right1 -0.2,2.2
	SetAxis bottom2 -1.2,1.2
	SetAxis right2 0.9,1.1
	SetAxis bottom3 -0.2,0
	ColorScale/C/N=text0/F=0/H={0,2,10}/A=MC/X=56.37/Y=-1.39 image=image1
	ColorScale/C/N=text0 heightPct=100
	AppendText "SEM intensity [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= left,linethick= 2,fillfgc= (65535,65535,65535,32768),fillbgc= (65535,65535,65535,32768)
	DrawRect -1.2,2.2,1.2,-0.2
	SetDrawEnv xcoord= bottom2,ycoord= right1,linethick= 2,fillfgc= (65535,65535,65535,32768),fillbgc= (65535,65535,65535,32768)
	DrawRect -0.2,1.1,0,0.9
	SetDrawEnv xcoord= bottom3,ycoord= right2,linethick= 2,fillfgc= (65535,65535,65535,32768),fillbgc= (65535,65535,65535,32768)
	DrawRect -0.199082568807339,1.1,0.000917431192660551,0.9
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 3
	DrawLine 1.41284403669725,0.187614678899083,3.41284403669725,0.187614678899083
	DrawText 0.209904127630451,0.309951580020388,"2µm"
	DrawText 0.526708535343949,0.323840468909276,"500nm"
	SetDrawEnv xcoord= bottom2,ycoord= prel,linethick= 3
	DrawLine 0.5,0.187614678899083,1,0.187614678899083
	DrawText 0.865551510550561,0.323840468909276,"50nm"
	SetDrawEnv xcoord= bottom3,ycoord= prel,linethick= 3
	DrawLine -0.0591743119266055,0.187614678899083,-0.00917431192660551,0.187614678899083
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6/LNASu!kATM4\"ASjIi"
EndMacro

Window mkc3sem_single() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(687.75,65.75,1145.25,334.25) as "mkc3sem_single"
	AppendImage :trenches_sem3:image1
	ModifyImage image1 ctab= {0,254,YellowHot,0}
	ModifyGraph margin(left)=108,margin(bottom)=14,margin(top)=14,margin(right)=108
	ModifyGraph gfSize=16,height={Plan,1,left,bottom}
	ModifyGraph mirror=0
	ModifyGraph nticks=0
	ModifyGraph noLabel=2
	ModifyGraph fSize=12
	ModifyGraph lblMargin(left)=176
	ModifyGraph standoff=0
	ModifyGraph axThick=0
	ModifyGraph lblPosMode(bottom)=1
	ModifyGraph lblPos(left)=48,lblPos(bottom)=43
	ModifyGraph lblLatPos(left)=-6,lblLatPos(bottom)=180
	ModifyGraph manTick(left)={0,2,0,0},manMinor(left)={1,2}
	ModifyGraph manTick(bottom)={0,2,0,0},manMinor(bottom)={1,2}
	SetAxis left 2.2,-0.2
	SetAxis bottom -1.2,1.2
	ColorScale/C/N=text0/F=0/H={0,2,10}/B=1/A=RC/X=-39.53/Y=-0.66 image=image1
	ColorScale/C/N=text0 heightPct=100
	AppendText "SEM intensity [a.u.]"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,linethick= 3,linefgc= (65535,65535,65535)
	DrawLine 0.621818181818182,0.157403198536545,1.12181818181818,0.157403198536545
	SetDrawEnv fsize= 16,textrgb= (65535,65535,65535)
	DrawText 0.769972451790633,0.245017877435628,"500nm"
	SetWindow kwTopWin,userdata(WM_IP_Data)= A"A7duFDf9_Y-Wa#=EbT]*FCjH^M^LPW:h4!V@6/LNASu!kATM4\"ASjIi"
EndMacro

