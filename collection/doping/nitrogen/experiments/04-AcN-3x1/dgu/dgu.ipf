#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <Multi-peak fitting 2.0>
#include <SaveGraph>

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("combined", saveVector = 1)
	SaveWindow("dguv2_65_wavelength", saveVector = 1)
End

Function buildmap(basepath, numFractions)
	String basePath // = "root:mk122v2d2f"
	Variable numFractions //= 19
	
	Variable i
	
	SetDataFolder root:

	wave template = $(basePath + "1")
	Duplicate/O template $(basePath + "X")
	wave map = $(basePath + "X")
	redimension/N=(DimSize(template, 0), numFractions) map
	
	for(i = 0; i < numFractions; i += 1)
		wave/Z current = $(basePath + num2str(i + 1))
		if (WaveExists(current))
			map[][i] = current[p][0]
		else
			map[][i] = 0
		endif
	endfor
	Display
	AppendImage map
End

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(34.5,41.75,493.5,227) mk121v2d1f6_norm,mk121v2d1f8_norm as "combined"
	AppendToGraph/R :PLEMd2:maps:mk121v2d1f8:PLEM[*][8] vs :PLEMd2:maps:mk121v2d1f8:xWavelength
	AppendToGraph/R :PLEMd2:maps:mk121v2d1f6_1:PLEM[*][8] vs :PLEMd2:maps:mk121v2d1f6_1:xWavelength
	AppendToGraph/R/B=bottom_left :PLEMd2:maps:mk120v2d1f7_25:PLEM[*][8] vs :PLEMd2:maps:mk120v2d1f7_25:xWavelength
	AppendToGraph/R/B=bottom_left :PLEMd2:maps:mk120v2d1f10_25:PLEM[*][8] vs :PLEMd2:maps:mk120v2d1f10_25:xWavelength
	AppendToGraph/B=bottom_left mk120v2d1f7_norm,mk120v2d1f10_norm
	ModifyGraph margin(left)=144,margin(top)=36,gbRGB=(61166,61166,61166)
	ModifyGraph mode(PLEM)=3,mode(PLEM#1)=3,mode(PLEM#2)=3,mode(PLEM#3)=3
	ModifyGraph marker(mk121v2d1f8_norm)=1,marker(PLEM)=60,marker(PLEM#1)=60,marker(PLEM#2)=60
	ModifyGraph marker(PLEM#3)=60
	ModifyGraph lStyle(PLEM)=3,lStyle(PLEM#1)=3
	ModifyGraph rgb(mk121v2d1f6_norm)=(0,0,0),rgb(PLEM#1)=(0,0,0),rgb(PLEM#2)=(1,16019,65535)
	ModifyGraph rgb(PLEM#3)=(0,65535,0),rgb(mk120v2d1f7_norm)=(1,16019,65535),rgb(mk120v2d1f10_norm)=(0,65535,0)
	ModifyGraph msize(PLEM)=1,msize(PLEM#1)=1,msize(PLEM#2)=1,msize(PLEM#3)=1
	ModifyGraph grid(bottom)=1,grid(bottom_left)=1
	ModifyGraph lblMargin(left)=102
	ModifyGraph gridRGB(bottom)=(24576,24576,65535,13107),gridRGB(bottom_left)=(24576,24576,65535,13107)
	ModifyGraph lblPosMode(left)=3,lblPosMode(bottom)=1
	ModifyGraph lblPos(left)=40,lblPos(bottom)=35
	ModifyGraph lblLatPos(bottom)=-70
	ModifyGraph prescaleExp(right)=13
	ModifyGraph freePos(bottom_left)=0
	ModifyGraph axisEnab(bottom)={0.55,1}
	ModifyGraph axisEnab(bottom_left)={0,0.45}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={3,2}
	Label left "norm. OD"
	Label bottom "wavelength [nm]"
	Label right "PL (norm to abs)"
	SetAxis left 0,1
	SetAxis bottom 1010,1080
	SetAxis right 0,5e-13
	SetAxis bottom_left 1010,1080
	Cursor/P A mk121v2d1f8_norm 318;Cursor/P B mk121v2d1f6_norm 312
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=-82.17/Y=9.69 "(7,5) chirality \renriched DGU fractions\r\\s(PLEM#1) PL \\s(mk121v2d1f6_norm) Abs\rPL@646nm bp 4nm\r\rfractions:"
	AppendText "0% AcN\r\\s(mk120v2d1f7_norm) top\r\n\\s(mk120v2d1f10_norm) bottom\r5% AcN\r\\s(mk121v2d1f6_norm) top\r\\s(mk121v2d1f8_norm) bottom"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=-23.00/Y=60.00 "0% AcN"
	TextBox/C/N=text2/F=0/B=1/A=MC/X=23.00/Y=60.00 "5% AcN"
EndMacro

Window dguv2_65_wavelength() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(31.5,250.25,489.75,434.75)/R :PLEMd2:maps:mk120v2d1f4_0:PLEM[*][15],:PLEMd2:maps:mk121v2d1f3:PLEM[*][15] as "dguv2_65_wavelength"
	AppendToGraph mk120v2d1f4_norm,mk121v2d1f3_norm
	ModifyGraph gbRGB=(61166,61166,61166,0)
	ModifyGraph marker(PLEM)=60,marker(PLEM#1)=60
	ModifyGraph lStyle(PLEM)=3,lStyle(PLEM#1)=3
	ModifyGraph rgb(PLEM)=(0,0,0),rgb(mk120v2d1f4_norm)=(0,0,0)
	ModifyGraph msize(PLEM)=1,msize(PLEM#1)=1
	ModifyGraph prescaleExp(right)=12
	ModifyGraph gridEnab(bottom)={0.4,0.6}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={3,2}
	Label right "PL norm. to Abs."
	Label bottom "wavelength [nm]"
	Label left "norm. OD"
	SetAxis right -1e-13,4.6e-12
	SetAxis bottom 800,1050
	SetAxis left -0.02,1
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=0.19/Y=47.06 "(6,5) enriched dgu (top) fraction\rexcitation at 570nm (4nm bp)"
	TextBox/C/N=text1/F=0/B=1/A=LT "AcN vol.%\r\n\\s(mk120v2d1f4_norm) 0%\r\n\\s(mk121v2d1f3_norm) 5%"
	TextBox/C/N=text2/F=0/B=1/A=LT/Y=39.64 "\\s(PLEM) PL\r\\s(mk120v2d1f4_norm) Abs"
EndMacro

