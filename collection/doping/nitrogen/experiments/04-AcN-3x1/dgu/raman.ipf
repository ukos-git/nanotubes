#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <TransformAxis1.2>

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("raman_rbm", saveVector = 1)
	SaveWindow("raman_gband", saveVector = 1)
	SaveWindow("raman_g2band", saveVector = 1)
End

Window raman_complete() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(543.75,80.75,938.25,289.25) mk120d1f4_full_raman vs mk120d1f4_full_wl as "raman_complete"
	AppendToGraph mk121v2d1f3_full_raman vs mk121v2d1f3_full_wl
	AppendToGraph mk122v2d1f3_full_raman vs mk122v2d1f3_full_wl
	ModifyGraph rgb(mk120d1f4_full_raman)=(0,0,0),rgb(mk122v2d1f3_full_raman)=(65535,49151,49151)
	SetAxis left -111.653794389165,8086.01933873902
	SetAxis bottom 120.681599928066,3307.26935409423
EndMacro

Window raman_rbm() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(34.5,47,492,269) mk121v2d1f3_full_raman vs mk121v2d1f3_full_wl as "raman_rbm"
	AppendToGraph/R mk120d1f4_full_raman vs mk120d1f4_full_wl
	AppendToGraph/R/T mk120d1f4_full_raman vs :Packages:AxisTransform_raman_rbm:AxisTransform_top:mk120d1f4_full_wl_TX
	ModifyGraph userticks(top)={:Packages:AxisTransform_raman_rbm:AxisTransform_top:tickVals,:Packages:AxisTransform_raman_rbm:AxisTransform_top:tickLabels}
	ModifyGraph rgb(mk120d1f4_full_raman)=(0,0,0)
	Label left "raman intensity [arb.u.]"
	Label bottom "chemical shift [cm⁻¹]"
	Label top "diameter [nm]"
	SetAxis left 57.194475417359,851.711971734087
	SetAxis bottom 50,500
	SetAxis right -0.635762711864402,1169.88203389831
	SetAxis top 50,500
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=29.84/Y=31.76 "(6,5)-fraction vol.% AcN\r\\s(mk121v2d1f3_full_raman) 5%\r\\s(mk120d1f4_full_raman) 0%"
	SetWindow kwTopWin,hook(TransformAxisHook)=TransformAxisWindowHook1_2
	SetWindow kwTopWin,userdata(TransAxFolder)=  "root:Packages:AxisTransform_raman_rbm:"
	SetWindow kwTopWin,userdata(TransAxVersion)=  "1.2"
EndMacro

Window raman_gband() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(504,45.5,960.75,262.25) mk121v2d1f3_full_raman vs mk121v2d1f3_full_wl as "raman_gband"
	AppendToGraph/R mk120d1f4_full_raman vs mk120d1f4_full_wl
	ModifyGraph rgb(mk120d1f4_full_raman)=(0,0,0)
	Label left "raman intensity [arb.u.]"
	Label bottom "chemical shift [cm⁻¹]"
	SetAxis left 131.698732699957,6409.9807955791
	SetAxis bottom 1100,1652.0032
	SetAxis right 109.12716,9550
	Cursor/P A mk120d1f4_full_raman 3783
	Legend/C/N=text0/J/F=0/A=MC/X=-26.70/Y=25.85 "(6,5)-fraction vol.% AcN\r\\s(mk121v2d1f3_full_raman) 5%\r\\s(mk120d1f4_full_raman) 0%"
EndMacro

Window raman_g2band() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(36.75,296,493.5,517.25) mk121v2d1f3_full_raman vs mk121v2d1f3_full_wl as "raman_g2band"
	AppendToGraph/R mk120d1f4_full_raman vs mk120d1f4_full_wl
	ModifyGraph rgb(mk120d1f4_full_raman)=(0,0,0)
	Label left "raman intensity [arb.u.]"
	Label bottom "chemical shift [cm⁻¹]"
	SetAxis left 223.576031181115,2214.25083160621
	SetAxis bottom 2551.30890052356,2650.26178010471
	SetAxis right 244.484795302289,3177.23365478379
	Legend/C/N=text0/J/F=0/A=MC/X=-26.70/Y=25.85 "(6,5)-fraction vol.% AcN\r\\s(mk121v2d1f3_full_raman) 5%\r\\s(mk120d1f4_full_raman) 0%"
EndMacro

