#pragma TextEncoding = "Windows-1252"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("selectivity")
	SaveWindow("selectivity_pfo_doc")
End

Window selectivity_pfo_doc() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(89.25,119,546,305.75)/B=bottom_left mk126_3 as "selectivity_pfo_doc"
	AppendToGraph mk126_3,mk121v2_50_norm
	AppendToGraph/R fit_mk121_2_norm,mk121_2_norm
	AppendToGraph/B=bottom_left fit_mk121v2_50_norm,mk121v2_50_norm
	AppendToGraph/R/B=bottom_left fit_mk121_2_norm,mk121_2_norm
	ModifyGraph margin(left)=28,margin(top)=14,margin(right)=14,height={Aspect,0.33}
	ModifyGraph mode(mk126_3)=7,mode(mk126_3#1)=7,mode(mk121v2_50_norm)=7,mode(fit_mk121_2_norm)=7
	ModifyGraph mode(mk121_2_norm)=7,mode(mk121v2_50_norm#1)=7,mode(mk121_2_norm#1)=7
	ModifyGraph lSize(mk121v2_50_norm)=1.2,lSize(mk121_2_norm)=1.2,lSize(mk121v2_50_norm#1)=1.2
	ModifyGraph lSize(mk121_2_norm#1)=1.2
	ModifyGraph rgb(mk126_3)=(1,16019,65535),rgb(mk126_3#1)=(1,16019,65535),rgb(mk121v2_50_norm)=(0,0,0)
	ModifyGraph rgb(fit_mk121_2_norm)=(65535,49151,49151),rgb(mk121v2_50_norm#1)=(0,0,0,32768)
	ModifyGraph hbFill(mk126_3)=5,hbFill(mk126_3#1)=5,hbFill(mk121v2_50_norm)=5,hbFill(fit_mk121_2_norm)=2
	ModifyGraph hbFill(mk121_2_norm)=5,hbFill(mk121v2_50_norm#1)=5,hbFill(mk121_2_norm#1)=5
	ModifyGraph plusRGB(mk121_2_norm)=(65535,32768,32768,32768)
	ModifyGraph offset(mk126_3)={0,-3.11813186813187},offset(mk126_3#1)={0,-2.25},offset(fit_mk121_2_norm)={4.494382022472,-0.0410000000000001}
	ModifyGraph muloffset(mk126_3)={0,4},muloffset(mk126_3#1)={0,2.4}
	ModifyGraph noLabel(left)=1,noLabel(right)=2
	ModifyGraph fSize=11
	ModifyGraph axThick(right)=0
	ModifyGraph lblPosMode(left)=1,lblPosMode(bottom_left)=1,lblPosMode(bottom)=1
	ModifyGraph lblPos(bottom)=45
	ModifyGraph lblLatPos(bottom)=87
	ModifyGraph axisOnTop(bottom_left)=1,axisOnTop(bottom)=1
	ModifyGraph freePos(bottom_left)={0,kwFraction}
	ModifyGraph axisEnab(bottom_left)={0.55,1}
	ModifyGraph axisEnab(bottom)={0,0.45}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(bottom_left)={0,200,0,0},manMinor(bottom_left)={1,50}
	ModifyGraph manTick(bottom)={0,200,0,0},manMinor(bottom)={1,5}
	Label left "OD norm."
	Label bottom "wavelength [nm]"
	SetAxis left 0,1.1
	SetAxis bottom_left 950,1225
	SetAxis bottom 500,800
	SetAxis right 0,1.1
	TextBox/C/N=text0/F=0/A=LT/X=35.03/Y=-5.37 "\\s(mk126_3) SDS\r\\s(mk121v2_50_norm) DOC\r\\s(mk121_2_norm) PFO"
EndMacro

Window selectivity() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(959.25,53.75,1267.5,372.5) mk126_1,mk126_2,mkbgtoluene,mk126_3 as "selectivity"
	ModifyGraph rgb(mk126_1)=(0,0,0),rgb(mk126_2)=(26205,52428,1),rgb(mkbgtoluene)=(1,16019,65535)
	ModifyGraph offset(mk126_1)={0,0.895484949832776},offset(mk126_2)={0,0.512709030100335}
	ModifyGraph offset(mkbgtoluene)={0,-0.192905802004826},offset(mk126_3)={0,0.877926421404682}
	ModifyGraph noLabel(left)=1
	ModifyGraph lblMargin(left)=21
	ModifyGraph lblLatPos(left)=3
	Label left "OD (spectra with offset to 0)"
	Label bottom "wavelength / nm"
	SetAxis left -0.1,2
	SetAxis bottom 925,1250
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=38.90/Y=19.90 "750 grad\r\\s(mk126_3) SDS\r\\s(mk126_1) pfo\r\\s(mk126_2) pfo-bpy\r\r\\s(mkbgtoluene) toluene"
EndMacro

