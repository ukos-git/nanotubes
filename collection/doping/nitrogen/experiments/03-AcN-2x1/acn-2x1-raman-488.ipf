#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <Multi-peak fitting 2.0>
#include <SaveGraph>
#include <TransformAxis1.2>
#include <Split Axis>

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("combined", saveVector = 1)
	SaveWindow("gplus", saveVector = 1)
	SaveWindow("gband", saveVector = 1)
	SaveWindow("g2band", saveVector = 1)
End

Function Cm1ToNm(inverse_centimeters)
	Variable inverse_centimeters
	// x nm = 10,000,000 / x cm–1
	return 1e9 / 1e2 / inverse_centimeters
End

Function NmToCm1(nanometer)
	Variable nanometer
	// y cm–1 = 10,000,000 / y nm
	return 1e9 / 1e2 / nanometer
End

// input wl of laser in nm
Function NmGpeak(laser)
	Variable laser
	return Cm1ToNm(NmToCm1(laser) - 1500)
End

Function ArgonGpeak()
	// 514,5 nm und die türkisblaue 488,0 nm Linie.
	print nmGpeak(488), nmGpeak(514.5)
	return 0
End

Function Argon488NmToCm(nanometer)
	Variable nanometer
	return NmToCm1(488) - NmToCm1(nanometer)
End

Function Argon488CmToNm(inversecm)
	Variable inversecm
	return Cm1ToNm(NmToCm1(488) - inversecm)
End

Function Argon515CmToNm(inversecm)
	Variable inversecm
	return Cm1ToNm(NmToCm1(514.5) - inversecm)
End

Function Argon515NmToCm(nanometer)
	Variable nanometer
	return NmToCm1(514.5) - NmToCm1(nanometer)
End

Function Argon()
//1092,3 	(infrarot)
//528,7 	grün
//514,5 	grün
//501,7 	grün
//496,5 	türkis
//488,0 	türkis
//476,5 	blau
//472,7 	blau
//465,8 	blau
//457,9 	blau
//454,5 	blau
//363,8 	(UV-A)
//351,1 	(UV-A)
	// 514,5 nm und die türkisblaue 488,0 nm Linie.
	print 488, 514.5
	return 0
End

Function colorize([colorTable])
	String colorTable
	if(ParamIsDefault(colorTable))
		colorTable = ""
	endif
	
	String myList ="mk109,mk110,mk111,mk112,mk113,mk114,mk115"
	String entity, entitynorm, entityfit
	String traceList, colorList
	Variable numList, i
	Variable color_offset, color_range, color
	
	SetDataFolder root:
	
	colorList = CTabList()
	if(FindListItem(colorTable, colorlist) >= 0)
		ColorTab2Wave $colorTable
	else
		ColorTab2Wave $(StringFromList(abs(enoise(ItemsInList(colorList) - 1)),colorList))
	endif
	
	wave/i/u M_colors
	color_range = numpnts(M_colors) / 3 - 1
	color_offset = floor(color_range * 0)
	color_range -= color_offset
	print color_range, color_offset
		
	traceList = TraceNameList("",";",1)
	
	numList = itemsinList(myList, ",")
	for(i = 0;i < numList; i += 1)
		entity = StringFromList(i, myList, ",")
		entitynorm = "" + entity + "_norm"
		entityfit = "fit_" + entitynorm
		
		wave original = $entity
		wave normalized = $entitynorm 
		wave fit = $entityfit
		wave concentration = root:concentration
		wave/T samplename = root:samplename
		
		FindValue/TEXT=(entity) samplename
		if(V_Value >= 0)
			color = color_offset + concentration[V_Value] / 100 * color_range
			if(FindListItem(entitynorm, traceList) >= 0)
				ModifyGraph rgb($entitynorm) = (M_colors[color][0], M_colors[color][1], M_colors[color][2])
			endif
			if(FindListItem(entityfit, traceList) >= 0)
				ModifyGraph rgb($entityfit) = (M_colors[color][0], M_colors[color][1], M_colors[color][2])
			endif
		endif
		
		//wavestats/q/R=[380,410] normalized		
		//wavestats/q/R=(1550,1600) fit		


		waveclear original, normalized, fit
	endfor
	killwaves/z M_colors
End

Function correctFit(input)
	string input
	
	wave normal = $input
	wave fit = $("fit_" + input)
	
	variable punkt1, punkt2
	variable max1, max2
		
	Wavestats/R=[255, 278] normal
	max1 = v_max
	
	Wavestats/R=(1280, 1515) fit
	max2 = v_max
	
	fit /= max2 / max1
End

Function colorTraces()
	string traces

	variable i,numTraces

	traces = GrepList(tracenamelist("",";",1), "mk110", 0, ";")
	numTraces = ItemsInList(traces)
	for(i = 0; i < numTraces; i += 1)
		ModifyGraph rgb($StringFromList(i, traces))=(65535,49151,49151)
	endfor

	traces = GrepList(tracenamelist("",";",1), "mk111", 0, ";")
	numTraces = ItemsInList(traces)
	for(i = 0; i < numTraces; i += 1)
		ModifyGraph rgb($StringFromList(i, traces))=(65535,32768,32768)
	endfor

	traces = GrepList(tracenamelist("",";",1), "mk113", 0, ";")
	numTraces = ItemsInList(traces)
	for(i = 0; i < numTraces; i += 1)
		ModifyGraph rgb($StringFromList(i, traces))=(65535,16385,16385)
	endfor

	traces = GrepList(tracenamelist("",";",1), "mk114", 0, ";")
	numTraces = ItemsInList(traces)
	for(i = 0; i < numTraces; i += 1)
		ModifyGraph rgb($StringFromList(i, traces))=(52428,1,1)
	endfor

	traces = GrepList(tracenamelist("",";",1), "mk115", 0, ";")
	numTraces = ItemsInList(traces)
	for(i = 0; i < numTraces; i += 1)
		ModifyGraph rgb($StringFromList(i, traces))=(26214,0,0)
	endfor
End
Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(22.5,57.5,480.75,257.75) mk109 vs wavenumber488 as "combined"
	AppendToGraph mk110 vs wavenumber488
	AppendToGraph mk111 vs wavenumber488
	AppendToGraph mk113 vs wavenumber488
	AppendToGraph mk114 vs wavenumber488
	AppendToGraph mk115 vs wavenumber488
	AppendToGraph/B=bottom_P2 mk109 vs wavenumber488
	AppendToGraph/B=bottom_P2 mk111 vs wavenumber488
	AppendToGraph/B=bottom_P2 mk113 vs wavenumber488
	AppendToGraph/B=bottom_P2 mk114 vs wavenumber488
	AppendToGraph/B=bottom_P2 mk115 vs wavenumber488
	AppendToGraph/R/T fit_mk115_norm,fit_mk113_norm,fit_mk109_norm,fit_mk110_norm
	AppendToGraph/R/T mk115_norm vs wavenumber488
	AppendToGraph/R/T mk113_norm vs wavenumber488
	AppendToGraph/R/T mk110_norm vs wavenumber488
	AppendToGraph/R/T mk109_norm vs wavenumber488
	AppendToGraph fit_mk109[50,110],fit_mk110[50,110],fit_mk111[50,110],fit_mk113[50,110]
	AppendToGraph fit_mk114[50,110],fit_mk115[50,110]
	AppendToGraph/B=bottom_P2 fit_mk109,fit_mk110,fit_mk111,fit_mk113,fit_mk114,fit_mk115
	ModifyGraph expand=-1,height={Aspect,0.33}
	ModifyGraph mode(mk109)=3,mode(mk110)=3,mode(mk111)=3,mode(mk113)=3,mode(mk114)=3
	ModifyGraph mode(mk115)=3,mode(mk109#1)=3,mode(mk111#1)=3,mode(mk113#1)=3,mode(mk114#1)=3
	ModifyGraph mode(mk115#1)=3,mode(mk115_norm)=3,mode(mk113_norm)=3,mode(mk110_norm)=3
	ModifyGraph mode(mk109_norm)=3
	ModifyGraph marker(mk109)=1,marker(mk110)=1,marker(mk111)=1,marker(mk113)=1,marker(mk114)=1
	ModifyGraph marker(mk115)=1,marker(mk109#1)=1,marker(mk111#1)=1,marker(mk113#1)=1
	ModifyGraph marker(mk114#1)=1,marker(mk115#1)=1,marker(mk115_norm)=8,marker(mk113_norm)=8
	ModifyGraph marker(mk110_norm)=8,marker(mk109_norm)=8
	ModifyGraph lSize(mk109)=1.5,lSize(mk110)=1.5,lSize(mk111)=1.5,lSize(mk113)=1.5
	ModifyGraph lSize(mk114)=1.5,lSize(mk115)=1.5,lSize(mk109#1)=1.5,lSize(mk111#1)=1.5
	ModifyGraph lSize(mk113#1)=1.5,lSize(mk114#1)=1.5,lSize(mk115#1)=1.5,lSize(fit_mk115_norm)=2
	ModifyGraph lSize(fit_mk113_norm)=2,lSize(fit_mk109_norm)=2,lSize(fit_mk110_norm)=2
	ModifyGraph lSize(mk115_norm)=1.5,lSize(mk113_norm)=1.5,lSize(mk110_norm)=1.5,lSize(mk109_norm)=1.5
	ModifyGraph lSize(fit_mk109)=1.5,lSize(fit_mk110)=1.5,lSize(fit_mk111)=1.5,lSize(fit_mk113)=1.5
	ModifyGraph lSize(fit_mk114)=1.5,lSize(fit_mk115)=1.5,lSize(fit_mk109#1)=1.5,lSize(fit_mk110#1)=1.5
	ModifyGraph lSize(fit_mk111#1)=1.5,lSize(fit_mk113#1)=1.5,lSize(fit_mk114#1)=1.5
	ModifyGraph lSize(fit_mk115#1)=1.5
	ModifyGraph rgb(mk109)=(0,0,0),rgb(mk110)=(65535,49151,49151),rgb(mk111)=(65535,32768,32768)
	ModifyGraph rgb(mk113)=(65535,16385,16385),rgb(mk114)=(52428,1,1),rgb(mk115)=(26214,0,0)
	ModifyGraph rgb(mk109#1)=(0,0,0),rgb(mk111#1)=(65535,32768,32768),rgb(mk113#1)=(65535,16385,16385)
	ModifyGraph rgb(mk114#1)=(52428,1,1),rgb(mk115#1)=(26214,0,0),rgb(fit_mk115_norm)=(26214,0,0)
	ModifyGraph rgb(fit_mk113_norm)=(65535,16385,16385),rgb(fit_mk109_norm)=(0,0,0)
	ModifyGraph rgb(fit_mk110_norm)=(65535,49151,49151),rgb(mk115_norm)=(26214,0,0)
	ModifyGraph rgb(mk113_norm)=(65535,16385,16385),rgb(mk110_norm)=(65535,49151,49151)
	ModifyGraph rgb(mk109_norm)=(0,0,0),rgb(fit_mk109)=(0,0,0),rgb(fit_mk110)=(65535,49151,49151)
	ModifyGraph rgb(fit_mk111)=(65535,32768,32768),rgb(fit_mk113)=(65535,16385,16385)
	ModifyGraph rgb(fit_mk114)=(52428,1,1),rgb(fit_mk115)=(26214,0,0),rgb(fit_mk109#1)=(0,0,0)
	ModifyGraph rgb(fit_mk110#1)=(65535,49151,49151),rgb(fit_mk111#1)=(65535,32768,32768)
	ModifyGraph rgb(fit_mk113#1)=(65535,16385,16385),rgb(fit_mk114#1)=(52428,1,1),rgb(fit_mk115#1)=(26214,0,0)
	ModifyGraph msize(mk109)=2,msize(mk110)=2,msize(mk111)=2,msize(mk113)=2,msize(mk114)=2
	ModifyGraph msize(mk115)=2,msize(mk109#1)=2,msize(mk111#1)=2,msize(mk113#1)=2,msize(mk114#1)=2
	ModifyGraph msize(mk115#1)=2,msize(mk115_norm)=3,msize(mk113_norm)=3,msize(mk110_norm)=3
	ModifyGraph msize(mk109_norm)=3
	ModifyGraph mrkThick(mk109)=0.2,mrkThick(mk110)=0.2,mrkThick(mk111)=0.2,mrkThick(mk113)=0.2
	ModifyGraph mrkThick(mk114)=0.2,mrkThick(mk115)=0.2,mrkThick(mk109#1)=0.2,mrkThick(mk111#1)=0.2
	ModifyGraph mrkThick(mk113#1)=0.2,mrkThick(mk114#1)=0.2,mrkThick(mk115#1)=0.2,mrkThick(mk115_norm)=1
	ModifyGraph mrkThick(mk113_norm)=1,mrkThick(mk110_norm)=1,mrkThick(mk109_norm)=1
	ModifyGraph opaque(mk109)=1,opaque(mk110)=1,opaque(mk111)=1,opaque(mk113)=1,opaque(mk114)=1
	ModifyGraph opaque(mk115)=1,opaque(mk109#1)=1,opaque(mk111#1)=1,opaque(mk113#1)=1
	ModifyGraph opaque(mk114#1)=1,opaque(mk115#1)=1,opaque(mk115_norm)=1,opaque(mk113_norm)=1
	ModifyGraph opaque(mk110_norm)=1,opaque(mk109_norm)=1
	ModifyGraph useMrkStrokeRGB(mk113_norm)=1
	ModifyGraph mrkStrokeRGB(mk113_norm)=(52428,52428,52428)
	ModifyGraph noLabel(left)=1,noLabel(right)=1
	ModifyGraph lblMargin(right)=70
	ModifyGraph standoff(bottom)=0
	ModifyGraph axThick(right)=0
	ModifyGraph lblPosMode(right)=1
	ModifyGraph lblPos(bottom)=45
	ModifyGraph lblLatPos(bottom)=111
	ModifyGraph axisOnTop=1
	ModifyGraph freePos(bottom_P2)={0,kwFraction}
	ModifyGraph axisEnab(bottom)={0,0.4779}
	ModifyGraph axisEnab(bottom_P2)={0.5221,1}
	ModifyGraph axisEnab(right)={0.6,1}
	ModifyGraph axisEnab(top)={0.7,0.9}
	ModifyGraph manTick(bottom)={0,100,0,0},manMinor(bottom)={1,50}
	ModifyGraph manTick(bottom_P2)={0,100,0,0},manMinor(bottom_P2)={1,50}
	ModifyGraph manTick(right)={0,0.1,0,1},manMinor(right)={1,50}
	ModifyGraph manTick(top)={0,20,0,0},manMinor(top)={1,50}
	Label left "intensity [a.u.]"
	Label bottom "wavenumber [cm⁻¹]"
	Label right "norm. intensity"
	Label top "wavenumber [cm⁻¹]"
	SetAxis left 0,5
	SetAxis bottom 1250,1598
	SetAxis bottom_P2 2500,2750
	SetAxis right 0.7,1.05
	SetAxis top 2610,2660
	TextBox/C/N=text0/F=0/B=1/A=MC/X=-30.09/Y=19.29 "\\JC\\s(fit_mk109) 0% \t\\s(fit_mk110) 5%\r\\s(fit_mk111) 12.5% \t\\s(fit_mk113) 50%"
	AppendText "\\s(fit_mk114) 75%\t\\s(fit_mk115) 100%"
	SetDrawLayer UserFront
	SetDrawEnv gstart,gname= WM_SplitMarkGroup0
	DrawLine 0.466834993084371,0.963369963369963,0.488965006915629,1.03663003663004
	DrawLine 0.511034993084371,0.963369963369963,0.533165006915629,1.03663003663004
	SetDrawEnv gstop
	SetDrawEnv xcoord= bottom,ycoord= left,arrow= 1
	DrawLine 2640.15873015873,1.02433862433863,2620.31746031746,1.02433862433863
	SetDrawEnv xcoord= bottom,ycoord= left,arrow= 1
	DrawLine 2640.15873015873,1.02433862433863,2620.31746031746,1.02433862433863
	SetDrawEnv xcoord= bottom,ycoord= left,arrow= 1
	DrawLine 2640.15873015873,1.02433862433863,2620.31746031746,1.02433862433863
	SetDrawEnv arrow= 1
	DrawLine 0.875229357798165,0.456852791878173,0.732110091743119,0.456852791878173
	SetWindow kwTopWin,userdata(WMSplitAxisList)=  "WMSplitAxis0;"
	SetWindow kwTopWin,userdata(WMSplitAxis0)=  "AXIS=bottom;NEWAXIS=bottom_P2;AXISSTART=1250;AXISEND=2700;AXISENABSTART=0;AXISENABEND=1;AXISHASSLASHR=0;SPLITMARKS=WM_SplitMarkGroup0;"
	SetWindow kwTopWin,userdata(WM_SplitMarkGroup0)=  "AXIS1=bottom;AXIS2=bottom_P2;"
	SetWindow kwTopWin,userdata(WM_SplitMarkGroupList)=  "WM_SplitMarkGroup0;"
EndMacro

Window gplus() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mpf:
	Display /W=(27,284,270.75,451.25) gpluslocation vs ::concentration_text as "gplus"
	AppendToGraph/T gpluslocation
	AppendToGraph/R/T gplusamplitude
	SetDataFolder fldrSav0
	ModifyGraph margin(right)=27
	ModifyGraph mode(gpluslocation#1)=4,mode(gplusamplitude)=4
	ModifyGraph mode(gpluslocation)=2
	ModifyGraph marker(gpluslocation#1)=8,marker(gplusamplitude)=8
	ModifyGraph rgb(gplusamplitude)=(0,0,0)
	ModifyGraph opaque(gpluslocation#1)=1,opaque(gplusamplitude)=1
	ModifyGraph nticks(top)=0
	ModifyGraph noLabel(right)=1
	ModifyGraph axRGB(left)=(65280,0,0)
	ModifyGraph tlblRGB(left)=(65280,0,0)
	ModifyGraph lblPosMode(right)=1
	Label left "wavenumber / cm\\S-1"
	Label bottom "vol-% AcN in EtOH"
	Label right "intensity / a.u."
	SetAxis top -0.5,6.5
	SetAxis right 0,*
	ErrorBars gpluslocation#1 Y,wave=(:mpf:gpluslocsigma,:mpf:gpluslocsigma)
	ErrorBars gplusamplitude Y,wave=(:mpf:gplusampsigma,:mpf:gplusampsigma)
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=4.26/Y=32.57 "\\s(gpluslocation#1) energy\r\\s(gplusamplitude) intensity"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=-2.66/Y=65.14 "G- band (488nm)"
EndMacro

Window gband() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mpf:
	Display /W=(285.75,284,532.5,452.75) glocation vs ::concentration_text as "gband"
	AppendToGraph/T glocation
	AppendToGraph/R=intensity/T gamplitude
	SetDataFolder fldrSav0
	ModifyGraph margin(right)=28
	ModifyGraph mode(glocation#1)=4,mode(gamplitude)=4
	ModifyGraph mode(glocation)=2
	ModifyGraph marker=8
	ModifyGraph rgb(glocation)=(0,0,0),rgb(gamplitude)=(0,0,0)
	ModifyGraph opaque=1
	ModifyGraph nticks(top)=0
	ModifyGraph noLabel(top)=2,noLabel(intensity)=1
	ModifyGraph axRGB(left)=(65280,0,0)
	ModifyGraph tlblRGB(left)=(65280,0,0)
	ModifyGraph lblPosMode(intensity)=1
	ModifyGraph freePos(intensity)=0
	Label left "wavenumber / cm\\S-1"
	Label bottom "vol-% AcN in EtOH"
	Label intensity "intensity / a.u."
	SetAxis left 1550,1570
	SetAxis top -0.5,6.5
	SetAxis intensity 0,*
	ErrorBars glocation#1 Y,wave=(:mpf:glocsigma,:mpf:glocsigma)
	ErrorBars gamplitude Y,wave=(:mpf:gampsigma,:mpf:gampsigma)
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=-8.61/Y=33.93 "\\s(glocation#1) energy\r\\s(gamplitude) intensity"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=-4.56/Y=64.88 "G+ band (488nm)"
EndMacro

Window g2band() : Graph
	PauseUpdate; Silent 1		// building window...
	String fldrSav0= GetDataFolder(1)
	SetDataFolder root:mpf:
	Display /W=(548.25,284.75,792.75,454.25) g2location vs ::concentration_text as "g2band"
	AppendToGraph/T g2location
	AppendToGraph/R=intensity/T g2amplitude
	SetDataFolder fldrSav0
	ModifyGraph margin(right)=28
	ModifyGraph mode(g2location#1)=4,mode(g2amplitude)=4
	ModifyGraph mode(g2location)=2
	ModifyGraph marker=8
	ModifyGraph rgb(g2location)=(0,0,0),rgb(g2amplitude)=(0,0,0)
	ModifyGraph opaque=1
	ModifyGraph nticks(top)=0
	ModifyGraph noLabel(top)=2,noLabel(intensity)=1
	ModifyGraph axRGB(left)=(65280,0,0)
	ModifyGraph tlblRGB(left)=(65280,0,0)
	ModifyGraph lblPosMode(intensity)=1
	ModifyGraph freePos(intensity)=0
	ModifyGraph manTick(left)={0,5,0,0},manMinor(left)={4,50}
	Label left "wavenumber / cm\\S-1"
	Label bottom "vol-% AcN in EtOH"
	Label intensity "intensity / a.u."
	SetAxis left 2625,2640
	SetAxis top -0.5,6.5
	SetAxis intensity 0,*
	ErrorBars g2location#1 Y,wave=(:mpf:g2locsigma,:mpf:g2locsigma)
	ErrorBars g2amplitude Y,wave=(:mpf:g2ampsigma,:mpf:g2ampsigma)
	Cursor/P A g2location#1 6
	Legend/C/N=text0/J/F=0/B=1/A=MC/X=7.47/Y=32.14 "\\s(g2location#1) energy\r\\s(g2amplitude) intensity"
	TextBox/C/N=text1/F=0/B=1/A=MC/X=-4.56/Y=64.88 "G2-Band (488nm)"
EndMacro

