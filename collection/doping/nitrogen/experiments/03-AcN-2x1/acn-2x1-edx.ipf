#pragma TextEncoding = "Windows-1252"		// For details execute DisplayHelpTopic "The TextEncoding Pragma"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("atomicratio", saveJSON=0, saveVector = 1)
End
Window atomicratio() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(195.75,135.5,656.25,335)/L=l115 mk115 vs energy_keV as "atomicratio"
	AppendToGraph/L=l114 mk114 vs energy_keV
	AppendToGraph/L=l113 mk113 vs energy_keV
	AppendToGraph/L=l112 mk112 vs energy_keV
	AppendToGraph/L=l111 mk111 vs energy_keV
	AppendToGraph/L=l110 mk110 vs energy_keV
	AppendToGraph/L=l109 mk109 vs energy_keV
	AppendToGraph/L=left_inset/B=bottom_inset NperC vs atomic_ratio
	AppendToGraph/L=left_inset/B=bottom_inset fit_NperC
	ModifyGraph margin(left)=42
	ModifyGraph mode(NperC)=3
	ModifyGraph rgb(mk114)=(65280,43520,0),rgb(mk113)=(52224,52224,0),rgb(mk112)=(26112,52224,0)
	ModifyGraph rgb(mk111)=(0,52224,52224),rgb(mk110)=(16384,28160,65280),rgb(mk109)=(0,0,0)
	ModifyGraph rgb(NperC)=(0,0,0)
	ModifyGraph offset(mk115)={0,-83.6109176899936},offset(mk114)={0,-1390.29751323714}
	ModifyGraph offset(mk113)={0,-3772.55437113819},offset(mk112)={0,-8451.17096045198}
	ModifyGraph offset(mk111)={0,-17746.205779661},offset(mk110)={0,-12133.0766666667}
	ModifyGraph offset(mk109)={0,-8544.14600550964}
	ModifyGraph log(l115)=1,log(l114)=1,log(l113)=1,log(l112)=1,log(l111)=1,log(l110)=1
	ModifyGraph log(l109)=1
	ModifyGraph nticks(left_inset)=0
	ModifyGraph minor(bottom)=1
	ModifyGraph noLabel(l115)=1,noLabel(l114)=1,noLabel(l113)=1,noLabel(l112)=1,noLabel(l111)=1
	ModifyGraph noLabel(l110)=1,noLabel(l109)=1,noLabel(left_inset)=1
	ModifyGraph fSize(l110)=10
	ModifyGraph tlblRGB(l110)=(65535,65535,65535)
	ModifyGraph lblPosMode(left_inset)=3,lblPosMode(bottom_inset)=1
	ModifyGraph lblPos(bottom)=38,lblPos(l109)=30,lblPos(left_inset)=12,lblPos(bottom_inset)=-2
	ModifyGraph lblLatPos(l109)=-4,lblLatPos(left_inset)=-2,lblLatPos(bottom_inset)=9
	ModifyGraph freePos(l115)={0,kwFraction}
	ModifyGraph freePos(l114)={0,kwFraction}
	ModifyGraph freePos(l113)={0,kwFraction}
	ModifyGraph freePos(l112)={0,kwFraction}
	ModifyGraph freePos(l111)={0,kwFraction}
	ModifyGraph freePos(l110)={0,kwFraction}
	ModifyGraph freePos(l109)={0,kwFraction}
	ModifyGraph freePos(left_inset)={0.65,kwFraction}
	ModifyGraph freePos(bottom_inset)={0,kwFraction}
	ModifyGraph tickEnab(l115)={-10,-1}
	ModifyGraph tickEnab(l114)={-10,-1}
	ModifyGraph tickEnab(l113)={-10,-1}
	ModifyGraph tickEnab(l112)={-10,-1}
	ModifyGraph tickEnab(l111)={-10,-1}
	ModifyGraph tickEnab(l110)={-10,-1}
	ModifyGraph tickEnab(l109)={-10,-1}
	ModifyGraph axisEnab(bottom)={0,0.5}
	ModifyGraph axisEnab(bottom_inset)={0.65,1}
	Label bottom "energy [keV]"
	Label l109 "log(intensity)"
	Label left_inset "nitrogen content"
	Label bottom_inset "source nitrogen [at.%]"
	SetAxis l115 1.5,13000
	SetAxis bottom 0.2,0.45
	SetAxis l114 0.9,22000
	SetAxis l113 1.5,23000
	SetAxis l112 1,35000
	SetAxis l111 2.1,55332
	SetAxis l110 0.55,26000
	SetAxis left_inset*,5
	SetAxis bottom_inset -6.3859649122807,65.6140350877193
	ErrorBars NperC Y,wave=(NperC_sdv,NperC_sdv)
	Legend/C/N=text0/J/F=0/B=(61166,61166,61166)/A=LB/X=41.11/Y=67.20 "\\JCsource N ratio \r\\JL\\s(mk115) 50\t\\s(mk114) 38\r\\s(mk113) 26\t\\s(mk112) 14"
	AppendText "\\s(mk111) 7\t\\s(mk110) 3\r\\s(mk109) 0\tin at.%"
	SetDrawLayer UserFront
	SetDrawEnv xcoord= bottom,ycoord= prel,dash= 3
	DrawLine 0.393103448275862,1,0.393103448275862,0.358925143953935
	SetDrawEnv xcoord= bottom,ycoord= prel,fstyle= 1
	DrawText 0.382331691297209,0.36364744234226,"N"
	SetDrawEnv xcoord= bottom,ycoord= prel,dash= 3
	DrawLine 0.273470862821864,1,0.273470862821864,-0.00575815738963532
	SetDrawEnv xcoord= bottom,ycoord= prel,fstyle= 1
	DrawText 0.261687210044385,0.0157520313688522,"C"
EndMacro

