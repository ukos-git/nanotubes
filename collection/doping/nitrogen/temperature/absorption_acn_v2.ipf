#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("combined", saveJSON=0, saveVector = 1)
End
Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(14.25,278,471.75,494)/L=L800/B=B800 mk123 as "combined"
	AppendToGraph/R=R850/B=B850 mk109
	AppendToGraph/R=R800/B=B800 mk120
	AppendToGraph/R=R750/B=B750 mk100
	AppendToGraph/L=L750/B=B750 mk124,mk139
	AppendToGraph/L=L800/B=B800 mk121
	AppendToGraph/L=L850/B=B850 mk110,mk111
	AppendToGraph/R=R800/B=B800 mk120
	AppendToGraph/R=R750/B=B750 mk100
	AppendToGraph/R=R850/B=B850 mk109
	ModifyGraph margin(left)=14,margin(bottom)=42,margin(right)=14
	ModifyGraph mode(mk123)=7,mode(mk109)=7,mode(mk120)=7,mode(mk100)=7,mode(mk124)=7
	ModifyGraph mode(mk139)=7,mode(mk121)=7,mode(mk110)=7,mode(mk111)=7
	ModifyGraph rgb(mk123)=(65535,32768,32768),rgb(mk109)=(0,0,0),rgb(mk120)=(0,0,0)
	ModifyGraph rgb(mk100)=(0,0,0),rgb(mk124)=(65535,49151,49151),rgb(mk111)=(39321,1,1)
	ModifyGraph rgb(mk120#1)=(0,0,0),rgb(mk100#1)=(0,0,0),rgb(mk109#1)=(0,0,0)
	ModifyGraph hbFill(mk123)=5,hbFill(mk109)=5,hbFill(mk120)=5,hbFill(mk100)=5,hbFill(mk124)=5
	ModifyGraph hbFill(mk139)=5,hbFill(mk121)=5,hbFill(mk110)=5,hbFill(mk111)=4
	ModifyGraph hBarNegFill(mk123)=1,hBarNegFill(mk109)=1,hBarNegFill(mk120)=1,hBarNegFill(mk100)=1
	ModifyGraph hBarNegFill(mk124)=1,hBarNegFill(mk139)=1,hBarNegFill(mk121)=1,hBarNegFill(mk110)=1
	ModifyGraph hBarNegFill(mk111)=1
	ModifyGraph offset(mk124)={0,-0.708557410374983},offset(mk139)={0,-0.532684679030581}
	ModifyGraph offset(mk121)={0,-0.170616113744076},offset(mk110)={0,-0.246445497630331}
	ModifyGraph nticks(L800)=0,nticks(R850)=0,nticks(R800)=0,nticks(R750)=0,nticks(L750)=0
	ModifyGraph nticks(L850)=0
	ModifyGraph lblPosMode(B800)=1,lblPosMode(R850)=1,lblPosMode(L750)=1
	ModifyGraph freePos(L800)={800,B800}
	ModifyGraph freePos(B800)=1
	ModifyGraph freePos(R850)={1350,B850}
	ModifyGraph freePos(B850)=1
	ModifyGraph freePos(R800)={1350,B800}
	ModifyGraph freePos(R750)={1350,B750}
	ModifyGraph freePos(B750)=1
	ModifyGraph freePos(L750)={0,kwFraction}
	ModifyGraph freePos(L850)={800,B850}
	ModifyGraph axisEnab(B800)={0.33,0.63}
	ModifyGraph axisEnab(B850)={0.66,1}
	ModifyGraph axisEnab(B750)={0,0.3}
	ModifyGraph manTick(B800)={0,200,0,0},manMinor(B800)={1,2}
	ModifyGraph manTick(B850)={0,200,0,0},manMinor(B850)={1,2}
	ModifyGraph manTick(B750)={0,200,0,0},manMinor(B750)={1,2}
	Label B800 "wavelength [nm]"
	Label R850 "OD"
	Label L750 "OD"
	SetAxis L800 0.4,4.9
	SetAxis B800 800,1350
	SetAxis R850 2,6
	SetAxis B850 800,1350
	SetAxis R800 1.5,6
	SetAxis R750 0,5
	SetAxis B750 800,1350
	SetAxis L750 0,0.5
	SetAxis L850 0.6,4.6
	Legend/C/N=legend850/J/D={1,2,-1}/S=3/A=MT/X=15.96/Y=0.00 "\\s(mk124) 1 vol.% AcN\r\n\\s(mk110) 5 vol.% AcN\r\\s(mk111) 12.5 vol.% AcN\r\\s(mk109) pure EtOH"
	TextBox/C/N=text0/F=0/A=LT/X=1.00/Y=1.00 "750°C"
	TextBox/C/N=text1/F=0/A=LT/X=34.00/Y=1.00 "800°C"
	TextBox/C/N=text2/F=0/X=1.00/Y=1.00 "850°C"
EndMacro

