#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#define IMAGES_EXPORT_PXP
#include "utilities-images"

Function export()
	SaveWindow("combined", saveVector = 1)
	SaveTableCopy/O/A=0/W=bandgapTable/F=0/N=1/T=1/Z/P=home as "heitmueller_bandgap.csv"
End

Window combined() : Graph
	PauseUpdate; Silent 1		// building window...
	Display /W=(672,284,1129.5,533) fit_mk128_E11_od as "combined"
	AppendToGraph comocat_E11_od[10,40] vs comocat_E11_potential[10,40]
	AppendToGraph mk129_E11_od[6,42] vs mk129_E11_potential[6,42]
	AppendToGraph mk130_E11_od[10,40] vs mk130_E11_potential[10,40]
	AppendToGraph mk127_E11_od[10,39] vs mk127_E11_potential[10,39]
	AppendToGraph/L=trion comocat_trion_od[10,40] vs comocat_trion_potential[10,40]
	AppendToGraph/L=trion mk127_trion_od[12,45] vs mk127_trion_potential[12,45]
	AppendToGraph/L=trion mk128_trion_od[12,40] vs mk128_trion_potential[12,40]
	AppendToGraph/L=trion mk129_trion_od[10,45] vs mk129_trion_potential[10,45]
	AppendToGraph/L=trion mk130_trion_od[10,45] vs mk130_trion_potential[10,45]
	AppendToGraph/L=trion :mk128:fit_mk129_trion_od,fit_comocat_trion_od_left,fit_comocat_trion_od_right
	AppendToGraph/L=trion fit_mk127_trion_od_right,fit_mk127_trion_od_left,fit_mk128_trion_od
	AppendToGraph/L=trion fit_mk130_trion_od
	AppendToGraph fit_mk130_E11_od_left,fit_mk129_E11_od_left,fit_mk128_E11_od_right
	AppendToGraph fit_mk127_E11_od_left,fit_comocat_E11_od_left,fit_comocat_E11_od,fit_mk127_E11_od_right
	AppendToGraph fit_mk129_E11_od_right,fit_mk130_E11_od_right
	AppendToGraph mk128_E11_od[10,45] vs mk128_E11_potential[10,45]
	ModifyGraph margin(left)=36
	ModifyGraph mode(comocat_E11_od)=3,mode(mk129_E11_od)=3,mode(mk130_E11_od)=3,mode(mk127_E11_od)=3
	ModifyGraph mode(comocat_trion_od)=3,mode(mk127_trion_od)=3,mode(mk128_trion_od)=3
	ModifyGraph mode(mk129_trion_od)=3,mode(mk130_trion_od)=3,mode(mk128_E11_od)=3
	ModifyGraph marker(comocat_E11_od)=19,marker(mk129_E11_od)=17,marker(mk130_E11_od)=23
	ModifyGraph marker(mk127_E11_od)=19,marker(comocat_trion_od)=8,marker(mk127_trion_od)=8
	ModifyGraph marker(mk128_trion_od)=5,marker(mk129_trion_od)=6,marker(mk130_trion_od)=22
	ModifyGraph marker(mk128_E11_od)=16
	ModifyGraph lSize(fit_mk128_E11_od)=1.2,lSize(fit_comocat_trion_od_left)=1.2,lSize(fit_comocat_trion_od_right)=1.2
	ModifyGraph lSize(fit_mk128_E11_od_right)=1.2
	ModifyGraph lStyle(mk129_E11_od)=2,lStyle(mk130_E11_od)=3,lStyle(fit_comocat_trion_od_left)=3
	ModifyGraph lStyle(fit_comocat_trion_od_right)=3,lStyle(fit_comocat_E11_od_left)=3
	ModifyGraph lStyle(fit_comocat_E11_od)=3,lStyle(mk128_E11_od)=1
	ModifyGraph rgb(fit_mk128_E11_od)=(65535,49151,49151),rgb(comocat_E11_od)=(34952,34952,34952)
	ModifyGraph rgb(mk130_E11_od)=(39321,1,1),rgb(mk127_E11_od)=(0,0,0),rgb(comocat_trion_od)=(34952,34952,34952)
	ModifyGraph rgb(mk127_trion_od)=(0,0,0),rgb(mk128_trion_od)=(49151,53155,65535)
	ModifyGraph rgb(mk129_trion_od)=(1,12815,52428),rgb(mk130_trion_od)=(29524,1,58982)
	ModifyGraph rgb(fit_mk129_trion_od)=(1,12815,52428),rgb(fit_comocat_trion_od_left)=(34952,34952,34952)
	ModifyGraph rgb(fit_comocat_trion_od_right)=(34952,34952,34952),rgb(fit_mk127_trion_od_right)=(0,0,0)
	ModifyGraph rgb(fit_mk127_trion_od_left)=(0,0,0),rgb(fit_mk128_trion_od)=(49151,53155,65535)
	ModifyGraph rgb(fit_mk130_trion_od)=(29524,1,58982),rgb(fit_mk130_E11_od_left)=(39321,1,1)
	ModifyGraph rgb(fit_mk128_E11_od_right)=(65535,49151,49151),rgb(fit_mk127_E11_od_left)=(0,0,0)
	ModifyGraph rgb(fit_comocat_E11_od_left)=(34952,34952,34952),rgb(fit_comocat_E11_od)=(34952,34952,34952)
	ModifyGraph rgb(fit_mk127_E11_od_right)=(0,0,0),rgb(fit_mk130_E11_od_right)=(39321,1,1)
	ModifyGraph rgb(mk128_E11_od)=(65535,49151,49151)
	ModifyGraph opaque(comocat_trion_od)=1,opaque(mk127_trion_od)=1,opaque(mk128_trion_od)=1
	ModifyGraph opaque(mk129_trion_od)=1,opaque(mk130_trion_od)=1
	ModifyGraph hbFill(mk129_E11_od)=5,hbFill(mk130_E11_od)=5,hbFill(mk128_E11_od)=5
	ModifyGraph useNegPat(fit_mk130_E11_od_left)=1
	ModifyGraph hBarNegFill(fit_mk130_E11_od_left)=5
	ModifyGraph plusRGB(mk127_E11_od)=(0,0,0,32768)
	ModifyGraph hideTrace(fit_mk128_E11_od)=1,hideTrace(comocat_E11_od)=1,hideTrace(mk129_E11_od)=1
	ModifyGraph hideTrace(comocat_trion_od)=1,hideTrace(mk128_trion_od)=1,hideTrace(mk129_trion_od)=1
	ModifyGraph hideTrace(fit_mk129_trion_od)=1,hideTrace(fit_comocat_trion_od_left)=1
	ModifyGraph hideTrace(fit_comocat_trion_od_right)=1,hideTrace(fit_mk128_trion_od)=1
	ModifyGraph hideTrace(fit_mk129_E11_od_left)=1,hideTrace(fit_mk128_E11_od_right)=1
	ModifyGraph hideTrace(fit_comocat_E11_od_left)=1,hideTrace(fit_comocat_E11_od)=1
	ModifyGraph hideTrace(fit_mk129_E11_od_right)=1,hideTrace(mk128_E11_od)=1
	ModifyGraph useMrkStrokeRGB(comocat_E11_od)=1,useMrkStrokeRGB(mk129_E11_od)=1,useMrkStrokeRGB(mk130_E11_od)=1
	ModifyGraph useMrkStrokeRGB(mk127_E11_od)=1,useMrkStrokeRGB(mk128_E11_od)=1
	ModifyGraph mrkStrokeRGB(mk129_E11_od)=(65535,49151,49151),mrkStrokeRGB(mk130_E11_od)=(65535,0,0)
	ModifyGraph mrkStrokeRGB(mk128_E11_od)=(39321,1,1)
	ModifyGraph offset(fit_mk128_E11_od)={0.3,0},offset(comocat_E11_od)={0.46,0},offset(mk129_E11_od)={0.55,0}
	ModifyGraph offset(mk130_E11_od)={0.48,0},offset(mk127_E11_od)={0.4,0},offset(comocat_trion_od)={0.46,0}
	ModifyGraph offset(mk127_trion_od)={0.4,0},offset(mk128_trion_od)={0.3,0},offset(mk129_trion_od)={0.55,0}
	ModifyGraph offset(mk130_trion_od)={0.48,0},offset(fit_mk129_trion_od)={0.55,0}
	ModifyGraph offset(fit_comocat_trion_od_left)={0.46,0},offset(fit_comocat_trion_od_right)={0.46,0}
	ModifyGraph offset(fit_mk127_trion_od_right)={0.4,0},offset(fit_mk127_trion_od_left)={0.4,0}
	ModifyGraph offset(fit_mk128_trion_od)={0.3,0},offset(fit_mk130_trion_od)={0.48,0}
	ModifyGraph offset(fit_mk130_E11_od_left)={0.48,0},offset(fit_mk129_E11_od_left)={0.55,0}
	ModifyGraph offset(fit_mk128_E11_od_right)={0.3,0},offset(fit_mk127_E11_od_left)={0.4,0}
	ModifyGraph offset(fit_comocat_E11_od_left)={0.46,0},offset(fit_comocat_E11_od)={0.46,0}
	ModifyGraph offset(fit_mk127_E11_od_right)={0.4,0},offset(fit_mk129_E11_od_right)={0.55,0}
	ModifyGraph offset(fit_mk130_E11_od_right)={0.48,0},offset(mk128_E11_od)={0.3,0}
	ModifyGraph lblPosMode(trion)=1
	ModifyGraph lblPos(left)=36
	ModifyGraph freePos(trion)={0,kwFraction}
	ModifyGraph axisEnab(left)={0,0.45}
	ModifyGraph axisEnab(trion)={0.55,1}
	ModifyGraph manTick(left)={0,1,0,0},manMinor(left)={3,2}
	ModifyGraph manTick(trion)={0,1,0,0},manMinor(trion)={3,2}
	Label left "norm. OD\r(1050nm)"
	Label bottom "potential [V]"
	Label trion "norm. OD\r(1225nm)"
	SetAxis left 0,1
	SetAxis bottom -1.5,1.5
	SetAxis trion 0,1.16
	ErrorBars comocat_E11_od Y,wave=(comocat_E11_odErr[10,40],comocat_E11_odErr[10,40])
	ErrorBars mk129_E11_od Y,wave=(mk129_E11_odErr[6,42],mk129_E11_odErr[6,42])
	ErrorBars mk130_E11_od Y,wave=(mk130_E11_odErr[10,39],mk130_E11_odErr[10,39])
	ErrorBars comocat_trion_od Y,wave=(comocat_trion_odErr[10,40],comocat_trion_odErr[10,40])
	ErrorBars mk127_trion_od Y,wave=(mk127_trion_odErr[12,45],mk127_trion_odErr[12,45])
	ErrorBars mk128_trion_od Y,wave=(mk128_trion_odErr[12,40],mk128_trion_odErr[12,40])
	ErrorBars mk129_trion_od Y,wave=(mk129_trion_odErr[10,45],mk129_trion_odErr[10,45])
	ErrorBars mk130_trion_od Y,wave=(mk130_trion_odErr[10,38],mk130_trion_odErr[10,38])
	ErrorBars mk128_E11_od Y,wave=(mk128_E11_odErr[10,45],mk128_E11_odErr[10,45])
	TextBox/C/N=text0/S=3/A=MB/X=0.00/Y=-1.00 "Trion and E\\B11\\M of (7,5)\\M\n\\s(mk127_trion_od)\\s(fit_mk127_E11_od_left)\\s(mk127_E11_od) ACCVD"
	AppendText "\\s(comocat_trion_od)\\s(fit_comocat_E11_od_left)\\s(comocat_E11_od) CoMoCat\r\\s(mk128_trion_od)\\s(mk128_E11_od) 1 vol.% AcN"
	AppendText "\\s(mk129_trion_od)\\s(fit_mk129_E11_od_right)\\s(mk129_E11_od) 5 vol.% AcN\r\n\\s(mk130_trion_od)\\s(fit_mk130_E11_od_left)\\s(mk130_E11_od) 25 vol.% AcN"
EndMacro

