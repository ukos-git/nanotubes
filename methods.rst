Spectroscopy
============

This chapter is a collection of instruments and analysis techniques used
throughout the thesis. The :doc:`sample preparation <samples>` and
:doc:`synthesis <cvd>` were described previously and the diameter reduction
with :doc:`nitrogen poisoning <nitrogen>` during synthesis is described
afterwards. The setup for :doc:`taking spectra <spectra>` and :doc:`images
<images>` is described in more detail after the introduction of these basic
methods here. All methods serve the investigations on :doc:`adsorption` on
carbon nanotubes.

Wet Dispersion
--------------

If not stated otherwise, aqueous samples were debundled using Deoxycholate
(DOC).\ :cite:`chhikara2012` Typically 50mg of the raw CCVD material was
dispersed in 10ml aqueous solution containing 1.5wt.% DOC. Water was purified
with a millipore filter.  Samples were sonicated for 1h with a Branson Horn
Sonicator in a 50% duty cycle and subsequently centrifuged for 3-5min with a
*Haereus Spatech* *Biofuge 15* at 14000rpm.

Density Gradient Ultracentrifugation
------------------------------------

Density Gradient Ultracentrifugation (DGU) was performed using a linear density
gradient :cite:`arnold2005,green2009` with a *Beckmann* *Optimatm L-90K* ultra
centrifuge. Fractioning of the centrifuged material was achieved with a *KD
Scientific* *KDS 210* syringe pump.

Absorption Spectroscopy
-----------------------

Absorption spectroscopy was performed on a *Varian* *Cary 5000 UV-Vis-NIR
spectrophotometer*. The background of the pure solution was measured as a
reference before every experiment. The spectra are processed and analyzed with
a custom `analysis program <https://github.com/ukos-git/igor-swnt-absorption>`_

.. only:: html

   .. literalinclude:: programs/igor-absorption/app/swnt-absorption-main.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-absorption/app/swnt-absorption-getter.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-absorption/app/swnt-absorption-display.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-absorption/app/swnt-absorption-tools.ipf
      :language: igor
      :linenos:

Raman Spectroscopy
------------------

Raman spectroscopy at 1064nm was done using a Nd:YAG laser together with a high
resolution FT-spectrometer (Bruker IFS 120 HR coupled to a FRA-106).

Raman spectroscopy at 488nm was achieved using an Argon laser and a dichroitic
mirror (520nm) coupled to an Andor Shamrock spectrometer with an Si-CCD array
(Andor Newton).  The grating has 300 lines/mm and a blaze wavelength at
500nm.

Raman spectroscopy at 570nm was done at a *SP2500 spectrograph* with a
*Princton Instruments* *Pixis 256* CCD array

PLE Spectroscopy
----------------

Photoluminescence excitation/emission spectroscopy and -microscopy was
performed using a home-built setup.  Spectra were acquired with a `LabVIEW
program <https://github.com/ukos-git/labview-plem>`_. It is based on a similar
setup :cite:`zuleeg2015` that was taken for taking PLE maps in solution. The
generated :doc:`spectra <spectra>` and :doc:`images <images>` are loaded and
processed with a `Igor Pro program for loading measured data
<https://github.com/ukos-git/labview-plem>`_.


.. only:: html

   .. literalinclude:: programs/igor-plem/app/plem.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-main.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-menu.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-prefs.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-structure.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-helper.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-gui.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-plem/app/plem-correction.ipf
      :language: igor
      :linenos:

Data Processing
===============

Statistical Analysis
--------------------

Statistical analysis, data preparation, and extraction was performed with a
custom `Igor Pro program for mass analysis
<https://github.com/ukos-git/igor-swnt-massanalysis>`_.

.. only:: html

   .. literalinclude:: programs/igor-sma/app/sma.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-coordinates.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-covariance.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-duplicateRange.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-images.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-infostructure.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-macro.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-main.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-menu.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-peakfind.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-prefs.ipf
      :language: igor
      :linenos:
   .. literalinclude:: programs/igor-sma/app/sma-tools.ipf
      :language: igor
      :linenos:

.. only: html

   The analysis of the subset of selected spectra and maps was performed with

   .. literalinclude:: collection/suspended/all/coordinates.ipf
      :language: igor
      :linenos:

   .. literalinclude:: collection/suspended/all/spectra/Procedure.ipf
      :language: igor
      :linenos:

   .. literalinclude:: collection/suspended/all/maps/Procedure.ipf
      :language: igor
      :linenos:

Data Export
-----------

Data export was performed on continuous integration installations using docker
containers with an automated Igor Pro pipeline. All graphs are available online
as Igor Experiment (`pxp`) files in their complete form to perform further
analyses and to extract data.

.. only:: html

   The *drone.io* pipeline can be triggered locally using :code:`make drone` or
   `drone exec`.

   .. literalinclude:: .drone.yml
      :language: yaml
      :linenos:

   The *GitLab runner* pipeline allows parallelisation of the export step with
   different docker containers and deploys the website on the master branch.

   .. literalinclude:: .gitlab-ci.yml
      :language: yaml
      :linenos:

CVD Control
-----------

The CVD setup has a `python GUI
<https://github.com/ukos-git/python-swnt-reactor>`_. The recording of CVD
parameters was performed with an `Arduino Uno
<https://github.com/ukos-git/arduino-reactor>`_ and has already been decribed
elsewhere :cite:`kastner2014`.

.. only:: html

   .. literalinclude:: programs/python-swnt-reactor/src/MKDatabase.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKArduino.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKDatabase.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKFlowCommunication.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKFlowInput.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKFlowLogFile.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKFlowMain.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKFlowMessage.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKFlowSocat.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKLogFile.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKMainDisplay.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKParser.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKSerial.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKTerminal.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/MKTkinker.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/serverArduino.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-swnt-reactor/src/serverFlowmeter.py
      :language: python
      :linenos:

The flow for various carbon sources is digitally read with the *Bronkhorst*
flow meters using a `custom python program
<https://github.com/ukos-git/python-flowmeter>`_

.. only:: html

   .. literalinclude:: programs/python-flowmeter/src/MKFlowMain.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/MKDatabase.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/MKFlowInput.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/MKFlowSocat.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/MKFlowCommunication.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/MKFlowLogFile.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/MKFlowMessage.py
      :language: python
      :linenos:
   .. literalinclude:: programs/python-flowmeter/src/serverFlowmeter.py
      :language: python
      :linenos:
