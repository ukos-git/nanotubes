#!/bin/sh

command -v heif-enc

for file in $(find collection -iname "*.png")
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    heif-enc "$file"
    test -e ${file%.*}.heic
    sha256sum $file > $file.sha256
  fi
done

for file in $(find analyzed -iname "*.png")
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    heif-enc "$file"
    test -e ${file%.*}.heic
    sha256sum $file > $file.sha256
  fi
done

for file in $(find collection -iname "*.jpg")
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    heif-enc "$file"
    test -e ${file%.*}.heic
    sha256sum $file > $file.sha256
  fi
done

for file in $(find analyzed -iname "*.jpg")
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    heif-enc "$file"
    test -e ${file%.*}.heic
    sha256sum $file > $file.sha256
  fi
done
