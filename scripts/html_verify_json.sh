#!/bin/sh

# use jq to test a plotly json file

for file in $(find . -iname "*.json")
do
  du -h "$file"
  if ! sha256sum -c "$file.sha256"
  then
    if ! jq . "$file" > /dev/null
    then
      echo "JSON error in $file" >2
      rm -vf $(dirname $file)/*.pxp.sha256
      exit 1
    fi
    if ! plotly2html $file
    then
      rm -vf $(dirname $file)/*.pxp.sha256
      exit 1
    fi
    sha256sum $file > "$file.sha256"
  fi
done
