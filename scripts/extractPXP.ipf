#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#include "utilities-images"

Function startup()
	String procedure

	PathInfo home
	NewPath/C/O/Q/Z saveImagesPath, S_path + IgorInfo(1)

	String procText = ProcedureText("export")
	if(strlen(procText) > 0)
		fprintf -1, "%s\n", "Calling export()"
		Execute/P/Q "export()"
	endif
End
