#!/bin/sh

# current work around to convert gif to pdf
convert -version

for file in $(find collection -iname "*.gif")
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    convert $file ${file%.*}.png
    test -e ${file%.*}.pdf
    sha256sum $file > ${file}.sha256
  fi
done

for file in $(find analyzed -iname "*.gif")
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    convert $file ${file%.*}.png
    test -e ${file%.*}.pdf
    sha256sum $file > ${file}.sha256
  fi
done
