#!/bin/sh

inkscape --version --without-gui

for pxp in $(git ls-files | grep "pxp$")
do
  for file in ${pxp%.*}/*.pxp
  do
    file=${file%.*}.pdf
    if [ ! -e "$file" ]
    then
      continue
    fi
    if ! sha256sum -c $file.sha256 2>/dev/null
    then
      echo "$file"
      inkscape --without-gui --file="$file" --export-plain-svg="${file%.*}.svg"
      test -e "${file%.*}.svg"
      sha256sum "$file" > "${file}.sha256"
    fi
  done
done

