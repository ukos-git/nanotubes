#!/bin/bash

TOPLEVEL="$(git rev-parse --show-toplevel)"

# This script extracts only the relevant graphs from a pxp file
BIG_FILES="$@"

IGORPRO_32bit="${HOME}/.wine/dosdevices/c:/Program Files/WaveMetrics/Igor Pro 8 Folder/IgorBinaries_Win32/Igor.exe"
IGORPRO_PROCEDURES="${HOME}/WaveMetrics/Igor Pro 8 User Files/Igor Procedures"
test -e "$IGORPRO_32bit"

if [ ! -e "${IGORPRO_PROCEDURES}/startup.ipf.disabled" ] && [ ! -e "${IGORPRO_PROCEDURES}/startup.ipf" ]; then
	echo "[${BASH_SOURCE}] Error: startup.ipf not found" 1>&2
	exit 1
fi

mv -f "${IGORPRO_PROCEDURES}/startup.ipf.disabled" "${IGORPRO_PROCEDURES}/startup.ipf" || true
trap 'mv "${IGORPRO_PROCEDURES}/startup.ipf" "${IGORPRO_PROCEDURES}/startup.ipf.disabled"' EXIT

for BIG_FILE in ${BIG_FILES}; do
	BIG_FILE="${TOPLEVEL}/${BIG_FILE}"
	echo "[${BASH_SOURCE}] ----- Experiment \"${BIG_FILE##*/}\" -----"
	ln -fs "${TOPLEVEL}/scripts/extractPXP.ipf" "${IGORPRO_PROCEDURES}/convert.ipf"
	if ! sha256sum -c "${BIG_FILE}.sha256"
	then
	  test -r "$BIG_FILE"
	  igorpro $BIG_FILE && sha256sum ${BIG_FILE} ${BIG_FILE%.*}.ipf > "${BIG_FILE}.sha256"
	fi
	mv "${IGORPRO_PROCEDURES}/convert.ipf" "${IGORPRO_PROCEDURES}/convert.ipf.disabled"
	echo "[${BASH_SOURCE}] ----- done ------"
done
