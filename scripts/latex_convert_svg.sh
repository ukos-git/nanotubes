#!/bin/sh

inkscape --version --without-gui

for file in $(git ls-files -- '*.svg' ':!:_static/*')
do
  if ! sha256sum -c $file.sha256 2>/dev/null
  then
    echo "$file"
    inkscape --without-gui --file="$file" --export-pdf="${file%.*}.pdf"
    inkscape --without-gui --file="$file" --export-png="${file%.*}.png" --export-dpi=300
    test -e "${file%.*}.pdf"
    sha256sum "$file" > "${file}.sha256"
  fi
done
