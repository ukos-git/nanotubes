#!/bin/bash

TOPLEVEL="$(git rev-parse --show-toplevel)"

# drone workaround for permissions
# sudo chown ${USER}:${USER} $(find "$TOPLEVEL" -iname "*.uxp.sha256")
# sudo chown -R ${USER}:${USER} ${TOPLEVEL}/build ${TOPLEVEL}/collection ${TOPLEVEL}/analyzed ${TOPLEVEL}/.git ${TOPLEVEL}/programs

# delete ALL sha256
for f in $(find "$TOPLEVEL" -iname "*.sha256")
do
	echo $f
	rm -f ${f}
done

# delete ONLY tracked images
for f in $(git ls-tree -r HEAD --name-only --full-tree | grep "pxp$")
do
	for ending in {pdf,svg,html,json,png}
		do
			rm -f ${TOPLEVEL}/${f%.*}.${ending}
	done
done
