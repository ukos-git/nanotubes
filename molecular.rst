.. vim: set et ts=3 sts=3 sw=3 tw=79:

Adsorption of Solid
-------------------

The strong adsorption forces on carbon nanotubes can also be used for binding
other solid molecules.  Similar to active carbon, the adsorptive properties can
be used for the treatment of water contamination :cite:`szlachta2013`.  The
most prominent usage, however, is the non-covalently functionalization of the
surface, which enhances the solubility of individualized carbon nanotubes while
mostly preserving their excitonic emission properties at room temperature.

Non-Covalent Functionalisation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: collection/doping/nitrogen/experiments/04-AcN-3x1/polymers/polymers/selectivity_pfo_doc.*

   PFO selectively binds to the (7,5) type while DOC almost equally suspends
   every chirality. The rise in absorption background comes from the dispersion
   of graphene flakes and metallic nanotubes.

The first successful individualization of carbon nanotubes was achieved using
*sodium dodecyl sulfate* (SDS) :cite:`weisman2003` in D₂O which encapsulates
the nanotubes in micelles.  Other amphiphilic molecules with water solubility
like sodium dodecylbenzene sulfonate (SDBS) :cite:`moore2003` followed. Both of
the aforementioned sodium salts are in constant equilibrium with the
surroundings leading to continuous ad- and desorption of the surfactants.
Strong binding forces are crucial for creating kinetically stable suspensions
:cite:`fujigaya2015` that preserves their emission properties on long time
scales.  Nowadays, thermodynamically more stable surfactants are employed to
the advantage of better emission properties. In H₂O dispersions, cholic acid
derivatives :cite:`chhikara2012` or single-stranded DNA (ssDNA) are used.
Furthermore, in organic solvents thiophene based polymers :cite:`lobez2013`
like *poly(3-hexylthiophene)* (P3HT), fluorene based polymers
:cite:`nish2007,sturzl2009,ozawa2011` like
*poly(9,9‐dioctylfluorenyl‐2,7‐diyl)* (PFO), thiophene-thiazol copolymers
:cite:`salazarrios2018` like *poly[2,6-(4,4-bis-(2-
dodecyl)-4H-cyclopenta[2,1-b;3,4b']dithiophene)-alt-4,7(2,1,3-
benzothiadiazole)]* (P12CPDTBT), and carbazole polymers :cite:`lemasson2012`
are used in combination with various other block co-polymers.

If the emission properties from nanotubes wrapped with different surfactants
are compared in the same solvent, it is found that the type of surfactant plays
a significant role for the excitonic energy as it also influences the
dielectric environment. The transfer from DOC to SDBS, for example, causes a
shift of the excitonic emission by 8nm :cite:`duque2009`, and SDS shows a
strong blue-shift :cite:`moore2003`. A time-dependent measurement of the
photoluminescence during dilution shows that the desorption barrier of a
polymer from the carbon nanotube surface is by far higher in organic solvents
than micellular systems like ssDNA in water.\ :cite:`brunecker2016` Such
observations emphasize the higher stability of suspensions generated with the
use of polymers compared to micellular systems.  Upon these polymers,
polyfluorenes are most frequently applied for the isolation and
chirality-specific dispersion of individualized SWNTs in organic solvents
:cite:`nish2007`, which is why in the following I want to investigate
this system in more detail.


.. _adsorption_pfo:

Adsorption of Polymer
^^^^^^^^^^^^^^^^^^^^^

The interaction of polymers with SWNT plays a key role in providing the desired
selectivity. The mechanism for the adsorption of polymers, however, is under
ongoing discussion.\ :cite:`fong2017` In this section, the different binding
processes are summarized to understand the interaction of polymers on nanotube
surfaces and introduce the experiment design that is used for this study to
help in clarifying the ongoing discussion.

P3HT is known to bind more strongly to SWNTs than PFO but has less selectivity
which is why a thiophene based polymer can exchange the fluorene polymer if
both are available in the solution.\ :cite:`stranks2013` Such behavior is
typically explained by the rigidity of the backbone of PFO which controls the
covalent binding strength of the π-stacking.\ :cite:`chen2002` A flexible
backbone gives stronger binding enthalpies and thus more stable suspensions.
Also, for their selectivity, the rigidity of the polymer backbone plays a
significant role. PFO has a more rigid backbone giving looser π-stacking, but
this very rigidity leads to selective interaction with the (7,5) type while its
more flexible block copolymer with *bipyridine* (PFO-byp) binds better to the
(6,5) type but is less selective. It can be shown that the roll-up angle of an
SWNT chirality influences the binding strength :cite:`ozawa2011` of different
co-polymers and thus alters the selectivity to certain chiralities. This
roll-up angle dependence on the dispersion efficiency has been shown for
various polymer combinations :cite:`lemasson2012` which underlines the
importance of both, π-stacking, and wrapping efficiency effects.

.. _selectivity:

.. figure:: analyzed/mk126/mk126/polymers.*

   The rigidity and structure of the polymer backbone cause selectivity
   differences between PFO-bpy and PFO: PFO-bpy preferentially selects the
   (6,5) type, PFO, on the other hand, is suitable for selecting the (7,5)
   type.

Metallic nanotubes are responsible for the exponentially increasing strong
absorption background :cite:`naumov2011` that is visible for SDS and DOC
dispersed samples between 900-450nm.  Polymers generally show little to no such
background absorption. The high polarizability of metallic nanotubes is thought
to lead to the effective removal of bundles :cite:`wang2015` in organic
solvents during centrifugation. Therefore, for measuring the selectivity of
polymers, the solvent effects usually have to be differentiated from the
pristine polymer effects that depend on the binding energies during
π-stacking.\ :cite:`fong2017` Brunecker *et al.* :cite:`brunecker2016` also
showed that for polymers, the *de*-sorption is neglectable. Consequently,
*ad*-sorption measurements have to be performed on a clean surface because
otherwise, the two competing processes are indistinguishable.

The dispersion efficiency for all polymers also strongly depends on the length
and functionalization of the side chains.\
:cite:`lobez2013,namal2013,fujigaya2015` SWNT that were processed with polymers
that have longer side chains also show stronger absorption after sonication.\
:cite:`lee2011` As the side chain length plays little to no role for the
intrinsic optical absorption of the polymer :cite:`namal2013`, charge transfer
can be excluded, and it is expected that the side chain length only increases
solubility.  The finding is affirmed by TDDFT calculations
:cite:`glanzmann2014` and allows us to suggest that polymers adsorb with the
aromatic backbone to the SWNT surface.  One side-chain wrapping the nanotube
surface and one side chain stands out to the solvent.  Such solubility
advantage effect of the dispersive medium is also found at high coverages of
ssDNA :cite:`brunecker2016` where the majority of the oligomers stands out to
the solvent and is not directly attached to the nanotube surface.  In polymer
suspensions, two decay rates of the polymer fluorescence are observed
:cite:`eckstein2016`, which can be explained with the difference of
fluorescence between partly adsorbed polymer where some polymer trains are
standing out to the solvent.

As outlined here, understanding the mechanism of non-covalent adsorptions on
the nanotube surface is utterly important for the study of their emission
properties. Single-molecule investigations were carried out using suspended
carbon nanotubes to focus on this mechanism in more detail.

Single Molecules
""""""""""""""""

A stable system that has gained considerable interest is the dispersion of
carbon nanotubes with
*poly[(9,9-dioctylfluorenyl-2,7-diyl)-alt-co-(6,6'-{2,2'-bipyridine})]*
(PFO-bpy) in toluene.\ :cite:`ozawa2011`. Investigations on the adsorption of
to the (6,5) type show that the polymer wraps around the cylindrical nanotube
at an angle of 14(2)° to the longitudinal axis.\ :cite:`spath2015` Although the
dispersion efficiency is low, these systems show more narrow line widths
:cite:`muller2019` than DOC wrapped nanotubes, and better quantum efficiencies
than most other polymers.\ :cite:`namal2018`

As outlined before, various effects during this adsorption process have to be
considered, which is the reason for divergent interpretations and are mostly
hard to distinguish. Most experiments in solution suffer from these overlapping
phenomena.  In this experiment, :ref:`toluene-suspended SWNT <toluene>` are
used as a defined starting point for studying polymer adsorption. The nanotubes
are :ref:`fixated on the substrate <wafer>` which hinders the nanotubes from
precipitation and diffusion while maintaining the solvent environment during
the investigation. It also allows to separate solvent and bundling effects from
the usually concomitant polymer stacking and wrapping effects to investigate
the undisturbed adsorption mechanism.  Due to the selectivity for PFO-Bpy on
smaller-diameter nanotubes, the experiment was performed with cooled
:ref:`silicon detectors <shamrock>` to detect the emission between
:ref:`830-1050nm <range>`. This allows investigating predominantly nanotubes of
diameters between :ref:`0.7-0.8nm <kataura>` with a good quantum efficiency of
the detectors to lower the integration times to 15-60s in solution with typical
observation times of 5h. The high single spectrum acquisition time is owed to
the generally :ref:`lowered nanotube emission in solvents <intensity_solvent>`.
For all SWNT with detectable diameters, PFO-Bpy is known to show
:ref:`selective dispersion <selectivity>`. This allows investigating the rather
low amount of nanotubes that survived the :ref:`transfer to toluene <toluene>`
while preserving their excellent emission properties.

.. figure:: collection/suspended/all/spectra/intensity_pfobpy.*

   The fluorescence of toluene-suspended carbon brightens after the addition of
   PFO-bpy. Some nanotubes even reach 75% of their initial emission in air.

The sample is placed into a :ref:`quartz cuvette <cuvette>` and wetted with an
excess of 6mℓ toluene. After staying for at least 24h in the closed cuvette,
50-100µℓ of a 1.5nM polymer solution with an average polymer weight of 33kDa
(60 monomers trains per molecule) is introduced. The sample is located :ref:`at
the bottom <cuvette>` of the cuvette, and the wafer hinders the polymer from
free diffusion to the area of observation. Nevertheless, the polymer is
expected to reach the sample within a few minutes. After 24h of exposure to
PFO-bpy, the emission intensity is around twice as high as in pristine toluene.
This is most probably explained by the polymer leading to the desorption of
toluene and preventing further adsorptions.

.. figure::
   collection/suspended/all/spectra/autocorrelation.*

   The excitonic emission in toluene with adsorbed PFO-Bpy is shifted to the
   red compared to the emission in pure toluene. The graph shows the unweighted
   spectral autocorrelation of individual measurements.

The polymer wrapping increases the dielectric environment. This increase leads
to a red-shift of the excitonic emission signal.  Energy shift and emission
intensity are used as a probe for polymer adsorption during the time-dependent
observation on single nanotubes.  For this measurement, the nanotubes are first
observed for 2h until :ref:`emission stability <air_time_stable>` at :ref:`low
excitation energies (5-30µW/3µm²) <powerrange>`.  The adsorption of the polymer
is observed in an excited thermodynamic equilibrium caused by the laser heat.
This elevated state allows observing the ad- and desorption process on a single
molecule basis.

.. figure:: analyzed/mkl8/mkl8si_pfobpy_timed/statistics.*

   Irreversible stepwise intensity decrease during the adsorption of PFO-bpy to
   a nanotube of the (6,4) type.

.. segment size (4.2-4.5)/34 * 2µm = 250(10)nm with steps of 1.7/34 * 2µm = 100nm


When observed over long time-scales, the emission intensity of the carbon
nanotubes in solvents depleaches step-wise. Such depletion is probably caused
by the generation of defects through reaction with oxygen which is known to
generate irreversible steps.\ :cite:`siitonen2010`


The time evolution of the energy is usually analyzed with the Allan deviation
:cite:`barnes1971,allan1987` to minimize the white noise in the emission
spectra.\ :cite:`noe2018` For nanotubes in toluene at room temperature with
rather high integration times of 30s per spectrum, the energy fluctuations are
minimal and are not expected to achieve a higher resolution with different
statistics.


.. figure:: analyzed/mkl34/mkl34timeseries11v2/main.*

   The figure shows the time-dependent adsorption of PFO-bpy on the (6,5) type.
   Adsorption is identified by an increase in the emission intensity and a
   red-shift of the emission wavelength.

There are also reversible steps visible that are caused by diffusion quenching
within a mean segment size of Λ = 20/180 · 2µm = 220(20)nm. Such segment sizes
are more than double the size as expected from the mean polymer train lengths.
Unfortunately, high exciton diffusion lengths are expected for this system as
proven with a simulation during :ref:`image analysis <excitondiffusion>`.  With
such high diffusion lengths, the observation of polymer adsorption is limited.
Nevertheless, the energy clearly shows a distinct shift to the red after the
intensity fluctuations stop and the observed steps are partly reversibile.
This allows assuming that we indeed observed polymer adsorption.

.. figure:: analyzed/mkl34/mkl34timeseries11v2/zoom.*

   The intensity decreases stepwise. The irreversible increase in intensity
   is followed by a shift of the emission wavelength to the red.

The vague insights do not allow to make assumptions on the exact binding
mechanism. A possible explaination of the observed signal could be that the the
octyl groups form an initial binding while the rearrangement of the π-stacking
from the polymer backbone to the exact angle around the nanotube axis takes
longer. As toluene gets displaced by octyl, the energy shifts to the red.

Conclusion
""""""""""

The adsorption of individual polymer molecules on suspended nanotubes is a
tedious and complicated measurement.  Nevertheless, we presented initial
results of the individual adsorption of
Poly(9,9-di-n-octylfluorenyl-2,7-diyl)bipyridine (PFO-bpy) on the surface of
suspended SWNTs in toluene using single molecule fluorescence microscopy and
spectroscopy with single molecule observations over more than 6h. Changes in
the intensity and central wavelength of the excitonic emission was used as a
probe for surface effects.

It could be shown that the polymer only minimally changes the dielectric
environment of the system and causes a red-shift compared to a system in
toluene. The segment sizes of the involved polymer trains were below the
resolvable limit defined by the exciton diffusion length. Such long polymer
trains take rather long time for the irreversible adsorption. A cooperative
binding mechanism is evident, although the exact mechanism leaves room for
interpretation.
