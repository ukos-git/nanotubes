.. vim: set et ts=3 sts=3 sw=3 tw=79:

.. _adsorption:

******************
Surface Adsorption
******************

.. _adsorption_intro:

.. figure:: collection/basics/exciton/exciton-trap.*

   Changes on the surface of a carbon nanotube form trap states that lead to a
   non-radiative decay of the excitonic state and changes in the dielectric
   surrounding influence the exciton energy.

Interactions with the surface of a carbon nanotube are predominantly dispersion
(London) interactions.

These strong intermolecular forces are responsible for stable suspensions of
carbon nanotubes in various solvents where non-covalently bound surfactants are
wrapped around the nanotube. Such wrappings prevent bundling and preserve the
excitonic emission features of carbon nanotubes in solution and films over
months. The exact mechanism and the kinetics of surfactant sorption on the
surface of carbon nanotubes is rather complex and not fully understood
:cite:`fong2017` as it usually includes concomitant effects. The separate study
of such effects is important and will be delved deeply for the
:doc:`molecular`.

A downside for the use of surfactants is that a significant percentage of
photoluminescence quantum yield is lost :cite:`siitonen2010` to exciton
quenching at the sites of adsorbed solvent and surfactant molecules.  Placing
carbon nanotubes in solvents is, however, inevidable for most wet processing
methods or for contacting nanotubes to electrolytes. The effect of the
:doc:`solvent` is therefore handled in more detail in its own chapter.

The use of surfactants reduces and alters the available free nanotube surface.
A clean surface, however, is required for sensing molecular interactions
:cite:`bradley2003`. A *clean* surface serves as a clear reference point for
studies on the adsorption process.  The search for this initially clean surface
and its definition as a reference is discussed in the :doc:`gas`.

.. toctree::

   gas.rst
   solvent.rst
   molecular.rst
