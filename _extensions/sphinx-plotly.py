import os.path
import json
import plotly.offline as py

from docutils import nodes
from docutils.parsers.rst import Directive

from sphinx.locale import _
from sphinx.util.docutils import SphinxDirective

class PlotlyDirective(SphinxDirective):

    has_content = True

    def run(self):

        filepath = os.path.normpath(self.env.relfn2path(self.content[0].strip())[1])
        print(filepath)

        with open(filepath) as json_file:
                graph = json.load(json_file)

        attributes = {'format': 'html'}
        plot_node = nodes.raw('', py.plot(graph, include_plotlyjs=False, output_type='div'), **attributes)
        return [plot_node]


def setup(app):
    app.add_directive("plotly", PlotlyDirective)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
